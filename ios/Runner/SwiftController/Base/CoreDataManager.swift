//
//  CoreDataManager.swift
//  Kollus Player
//
//  Created by kiwan on 2020/11/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation
import CoreData


class CoreDataManager {
    
    static let shared: CoreDataManager = CoreDataManager()
    
    // MARK: - utility routines
    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    
    // MARK: - Core Data stack (generic)
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "DataModel", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
        
    }()
    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("dataModel").appendingPathExtension("xcdatamodeld")
        
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            let dict : [String : Any] = [NSLocalizedDescriptionKey        : "Failed to initialize the application's saved data" as NSString,
                                         NSLocalizedFailureReasonErrorKey : "There was an error creating or loading the application's saved data." as NSString,
                                         NSUnderlyingErrorKey             : error as NSError]
            
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            fatalError("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
        }
        
        return coordinator
    }()
    
    // MARK: - Core Data stack (iOS 9)
    @available(iOS 9.0, *)
    lazy var managedObjectContext: NSManagedObjectContext = {
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentStoreCoordinator
        return managedObjectContext
    }()
    
    
    // MARK: - Core Data stack (iOS 10)
    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "DataModel")
        container.loadPersistentStores(completionHandler: {
            (storeDescription, error) in
            if let error = error as NSError?
            {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        }
        )
        
        return container
    }()
    
    // MARK: - Core Data context
    lazy var databaseContext : NSManagedObjectContext = {
        if #available(iOS 10.0, *) {
            return self.persistentContainer.viewContext
        } else {
            return self.managedObjectContext
        }
    }()
    
    // MARK: - Core Data save
    func saveContext () {
        do {
            if databaseContext.hasChanges {
                try databaseContext.save()
            }
        } catch {
            let nserror = error as NSError
            
            fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
        }
    }
    
    
    // MARK: - Create Entity
    
    func saveKollusContentData(data: KollusContent) {        
        let context = databaseContext
        let entity = NSEntityDescription.entity(forEntityName: "KollusContentEntity", in: context)
        
        guard fetchKollusContentData(data: data) == nil else { return }
        
        if let entity = entity {
            let kollusContent = KollusContentEntity(entity: entity, insertInto: context)
            kollusContent.mediaContentKey = data.mediaContentKey
            
            do {
                try context.save()
            }
            catch {
                print(error.localizedDescription)
            }
        }
    }
    
    func fetchKollusContentData(data: KollusContent) -> KollusContentEntity? {
        let context = databaseContext
        
        do {
            let content = try context.fetch(KollusContentEntity.fetchRequest())
            
            for entity in content {
                if entity.mediaContentKey == data.mediaContentKey {
                    if data.contentType == .streaming {
                        entity.subtitle = -1
                        entity.playSpeed = 5
                        entity.resolution = 0
                        entity.scaleMode = 0
                    }
                    
                    return entity
                }
            }
            
            return nil
            
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }
    
    //    func updateKollusContentData(data: KollusContent) {
    //        if #available(iOS 10.0, *) {
    //            let container = CoreDataManager.shared.persistentContainer
    //            let context = container.viewContext
    //            let fetchRequest: NSFetchRequest<KollusContentEntity> = NSFetchRequest(entityName: "KollusContentEntity")
    //            fetchRequest.predicate = NSPredicate(format: "mediaContentKey = %@", data.mediaContentKey)
    //            do {
    //                let entities = try context.fetch(fetchRequest)
    //                guard let selectedEntity = entities.first else { return }
    //                selectedEntity.playSpeed = 1
    //
    //                try context.save()
    //            }
    //            catch {
    //                print(error)
    //            }
    //        }
    //        else {
    //
    //        }
    //
    //    }
    
    func removeKollusContentData(data: KollusContent) {
        let context = databaseContext
        let fetchRequest: NSFetchRequest<KollusContentEntity> = NSFetchRequest(entityName: "KollusContentEntity")
        fetchRequest.predicate = NSPredicate(format: "mediaContentKey = %@", data.mediaContentKey)
        do {
            let entities = try context.fetch(fetchRequest)
            guard let selectedEntity = entities.first else { return }
            context.delete(selectedEntity)
            
            try context.save()
        }
        catch {
            print(error)
        }
    }
    
    // All move
    func resetKollusContentData() {
        let context = databaseContext
        let fetchRequest: NSFetchRequest<KollusContentEntity> = NSFetchRequest(entityName: "KollusContentEntity")
        
        do {
            let entities = try context.fetch(fetchRequest)
            
            for entitiy in entities {
                context.delete(entitiy)
            }
            
            try context.save()
        }
        catch {
            print(error)
        }
    }
    
}
