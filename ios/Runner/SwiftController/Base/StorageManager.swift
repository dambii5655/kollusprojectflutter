//
//  KollusSDKManager.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class StorageManager: NSObject, PlayerViewControllerDelegate {
    func onVideoViewClosed() {
        print(" ggggg  onVideoViewClosed")
    }
    
    
    static let shared = StorageManager()
    
    let storage: KollusStorage
    
    // my add code
    var contentsList: [KollusContent] = []
    
    override init() {
        storage = KollusStorage()
        
        super.init()

        let dateString = "2200/12/31"
        
        storage.applicationKey = "82b37680c25412fbedbec2eb540be2c316410652" // "dc2b1bbbb5cbd5d2d3a3078aef7e9eeb7938a748"//
        storage.applicationBundleID = "com.kollus.KollusPlayerree"
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy/MM/dd"
        let expireDate = dateFormatter.date(from: dateString)
        
        storage.applicationExpireDate = expireDate
        print(" -------- storage")
    }
    
    public func validateSDK() {
        do {
            AppDelegate.startStorage = true
            try storage.start()
            storage.setBackgroundDownload(true)
            print(" -------- storage start")
            // self.requestContentsList()
        }
        catch {
            AppDelegate.startStorage = false
            print(" =====  ERROR", error.localizedDescription)
            print(" =====  ERROR", error)
            // UIApplication.presentErrorViewController(title: "Error Code : \((error as NSError).code)", errorDescription: nil, errorReason: error.localizedDescription)
        }
    }
    
    // TODO gggg add my code
    public func requestContentsList() {
        self.requestSampleContentsData { success, error in
            print(" gggg testGetFile ", success)
            if ( success ) {
                self.contentsList.removeAll()
                self.contentsList.append(contentsOf: self.getContentsList())
                AppDelegate.contentsList = self.contentsList
                print(" ddddd 3 duuslaa ")
            }
        }
    }
    
    public func returnContentList()  -> [KollusContent] {
        return self.contentsList
    }
    
    public func playAction(mediaContentKey: String) {
        
        let contentsList = contents()
        
        contentsList.forEach { content in
            if content.mediaContentKey == mediaContentKey {
                // let data = content
                
                print(" list ", content.fileType)
                if self.isExpired(info: content) && content.drmExpireRefreshPopup {
                    let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
                    let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
                    // confirmPopupViewController.delegate = self
                    confirmPopupViewController.type = .drmRenew
                    confirmPopupViewController.data = content
                    UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
                    return
                }
                
                print(" ==== doosh ")
                
                let storyBoard = UIStoryboard(name: "PlayerViewController", bundle: nil)
                let playerViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
                playerViewController.data = content
                playerViewController.playListItems = contentsList
                playerViewController.delegate = self
                UIApplication.getTopViewController()?.present(playerViewController, animated: false, completion: nil)
            }
        }
    }
    
    func isExpired(info:KollusContent) -> Bool {
        // check date
        if let expireDate = info.drmExpireDate {
        
            let now = Date()
            if now.compare(expireDate) == ComparisonResult.orderedDescending {
                return true
            }
        }
        
        // check count
        if info.drmExpireCountMax > 0 && info.drmExpireCount == 0 {
            return true
        }
        
        // check play time
        if info.drmTotalExpirePlayTime > 0 && info.drmExpirePlayTime == 0 {
            return true
        }

        // check flag
        if info.drmExpired {
            return true
        }

        return false
    }
    
    
    // ================== xxxx ===========
    func contents() -> [KollusContent] {
        let data = storage.contents() as! [KollusContent]

        return data
    }

    func checkIsDownloadContent(url: String, completion: @escaping (Bool, KollusContent?) -> Void) {
        DispatchQueue.global().async { [unowned self] in
            do {

                let mck = try storage.checkContentURL(url)

                let contentList = contents()
                for content in contentList {
                    if mck == content.mediaContentKey {
                        DispatchQueue.main.async {
                            completion(true, content)
                        }
                        break
                    }
                }
            }
            catch {
                print(error.localizedDescription)
                DispatchQueue.main.async {
                    completion(false, nil)
                }
            }

        }
    }

    // MARK: - Device Information || SettingViewController

    func getDeviceID() -> String {
        storage.applicationDeviceID
    }

    func getCacheDataSize() -> String {
        KollusUtil.fileSizeToString(fileSize: storage.cacheDataSize)
    }

    func getCacheDataSizeAccessibility() -> String {
        KollusUtil.fileSizeToAccessibility(fileSize: storage.cacheDataSize)
    }

    func getContentsTotalSize() -> String {
        var size = storage.storageSize - 1000
        if size < 0 {
            size = 0
        }

        return KollusUtil.fileSizeToString(fileSize: size)
    }

    func getContentsTotalSizeAccessibility() -> String {
        var size = storage.storageSize - 1000
        if size < 0 {
            size = 0
        }

        return KollusUtil.fileSizeToAccessibility(fileSize: size)
    }

    func getfreeDiskSize() -> String {
        var size = DiskStatus.freeDiskSpaceInBytes
        if size < 0 {
            size = 0
        }

        return KollusUtil.fileSizeToString(fileSize: size)
    }

    func getfreeDiskSizeAccessibility() -> String {
        var size = DiskStatus.freeDiskSpaceInBytes
        if size < 0 {
            size = 0
        }

        return KollusUtil.fileSizeToAccessibility(fileSize: size)
    }


    func deleteCacheDatas() {
        try? storage.removeCache()
    }

    func renewDRMContents(isAll: Bool) {
        storage.updateDownloadDRMInfo(isAll)
    }

    func removeAllDownloadContents() {
        let contentsList = contents()

        for content in contentsList {
            if content.contentType == .sample {
                continue
            }

            removeContent(content: content)
        }
    }

    // MARK: - Load
    public func loadContentURL(URL: String, completion: @escaping (Error?, String) -> ()) {
//        DispatchQueue.global().async { [unowned self] in
            do {
                let mck = try storage.loadContentURL(URL)
                completion(nil, mck)
            }
            catch {
                completion(error, error.localizedDescription)
            }
//        }

    }

    public func loadSampleContentURL(URL: String) {
        do {
            try storage.loadSampleURL(URL)
        }
        catch {
            print(error.localizedDescription)
        }
    }

    //MARK: - Remove

    private func removeContent(mediaContentsKey: String) {
        do {
            DirectoryJson.removeFile(mediaContentsKey, pathDirectory: Path.shared.currentPath, originalJsonData: DirectoryJson.getData())
            try storage.removeContent(mediaContentsKey)
        }
        catch {
            print(error.localizedDescription)
        }
    }

    private func removeDirectory(path: [String]) {
        let removeItems = DirectoryJson.checkFiles(path, originalJsonData: DirectoryJson.getData()) as! [String]

        for mck in removeItems {
            removeContent(mediaContentsKey: mck)
        }

        DirectoryJson.removeDirectory(path, originalJsonData: DirectoryJson.getData())
    }

    func removeContent(content: KollusContent) {
        if content.fileType == 0 {     // File
            removeContent(mediaContentsKey: content.mediaContentKey)

            let path = DirectoryJson.searchPath(content.mediaContentKey, originalJsonData: DirectoryJson.getData())

            DirectoryJson.removeFile(content.mediaContentKey, pathDirectory: path, originalJsonData: DirectoryJson.getData())


            if let mck = content.mediaContentKey {
                PreferenceManager.playedContents[content.mediaContentKey] = false
            }

//            CoreDataManager.shared.removeKollusContentData(data: content)
        }
        else {
            var directoryPath = Path.shared.currentPath
            directoryPath.append(content.title)
            removeDirectory(path: directoryPath)
        }

    }

    //MARK: - Download

    func startDownloadContent(mediaContentKey: String) {
        do {
            try storage.downloadContent(mediaContentKey)
        }
        catch {
            print(error.localizedDescription)
        }
    }

    func cancelDownloadContent(mediaContentKey: String) {
        NLog("cancelDownloadContent +++")
        do {
            try storage.downloadCancelContent(mediaContentKey)
        }
        catch {
            print(error.localizedDescription)
        }
        NLog("cancelDownloadContent ---")
    }

    func addDownloadedFile(mediaContentKey: String, toPath: [String]) {
        DirectoryJson.addFile(mediaContentKey, pathDirectory: toPath, originalJsonData: DirectoryJson.getData()!)
    }



    // MARK: - Request
    public func requestSampleContentsData(completionHandler: @escaping (Bool, Error?) -> ()) {

        DispatchQueue.global().async {
            let request = Request()
            let url = URL(string: "https://file.kollus.com/public/kollus_ios/sample/sample_contents.json")!
            request.get(url: url, completionHandler: { [unowned self] (data, response, error) in
                if error != nil {
                    DispatchQueue.main.async {
                        completionHandler(false, error)
                    }
                }
                else {
                    if let jsonData = data {
                        let decoder = JSONDecoder()

                        let data = try! decoder.decode(ResponseSampleContents.self, from: jsonData)
                        
                        print("gggggg requestSampleContentsData ", data)
                        for contents in data.result {
                            print(" ggg url ", contents.video_url)
                            self.loadSampleContentURL(URL: contents.video_url)

                            var mediaContentsKeyList: [String] = []
                            let contents = StorageManager.shared.contents()

                            for item in contents {
                                mediaContentsKeyList.append(item.mediaContentKey)
                            }

                            DirectoryJson.make(mediaContentsKeyList)
                        }

                        PreferenceManager.sampleContents = data.result

                        DispatchQueue.main.async {
                            completionHandler(true, nil)
                        }
                    }

                }
            })
        }
    }

    //MARK: - Contents

    func getContentsList(path: [String] = Path.shared.currentPath) -> [KollusContent] {
        guard let jsonData = DirectoryJson.getData() else { print(Path.shared.currentPath , "error getContentsList"); return [] }

        var contentsList: [KollusContent] = []

        guard let currentDirectoryItemList: [ItemInfo] = DirectoryJson.getList(path, originalJsonData: jsonData) as? [ItemInfo]
        else {
            return []
        }

        let storageContentsList = contents()
        
        // TODO gggg change my code
        print("cDir ", currentDirectoryItemList.count)
        print("sCon ", storageContentsList.count)
        
        //
        // for item in currentDirectoryItemList {
            for contents in storageContentsList {
           //     if contents.mediaContentKey == item.title {
                    if contents.downloaded == true || contents.downloadProgress == 100 || contents.contentType == .sample {
                        contentsList.append(contents)
             //           break
                    }
          //      }
            }
        //}

        for item in currentDirectoryItemList {
            if item.type == 1 {
                let folder = KollusContent(directory: item.title)

                if let title = folder?.title {
                    let subFolderPath = path + [title]
                    let folderFileList: [ItemInfo] = DirectoryJson.getList(subFolderPath, originalJsonData: jsonData) as! [ItemInfo]
                    folder!.directoryFileNumber = folderFileList.count
                }

                contentsList.insert(folder!, at: 0)
            }
        }

        sortContentsList(list: &contentsList)

        return contentsList
    }

    func getFolderList(path: [String]) -> [KollusContent] {
        var folderList: [KollusContent] = []

        let currentDirectoryItemList: [ItemInfo] = DirectoryJson.getList(path, originalJsonData: DirectoryJson.getData()!) as! [ItemInfo]

        for item in currentDirectoryItemList {
            if item.type == 1 {
                let folder = KollusContent(directory: item.title)
                folderList.insert(folder!, at: 0)
            }
        }

        sortContentsList(list: &folderList)

        return folderList
    }

    // MARK: - Download List View Controller
    func getDownloadList() -> [KollusContent] {
        var downloadContentsList: [KollusContent] = []

        let storageContentsList = contents()

        for contents in storageContentsList {
            let downloadProgress = CGFloat(contents.downloadSize) / CGFloat(contents.fileSize)
            if (downloadProgress < 1 && contents.contentType == .downloading) || (downloadProgress < 1 && contents.contentType == .adaptiveDownload) {
                downloadContentsList.append(contents)
            }
        }

        downloadContentsList.sort {
            $0.downloadedTime > $1.downloadedTime
        }


        return downloadContentsList

    }

    func getRecentlyAddedFileList() -> [KollusContent] {
        var contentsList = contents()

        contentsList.sort {
            $0.downloadedTime > $1.downloadedTime
        }

        var recentlyAddedFileList: [KollusContent] = []
        let now = Date().toLocalTime()

        for content in contentsList {
            if (content.contentType == .downloading && content.downloaded) || (content.contentType == .adaptiveDownload && content.downloadProgress == 100) {
                let downloadedDate = KollusUtil.integerToDate(int: content.downloadedTime)
                let interval = downloadedDate.daysBetween(date: now)

                if interval <= 7 {
                    recentlyAddedFileList.append(content)

                    if recentlyAddedFileList.count > 29 {
                        break
                    }
                }
            }
        }

        recentlyAddedFileList.sort {
            $0.downloadedTime > $1.downloadedTime
        }



        return recentlyAddedFileList
    }


    //MARK: - Sort
    func sortContentsList(list: inout [KollusContent]) {
        let sortDescription = PreferenceManager.sortOrder
        // 0 파일, 1 날짜, 2, 용량 순

        list.sort {

            if $0.fileType != $1.fileType { // first, compare by last names
                return $0.fileType > $1.fileType
            }
            else { // All other fields are tied, break ties by last name
                if $0.fileType == $1.fileType && $0.fileType == 1 { // directory
                    return $0.title < $1.title
                }
                else if sortDescription == Sort.name.rawValue {
                    return $0.title < $1.title
                }
                else if sortDescription == Sort.downloadedTime.rawValue {
                    return $0.downloadedTime > $1.downloadedTime
                }
                else {
                    return $0.fileSize > $1.fileSize
                }

            }


            //            return false

            //              if $0.lastName != $1.lastName { // first, compare by last names
            //                  return $0.lastName < $1.lastName
            //              }
            //              /*  last names are the same, break ties by foo
            //              else if $0.foo != $1.foo {
            //                  return $0.foo < $1.foo
            //              }
            //              ... repeat for all other fields in the sorting
            //              */
            //              else { // All other fields are tied, break ties by last name
            //                  return $0.firstName < $1.firstName
            //              }
        }
    }


}
