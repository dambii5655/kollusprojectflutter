//
//  UIColor+.swift
//  VideoPlayer
//
//  Created by kiwan on 30/07/2019.
//  Copyright © 2019 kiwan. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(hexFromString: String, alpha: CGFloat = 1.0) {
        var cString: String = hexFromString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue: UInt64 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt64(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
}

extension UIColor {
//    var hexString: String? {
//        var red: CGFloat = 0
//        var green: CGFloat = 0
//        var blue: CGFloat = 0
//        var alpha: CGFloat = 0
//
//        let multiplier = CGFloat(255.999999)
//
//        guard self.getRed(&red, green: &green, blue: &blue, alpha: &alpha) else {
//            return nil
//        }
//
//        if alpha == 1.0 {
//            return String(
//                format: "#%02lX%02lX%02lX",
//                Int(red * multiplier),
//                Int(green * multiplier),
//                Int(blue * multiplier)
//            )
//        }
//        else {
//            return String(
//                format: "#%02lX%02lX%02lX%02lX",
//                Int(red * multiplier),
//                Int(green * multiplier),
//                Int(blue * multiplier),
//                Int(alpha * multiplier)
//            )
//        }
//    }
    
    func toHexString() -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        return String(format:"#%06x", rgb)
    }
    
}
