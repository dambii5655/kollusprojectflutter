//
//  CALayer.swift
//  Kollus Player
//
//  Created by 이기완 on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

extension CALayer {
    func applySketchShadow( color: UIColor = .black, alpha: Float = 0.08, x: CGFloat = 0, y: CGFloat = 1, blur: CGFloat = 4, spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        }
        else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
    }
    
}

