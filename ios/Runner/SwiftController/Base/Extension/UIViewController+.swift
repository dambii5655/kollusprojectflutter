//
//  UIViewController+.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/07.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

extension UIViewController {
    
    func presentAlert(withTitle title: String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let OKAction = UIAlertAction(title: "확인", style: .default) { action in
            print("You've pressed OK Button")
        }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion: nil)
    }
}

