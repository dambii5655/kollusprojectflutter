//
//  UIImage+.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/24.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

extension UIImage {
    class func imageWithView(_ view: UIView) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.isOpaque, 0)
        defer { UIGraphicsEndImageContext() }
        view.drawHierarchy(in: view.bounds, afterScreenUpdates: true)
        return UIGraphicsGetImageFromCurrentImageContext() ?? UIImage()
    }
}
