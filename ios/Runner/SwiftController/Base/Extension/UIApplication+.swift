//
//  UIApplication+.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/01.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

extension UIApplication {
    class func getTopViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let nav = base as? UINavigationController {
            return getTopViewController(base: nav.visibleViewController)
            
        } else if let tab = base as? UITabBarController, let selected = tab.selectedViewController {
            return getTopViewController(base: selected)
            
        } else if let presented = base?.presentedViewController {
            return getTopViewController(base: presented)
        }
        return base
    }
    
    class func presentErrorViewController(title: String? = nil, errorDescription: String?, errorReason: String?) {
        DispatchQueue.main.async {
            
//            guard let topVC = self.getTopViewController(), !topVC.isKind(of: ErrorPopupViewController.self) else { return }
            
            let topVC = self.getTopViewController()
            print(" ======== ErrorPopupViewController bsan")
            let storyBoard = UIStoryboard(name: "ErrorPopupViewController", bundle: nil)

            guard let errorPopupViewController = storyBoard.instantiateInitialViewController() as? ErrorPopupViewController else { return }

            errorPopupViewController.titleText = title
            errorPopupViewController.errorDescriptionText = errorDescription
            errorPopupViewController.errorReasonText = errorReason

            topVC!.present(errorPopupViewController, animated: false, completion: nil)

            
        }
    }
}
