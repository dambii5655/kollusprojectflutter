//
//  UILabel+.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/30.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation


extension UILabel {
    func setHTMLFromString(htmlText: String, baseFont color: UIColor, baseFont size: CGFloat ) {
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        var strokeTextAttributes: [NSAttributedString.Key : Any] = [:]
        
        if #available(iOS 14.0, *) {
            strokeTextAttributes = [
                NSAttributedString.Key.strokeColor : UIColor.black,
                NSAttributedString.Key.strokeWidth : -4.0,
            ]
            
        } else {
            strokeTextAttributes = [
                NSAttributedString.Key.strokeColor : UIColor.black,
                NSAttributedString.Key.strokeWidth : -4.0,
            ]
            
        }

        let modifiedFont = String(format:"<span style=\"font-family: 'HelveticaNeue'; color:\(color.toHexString()); font-size: \(size)\"><b>%@</b></span>", htmlText)

        guard let attrStr = try? NSMutableAttributedString(data: modifiedFont.data(using: .unicode, allowLossyConversion: true)!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil) else { return }
        
        attrStr.addAttributes([NSAttributedString.Key.paragraphStyle: style], range: NSMakeRange(0, attrStr.length))
        
        attrStr.addAttributes(strokeTextAttributes, range: NSMakeRange(0, attrStr.length))
        
        
        
        self.attributedText = attrStr
        
    }
}



