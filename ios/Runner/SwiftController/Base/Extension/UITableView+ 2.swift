//
//  UITableView+.swift
//  Kollus Player
//
//  Created by 이기완 on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

extension UITableView {
    //set the tableHeaderView so that the required height can be determined, update the header's frame and set it again
    func setAndLayoutTableHeaderView(header: UIView) {
        self.tableHeaderView = header
        header.setNeedsLayout()
        header.layoutIfNeeded()
        header.frame.size = header.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        self.tableHeaderView = header
    }
}

