//
//  UIStoryboard+.swift
//  Kollus Player
//
//  Created by kiwan on 2020/12/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

extension UIStoryboard {
        
    static func storyboard(name: String) -> UIStoryboard {
        
        if UIDevice.current.userInterfaceIdiom == .phone {
            return UIStoryboard(name: name, bundle: nil)
        }
        else {
            return UIStoryboard(name: "\(name)_iPad", bundle: nil)
        }
            
        
    }
    
}
