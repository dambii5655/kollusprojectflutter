//
//  AppUtility.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/01.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

struct AppUtility {

    static func lockOrientation(_ orientation: UIInterfaceOrientationMask) {
    
        if UIDevice.current.userInterfaceIdiom == .phone {
            if let delegate = UIApplication.shared.delegate as? AppDelegate {
                delegate.orientationLock = orientation
            }
        }
        
    }

    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    static func lockOrientation(_ orientation: UIInterfaceOrientationMask, andRotateTo rotateOrientation: UIInterfaceOrientation) {
        NLog("lockOrientation : \(rotateOrientation.rawValue) +++")
        if UIDevice.current.userInterfaceIdiom == .phone {
            
            self.lockOrientation(orientation)
        
            UIDevice.current.setValue(rotateOrientation.rawValue, forKey: "orientation")
            UINavigationController.attemptRotationToDeviceOrientation()
        }
        
        NLog("lockOrientation : \(rotateOrientation.rawValue) ---")
    }

}
