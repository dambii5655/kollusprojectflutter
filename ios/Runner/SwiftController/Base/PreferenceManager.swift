//
//  PreferenceManager.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class PreferenceManager: NSObject {
    static var sampleContents: [SampleContents] {
        get {
            if let data = UserDefaults.standard.value(forKey:"SampleContents") as? Data {
                return try! PropertyListDecoder().decode(Array<SampleContents>.self, from: data)
            }
            return []
        }
        
        set {
            UserDefaults.standard.set(try? PropertyListEncoder().encode(newValue), forKey: "SampleContents")
        }
    }
    
    @UserDefault("playedContents", defaultValue: [:])
    static var playedContents: [String: Bool]
    
    
    @UserDefault("isFirstExecuted", defaultValue: true)
    static var isFirstExecuted: Bool
    
    @UserDefault("isFirstWatched", defaultValue: true)
    static var isFirstWatched: Bool
        
    @UserDefault("sortOrder", defaultValue: Sort.name.rawValue)
    static var sortOrder: Int
    
    @UserDefault("seekerRange", defaultValue: SeekRange.r10.rawValue)
    static var seekRange: Int
    
    @UserDefault("isUseSubtitleBackground", defaultValue: true)
    static var isUseSubtitleBackground: Bool
    
    @UserDefault("subtitleSize", defaultValue: SubtitleSize.normal.rawValue)
    static var subtitleSize: Int
    
    @UserDefault("subtitleColor", defaultValue: SubtitleColor.white.rawValue)
    static var subtitleColor: Int
    
    //MARK: - SettingViewController
    
    @UserDefault("DRMCheckBox", defaultValue: true)
    static var DRMCheckBox: Bool
    
    @UserDefault("UsePlayerType", defaultValue: true)
    static var UsePlayerType: Bool
    
    @UserDefault("isBackgroundAudioPlay", defaultValue: false)
    static var isBackgroundAudioPlay: Bool
    
    @UserDefault("isUseNetworkData", defaultValue: true)
    static var isUseNetworkData: Bool
    
    @UserDefault("playerCodec", defaultValue: PlayerCodec.nativePlayer.rawValue)
    static var playerCodec: Int


    
    //MARK: - Reset
    static func resetPreferenceDatas() {
        let userDefaults = UserDefaults.standard
        let keys = userDefaults.dictionaryRepresentation()
        
        for key in keys.keys {
            if key == "isFirstExecuted" || key == "SampleContents" || key == "isFirstWatched" || key == "playedContents" {
                continue
            }
            userDefaults.removeObject(forKey: String(describing: key))
        }
        
    }
}


@propertyWrapper
struct UserDefault<T> {
    let key: String
    let defaultValue: T
    
    init(_ key: String, defaultValue: T) {
        self.key = key
        self.defaultValue = defaultValue
    }
    
    var wrappedValue: T {
        get {
            return UserDefaults.standard.object(forKey: key) as? T ?? defaultValue
        }
        set {
            UserDefaults.standard.set(newValue, forKey: key)
        }
    }
}

