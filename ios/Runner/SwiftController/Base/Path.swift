
//
//  Path.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/05.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class Path {
    
    static let shared = Path()
    
    var currentPath: [String] = []
    
    init() {
    }
    
    func appendPath(directoryName: String) {
        currentPath.append(directoryName)
    }
    
    func removeLastPath() {
        currentPath.removeLast()
    }
    
    func getLastPath() -> String {
        return currentPath.last ?? ""
    }
    
//    func getContentsList() -> [KollusContent] {
//        guard let jsonData = DirectoryJson.getData() else { return [] }
//        
//        var contentsList: [KollusContent] = []
//        
//        let currentDirectoryItemList: [ItemInfo] = DirectoryJson.getList(currentPath, originalJsonData: jsonData) as! [ItemInfo]
//        
//        let storageContentsList = StorageManager.shared.contents()
//        //
//        for item in currentDirectoryItemList {
//            for contents in storageContentsList {
//                if contents.mediaContentKey == item.title {
////                    if contents.downloaded == true || contents.downloadProgress == 100 || contents.contentType == .sample {
//                        contentsList.append(contents)
//                        break
////                    }
//                }
//            }
//        }
//        
//        for item in currentDirectoryItemList {
//            if item.type == 1 {
//                let folder = KollusContent(directory: item.title)
//                contentsList.insert(folder!, at: 0)
//            }
//        }
//        
//        sortContentsList(list: &contentsList)
//        
//        return contentsList
//    }
//    
//    func getFolderList(path: [String]) -> [KollusContent] {
//        var folderList: [KollusContent] = []
//        
//        let currentDirectoryItemList: [ItemInfo] = DirectoryJson.getList(path, originalJsonData: DirectoryJson.getData()!) as! [ItemInfo]
//        
//        for item in currentDirectoryItemList {
//            if item.type == 1 {
//                let folder = KollusContent(directory: item.title)
//                folderList.insert(folder!, at: 0)
//            }
//        }
//        
//        sortContentsList(list: &folderList)
//        
//        return folderList
//    }
//    
//    func getDownloadList() {
//        
//    }
    
//    func sortContentsList(list: inout [KollusContent]) {
//        let sortDescription = PreferenceManager.sortOrder
//        // 0 파일, 1 날짜, 2, 용량 순
//        
//        list.sort {
////            if sortDescription == 0 {
////                return ($0.fileType, $0.title) > ($1.fileType, $1.title)
////            }
////            else if sortDescription == 1 {
////                return ($0.fileType, $0.downloadedTime) > ($1.fileType, $1.downloadedTime)
////            }
////            else {
////                return ($0.fileType, $0.fileSize) > ($1.fileType, $1.fileSize)
////            }
//            
//            if $0.fileType == 1 {
//                return true //this will return true: s1 is priority, s2 is not
//            }
//            
//            if $0.fileType == 0 {
//                return false //this will return true: s1 is priority, s2 is not
//            }
//            
//            if $0.fileType == $1.fileType {
//                 if sortDescription == 0 {
//                    return $0.title > $1.title
//                 }
//                 else if sortDescription == 1 {
//                    return $0.downloadedTime > $1.downloadedTime
//                 }
//                 else {
//                    return $0.fileSize > $1.fileSize
//                }
//            }
////            if !s1.isPriority && s2.isPriority {
////                return false //this will return false: s2 is priority, s1 is not
////            }
////            if s1.isPriority == s2.isPriority {
////                return s1.ordering < s2.ordering //if both save the same priority, then return depending on the ordering value
////            }
//            return false
//            
//        }
//    }
}
