//
//  ShadowButton.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/30.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

//@IBDesignable
class ShadowButton: UIButton {

    override init(frame: CGRect) {
        super.init(frame: frame)
        commonInit()
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        commonInit()
    }

    func commonInit() -> Void {

        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = .zero
        self.layer.shadowOpacity = 0.4
        self.layer.shadowRadius = 3.0
        self.layer.masksToBounds = false

    }

}

