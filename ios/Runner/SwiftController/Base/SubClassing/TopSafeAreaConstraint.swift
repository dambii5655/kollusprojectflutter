//
//  TopSafeAreaConstraint.swift
//  Kollus Player
//
//  Created by kiwan on 2021/01/13.
//  Copyright © 2021 kiwan. All rights reserved.
//

import Foundation

class TopSafeAreaConstraint: NSLayoutConstraint {
    override func awakeFromNib() {
        super.awakeFromNib()
        if #available(iOS 11.0, *) {
            let insets = UIApplication.shared.keyWindow?.safeAreaInsets ?? .zero
            self.constant = max(insets.top, 20)
        } else {
            // Pre-iOS 11.0
            self.constant = 20.0
        }
    }
}

