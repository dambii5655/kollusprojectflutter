//
//  Prefix.swift
//  Kollus Player
//
//  Created by 김용기 on 2021/05/24.
//  Copyright © 2021 kiwan. All rights reserved.
//

import Foundation



//#if true
#if DEBUG
    func NLog(_ message: String, filename: String = #file, function: String = #function, line: Int = #line) {
        print("[\(filename):\(line)] \(function) - \(message)")
    }
#else
    func NLog(_ message: String) {
        print("[ ggg  - \(message)")
    }
#endif
