//
//  includes.h
//  TestJson
//
//  Created by Franky.Jung on 2015. 5. 28..
//  Copyright (c) 2015년 Franky.Jung. All rights reserved.
//

#ifndef TestJson_includes_h
#define TestJson_includes_h

#import "ItemInfo.h"
#import "DirectoryJson.h"

#define TEST_MODE               1
#define USING_BAR_BUTTON_ITEM   1

typedef NS_ENUM (NSInteger, EditMode) {
    EditModeNone = 0,
    EditModeNormal,         // blank  -------- add|edit
    EditModeNoneSelected,   // cancel -------- blank
    EditModeSelected,       // cancel -------- cut|delete
    EditModeCut,            // blank  -------- paste|cancel
};

#endif
