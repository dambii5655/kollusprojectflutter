//
//  ItemInfo.h
//  TestJson
//
//  Created by Franky.Jung on 2015. 5. 27..
//  Copyright (c) 2015년 Franky.Jung. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ItemInfo : NSObject

@property NSString* title;
@property NSInteger type;   // 0: file, 1: dir

@end
