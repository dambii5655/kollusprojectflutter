//
//  DirectoryJson.h
//  TestJson
//
//  Created by Franky.Jung on 2015. 5. 19..
//  Copyright (c) 2015년 Franky.Jung. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItemInfo.h"

@interface DirectoryJson : NSObject

/**
 생성된 Json 파일의 유무를 판단
 */
+ (BOOL)checkJsonFile;

/**
 Json 파일의 내용을 dictionary 형태로 반환
 */
+ (NSMutableDictionary*)getJsonData;

/**
 입력된 dictionary를 사용하여 Json 파일을 생성 및 저장
 */
//+ (BOOL)writeJsonToFile:(NSMutableDictionary*)dictionary;
//
/**
 키 배열을 입력하여 dictionary 생성
 */
+ (NSMutableDictionary*)makeJson:(NSArray*)arrayKeys;

/**
 디렉토리 경로를 사용하여 새로운 디렉토리를 생성
 @param strName 추가될 디렉토리 이름
 @param dirPath 디렉토리를 추가할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)addDirectory:(NSString*)strName pathDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json error:(NSError **)error;

/**
 디렉토리 경로 삭제
 @param dirPath 삭제할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)removeDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;


/**
 디렉토리 서브 파일 리스트
 @param dirPath 삭제할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return 디렉토리 서브 파일 mck 리스트
 */

+ (NSArray *)checkFiles:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;

/**
 디렉토리 이동
 @param moveFrom 원본 디렉토리 경로
 @param moveTo 이동할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)moveDirectory:(NSArray*)moveFrom to:(NSArray*)moveTo originalJsonData:(NSMutableDictionary*)json error:(NSError **)error;

/**
 문자열을 사용하여 디렉토리 배열 생성
 @param strPath 입력 문자열
 @return 생성된 디렉토리 배열
 */
+ (NSArray*)createPathFromString:(NSString*)strPath;

/**
 디렉토리 경로 생성
 @param path 생성할 디렉토리 경로
 */
+ (NSMutableDictionary*)createDirectoryPath:(NSArray*)path originalJsonData:(NSMutableDictionary*)json;

/**
 디렉토리 경로에 파일 추가
 @param strKey 추가할 파일 키
 @param dirPath 추가할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)addFile:(NSString*)strKey pathDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;

/**
 디렉토리 경로에서 파일 삭제
 @param strKey 삭제할 파일 키
 @param dirPath 삭제할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)removeFile:(NSString*)strKey pathDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;

/**
 파일 이동
 @param strKey 이동할 파일 키
 @param moveFrom 원본 디렉토리 경로
 @param moveTo 이동할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)moveFile:(NSString*)strKey from:(NSArray*)moveFrom to:(NSArray*)moveTo originalJsonData:(NSMutableDictionary*)json;

/**
  현재의 dictionary 반환
 @param dirPath 리스트를 검출할 디렉토리
 @param json 원본 Json dictioinary
 */
+ (NSMutableDictionary*)getCurrentDict:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;

/**
 디렉토리에 포함된 파일/디렉토리 리스트 반환
 @param dirPath 리스트를 검출할 디렉토리
 @param json 원본 Json dictioinary
 */
+ (NSArray*)getList:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;


/**
 파일 경로 리턴
 @param mck mck
 @return array 파일 경로 리턴
 */
+ (NSArray *)searchPath:(NSString *)mck originalJsonData:(NSMutableDictionary*)json;


+ (NSMutableDictionary*)changeTitleDirectory:(NSArray*)dirPath to:(NSString *)moveTo originalJsonData:(NSMutableDictionary*)json error:(NSError **)error;

@end
