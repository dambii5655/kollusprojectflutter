//
//  DirectoryJson.m
//  TestJson
//
//  Created by Franky.Jung on 2015. 5. 19..
//  Copyright (c) 2015년 Franky.Jung. All rights reserved.
//

#import "DirectoryJson.h"

#define KOLLUS_DIRECTORY    NSDocumentDirectory // Documents 폴더

@implementation DirectoryJson

#pragma mark - JSON Data Hadle

/**
 생성된 Json 파일의 유무를 판단
 */
+ (BOOL)checkJsonFile
{
    NSString *directory = [NSSearchPathForDirectoriesInDomains(KOLLUS_DIRECTORY, NSUserDomainMask, YES) firstObject];
    
    // 폴더내에 파일 존재유무 확인
    NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, @"dirStruct.json"];
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath] == YES) {
        return YES;
    }
    return NO;
}

/**
 Json 파일의 내용을 dictionary 형태로 반환
 */
+ (NSMutableDictionary*)getJsonData
{
    if ([self checkJsonFile]) {
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        
        NSString *directory = [NSSearchPathForDirectoriesInDomains(KOLLUS_DIRECTORY, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, @"dirStruct.json"];
        NSData *jsonData = [NSData dataWithContentsOfFile:filePath];
        dict = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:nil];
        return [dict mutableCopy];
//        return dict;
    }
    return nil;
}

/**
 키 배열을 입력하여 dictionary 생성
 */
+ (NSMutableDictionary*)makeJson:(NSArray*)arrayKeys
{
    NSLog(@"[DirectoryJson]makeJson +++");
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
    [dict setValue:@"/" forKey:@"name"];
    NSMutableArray *files = [[NSMutableArray alloc] init];
    for (int i = 0; i < arrayKeys.count; i++) {
        NSDictionary *dic = [[NSDictionary alloc] initWithObjectsAndKeys:[arrayKeys objectAtIndex:i], @"key", nil];
        [files addObject:dic];
    }
    [dict setObject:files forKey:@"files"];
    NSMutableArray *dirs = [[NSMutableArray alloc] init];
    [dict setObject:dirs forKey:@"dirs"];
    [self writeJsonToFile:dict];
    NSLog(@"[DirectoryJson]makeJson ---");
    return dict;
}


#pragma mark - Directory Handle

/**
 디렉토리 경로를 사용하여 새로운 디렉토리를 생성
 @param strName 추가될 디렉토리 이름
 @param dirPath 디렉토리를 추가할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)addDirectory:(NSString*)strName pathDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json error:(NSError **)error {

    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
        }
    }
    
    // check "dirs"
    if (dirDict == nil) {
        NSLog(@"Dirs is invalid");
        dirDict = [[NSMutableArray alloc] init];
    }
    else {
        for (int i = 0; i < dirDict.count; i++) {
            NSMutableDictionary *dir = [dirDict objectAtIndex:i];
            if ([[dir objectForKey:@"name"] isEqualToString:strName])
            {
                NSLog(@"\'%@\' is aleady exist!", strName);
                return dict;
            }
        }
    }
    
    // add directory usig strName
    NSMutableDictionary *newDir = [[NSMutableDictionary alloc] init];
    [newDir setObject:strName forKey:@"name"];
    
    NSMutableArray *dirs = [[NSMutableArray alloc] init];
    [newDir setObject:dirs forKey:@"dirs"];
    
    NSMutableArray *files = [[NSMutableArray alloc] init];
    [newDir setObject:files forKey:@"files"];
    
    [dirDict addObject:newDir];
    [currentDict setObject:dirDict forKey:@"dirs"];
    
    [self writeJsonToFile:dict];
    return dict;
}

/**
 디렉토리 경로 삭제
 @param dirPath 삭제할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)removeDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json;
{
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    if (i == (dirPath.count-1)) {
                        [dirDict removeObjectAtIndex:j];
                    }
                    else {
                        currentDict = [dirDict objectAtIndex:j];
                        dirDict = [currentDict objectForKey:@"dirs"];
                    }
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
        }
    }
    
    [self writeJsonToFile:dict];
    return dict;
}

+ (NSArray *)checkFiles:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json
{
    NSMutableDictionary *dict = json;
    
    NSError *error = nil;
    NSData *data = nil;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    if (i == (dirPath.count-1)) {
                        NSLog(@"Remove Dir:\n%@", dirDict.description);
                        dirDict = [dirDict objectAtIndex:j];
//                        [self printJsonData:dirDict];
                        goto make_array;
                    }
                    else {
                        currentDict = [dirDict objectAtIndex:j];
                        dirDict = [currentDict objectForKey:@"dirs"];
                    }
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return nil;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
        }
    }
    return nil;
    
make_array:
    
    data = [NSJSONSerialization dataWithJSONObject:dirDict options:NSJSONWritingPrettyPrinted error:&error];
    NSString *stringJson = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    stringJson = [stringJson stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"stringJson >>> \n%@", stringJson);
    NSArray * files = [stringJson componentsSeparatedByString:@"\"key\":\""];
    NSMutableArray *keys = [[NSMutableArray alloc] init];
    for (int i = 1; i < files.count; i++) {
        [keys addObject:[[files objectAtIndex:i] substringToIndex:8]];
    }
    return [keys copy];

}

/**
 디렉토리 이동
 @param moveFrom 원본 디렉토리 경로
 @param moveTo 이동할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)moveDirectory:(NSArray*)moveFrom to:(NSArray*)moveTo originalJsonData:(NSMutableDictionary*)json error:(NSError **)error
{
    NSMutableDictionary *dict = json;
    
    // check dest dir vs. source dir
    for (int i = 0; i < moveTo.count; i++) {
        if ([[moveTo objectAtIndex:i] isEqualToString:[moveFrom objectAtIndex:i]]) {
            if (i >= moveFrom.count-1) {
                // ERROR: 이동할 수 없는 경로
                NSLog(@"Destination is not valid.");
                return dict;
            }
        }
        else break;
    }
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableDictionary *currentDict;
    NSMutableDictionary *sourceDict;
    BOOL    bFoundDir = NO;
    for (int i = 0; i < moveFrom.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[moveFrom objectAtIndex:i]])
                {
                    if (i == (moveFrom.count-1)) {
                        sourceDict = [[dirDict objectAtIndex:j] mutableCopy];
                        [dirDict removeObjectAtIndex:j];
                        dirDict = nil;
                        bFoundDir = YES;
                    }
                    else {
                        currentDict = [dirDict objectAtIndex:j];
                        dirDict = [currentDict objectForKey:@"dirs"];
                    }
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[moveFrom objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
        }
    }
    
    if (!bFoundDir) {
        NSLog(@"not found dir path");
        return dict;
    }
    
    for (int i = 0; i < moveTo.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[moveTo objectAtIndex:i]])
                {
                    if (i == (moveTo.count-1)) {
                        [[[dirDict objectAtIndex:j] objectForKey:@"dirs"] addObject:sourceDict];
                    }
                    else {
                        currentDict = [dirDict objectAtIndex:j];
                        dirDict = [currentDict objectForKey:@"dirs"];
                    }
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[moveTo objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            if (i == (moveTo.count-1)) {
                [dirDict addObject:sourceDict];
                [self writeJsonToFile:dict];
                return dict;
            }
        }
    }
    
    [self writeJsonToFile:dict];
    return dict;
}

/**
 디렉토리 경로 생성
 @param path 생성할 디렉토리 경로
 */
+ (NSMutableDictionary*)createDirectoryPath:(NSArray*)path originalJsonData:(NSMutableDictionary*)json {
    NSMutableDictionary *dict = json;
    
    for (int i = 1; i < path.count; i++) {
        NSArray *parent = [path subarrayWithRange:NSMakeRange(0, i)];
        dict = [DirectoryJson addDirectory:[path objectAtIndex:i] pathDirectory:parent originalJsonData:dict error:nil];
    }
    [self writeJsonToFile:dict];
    return dict;
}


#pragma mark - File Handle

/**
 디렉토리 경로에 파일 추가
 @param strKey 추가할 파일 키
 @param dirPath 추가할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)addFile:(NSString*)strKey pathDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json
{
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
    
    // check "files"
    if (dirFiles == nil) {
        NSLog(@"files is invalid");
        dirFiles = [[NSMutableArray alloc] init];
    }
    else {
        NSLog(@"files is valid");
        for (int i = 0; i < dirFiles.count; i++) {
            NSMutableDictionary *dir = [dirFiles objectAtIndex:i];
            if ([[dir objectForKey:@"key"] isEqualToString:strKey])
            {
                NSLog(@"\'%@\' is aleady exist!", dirFiles);
                return dict;
            }
        }
    }
    
    // add file usig strKey
    NSMutableDictionary *newFile = [[NSMutableDictionary alloc] init];
    [newFile setObject:strKey forKey:@"key"];
    [dirFiles addObject:newFile];
  
    [self writeJsonToFile:dict];
    
    return dict;
}

/**
 디렉토리 경로에서 파일 삭제
 @param strKey 삭제할 파일 키
 @param dirPath 삭제할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)removeFile:(NSString*)strKey pathDirectory:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json
{
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
    
    // check "files"
    if (dirFiles == nil) {
        NSLog(@"files is invalid");
        dirFiles = [[NSMutableArray alloc] init];
    }
    else {
        NSLog(@"files is valid");
    }
    
    // remove file usig strKey
    for (int i = 0; i < dirFiles.count; i++) {
        if ([[[dirFiles objectAtIndex:i] objectForKey:@"key"] isEqual:strKey]) {
            [dirFiles removeObjectAtIndex:i];
            break;
        }
    }
    
    [self writeJsonToFile:dict];
    return dict;
}

/**
 파일 이동
 @param strKey 이동할 파일 키
 @param moveFrom 원본 디렉토리 경로
 @param moveTo 이동할 디렉토리 경로
 @param json 원본 Json dictioinary
 @return NSMutableDictionary 생성된 Json dictionary
 */
+ (NSMutableDictionary*)moveFile:(NSString*)strKey from:(NSArray*)moveFrom to:(NSArray*)moveTo originalJsonData:(NSMutableDictionary*)json
{
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    NSMutableDictionary *srcFile;
    for (int i = 0; i < moveFrom.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[moveFrom objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[moveFrom objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
    
    // check "files"
    if (dirFiles == nil) {
        NSLog(@"files is invalid");
        dirFiles = [[NSMutableArray alloc] init];
    }
    else {
        NSLog(@"files is valid");
    }
    
    // add directory usig strName
    for (int i = 0; i < dirFiles.count; i++) {
        if ([[[dirFiles objectAtIndex:i] objectForKey:@"key"] isEqual:strKey]) {
            srcFile = [[dirFiles objectAtIndex:i] copy];
            [dirFiles removeObjectAtIndex:i];
            break;
        }
    }
    
    dict = [self addFile:[srcFile objectForKey:@"key"] pathDirectory:moveTo originalJsonData:dict];
    [self writeJsonToFile:dict];
    
    return dict;
}

+ (NSMutableDictionary*)changeTitleDirectory:(NSArray*)dirPath to:(NSString *)moveTo originalJsonData:(NSMutableDictionary*)json error:(NSError **)error
{
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    if (i == (dirPath.count-1)) {
                        
                        for(int k = 0 ; k < dirDict.count ; k++) {
                            NSDictionary *direcotry = [dirDict objectAtIndex:k];
                            NSString *name = [direcotry objectForKey:@"name"];
                            if([name isEqualToString:moveTo]) {
//                                NSString *errorMessage = NSLocalizedString(@"It has the same name.", nil);
//
//                                NSMutableDictionary* details = [NSMutableDictionary dictionary];
//                                [details setValue:errorMessage forKey:NSLocalizedDescriptionKey];
//                                *error = [NSError errorWithDomain:@"error" code:200 userInfo:details];
                                return nil;
                            }
                        }
                        
                        NSMutableDictionary *selectedDict = [dirDict objectAtIndex:j];
                        [selectedDict setObject:moveTo forKey:@"name"];
                        
                        NSLog(@"%@", dirDict);
                        NSLog(@"%@" , selectedDict);
                    }
                    else {
                        currentDict = [dirDict objectAtIndex:j];
                        dirDict = [currentDict objectForKey:@"dirs"];
                    }
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return dict;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
        }
    }
    
//    NSLog(@"%@", dict);
    
    [self writeJsonToFile:dict];
    return dict;
}
#pragma mark - List Handle

/**
  현재의 dictionary 반환
 @param dirPath 리스트를 검출할 디렉토리
 @param json 원본 Json dictioinary
 */
+ (NSMutableDictionary*)getCurrentDict:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json {
    
    NSMutableArray * list = [[NSMutableArray alloc] init];
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return nil;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
    
    for (int i = 0; i < dirDict.count; i++) {
        ItemInfo * item = [[ItemInfo alloc] init];
        item.type = 1;
        item.title = [[dirDict objectAtIndex:i] objectForKey:@"name"];
        [list addObject:item];
    }
    
    for (int i = 0; i < dirFiles.count; i++) {
        ItemInfo * item = [[ItemInfo alloc] init];
        item.type = 0;
        item.title = [[dirFiles objectAtIndex:i] objectForKey:@"key"];
        [list addObject:item];
    }
    
    return currentDict;
}


/**
 디렉토리에 포함된 파일/디렉토리 리스트 반환
 @param dirPath 리스트를 검출할 디렉토리
 @param json 원본 Json dictioinary
 */
+ (NSArray*)getList:(NSArray*)dirPath originalJsonData:(NSMutableDictionary*)json {
    
    NSMutableArray * list = [[NSMutableArray alloc] init];
    NSMutableDictionary *dict = json;
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return nil;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
    
    for (int i = 0; i < dirDict.count; i++) {
        ItemInfo * item = [[ItemInfo alloc] init];
        item.type = 1;
        item.title = [[dirDict objectAtIndex:i] objectForKey:@"name"];
        [list addObject:item];
    }
    
    for (int i = 0; i < dirFiles.count; i++) {
        ItemInfo * item = [[ItemInfo alloc] init];
        item.type = 0;
        item.title = [[dirFiles objectAtIndex:i] objectForKey:@"key"];
        [list addObject:item];
    }
    
    return list;
}



+ (NSArray *)searchPath:(NSString *)mck originalJsonData:(NSMutableDictionary*)json {
    NSMutableDictionary *dict = json;
    
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    
    NSMutableArray *dirPath = [[NSMutableArray alloc] initWithObjects:@"/", nil];
    
    for (int i = 0; i < dirPath.count; i++) {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]]) {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return nil;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
    
    NSLog(@"%@", dirDict);
    
    for(int i = 0 ; i < dirFiles.count ; i++) {
        NSString *key = [dirFiles[i] objectForKey:@"key"];
        NSLog(@"key %@ , mck %@" , key, mck);
        if([key isEqualToString: mck]) {
            return dirPath;
        }
    }
    
    for(int i = 0; i < dirDict.count ; i++) {
        NSDictionary *subDict = dirDict[i];
        NSString *subDirectoryName = [subDict objectForKey:@"name"];
        
        [dirPath addObject:subDirectoryName];
        
        NSLog(@"subPath : %@" ,dirPath);
        
        if([self isExistContents:dirPath mck:mck]) {
            return dirPath;
        }
        
        [dirPath removeObject:subDirectoryName];
        
    }
    
    
    
    
    return nil;
}

+ (BOOL)isExistContents:(NSMutableArray *)dirPath mck:(NSString *)mck {
    NSMutableDictionary *dict = [self getJsonData];
    
    // seach directory using dirPath
    NSMutableDictionary *parentDict = dict;
    NSMutableArray *dirDict;
    NSMutableArray *dirFiles;
    NSMutableDictionary *currentDict;
    for (int i = 0; i < dirPath.count; i++)
    {
        if (dirDict) {
            for (int j = 0; j < dirDict.count; j++) {
                if ([[[dirDict objectAtIndex:j] objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
                {
                    currentDict = [dirDict objectAtIndex:j];
                    dirDict = [currentDict objectForKey:@"dirs"];
                    dirFiles = [currentDict objectForKey:@"files"];
                    break;
                }
            }
        }
        else
        {
            if (![[parentDict objectForKey:@"name"] isEqualToString:[dirPath objectAtIndex:i]])
            {
                NSLog(@"not found dir path");
                return NO;
            }
            currentDict = parentDict;
            dirDict = [currentDict objectForKey:@"dirs"];
            dirFiles = [currentDict objectForKey:@"files"];
        }
    }
        
    for(int i = 0 ; i < dirFiles.count ; i++) {
        NSString *key = [dirFiles[i] objectForKey:@"key"];
        NSLog(@"key %@ , mck %@" , key, mck);
        if([key isEqualToString: mck]) {
            
            NSLog(@"%@", dirPath);
            return YES;
        }
    }
    
    
    for(int i = 0; i < dirDict.count ; i++) {
        NSDictionary *subDict = dirDict[i];
        NSString *subDirectoryName = [subDict objectForKey:@"name"];
        
        NSMutableArray *subPath = dirPath;
        [subPath addObject:subDirectoryName];
        if([self isExistContents:subPath mck:mck]) {
            return YES;
        }
        [subPath removeObject:subDirectoryName];
    }
    return NO;
    
}



#pragma mark - Util Methods

/**
 문자열을 사용하여 디렉토리 배열 생성
 @param strPath 입력 문자열
 @return 생성된 디렉토리 배열
 */
+ (NSArray*)createPathFromString:(NSString*)strPath {
    NSMutableArray *path = [[NSMutableArray alloc] init];
    NSMutableArray *inPath = (NSMutableArray*)[strPath componentsSeparatedByString:@"/"];
    NSLog(@"inpath: %@", inPath.description);
    if ([[inPath firstObject] isEqual:@""])
    {
        [inPath removeObjectAtIndex:0];
    }
    if ([[inPath lastObject] isEqual:@""])
    {
        [inPath removeLastObject];
    }
    [path addObject:@"/"];
    [path addObjectsFromArray:inPath];
    return [path copy];
}

#pragma mark - PRIVATE Methods

/**
 Unique ID 생성 및 반환
 */
+ (NSString*)makeUniqueID {
    NSInteger nid = (NSInteger)([NSDate date].timeIntervalSinceReferenceDate*1000);
    NSString *uid = [NSString stringWithFormat:@"%ld", (long)nid];
    return uid;
}

/**
 입력된 dictionary를 사용하여 Json 파일을 생성 및 저장
 */
+ (BOOL)writeJsonToFile:(NSMutableDictionary*)dictionary
{
//    NSLog(@"[DirectoryJson]writeJsonToFile +++");
    BOOL bRet = NO;
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&error];
    if (!error) {
        bRet = YES;
        NSString *jsonStr = [[NSString alloc] initWithData:data
                                                  encoding:NSUTF8StringEncoding];
        NSLog(@"Made Json String\n%@", jsonStr);
        
        NSString *directory = [NSSearchPathForDirectoriesInDomains(KOLLUS_DIRECTORY, NSUserDomainMask, YES) firstObject];
        NSString *filePath = [NSString stringWithFormat:@"%@/%@", directory, @"dirStruct.json"];
        
        [jsonStr writeToFile:filePath atomically:YES encoding:NSUTF8StringEncoding error:&error];
        if (error) {
            NSLog(@"%@", error.description);
        }
    }
    
//    NSLog(@"[DirectoryJson]writeJsonToFile ---");
    return YES;
}

+ (void)printJsonData:(NSMutableDictionary*)dictionary
{
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:dictionary
                                                   options:NSJSONWritingPrettyPrinted
                                                     error:&error];
    NSString *stringJson = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    stringJson = [stringJson stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"stringJson >>> \n%@", stringJson);
    NSArray * files = [stringJson componentsSeparatedByString:@"\"key\":\""];
    NSMutableArray *keys = [[NSMutableArray alloc] init];
    for (int i = 1; i < files.count; i++) {
        [keys addObject:[[files objectAtIndex:i] substringToIndex:8]];
    }
    NSLog(@">>>>>>>>> REMOVE FILE LIST >>>>>>>>>>>");
    for (int i = 0; i < keys.count; i++) {
        NSLog(@"[%d] %@", i, [keys objectAtIndex:i]);
    }
    NSLog(@">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
}

@end
