//
//  UIListTableView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/15.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

class UIListTableView: UITableView {
    
    var touchesBegan: (() -> ())?
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        touchesBegan?()
    }
}
