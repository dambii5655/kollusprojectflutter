//
//  ChattingWebView.swift
//  Kollus Player
//
//  Created by kiwan on 2021/01/13.
//  Copyright © 2021 kiwan. All rights reserved.
//

import Foundation
import WebKit

protocol ChattingWebViewDelegate: class {
    func onByPasstouched(view: ChattingWebView)
}

class ChattingWebView: UIView {
    weak var delegate: ChattingWebViewDelegate?
    
    var webView: WKWebView!
    
    private var webViewBottomMargin: Constraint? = nil
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let contentController = WKUserContentController()
        contentController.add(self, name: "kollushandler")
        
        let configuration = WKWebViewConfiguration()
        configuration.userContentController = contentController
        
        webView = WKWebView(frame: .zero, configuration: configuration)
//
        addSubview(webView)

        webView.snp.makeConstraints { [weak self] (maker) in
            maker.top.trailing.leading.equalToSuperview()
            self?.webViewBottomMargin = maker.bottom.equalToSuperview().constraint
        }
    }
    
    
    private func setUI() {
        backgroundColor = .clear
        webView.isOpaque = false
        webView.backgroundColor = .clear
//        webView.autoresizingMask = .
    }
    
    private func setEvent() {
        webView.uiDelegate = self
        webView.navigationDelegate = self
        webView.scrollView.delegate = self
        
        webView.scrollView.showsVerticalScrollIndicator = false
        webView.scrollView.showsHorizontalScrollIndicator = false
        webView.scrollView.bounces = false        
    }
    
    func setURL(_ url: URL) {
        let request = URLRequest(url: url)
        webView.load(request)
        self.isHidden = false
    }
    
    func isHiddenChattingView(_ isHidden: Bool) {
        self.isHidden = isHidden
        
        if !isHidden {
            onChattingVisibleChanged(isVisible: true)
        }
    }
    
    
    func initChattingView() {
        guard let playerView = (parentViewController as? PlayerViewController)?.playerView else { return }
        
        var dict: [String : Any] = [:]
        dict["admin"] = playerView.kollusChat.isAdmin ? 1 : 0
        dict["anonymous"] = playerView.kollusChat.isAnonymous ? 1 : 0
        dict["roomId"] = playerView.kollusChat.roomId
        dict["chattingUrl"] = playerView.kollusChat.chattingServer
        dict["userId"] = playerView.kollusChat.userId
        dict["nickName"] = playerView.kollusChat.nickName
        dict["photoUrl"] = playerView.kollusChat.photoUrl
        
        guard let data = try? JSONSerialization.data(withJSONObject: dict, options: []) else { return }
        guard let parameter = String(data: data, encoding: .utf8) else { return }
        
        let scriptFunctionName = "KOLLUS_CHATTING.KollusExternalDevice.onInit('\(parameter)')"
        
        webView.evaluateJavaScript(scriptFunctionName) { [weak self] (result, error) in
            if error != nil {
                print(error as Any)
            }
            else {
                self?.onControlUIVisibleChanged(isVisible: false)
            }
        }
    }
    
    func onChattingResume() {
        webView.evaluateJavaScript("KOLLUS_CHATTING.KollusExternalDevice.onResume()") { (result, error) in
//            print("error: \(error?.localizedDescription)")
        }
    }
    
    func onChattingVisibleChanged(isVisible: Bool) {
        let parameter = isVisible ? 1 : 0
        webView.evaluateJavaScript("KOLLUS_CHATTING.KollusExternalDevice.onChatVisibleChanged(\(parameter))") { (result, error) in
//            print("error: \(error?.localizedDescription)")
        }
    }
    
    func onControlUIVisibleChanged(isVisible: Bool) {
        let parameter = isVisible ? 1 : 0
        webView.evaluateJavaScript("KOLLUS_CHATTING.KollusExternalDevice.onControlUIVisibleChanged(\(parameter))") { (result, error) in
        }
    }
    
    func onChattingClose() {
        webView.evaluateJavaScript("KOLLUS_CHATTING.KollusExternalDevice.onClose()") { (result, error) in
//            print("error: \(error?.localizedDescription)")
        }
    }
    
    func isTouchableWebView(isEnable: Bool) {
        webView.isUserInteractionEnabled = isEnable
        self.isUserInteractionEnabled = isEnable
    }
    
    func updateChattingWebViewBottomMargin(value: Float) {
        guard let margin = webViewBottomMargin else { return }
        
        margin.update(offset: value)
    }
}

extension ChattingWebView: WKScriptMessageHandler, WKUIDelegate, WKNavigationDelegate {
    //MARK: - WKScriptMessageHandler
    func userContentController(_ userContentController: WKUserContentController, didReceive message: WKScriptMessage) {
        if message.name == "kollushandler" {
            
            if let body = message.body as? [String], let command = body.first {
                print(command)
                
                if command == "ready" {
                    initChattingView()
                }
                else if command == "bypassTap" {
                    
                    if let vc = self.parentViewController as? PlayerViewController {
                        if !vc.isShownKeyboard {
                            delegate?.onByPasstouched(view: self)
                        }
                    }
                    
                    
                }
            }
        }
        
    }
    
}


extension ChattingWebView: UIScrollViewDelegate {
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        scrollView.pinchGestureRecognizer?.isEnabled = false
    }
}
