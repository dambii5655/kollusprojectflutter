//
//  CheckTableViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/13.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class CheckTableViewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImgeView: UIImageView!
    @IBOutlet weak var languageButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        languageButton.layer.masksToBounds = true
        languageButton.layer.cornerRadius = 6
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        checkImgeView.isHidden = !selected
        
        if selected {
            titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .bold)
            titleLabel.textColor = UIColor(hexFromString: "#212529")
            
            languageButton.backgroundColor = UIColor(hexFromString: "#343a40")
            languageButton.setTitleColor(UIColor(hexFromString: "#ced4da"), for: .normal)
        }
        else {
            titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .regular)
            titleLabel.textColor = UIColor(hexFromString: "#495057")
            
            languageButton.setTitleColor(UIColor(hexFromString: "#adb5bd"), for: .normal)
            languageButton.backgroundColor = UIColor(hexFromString: "#f1f3f3")
        }
        
    }
    
    func setData(data: ActionSheetData) {
        if data.viewType == .language {
            titleLabel.text = data.title
            
            if data.selectedItem.count == 0 {
                languageButton.isHidden = true
            }
            else {
                languageButton.isHidden = false
                languageButton.setTitle(data.selectedItem.uppercased(), for: .normal)
            }
            
        }
        else {
            titleLabel.text = data.title
            languageButton.isHidden = true
        }
    }
    
}
