
//
//  DisclosureTableViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/15.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class DisclosureTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var selectedItemLabel: UILabel!
    
    @IBOutlet weak var disclosureImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
 
    
    func setData(data: ActionSheetData) {
        if data.viewType == .disclosureIndicator {
            disclosureImageView.isHidden = false
        }
        else {
            disclosureImageView.isHidden = true
        }
        
        titleLabel.text = data.title
        selectedItemLabel.text = data.selectedItem
    }
    
}
