//
//  HighlightedButton.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/04.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class HighlightedButton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            if (isHighlighted) {
                self.backgroundColor = UIColor(hexFromString: "#f1f3f5")
            }
            else {
                self.backgroundColor = UIColor.clear
            }

        }
    }
    
}
