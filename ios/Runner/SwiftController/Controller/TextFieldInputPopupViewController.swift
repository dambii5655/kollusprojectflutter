//
//  TextFieldInputPopupViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/04.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol TextFieldInputPopupViewDelegate: class {
    func onCreateFolderTouched(viewController: TextFieldInputPopupViewController, name: String)
}



class TextFieldInputPopupViewController: UIViewController {
    enum ViewType {
        case newFolder
        case changeTitle
        case bookmarkChangeTitle
    }
    
    var type: ViewType = .newFolder {
        didSet {
            updateUI()
        }
    }
    
    var currentPathByMoveViewController: [String]?
    
    weak var delegate: TextFieldInputPopupViewDelegate?
    
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var borderView: UIView!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var textField: UITextField!
    
    @IBOutlet weak var popupView: UIView!
    
    @IBOutlet weak var bottomMargin: NSLayoutConstraint!
    
    var data: KollusContent?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow(notification:)), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillBeHidden(notification:)), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(textDidChange(_:)), name: UITextField.textDidChangeNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UITextField.textDidChangeNotification, object: nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        textDidChange(Notification(name: UITextField.textDidChangeNotification))
        
        textField.becomeFirstResponder()
        
        if type == .newFolder || type == .changeTitle {
            textField.selectAll(nil)
        }
    }
    
    
    func setUI() {
        borderView.layer.masksToBounds = true
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor(hexFromString: "#dee2e6").cgColor
        borderView.layer.cornerRadius = 4
        
        popupView.layer.masksToBounds = true
        popupView.layer.cornerRadius = 8
        
        confirmButton.layer.cornerRadius = 4
    }
    
    func updateUI() {
        if type == .changeTitle {
            titleLabel.text = "Change folder name".localized()
            
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Confirm".localized(), for: .normal)
            
            guard let data = data else { return }
            
            textField.text = data.title
        }
        else if type == .bookmarkChangeTitle {
            titleLabel.text = "Change bookmark name".localized()
            
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Confirm".localized(), for: .normal)
            
        }
        else if type == .newFolder {
            titleLabel.text = "Create a new folder".localized()
                        
            
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Confirm".localized(), for: .normal)
            
            var path = Path.shared.currentPath
            
            if let currentPath = currentPathByMoveViewController {
                path = currentPath
            }
                        
            let folderList = StorageManager.shared.getFolderList(path: path)
            
            var defaultFolderList: [String] = []
            
            for folder in folderList {
                let result = folder.title.getArrayAfterRegex(regex: "New folder".localized() + "\\(\\d+\\)$")
                
                if folder.title == "New folder".localized() || result.count > 0 {
                    defaultFolderList.append(folder.title)
                }
            }
            
                        
            defaultFolderList.sort { (lhs, rhs) -> Bool in
                let lhsString = lhs.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                let rhsString = rhs.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "")
                
                guard let lhsInt = Int(lhsString), let rhsInt = Int(rhsString) else { return true}
                
                if lhsInt < rhsInt {
                    return false
                }
                
                return true
            }
            
            
            
            if defaultFolderList.count == 0 {
                textField.text = "New folder".localized()
            }
            else if defaultFolderList.count == 1 && defaultFolderList.first! == "New folder".localized() {
                textField.text = "New folder".localized() + "(1)"
            }
            else {
                guard let maxNumber = defaultFolderList.first?.components(separatedBy: CharacterSet.decimalDigits.inverted).joined(separator: "") else { return }
                if let number = Int(maxNumber) {
                    textField.text = "New folder".localized() + "(\(number + 1))"
                }
            }
            
//            textField.becomeFirstResponder()
            textField.selectAll(nil)
            
        }
    }
    
//    func nextFolderName() -> String {
    @IBAction func onBackgroundTouched(_ sender: UIButton) {
        view.endEditing(true)
    }
    
    @IBAction func onCancelTouched(_ sender: UIButton) {
        if textField.isFirstResponder {
            view.endEditing(true)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [weak self] in
                self?.dismiss(animated: false, completion: nil)
            }
        }
        else {
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    
    @IBAction func onConfirmTouched(_ sender: UIButton) {
        guard let folderName = textField.text else { return }
        //
        view.endEditing(true)
        
        if type == .newFolder {
            do {
                var path = Path.shared.currentPath
                if let movePath = currentPathByMoveViewController {
                    path = movePath
                }
                else {
                    path = Path.shared.currentPath
                }
                
                try DirectoryJson.addDirectory(folderName, pathDirectory: path, originalJsonData: DirectoryJson.getData())
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [unowned self] in
                    self.dismiss(animated: false) { [unowned self] in
                        self.delegate?.onCreateFolderTouched(viewController: self, name: folderName)
                    }
                }
                
            }
            catch {
                self.backgroundView.makeToast(error.localizedDescription)
            }
        }
        else if type == .bookmarkChangeTitle {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [unowned self] in
                self.dismiss(animated: false) { [unowned self] in
                    self.delegate?.onCreateFolderTouched(viewController: self, name: folderName)
                }
            }
        }
        else {
            guard let data = data else { return }
            guard let folderName = textField.text else { return }
            do {
                var fromPath = Path.shared.currentPath
                fromPath.append(data.title)
           
                try DirectoryJson.changeTitleDirectory(fromPath, to: folderName, originalJsonData: DirectoryJson.getData())
                
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [unowned self] in
                    self.dismiss(animated: false) { [unowned self] in
                        self.delegate?.onCreateFolderTouched(viewController: self, name: folderName)
                    }
                }
            }
            catch {
                self.backgroundView.makeToast(error.localizedDescription)
            }
        }
        
    }
    
    //MARK: - UIKeyBoardNotification
    @objc func keyboardWillShow(notification: NSNotification){
        let keyboardSize = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.size
        var keyboardHeight = keyboardSize.height
        
        if #available(iOS 11.0, *) {
            let bottomInset = view.safeAreaInsets.bottom
            keyboardHeight += bottomInset
        }
        
        self.bottomMargin.constant = keyboardHeight
        self.view.layoutIfNeeded()
        
    }
    
    @objc func keyboardWillBeHidden(notification: NSNotification){
        self.bottomMargin.constant = 0
        self.view.layoutIfNeeded()
    }
    
}


extension TextFieldInputPopupViewController: UITextFieldDelegate {    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
    }
    
    @objc private func textDidChange(_ notification: Notification) {
        if type == .bookmarkChangeTitle {
            confirmButton.isEnabled = true
            return
        }
        
        
        if let textField = textField {
            if textField.text?.count ?? 0 > 0 {
                confirmButton.isEnabled = true
                confirmButton.backgroundColor = UIColor(hexFromString: "#007aff")
            }
            else {
                confirmButton.isEnabled = false
                confirmButton.backgroundColor = UIColor(hexFromString: "#c1dfff")
            }
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        borderView.layer.borderColor = UIColor(hexFromString: "#626262").cgColor
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        borderView.layer.borderColor = UIColor(hexFromString: "#dee2e6").cgColor
    }
}
