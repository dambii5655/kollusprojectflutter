//
//  ActionSheetViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/07.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation
import UIKit

protocol ActionSheetViewDelegate: AnyObject {
    
    func onActionSheetClose()
    func onScreenRotation()
    func onVideoScaleChanged(videoScale: Int)
    func onSeekRangeChanged(seekRange: Int)
    func onVideoResolutionChanged(videoResolution: Int)
    func onSubtitleLanguageChanged(subtitleLanguage: Int)
    func onSubtitleStyleChanged()
    func onSubtitleBgChanged()
}

class ActionSheetViewController: UIViewController {
    weak var kollusContentEntity: KollusContentEntity?
    
    enum Mode {
        case sort
        case setting
        case videoScale
        case seekRange
        case videoResolution
        case subtitleLanguage
        case subtitleStyle
    }
    
    weak var delegate: ActionSheetViewDelegate?
    
    weak var currentListViewController: ListViewController?
    
    var sortDatas: [ActionSheetData] = []
    
    lazy var animationHeight = {
        return (sortDatas.count + 5) * 46
    }()
    
    var mode: Mode = .sort
    var isLiveMode = false
    var canModifyPlaybackRate = true
    var isAudioWatermarkingPlay = false
    
    var subtitleList: [SubTitleInfo] = []
    var streamInfoList: [StreamInfo] = []
    
    var selectedSubtitleIndex = 1
    var selectedStreamInfoIndex = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var closeBottomButtom: UIButton!
    @IBOutlet weak var closeSettingButton: UIButton!
    
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionSheetBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var actionSheetRightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var subtitleStyleActionSheetView: SubtitleStyleActionSheetView!

    @IBOutlet weak var closeLabel: UILabel!
    
    private var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        } else {
            return UIApplication.shared.statusBarOrientation
        }
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        NLog("viewDidLoad +++")
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CheckTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckTableViewCell")
        tableView.register(UINib(nibName: "DisclosureTableViewCell", bundle: nil), forCellReuseIdentifier: "DisclosureTableViewCell")
        
        fetchActionSheetDatas()
        subtitleStyleActionSheetView.delegate = self
        
        
        self.view.layoutIfNeeded()
        if UIApplication.shared.statusBarOrientation == .portrait || UIDevice.current.userInterfaceIdiom == .pad {
            tableViewHeightConstraint.constant = CGFloat(getTableViewHeight())
            actionSheetBottomConstraint.constant = -CGFloat(animationHeight)
        }
        else {
            tableViewHeightConstraint.constant = CGFloat(getTableViewHeight())
            actionSheetRightConstraint.constant = -CGFloat(UIScreen.main.bounds.width / 2) - 50
        }
        closeButton.accessibilityLabel = "Close".localized()
        closeLabel.text = "Close".localized()
        closeBottomButtom.accessibilityLabel = "Close".localized()
        closeSettingButton.accessibilityLabel = "Close setting".localized()
        
        NLog("viewDidLoad ---")
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.25) { [unowned self] in
            if UIApplication.shared.statusBarOrientation == .portrait || UIDevice.current.userInterfaceIdiom == .pad  {
                self.actionSheetBottomConstraint.constant = 0
                self.actionSheetRightConstraint.constant = 0
            }
            else {
                self.actionSheetBottomConstraint.constant = 0
                self.actionSheetRightConstraint.constant = 0
            }
            self.view.layoutIfNeeded()
        }
    }

    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            self?.subtitleStyleActionSheetView.fetchDatas()
        }        
    }

    
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        super.willTransition(to: newCollection, with: coordinator)
        
        if mode == .setting {
            coordinator.animate(alongsideTransition: { [weak self] (context) in
                self?.updateDeviceOrientationLabel()
            })
        }
        else {
            tableViewHeightConstraint.constant = CGFloat(getTableViewHeight())
        }
    }
    
    
    //MARK: - Func
    
    func updateDeviceOrientationLabel() {
        guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? DisclosureTableViewCell else { return }
        if UIApplication.shared.statusBarOrientation == .portrait {
            cell.selectedItemLabel.text = "Vertical".localized()
        }
        else {
            cell.selectedItemLabel.text = "Horizontal".localized()
        }
    }
    
    func getTableViewHeight() -> Int{
        return sortDatas.count * 46
    }
    
    func fetchActionSheetDatas() {
        NLog("fetchActionSheetDatas mode : \(mode)")
        switch mode {
        case .sort:
            titleLabel.text = "Sort".localized()
            sortDatas = ActionSheetData.createSortDatas()
            NLog("fetchActionSheetDatas .sort sortDatas : \(sortDatas)")
        case .setting:
            titleLabel.text = "Setting".localized()
            let isExistSubtitle = subtitleList.count > 1 ? true : false   // 감추기로 1 이상
            let isExistStreamInfo = streamInfoList.count > 1 ? true : false   // 감추기로 1 이상
            sortDatas = ActionSheetData.createSettingDatas(canModifyPlaybackRate, isLiveMode, isExistSubtitle, isExistStreamInfo)            
            setSelectedSubtitleText()
            setStreamInfoText()
            NLog("fetchActionSheetDatas subtitleList : \(subtitleList)")
            NLog("fetchActionSheetDatas streamInfoList : \(streamInfoList)")
            NLog("fetchActionSheetDatas .setting sortDatas : \(sortDatas)")
        case .videoScale:
            titleLabel.text = "Screen mode".localized()
            sortDatas = ActionSheetData.createVideoScaleDatas()
            NLog("fetchActionSheetDatas .videoScale sortDatas : \(sortDatas)")

        case .seekRange:
            titleLabel.text = "Moving range".localized()
            sortDatas = ActionSheetData.createSeekRangeDatas()
            NLog("fetchActionSheetDatas .seekRange sortDatas : \(sortDatas)")
        case .videoResolution:
            titleLabel.text = "Quality".localized()
            sortDatas = createStreamInfoDatas()
            NLog("fetchActionSheetDatas .videoResolution sortDatas : \(sortDatas)")
        case .subtitleLanguage:
            titleLabel.text = "Subtitle language".localized()
            sortDatas = createSubtitleDatas()
            NLog("fetchActionSheetDatas .subtitleLanguage sortDatas : \(sortDatas)")

        case .subtitleStyle:
            titleLabel.text = "Subtitle style".localized()
            NLog("fetchActionSheetDatas .subtitleStyle sortDatas : \(sortDatas)")
        }
    }
    
    func reloadDataAnimation() {
        if UIApplication.shared.statusBarOrientation == .portrait || UIDevice.current.userInterfaceIdiom == .pad {
            UIView.animate(withDuration: 0.25, animations: { [unowned self] in
                self.tableViewHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
            }) { (completion) in
                UIView.animate(withDuration: 0.25) { [unowned self] in
                    self.tableViewHeightConstraint.constant = CGFloat(self.getTableViewHeight())
                    if mode == .subtitleStyle {
                        self.tableView.alpha = 0
                        self.subtitleStyleActionSheetView.alpha = 1                        
                        UIAccessibility.post(notification: .layoutChanged, argument: titleLabel)
                    }
                    else {
                        self.tableView.reloadData()
                    }
                    
                    self.view.layoutIfNeeded()
                }
            }
        }
        else {
            if mode == .subtitleStyle {
                UIView.animate(withDuration: 0.5) { [unowned self] in
                    self.subtitleStyleActionSheetView.alpha = 1
                    self.tableView.alpha = 0
                    self.subtitleStyleActionSheetView.layoutIfNeeded()
                    UIAccessibility.post(notification: .layoutChanged, argument: titleLabel)
               }
                
            }
            else {
                UIView.transition(with: tableView, duration: 0.5, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() })
            }
        }
    }
    
    
    func tableViewReloadData() {
        fetchActionSheetDatas()
        tableView.reloadData()
    }
    
    //MARK: - Datas
    private func createSubtitleDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        for i in 0 ..< subtitleList.count {
            let subtitle = subtitleList[i]
            let isSelected = i == selectedSubtitleIndex
        
            sortData.append(ActionSheetData(viewType: .language, index: i, isSelected: isSelected, title: subtitle.strName, selectedItem: subtitle.strLanguage))
        }
        return sortData
    }
    
    private func setSelectedSubtitleText() {
        let isExistSubtitle = subtitleList.count > 1 ? true : false
        if isExistSubtitle {
            for i in 0 ..< sortDatas.count {
                if sortDatas[i].title == "Subtitle language".localized() {
                    sortDatas[i].selectedItem = subtitleList[selectedSubtitleIndex].strName
                }
            }
        }
    }
    
    private func createStreamInfoDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        for i in 0 ..< streamInfoList.count {
            let streamInfo = streamInfoList[i]
            
            var title = ""
            
            if i == 0 {
                title = streamInfo.bandwidth
            }
            else if Int(streamInfo.streamHeight) != 0 {
                title = "\(streamInfo.streamHeight)P"
            }
            else {
                title = "\(Int(streamInfo.bandwidth) ?? 0 / 1000)Kbps"
            }
            
            let isSelected = i == selectedStreamInfoIndex
        
            sortData.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: isSelected, title: title, selectedItem: ""))
        }
        return sortData
    }
    
    private func setStreamInfoText() {
        let isExistStreamInfo = streamInfoList.count > 1 ? true : false
        if isExistStreamInfo {
            for i in 0 ..< sortDatas.count {
                if sortDatas[i].title == "Quality".localized() {
                    var title = ""
                    
                    if selectedStreamInfoIndex == 0 {
                        title = streamInfoList[selectedStreamInfoIndex].bandwidth
                    }
                    else if Int(streamInfoList[selectedStreamInfoIndex].streamHeight) != 0 {
                        title = "\(streamInfoList[selectedStreamInfoIndex].streamHeight)P"
                    }
                    else {
                        title = "\(Int(streamInfoList[selectedStreamInfoIndex].bandwidth) ?? 0 / 1000)Kbps"
                    }
                    
                    sortDatas[i].selectedItem = title
                }
            }
        }
    }
    
    //MARK: - Event
    
    @IBAction func onCloseTouched(_ sender: UIButton?) {
        UIView.animate(withDuration: 0.25, animations: { [unowned self] in
            if UIApplication.shared.statusBarOrientation == .portrait || UIDevice.current.userInterfaceIdiom == .pad {
                self.actionSheetBottomConstraint.constant = -CGFloat(self.animationHeight)
            }
            else {
                self.actionSheetRightConstraint.constant = -CGFloat(UIScreen.main.bounds.width / 2) - 50
            }
            self.view.layoutIfNeeded()
        }) { (complete) in
            self.dismiss(animated: false) { [unowned self] in
                self.currentListViewController?.reloadContentsList()
                self.delegate?.onActionSheetClose()

            }
        }
    }
        
    deinit {
        print("ActionSheetViewController Deinitted")
    }
}

extension ActionSheetViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        NLog("sortDatas.count : \(sortDatas.count)")
        return sortDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = sortDatas[indexPath.row]
        NLog("data.title : \(data.title)")
        NLog("mode : \(mode)")
        if mode == .setting {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DisclosureTableViewCell", for: indexPath) as! DisclosureTableViewCell
            cell.setData(data: data)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckTableViewCell", for: indexPath) as! CheckTableViewCell
            cell.setData(data: data)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if mode != .setting {
            let data = sortDatas[indexPath.row]
            
            if data.isSelected {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
        else {
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if mode == .sort {
            PreferenceManager.sortOrder = indexPath.row
            tableViewReloadData()
        }
        else if mode == .setting && indexPath.row == 0 {
            NLog("ActionSheetViewController setting")
            if UIApplication.shared.statusBarOrientation == .portrait {
                AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
            }
            else {
                AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            }
            delegate?.onScreenRotation()
            self.dismiss(animated: false)
            
//            updateDeviceOrientationLabel()
        }
        else if mode == .videoScale {
            delegate?.onVideoScaleChanged(videoScale: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .seekRange {
            PreferenceManager.seekRange = indexPath.row
            delegate?.onSeekRangeChanged(seekRange: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .videoResolution {
            selectedStreamInfoIndex = indexPath.row
            delegate?.onVideoResolutionChanged(videoResolution: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .subtitleLanguage {
            selectedSubtitleIndex = indexPath.row
            delegate?.onSubtitleLanguageChanged(subtitleLanguage: indexPath.row)
            tableViewReloadData()            
        }
        else if mode == .subtitleStyle {
            
        }
        else {
            let title = sortDatas[indexPath.row].title
            
            if title == "Screen mode".localized() {
                mode = .videoScale
            }
            else if title == "Moving range".localized() {
                mode = .seekRange
            }
            else if title == "Quality".localized() {
                mode = .videoResolution
            }
            else if title == "Subtitle language".localized() {
                mode = .subtitleLanguage
            }
            else if title == "Subtitle style".localized() {
                mode = .subtitleStyle
            }
            
            fetchActionSheetDatas()
            reloadDataAnimation()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension ActionSheetViewController: SubtitleStyleActionSheetViewDelegate {
    func onSubtitleStyleChanged(view: SubtitleStyleActionSheetView) {
        self.delegate?.onSubtitleStyleChanged()
    }
    func onSubtitleBgChanged(view: SubtitleStyleActionSheetView) {
        self.delegate?.onSubtitleBgChanged()
    }
}
