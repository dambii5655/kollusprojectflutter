//
//  SubtitleColorCollectionViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/25.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class SubtitleColorCollectionViewCell: UICollectionViewCell {

    var data: SubtitleColorData? {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var circleView: UIView!
    @IBOutlet weak var checkImageView: UIImageView!
    
    
    override func awakeFromNib() {
        circleView.layer.borderWidth = 1
    }
    
    override func layoutIfNeeded() {
        super.layoutIfNeeded()
        
        circleView.layer.cornerRadius = circleView.bounds.width / 2
    }
    
    func updateUI() {
        guard let data = data else { return }
        circleView.layer.borderColor = UIColor(hexFromString: "#868e96").cgColor
        circleView.backgroundColor = data.color
        
        if data.color == SubtitleColor.white.toColor || data.color == SubtitleColor.lightGray.toColor || data.color == SubtitleColor.blue.toColor
            || data.color == SubtitleColor.yellow.toColor || data.color == SubtitleColor.green.toColor {
            checkImageView.image = #imageLiteral(resourceName: "check18black")
        }
        else {
            checkImageView.image = #imageLiteral(resourceName: "check18gray")
        }
        
        if data.isSelected {
            checkImageView.isHidden = false
        }
        else {
            checkImageView.isHidden = true
        }
    }
}
