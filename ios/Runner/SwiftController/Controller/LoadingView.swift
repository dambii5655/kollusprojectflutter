//
//  LoadingView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit
import Lottie

class LoadingView: UIView {
    var indicatorAnimationView: AnimationView!
    
    var indicatorView: UIActivityIndicatorView!
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }

    private func setNib() {
        let view = Bundle.main.loadNibNamed("LoadingView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }

    private func setUI() {
        self.backgroundColor = .clear
        
        if #available(iOS 10.0, *) {
            indicatorAnimationView = AnimationView(name: "indicator")
            
            self.addSubview(indicatorAnimationView)
            
            indicatorAnimationView.snp.makeConstraints { (maker) in
                maker.center.equalToSuperview()
            }
        }
        else {
            indicatorView = UIActivityIndicatorView()
            
            self.addSubview(indicatorView)
            
            indicatorView.snp.makeConstraints { (maker) in
                maker.center.equalToSuperview()
                maker.width.height.equalTo(30)
            }
            
            indicatorView.style = .whiteLarge
            indicatorView.tintColor = .white
            indicatorView.startAnimating()
            indicatorView.hidesWhenStopped = true
        }
        
    }

    private func setEvent() {
        if #available(iOS 10.0, *) {
            if let indicator = indicatorAnimationView {
                indicator.loopMode = .loop
            }
        }
        
    }

    override var isHidden: Bool {
        didSet {
            if isHidden {
                guard let indicatorAnimationView = indicatorAnimationView else { return }
                indicatorAnimationView.stop()
            }
            else {
                guard let indicatorAnimationView = indicatorAnimationView else { return }
                indicatorAnimationView.play()
            }
        }
    }
    
}
