//
//  PlayerCodecViewController.swift
//  Kollus Player
//
//  Created by 김용기 on 2022/08/10.
//  Copyright © 2022 kiwan. All rights reserved.
//

import UIKit

class PlayerCodecViewController: UIViewController{

    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    
    var playerCodecDatas: [ActionSheetData] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CheckTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckTableViewCell")
        playerCodecDatas = ActionSheetData.createPlayerCodecDatas()
        
        backButton.accessibilityLabel = "back".localized()
        NLog("playerCodecDatas.count : \(playerCodecDatas.count)")


        // Do any additional setup after loading the view.
    }
    
    @IBAction func onBackTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func tableViewReloadData() {
        playerCodecDatas = ActionSheetData.createPlayerCodecDatas()
        tableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlayerCodecViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        NLog("playerCodecDatas.count : \(playerCodecDatas.count)")
        return playerCodecDatas.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = playerCodecDatas[indexPath.row]
        NLog("data.title : \(data.title)")
        let cell = tableView.dequeueReusableCell(withIdentifier: "CheckTableViewCell", for: indexPath) as! CheckTableViewCell
        cell.setData(data: data)
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        let data = playerCodecDatas[indexPath.row]
        NLog("indexPath : \(indexPath.row),  data.isSelected : \(data.isSelected)")

        if data.isSelected {
            tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        PreferenceManager.playerCodec = indexPath.row
        tableViewReloadData()
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
