//
//  GestureViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/05.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class GestureViewController: UIViewController {

    @IBOutlet var borderViewArray: [UIView]!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for view in borderViewArray {
            view.layer.borderColor = UIColor(hexFromString: "#e9ecef").cgColor
        }
        
        backButton.accessibilityLabel = "back".localized()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onBackTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
