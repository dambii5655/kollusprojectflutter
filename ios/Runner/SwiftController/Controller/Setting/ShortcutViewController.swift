//
//  ShortcutViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/29.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class ShortcutViewController: UIViewController {

    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        backButton.accessibilityLabel = "back".localized()
    }
    

    @IBAction func onCloseTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
        
    }
}
