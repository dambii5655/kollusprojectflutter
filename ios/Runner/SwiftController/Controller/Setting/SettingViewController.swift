
//
//  SettingViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/14.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class SettingViewController: UIViewController {
    
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var playerVersionLabel: UILabel!
    
    @IBOutlet weak var DRMContentsCheckBoxButton: UIButton!
    
    @IBOutlet weak var playerCodec: UIButton!
    
    
    @IBOutlet weak var backgroundAudioPlaySiwtch: UISwitch!
    @IBOutlet weak var useNetworkDataSwitch: UISwitch!
    
    @IBOutlet weak var downloadSizeLabel: UILabel!
    @IBOutlet weak var downloadContentsDeleteButton: UIButton!
    @IBOutlet weak var freeSizeLabel: UILabel!
    @IBOutlet weak var cacheSizeLabel: UILabel!
    @IBOutlet weak var cacheDeleteButton: UIButton!
    
    @IBOutlet weak var frequentQuestionButton: UIButton!
    @IBOutlet weak var GestureButton: UIButton!
    @IBOutlet weak var ShortcutsButton: UIButton!
    @IBOutlet weak var DeviceInformationButton: UIButton!
    
    @IBOutlet weak var loadingView: LoadingView!
    
    
    
    var version: String? {
        guard let dictionary = Bundle.main.infoDictionary, let version = dictionary["CFBundleShortVersionString"] as? String else {
            return nil
        }
        
        return version
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
        
        if #available(iOS 13.0, *) {
            isModalInPresentation = false
        }
    }
        
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if let topViewController = UIApplication.getTopViewController() as? ListViewController {
            topViewController.reloadContentsList()
        }
        else if let topViewController = UIApplication.getTopViewController() as? RecentlyAddedFileViewController {
            topViewController.reloadContentsList()
        }
    }
    
    private func updateUI() {
        closeButton.accessibilityLabel = "Close".localized()
        playerCodec.accessibilityLabel = "Decoder settings".localized()
        frequentQuestionButton.accessibilityLabel = "Frequestly Asked Questions".localized()
        GestureButton.accessibilityLabel = "Gesture".localized()
        ShortcutsButton.accessibilityLabel = "Shortcuts".localized()
        DeviceInformationButton.accessibilityLabel = "Device information".localized()
        
        playerVersionLabel.text = version
        
        downloadSizeLabel.text = StorageManager.shared.getContentsTotalSize()
        downloadSizeLabel.accessibilityLabel = StorageManager.shared.getContentsTotalSizeAccessibility()
        
//        freeSizeLabel.text = DiskStatus.freeDiskSpace
        freeSizeLabel.text = StorageManager.shared.getfreeDiskSize()
        freeSizeLabel.accessibilityLabel = StorageManager.shared.getfreeDiskSizeAccessibility()
        
        cacheSizeLabel.text = StorageManager.shared.getCacheDataSize()
        cacheSizeLabel.accessibilityLabel = StorageManager.shared.getCacheDataSizeAccessibility()
        
        DRMContentsCheckBoxButton.isSelected = PreferenceManager.DRMCheckBox
        backgroundAudioPlaySiwtch.isOn = PreferenceManager.isBackgroundAudioPlay
        useNetworkDataSwitch.isOn = PreferenceManager.isUseNetworkData
        
        
        DRMContentsCheckBoxButton.titleLabel?.adjustsFontSizeToFitWidth = true
        DRMContentsCheckBoxButton.titleLabel?.minimumScaleFactor = 0.5
        DRMContentsCheckBoxButton.titleLabel?.baselineAdjustment = .alignCenters
        
    }
    
    //MARK: - Event
    @IBAction func onPlayerCodecTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "PlayerCodecViewController", bundle: nil)
        let playerCodecViewController = storyBoard.instantiateInitialViewController() as! PlayerCodecViewController
        self.navigationController?.pushViewController(playerCodecViewController, animated: true)
    }
    
    @IBAction func onDRMCheckBoxTouched(_ sender: UIButton) {
        DRMContentsCheckBoxButton.isSelected.toggle()
        PreferenceManager.DRMCheckBox = DRMContentsCheckBoxButton.isSelected
        if DRMContentsCheckBoxButton.isSelected == false{
            if UIAccessibility.isVoiceOverRunning {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                    let stringLocal = "Cancel".localized()
                    UIAccessibility.post(notification: .announcement, argument: stringLocal)
                }
            }
        }
    }
    
    @IBAction func onDRMRenewTouched(_ sender: UIButton) {
        StorageManager.shared.renewDRMContents(isAll: !DRMContentsCheckBoxButton.isSelected)
        
        view.makeToast("DRM renewal is complete.".localized())
    }
        
    @IBAction func onUsePlayerType(_ sender: UISwitch) {
        PreferenceManager.UsePlayerType = sender.isOn
    }
    
    @IBAction func onBackgroundAudioPlayValueChanged(_ sender: UISwitch) {
        PreferenceManager.isBackgroundAudioPlay = sender.isOn
    }
    
    @IBAction func onUseNetworkValueChanged(_ sender: UISwitch) {
        PreferenceManager.isUseNetworkData = sender.isOn
    }
    
    @IBAction func onDownloadContentsDeleteTouched(_ sender: UIButton) {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        
        if let downloadListViewController = appDelegate.downloadListViewController {
            if downloadListViewController.downloadContentsList.count > 0 {
                view.makeToast("There is content being downloaded. \nCancel download and resume it.".localized())
                return
            }
        }
        
        
        let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
        let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
        confirmPopupViewController.delegate = self
        confirmPopupViewController.type = .contentsAllDelete
        present(confirmPopupViewController, animated: false, completion: nil)
    }
    
    @IBAction func onCacheDeleteTouched(_ sender: UIButton) {
        loadingView.isHidden = false
        DispatchQueue.global().async {
            StorageManager.shared.deleteCacheDatas()
            
            DispatchQueue.main.async { [unowned self] in
                loadingView.isHidden = true
                updateUI()
                print("updateUI")
            }
        }
        
        
    }
    
    @IBAction func onFrequentQuestionTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "WebViewController", bundle: nil)
        let shortcutViewController = storyBoard.instantiateInitialViewController() as! WebViewController
        self.navigationController?.pushViewController(shortcutViewController, animated: true)
    }
    
    @IBAction func onGestureTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "GestureViewController", bundle: nil)
        let shortcutViewController = storyBoard.instantiateInitialViewController() as! GestureViewController
        self.navigationController?.pushViewController(shortcutViewController, animated: true)
    }
    
    @IBAction func onShortcutTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ShortcutViewController", bundle: nil)
        let shortcutViewController = storyBoard.instantiateInitialViewController() as! ShortcutViewController
        self.navigationController?.pushViewController(shortcutViewController, animated: true)
    }
    
    @IBAction func onDeviceInformationTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "DeviceInformationViewController", bundle: nil)
        let shortcutViewController = storyBoard.instantiateInitialViewController() as! DeviceInformationViewController
        self.navigationController?.pushViewController(shortcutViewController, animated: true)
        
        
    }
    
    @IBAction func onInitilaizeAppDataTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
        let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
        confirmPopupViewController.delegate = self
        confirmPopupViewController.type = .initializeAppData
        present(confirmPopupViewController, animated: false, completion: nil)        
    }
    
    @IBAction func onCloseTouched(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
        
}

extension SettingViewController: ConfirmPopupViewDelegate {
    func onConfirmTouched(viewController: ConfirmPopupViewController) {
        if viewController.type == .initializeAppData {
            PreferenceManager.resetPreferenceDatas()
            CoreDataManager.shared.resetKollusContentData()
            self.view.makeToast("Initialization is complete.".localized())
        }
        else if viewController.type == .contentsAllDelete {
            loadingView.isHidden = false
            DispatchQueue.global().async {
                StorageManager.shared.removeAllDownloadContents()
                
                DispatchQueue.main.async { [weak self] in
                    self?.loadingView.isHidden = true
                    self?.updateUI()
                }
            }
            
            
        }
    }
    
    func onCancelTouched(viewController: ConfirmPopupViewController) {
        
    }
}
