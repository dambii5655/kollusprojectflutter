//
//  DeviceInformationViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/29.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class DeviceInformationViewController: UIViewController {
    
    
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var appVersionLabel: UILabel!
    
    @IBOutlet weak var deviceIDLabel: UILabel!        
    @IBOutlet weak var deviceVersionLabel: UILabel!
    @IBOutlet weak var modelNameLabel: UILabel!
    
    @IBOutlet weak var modelNumberLabel: UILabel!
    
    
    var version: String? {
        guard let dictionary = Bundle.main.infoDictionary, let version = dictionary["CFBundleShortVersionString"] as? String, let build = dictionary["CFBundleVersion"] as? String else {
            return nil
            
        }
        let versionAndBuild: String = "version: \(version), build: \(build)"
        return versionAndBuild
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }
    
    
    private func updateUI() {
        backButton.accessibilityLabel = "back".localized()
        appVersionLabel.text = version
        deviceIDLabel.text = StorageManager.shared.getDeviceID()
        
        deviceVersionLabel.text = UIDevice.current.systemVersion
        
        modelNameLabel.text = UIDevice.modelName
        
        modelNumberLabel.text = UIDevice.modelNumber
    }
    
    @IBAction func onBackTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}
