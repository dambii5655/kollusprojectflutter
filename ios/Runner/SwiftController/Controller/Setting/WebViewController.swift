//
//  WebViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/29.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit
import WebKit
import Reachability

class WebViewController: UIViewController {

    let webView = WKWebView()
    
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    @IBOutlet weak var backButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.addSubview(webView)
        
        webView.snp.makeConstraints { [unowned self] (maker) in
            maker.top.equalTo(self.topView.snp.bottom)
            maker.bottom.leading.trailing.equalToSuperview()
        }
        
        webView.uiDelegate = self
        webView.navigationDelegate = self
        
        backButton.accessibilityLabel = "back".localized()
        
        
//        guard let url = URL(string: "http://info.kollus.com/faq/") else {return}
        guard let url = URL(string: "https://support.catenoid.net/pages/viewpage.action?pageId=1081689") else {return}
        let request = URLRequest(url: url)
        webView.load(request)

        self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil);
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let reachability = try? Reachability() {
            if reachability.connection == .unavailable {
                self.view.makeToast("Network connection required".localized())
            }
        }
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "estimatedProgress" {
            self.progressView.setProgress(Float(self.webView.estimatedProgress), animated: true)
        }
    }
    
    @IBAction func onBackTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    deinit {
        self.webView.removeObserver(self, forKeyPath: "estimatedProgress")
    }
    
}

extension WebViewController: WKUIDelegate, WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        self.progressView.progress = 0
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        self.progressView.progress = 0
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        let allHTTPHeaderFields = navigationAction.request.allHTTPHeaderFields ?? [:]
        for (key, value) in allHTTPHeaderFields {
            print("key:", key, "value:", value)
        }
        decisionHandler(.allow)

    }

}
