//
//  PlayerViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/28.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import CoreMotion

protocol PlayerViewControllerDelegate: class {
    func onVideoViewClosed()
}

class PlayerViewController: UIViewController {
    var pictureInPictureController: AVPictureInPictureController!

    var streamingURL: String?
    var data: KollusContent?
    var playerView: KollusPlayerView!
    
    var subtitleList: [SubTitleInfo] = []
    var streamInfoList: [StreamInfo] = []
    
    @IBOutlet weak var audioImageView: UIImageView!
    @IBOutlet weak var subtitleBackgroundView: UIView!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var controlView: VideoControlView!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var watermarkLabel: UILabel!
    @IBOutlet weak var watermarkLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var watermarkTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var audioWatermarkView: UIView!
    @IBOutlet weak var playSpeedPopupView: PlaySpeedView!
    
    var isLive = false
    var isSeekable = true
    var nSeekableEnd = -1
    
    var playListItems: [KollusContent] = []
    
    var selectedSubtitleIndex = 1
    var selectedStreamInfoindex = 0
    
    var timelineTimer = Timer()
    
    var repeatStartPoint: Double = 0
    var repeatEndPoint: Double = 0
    
    var thumbnailImages: UIImage?
    var thumbnailImageWidth = 0
    var thumbnailImageHeight = 0
    var thumbnailImageCount = 0
    
    var currentSubtitleText = ""
    
    var isSectionPlay = false
    
    var cntPlay = 0
    
    @IBOutlet weak var scrollView: UIScrollView!
    
    weak var actionSheetViewController: ActionSheetViewController?
    weak var actionSheetViewController_iPad: ActionSheetViewController_iPad?

    var motionManager   : CMMotionManager!
    var orientationLast    = UIInterfaceOrientation(rawValue: 0)!

    weak var delegate: PlayerViewControllerDelegate?

    override var prefersHomeIndicatorAutoHidden: Bool {
        true
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .lightContent
    }
    
    override var prefersStatusBarHidden: Bool {
        statusBarHidden
    }
    
    var statusBarHidden = false {
        didSet {
            UIView.animate(withDuration: 0.1) { [weak self] in
                self?.setNeedsStatusBarAppearanceUpdate()
                self?.view.layoutIfNeeded()
            }
            
        }
    }
    
    var kollusContentEntity: KollusContentEntity?
    
    var isPlayed = false
    
    
    var savedCurrentTime: Double = 0
    
    var isShownKeyboard = false

    var playListIndex = 0
    var playSpeed: Int16 = 5

    //MARK: - Life Cycle
    override func viewDidLoad() {
        NLog("viewDidLoad +++")
        print("viewDidLoad +++")
        super.viewDidLoad()
        controlView.delegate = self
        playSpeedPopupView.delegate = self
        playVideoItem()

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(updateAudioWatermark), name: UIApplication.didBecomeActiveNotification, object: nil)
        
        AVAudioSession.sharedInstance().addObserver(controlView.soundProgressView, forKeyPath: "outputVolume", options: NSKeyValueObservingOptions.new, context: nil)
        do { try AVAudioSession.sharedInstance().setActive(true) }
        catch { debugPrint("\(error)") }
        
        addCoreMotion()
//        subtitleLabel.accessibilityTraits.insert(UIAccessibilityTraits.updatesFrequently)
        UIApplication.shared.beginReceivingRemoteControlEvents()
        NLog("viewDidLoad ---")
    }
    
    override func viewDidAppear(_ animated: Bool) {
//        NLog("viewDidAppear +++")
        print("viewDidAppear +++")
        super.viewDidAppear(animated)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            self?.setNeedsStatusBarAppearanceUpdate()
        }
        registerToBackFromBackground()
//        NLog("viewDidAppear ---")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NLog("viewWillDisappear +++")
        super.viewWillDisappear(animated)
        print("viewWillDisappear +++")
        
        motionManager?.stopAccelerometerUpdates()
        unregisterFromBackFromBackground()
        NLog("viewWillDisappear ---")
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIApplication.didBecomeActiveNotification, object: nil)
        AVAudioSession.sharedInstance().removeObserver(self.controlView.soundProgressView, forKeyPath: "outputVolume")
        do { try AVAudioSession.sharedInstance().setActive(false) }
        catch { debugPrint("\(error)") }

        print("VideoPlayerViewController deinitted")
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        guard let playerView = playerView else { return }
        guard let layer = playerView.subviews.first?.layer.sublayers?.first as? AVPlayerLayer else { return }
        if layer.frame == .zero {
            playerView.frame = containerView.bounds
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        NLog("viewWillTransition +++")
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { [weak self] (context) in
            self?.initializeVideoViewLayout()
        })
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            guard let self = self else { return }
            
            if self.controlView.isDisplayBookmark {
                self.controlView.bookmarkViewBottomConstraint.constant = 0
            }
        }
        NLog("viewWillTransition ---")
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            guard let self = self else { return }
            self.initializeVideoViewLayout()
            if self.controlView.isDisplayBookmark {
                self.controlView.bookmarkViewBottomConstraint.constant = 0
            }
        }
    }

    private var registeredToBackgroundEvents = false

    /// register to back from backround event
    private func registerToBackFromBackground() {
        if(!registeredToBackgroundEvents) {
            print(" registerToBackFromBackground +++  ")
            NotificationCenter.default.addObserver(self,
            selector: #selector(viewDidBecomeActive),
            name: UIApplication.didBecomeActiveNotification, object: nil)
            registeredToBackgroundEvents = true
        }
    }

    /// unregister from back from backround event
    private func unregisterFromBackFromBackground() {
        if(registeredToBackgroundEvents) {
            NotificationCenter.default.removeObserver(self,
            name: UIApplication.didBecomeActiveNotification, object: nil)
            registeredToBackgroundEvents = false
        }

    }
    
    @objc func viewDidBecomeActive(){
        print(" viewDidBecomeActive  ++ ")
        if UIAccessibility.isVoiceOverRunning {
            print(" viewDidBecomeActive  ++ iff ")
            controlView.displayControlView(true)
        }
    }

    func initializeVideoViewLayout() {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.containerView.transform = .identity
            
            if self.playerView == nil {
                return
            }
            let width = self.containerView.bounds.width
            let height = self.containerView.bounds.height
            if width > height {
                self.controlView.titleLabel.numberOfLines = 1
            }
            else {
                self.controlView.titleLabel.numberOfLines = 2
            }
            NLog("width : \(width), height : \(height)")
            self.playerView.frame = self.containerView.bounds
            
            self.scrollView.contentInset = .zero
            self.controlView.sliderView.updateRepeatViewFrame()
            self.updateWatermarkPosition()
        }
    }
    
    
    // MARK: - Func
    private func playVideoItem() {
        NLog("playVideoItem +++")
        print("playVideoItem +++")
        timelineTimer.invalidate()
        updateSubtitleLabel()
        controlView.displayControlView(false)
        controlView.setIndicatorView(isBuffering: true)
        controlView.updateReachToEndTimeView()
        thumbnailImages = nil
        
        if self.playerView != nil {
//            try? playerView.stop()
            playerView.removeFromSuperview()
        }
        
        DispatchQueue.global().async { [unowned self] in
            if let data = data, let streamingURL = self.sampleContentURL(data: data) {
                self.play(streamingURL: streamingURL)
            }
            else if data?.contentType == .downloading  || data?.contentType == .adaptiveDownload {
                self.play(mediaContentKey: data!.mediaContentKey)
            }
            else {
                guard let streamingURL = self.streamingURL else { return }
                self.play(streamingURL: streamingURL)
            }
        }
        NLog("playVideoItem ---")
    }
    
    private func sampleContentURL(data: KollusContent) -> String? {
        let sampleContents = PreferenceManager.sampleContents
        
        for sample in sampleContents {
            if sample.media_content_key == data.mediaContentKey {
                return sample.video_url
            }
        }
        return nil
    }
    
    func play(streamingURL: String) {
        DispatchQueue.main.async { [weak self] in
            self?.playerView = KollusPlayerView(contentURL: streamingURL)
            DispatchQueue.global().async {
                self?.initPlayerView()
            }
        }
        
    }
    
    func play(mediaContentKey: String) {
        DispatchQueue.main.async { [weak self] in
            self?.playerView = KollusPlayerView(mediaContentKey: mediaContentKey)
            DispatchQueue.global().async {
                self?.initPlayerView()
            }
        }
    }
    
    func initPlayerView() {
        NLog("initPlayerView +++")
        playerView.debug = false
        playerView.storage = StorageManager.shared.storage
        playerView.delegate = self
        playerView.bookmarkDelegate = self
        playerView.lmsDelegate = self
        playerView.drmDelegate = self
        playerView.scalingMode = .scaleAspectFit
        playerView.proxyPort = 8488
//        if PreferenceManager.playerCodec < 2 && PreferenceManager.playerCodec >= 0{
//            var bHW : Bool
//            bHW = (PreferenceManager.playerCodec == 0)
//            playerView.setDecoder(bHW)
//        }

        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { [unowned self] in
            playerView.makeSecure()
            self.containerView.insertSubview(self.playerView, at: 0)
            self.playerView.snp.makeConstraints { (maker) in
                maker.bottom.top.leading.trailing.equalToSuperview()
            }
            
            do {
                PreferenceManager.playerCodec = 1
                NLog("prepareToPlay PreferenceManager.playerCodec : \(PreferenceManager.playerCodec)")
                 if PreferenceManager.playerCodec == 1 {
                    playerView.setDecoder(false)
                    try self.playerView.prepareToPlay(withMode: .PlayerTypeKollus)
                }
                else if PreferenceManager.playerCodec == 2{
                    try self.playerView.prepareToPlay(withMode: .PlayerTypeNative)
                }
                else  {
                    playerView.setDecoder(true)
                    try self.playerView.prepareToPlay(withMode: .PlayerTypeKollus)
                }
//                NLog("prepareToPlay PreferenceManager.UsePlayerType : \(PreferenceManager.UsePlayerType)")
//                if PreferenceManager.UsePlayerType == true{
//                    try self.playerView.prepareToPlay(withMode: .PlayerTypeNative)
//                }
//                else if PreferenceManager.UsePlayerType == false {
//                    try self.playerView.prepareToPlay(withMode: .PlayerTypeKollus)
//                }
            }
            catch {
                self.dismiss(animated: false) { [unowned self] in
                    try? playerView.stop()
                    UIApplication.presentErrorViewController(title: "Code : \((error as NSError).code)", errorDescription: "Can't play.".localized(), errorReason: (error as NSError).localizedDescription)
                }
            }
        }
        NLog("initPlayerView ---")
    }
    
    func setupPictureInPicture() {
        if (AVPictureInPictureController.isPictureInPictureSupported() && playerView.seekable && !isSectionPlay) ||
            (AVPictureInPictureController.isPictureInPictureSupported() && playerView.isLive) {
            guard let layer = playerView.subviews.first?.layer.sublayers?.first as? AVPlayerLayer else { return }
            pictureInPictureController = AVPictureInPictureController(playerLayer: layer)
            pictureInPictureController.delegate = self
            pictureInPictureController.startPictureInPicture()
        }
        else {
            if let pictureInPictureController = pictureInPictureController {
                pictureInPictureController.stopPictureInPicture()
            }
        }
    }
    
    func resetTimelineTimer() {
        timelineTimer.invalidate()
        timelineTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(updateTimeLine), userInfo: nil, repeats: true)
    }
    
    func initialPlay() {
        NLog("initialPlay +++")
        playerView.makeSecure()
        self.containerView.insertSubview(self.playerView, at: 0)
        self.playerView.frame = self.containerView.bounds
        
        self.playerView.snp.makeConstraints { (maker) in
            maker.bottom.top.leading.trailing.equalToSuperview()
        }
        
        try? playerView.play()
//        if playListIndex == 1 {
//            controlView.displayControlView(true, resetTimer: 3)
//        }
        
        updateKollusContentSetting()
        
        DispatchQueue.global().asyncAfter(deadline: .now() + Double(playerView.nVideoWaterMarkHideTime)) { [weak self] in
            self?.updateWatermark()
        }
        
        controlView.nSecSkipView.startCountDownTimer()
        NLog("initialPlay ---")
    }
    
    func resetABRepeatNone() {
         controlView.repeatMode = .none
        // TODO ffff change
        controlView.bottomButtonsStackView.repeatImageView.image = #imageLiteral(resourceName: "sectionRepeat18")
        onBottomStackRepeatTouched(view: controlView, repeatMode: .none)
    }

    // MARK: - Keyboard Notification
    
    @objc func keyboardWillShow(notification: Notification) {
        if let _: NSValue = notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue {
//            let keyboardRectangle = keyboardFrame.cgRectValue
//            let keyboardHeight = keyboardRectangle.height
            
            isShownKeyboard = true
            
            controlView.soundProgressView.isUserInteractionEnabled = false
            controlView.brightnessProgressView.isUserInteractionEnabled = false
                        
//            if let webView = controlView.chattingWebView {
//                webView.updateChattingWebViewBottomMargin(value: -Float(keyboardHeight))
//            }
            
        }

    }

    @objc func keyboardWillHide(notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            isShownKeyboard = false
            
            controlView.soundProgressView.isUserInteractionEnabled = true
            controlView.brightnessProgressView.isUserInteractionEnabled = true
                        
//            if let webView = controlView.chattingWebView {
//                webView.updateChattingWebViewBottomMargin(value: 0)
//            }
        }
    }
    
    
    // MARK: - Background Play
    
    var playerToRestore: AVPlayer? = nil
    
    public func disconnectAVPlayer() {
        if playerView == nil { return }
        guard let playerLayer = playerView.subviews.first?.layer.sublayers?.first as? AVPlayerLayer else { return }
        if playerToRestore != nil { return }
        playerToRestore = playerLayer.player
        playerLayer.player = nil
    }
    
    public func reconnectAVPlayer() {
        if playerView == nil || playerToRestore == nil { return }
        guard let playerLayer = playerView.subviews.first?.layer.sublayers?.first as? AVPlayerLayer else { return }
        NLog("playerLayer.player = \(playerLayer.player)")
        if playerLayer.player == nil {
            playerLayer.player = playerToRestore
        }
        playerToRestore = nil
    }
    
    
    //MARK: - Motion
    func addCoreMotion() {

        let splitAngle:Double = 0.75
        let updateTimer:TimeInterval = 0.5

        motionManager = CMMotionManager()
        motionManager?.gyroUpdateInterval = updateTimer
        motionManager?.accelerometerUpdateInterval = updateTimer

//        var orientationLast    = UIInterfaceOrientation(rawValue: 0)!

        motionManager?.startAccelerometerUpdates(to: (OperationQueue.current)!, withHandler: {
            (acceleroMeterData, error) -> Void in
            if error == nil {

                let acceleration = (acceleroMeterData?.acceleration)!
                var orientationNew = UIInterfaceOrientation(rawValue: 0)!

                if acceleration.x >= splitAngle {
                    orientationNew = .landscapeLeft
                }
                else if acceleration.x <= -(splitAngle) {
                    orientationNew = .landscapeRight
                }
                else if acceleration.y <= -(splitAngle) {
                    orientationNew = .portrait
                }
                else if acceleration.y >= splitAngle {
                    orientationNew = .portraitUpsideDown
                }

                if orientationNew != self.orientationLast && orientationNew != .unknown{
                    self.orientationLast = orientationNew
 //                   NLog("orinetation :\(self.orientationLast.rawValue)")
                }
            }
            else {
                print("error : \(error!)")
            }
        })
    }

    
    //MARK: - Update
    @objc func updateTimeLine() {
//        NLog("isSectionPlay : \(isSectionPlay), sliderView.value : \(controlView.sliderView.value)")
        guard playerView != nil else { return }
        
        if isLive && isSeekable {
            NLog("updateTimeLine liveDuration : \(totalDuration), currentTime : \(currentPlayTime)")
            controlView.updateLivePlaySliderView(currentTime: currentPlayTime, totalDuration: totalDuration)

            return
        }
        
        if isSectionPlay && controlView.sliderView.value >= 1 {
            NLog("isSectionPlay : \(isSectionPlay), sliderView.value : \(controlView.sliderView.value)")
            playerView.setSkipPlay()
            timelineTimer.invalidate()
            return
        }
        
        if controlView.repeatMode == .end {
            if currentPlayTime >= repeatEndPoint {
                playerView.currentPlaybackTime = repeatStartPoint
                return
            }
        }
        controlView.updatePlaySliderView(currentTime: currentPlayTime - playSectionStartTime, totalDuration: totalDuration)
        
        let cur = Int(currentPlayTime - playSectionStartTime)
        controlView.sliderView.isSeekable = isSeekable
        if nSeekableEnd > 0 {
            if cur > nSeekableEnd {
                nSeekableEnd = cur
            }
        }
        controlView.sliderView.seekableEnd = nSeekableEnd
    }
    

    func updateRotation() {
        let width = data?.naturalSize.width ?? 0
        let height = data?.naturalSize.height ?? 0
        NLog("updataRotation width : \(width), height : \(height)")
        if playListIndex == 1 {
            if width >= height || playerView.isAudioOnly {
                if orientationLast == .landscapeLeft {
                    AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeLeft)
                }
                else {
                    AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
                }
            }
            else {
                AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            }
        }
        
        playerView.frame = containerView.bounds
        controlView.updateSliderThumnailView(width: width, height: height)
    }
    
    func updateKollusContentSetting() {
        guard let kollusContentEntity = kollusContentEntity else { return }
        
        onVideoScaleChanged(videoScale: Int(kollusContentEntity.scaleMode))
        onPlaySpeedChanged(playSpeed: Int(kollusContentEntity.playSpeed))
        updateSubtitleList()
        updateStreamInfoList()
    }
    
    
    func updateWatermark() {
        if playerView == nil { return }
        guard let watermarkString = playerView.strVideoWaterMark else { return }
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            self.watermarkLabel.text = watermarkString
            self.watermarkLabel.alpha = CGFloat((self.playerView.nVideoWaterMarkAlpha )) / 255.0
            self.watermarkLabel.font = UIFont.boldSystemFont(ofSize: CGFloat(self.playerView.nVideoWaterMarkFontSize))
            NLog("updataRotation strVideoWaterMarkFontColor : \(self.playerView.strVideoWaterMarkFontColor)")
            if self.playerView.strVideoWaterMarkFontColor == nil {
                return
            }
            self.watermarkLabel.textColor = UIColor(hexFromString: self.playerView.strVideoWaterMarkFontColor)
            self.watermarkLabel.isHidden = false
            self.watermarkLabel.sizeToFit()
            self.updateWatermarkPosition()
        }
        
        DispatchQueue.global().asyncAfter(deadline: .now() + Double(playerView.nVideoWaterMarkShowTime)) { [weak self] in
            guard let self = self else { return }
            if self.playerView == nil { return }
            DispatchQueue.main.async {
                self.watermarkLabel.isHidden = true
            }
                        
            Thread.sleep(forTimeInterval: Double(self.playerView.nVideoWaterMarkHideTime))
            
            DispatchQueue.main.async {
                self.updateWatermark()
            }
        }
        
    }
    
    func updateWatermarkPosition() {
        let videoRect = playerView.getVideoPosition()
        if videoRect == .zero || playerView.strVideoWaterMark == nil { return }
        
        NLog("watermarkLabel.frame : \(watermarkLabel.frame)")
        if watermarkLabel.frame.size.width > videoRect.size.width {
            watermarkLabel.frame.size.width = videoRect.size.width
        }
        if watermarkLabel.frame.size.height > videoRect.size.height {
            watermarkLabel.frame.size.height = videoRect.size.height
        }

        let positionX = Int.random(in: Int(videoRect.origin.x) ... Int(videoRect.origin.x + videoRect.size.width - watermarkLabel.frame.size.width))
        let positionY = Int.random(in: Int(videoRect.origin.y) ... Int(videoRect.origin.y + videoRect.size.height - watermarkLabel.frame.size.height))
        watermarkLabel.frame.origin.x = CGFloat(positionX)
        watermarkLabel.frame.origin.y = CGFloat(positionY)
//        watermarkLeadingConstraint.constant = CGFloat(positionX)
//        watermarkTopConstraint.constant = CGFloat(positionY)
    }
    
    @objc func updateAudioWatermark() {
        guard playerView != nil else { return }
        
        audioWatermarkView.isHidden = !playerView.isWaterMark
        if playerView.isWaterMark {
            audioWatermarkView.alpha = 1
            UIView.animate(withDuration: 3, delay: 0, options: [.autoreverse, .repeat], animations: { [weak self] in
                self?.audioWatermarkView.alpha = 0
            }, completion: nil)
        }
        
    }
    
    
    func updatePlaySpeedDatas() {
        var playSpeedDatas: [PlaySpeed] = []
        for i in 0 ..< PlaySpeed.count {
            if playerView.isWaterMark {
                if i == 0 || i == 1 || i == 5 || i == 6 {
                    continue
                }
            }
            playSpeedDatas.append(PlaySpeed(rawValue: i)!)
        }
        
        playSpeedPopupView.isWatermark = playerView.isWaterMark
        playSpeedPopupView.playSpeed = playSpeedDatas
        playSpeedPopupView.selectedPlaySpeed = PlaySpeed(rawValue: Int(kollusContentEntity!.playSpeed))!
        controlView.bottomButtonsStackView.playSpeed = PlaySpeed(rawValue: Int(kollusContentEntity!.playSpeed))!
    }
    
    
    // MARK: - PlayList
    
    func getNextVideoData() -> KollusContent? {
        for i in 0 ..< playListItems.count {
            let item = playListItems[i]
            if item.mediaContentKey == data?.mediaContentKey {
                return playListItems[safe: i + 1]
            }
        }
        return nil
    }
    
    func getPreviousVideoData() -> KollusContent? {
        for i in 0 ..< playListItems.count {
            let item = playListItems[i]
            if item.mediaContentKey == data?.mediaContentKey {
                return playListItems[safe: i - 1]
            }
        }
        return nil
    }

    // MARK: - ThumbnailIamges
    func updateThumbnailImages() {
        if playerView.isThumbnailEnable {
            DispatchQueue.global().async { [unowned self] in
                let thumbnail: NSString = self.playerView.content.thumbnail as NSString
                guard thumbnail.length > 0 else {
                    thumbnailImages = nil
                    thumbnailImageWidth = 0
                    thumbnailImageHeight = 0
                    thumbnailImageCount = 0
                    return
                }
                
                let lastComponent = (thumbnail.lastPathComponent as NSString).deletingPathExtension
                
                guard let thumbnailMatrixString = lastComponent.components(separatedBy: ".").last else { return }
                let thumbnailMatrix = thumbnailMatrixString.components(separatedBy: "x")
                
                thumbnailImageWidth = Int(thumbnailMatrix[0]) ?? 0
                thumbnailImageHeight = Int(thumbnailMatrix[1]) ?? 0
                thumbnailImageCount = Int(thumbnailMatrix[2]) ?? 0
                
                thumbnailImages = UIImage(contentsOfFile: thumbnail as String)
            }
        }
        else {
            thumbnailImages = nil
            thumbnailImageWidth = 0
            thumbnailImageHeight = 0
            thumbnailImageCount = 0
        }
    }
    
    func getThumnailImage(position: Double, completion: @escaping (UIImage?) -> ()) {
        DispatchQueue.global().async { [unowned self] in
            guard let thumbnailImages = self.thumbnailImages else {
                NLog("no thumbnailImages")
                if playerView.isThumbnailEnable {
                    //let snapshotImage = UIImage(named: data!.snapshot)
                    let snapshotImage = UIImage(contentsOfFile: data!.snapshot)
                    if snapshotImage != nil {
                        DispatchQueue.main.async {
                            completion(snapshotImage)
                        }
                        return
                    }
                }
    
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            
            var imagePosition = Int(position / self.data!.duration * Double(self.thumbnailImageCount))
            
            NLog("imagePosition position : \(position)")
            NLog("imagePosition : \(imagePosition)")

            if imagePosition < 0 {
                imagePosition = 0
            }
            else if imagePosition >= self.thumbnailImageCount {
                imagePosition =  self.thumbnailImageCount
            }
            
            
            let row = imagePosition / 10
            let column = imagePosition % 10
            
            let imageRect = CGRect(x: column * self.thumbnailImageWidth, y: row * self.thumbnailImageHeight, width: self.thumbnailImageWidth, height: self.thumbnailImageHeight)
            
            guard let cgImage = thumbnailImages.cgImage?.cropping(to: imageRect) else {
                DispatchQueue.main.async {
                    completion(nil)
                }
                return
            }
            let positionImage = UIImage(cgImage: cgImage)
            
            DispatchQueue.main.async {
                completion(positionImage)
            }
            
        }
    }
    
    // MARK: - Subtitle
    func updateSubtitleLabel(text: String = "") {
        if playerView?.strCaptionStyle == "bg" {
            subtitleBackgroundView.isHidden = false
        }
        else {
            subtitleBackgroundView.isHidden = !PreferenceManager.isUseSubtitleBackground
        }
        subtitleLabel.isHidden = false
        
        
        let fontSize = SubtitleSize(rawValue: PreferenceManager.subtitleSize)?.toFloat ?? 25
        let fontColor = SubtitleColor(rawValue: PreferenceManager.subtitleColor)?.toColor ?? .white
        
        let topViewController = UIApplication.getTopViewController()
        
        if ((topViewController as? PlayerViewController) != nil) {
            if playSpeedPopupView.isHidden == true && self.controlView.bookmarkCoverView.isHidden == true {
                if /*subtitleLabel.accessibilityElementIsFocused() || */controlView.isDisplayControl == false {
                    UIAccessibility.post(notification: .layoutChanged, argument: subtitleLabel)
                }
            }
        }
        
        subtitleLabel.setHTMLFromString(htmlText: text, baseFont: fontColor, baseFont: fontSize)

        if subtitleLabel.text?.count == 0 {
            subtitleBackgroundView.isHidden = true
        }
    }

    func updateSubtitleBg(text: String = "") {
        if playerView?.strCaptionStyle == "bg" {
            subtitleBackgroundView.isHidden = false
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            self.view.hideAllToasts()
            appDelegate.window?.hideAllToasts()
            let stringLocal = "The content background is set, so the user's background setting is not applied.".localized()
            appDelegate.window?.makeToast(stringLocal)
            DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
                UIAccessibility.post(notification: .announcement, argument: stringLocal)
            }
        }
        else {
            subtitleBackgroundView.isHidden = !PreferenceManager.isUseSubtitleBackground
        }
        subtitleLabel.isHidden = false
        
        
        let fontSize = SubtitleSize(rawValue: PreferenceManager.subtitleSize)?.toFloat ?? 25
        let fontColor = SubtitleColor(rawValue: PreferenceManager.subtitleColor)?.toColor ?? .white
        
        subtitleLabel.setHTMLFromString(htmlText: text, baseFont: fontColor, baseFont: fontSize)
        
        if subtitleLabel.text?.count == 0 {
            subtitleBackgroundView.isHidden = true
        }
    }
    
    func updateSubtitleList() {
        if var subTitleList = playerView.listSubTitle as? [SubTitleInfo], subTitleList.count > 0 {
            let hideSubtitle = SubTitleInfo()
            hideSubtitle.strName = "Hide subtitles".localized()
            hideSubtitle.strLanguage = ""
            subTitleList.insert(hideSubtitle, at: 0)
            subtitleList = subTitleList
            NLog("updateSubtitleList subtitleList : \(subtitleList)")
            
//            self.subtitleBackgroundView.snp.remakeConstraints { (maker) in
//                maker.bottom.equalToSuperview().offset(-100)
//            }

        }
        else {
            subtitleList.removeAll()
        }
        
        if kollusContentEntity?.subtitle == -1 {
            let language =  NSLocale.preferredLanguages.first!   // "en-US"
            let languageDictionary = NSLocale.components(fromLocaleIdentifier: language)
            
            guard let languageCode = languageDictionary["kCFLocaleLanguageCodeKey"] else { return } // en
            
            for i in 0 ..< subtitleList.count {
                let subtitle = subtitleList[i]
                let language = subtitle.strLanguage
                
                if languageCode.caseInsensitiveCompare(language!) == .orderedSame {
                    selectedSubtitleIndex = i
                    playerView.setSubTitlePath(strdup(subtitle.strUrl))
                    break
                }
            }
        }
        else {
            selectedSubtitleIndex = Int(kollusContentEntity!.subtitle)
        }
        
        updateSubtitleLanguageChanged(subtitleLanguage: selectedSubtitleIndex)
    }

    func updateSubtitleLanguageChanged(subtitleLanguage: Int) {
        selectedSubtitleIndex = subtitleLanguage
        
        if selectedSubtitleIndex == 0 { // 감추기
            subtitleBackgroundView.isHidden = true
            subtitleLabel.isHidden = true
        }
        else {
            if let listSubList = playerView.listSubTitle, listSubList.count > 0 {
                let selectedSubTitle = playerView.listSubTitle[subtitleLanguage - 1] as! SubTitleInfo
                playerView.setSubTitlePath(strdup(selectedSubTitle.strUrl))
            }
        }
        
        kollusContentEntity?.subtitle = Int16(selectedSubtitleIndex)
        if data?.contentType != .streaming {
            CoreDataManager.shared.saveContext()
        }
    }

    //MARK: - 스트리밍 화질
    func updateStreamInfoList() {
        do {
            guard let streamInfoList = playerView.streamInfoList else { return }
            
            let json = try JSONSerialization.data(withJSONObject: streamInfoList)
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            self.streamInfoList = try decoder.decode([StreamInfo].self, from: json)
            
            let auto = StreamInfo(bandwidth: "Auto", streamWidth: "", streamHeight: "")
            self.streamInfoList.insert(auto, at: 0)
            
        } catch {
            print(error)
        }
    }
    
    //MARK: - Keybaord
    override var canBecomeFirstResponder: Bool {
        return true
    }

    override func remoteControlReceived(with event: UIEvent?) {
        NLog("remoteControlReceived")
        if event?.type == .remoteControl {
            switch event?.subtype {
            case .remoteControlPause:
                NLog("remoteControlPause")
                try? playerView.pause()
                break
            case .remoteControlPlay:
                NLog("remoteControlPlay")
                try? playerView.play()
                break
            case .remoteControlTogglePlayPause:
                NLog("remoteControlTogglePlayPause")
                if playerView.isPlaying {
                    try? playerView.pause()
                }
                else {
                    try? playerView.play()
                }
                break
            default:
                break
            }
        }
    }

    #if false
    override var keyCommands: [UIKeyCommand]? {
    return [
        UIKeyCommand(input: UIKeyCommand.inputLeftArrow, modifierFlags: [], action: #selector(LeftArrowPress)),
        UIKeyCommand(input: UIKeyCommand.inputRightArrow, modifierFlags: [], action: #selector(RightArrowPress)),
        UIKeyCommand(input: UIKeyCommand.inputDownArrow, modifierFlags: [], action: #selector(DownArrowPress)),
        UIKeyCommand(input: UIKeyCommand.inputUpArrow, modifierFlags: [], action: #selector(UpArrowPress)),
        UIKeyCommand(input: " ", modifierFlags: [], action: #selector(SpaceKeyPress)),
        UIKeyCommand(input: "x", modifierFlags: [], action: #selector(xKeyPress)),
        UIKeyCommand(input: "c", modifierFlags: [], action: #selector(cKeyPress)),
        UIKeyCommand(input: "z", modifierFlags: [], action: #selector(zKeyPress)),
        UIKeyCommand(input: "[", modifierFlags: [], action: #selector(ABRepeatPress)),
        UIKeyCommand(input: "]", modifierFlags: [], action: #selector(ABRepeatPress)),
        UIKeyCommand(input: "\\", modifierFlags: [], action: #selector(ABRepeatNonePress)),
        UIKeyCommand(input: "p", modifierFlags: [], action: #selector(ABRepeatNonePress))
      ]
    }
    
    @objc func LeftArrowPress(sender:UIKeyCommand) {
        NLog("LeftArrowPress")
        controlView.executeDoubleTapTouched(direction: .backward)
    }
    
    @objc func RightArrowPress(sender:UIKeyCommand) {
        NLog("RightArrowPress")
        controlView.executeDoubleTapTouched(direction: .forward)
    }

    @objc func DownArrowPress(sender: UIKeyCommand) {
        NLog("DownArrowPress")
        let soundProgressView = controlView.soundProgressView
        controlView.soundProgressView.updateSoundUI(value: Float(soundProgressView!.volumeModifierValue))
    }
    
    @objc func UpArrowPress(sender: UIKeyCommand) {
        NLog("UpArrowPress")
        let soundProgressView = controlView.soundProgressView
        controlView.soundProgressView.updateSoundUI(value: -Float(soundProgressView!.volumeModifierValue))
    }
    
    @objc func SpaceKeyPress(sender: UIKeyCommand) {
        NLog("SpacePress")
        controlView.onPlayAndPauseTouched(controlView.playAndPauseButton)
    }

    @objc func xKeyPress(sender: UIKeyCommand) {
        NLog("xKeyPress")
        guard let kollusContentEntity = kollusContentEntity else { return }
        let currentSpeedIndex = Int(kollusContentEntity.playSpeed)
        onPlaySpeedChanged(playSpeed: currentSpeedIndex - 1)
        actionSheetViewController?.tableViewReloadData()
        actionSheetViewController_iPad?.tableViewReloadData()
    }
    
    @objc func cKeyPress(sender: UIKeyCommand) {
        NLog("cKeyPress")
        guard let kollusContentEntity = kollusContentEntity else { return }
        let currentSpeedIndex = Int(kollusContentEntity.playSpeed)
        onPlaySpeedChanged(playSpeed: currentSpeedIndex + 1)
        actionSheetViewController?.tableViewReloadData()
        actionSheetViewController_iPad?.tableViewReloadData()
    }

    @objc func zKeyPress(sender: UIKeyCommand) {
        NLog("zKeyPress")
        onPlaySpeedChanged(playSpeed: PlaySpeed.x1.rawValue)
        actionSheetViewController?.tableViewReloadData()
        actionSheetViewController_iPad?.tableViewReloadData()
    }

    @objc func ABRepeatPress(sender: UIKeyCommand) {
        NLog("ABRepeatNonePress")
        controlView.bottomStackRepeatTouched(view: controlView.bottomButtonsStackView)
    }
    
    @objc func ABRepeatNonePress(sender: UIKeyCommand) {
        NLog("ABRepeatNonePress")
        controlView.repeatMode = .none
        controlView.bottomButtonsStackView.repeatImageView.image = #imageLiteral(resourceName: "sectionRepeat18")
        onBottomStackRepeatTouched(view: controlView, repeatMode: .none)
    }
    #endif
    
    override func pressesBegan(_ presses: Set<UIPress>, with event: UIPressesEvent?) {
        guard let key = presses.first?.key else { return }
        guard let topViewController = UIApplication.getTopViewController() else { return }

        if topViewController is PlayerViewController {

        switch key.keyCode {
        case .keyboardLeftArrow:
            LeftArrowPress()
        case .keyboardRightArrow:
            RightArrowPress()
        case .keyboardDownArrow:
            DownArrowPress()
        case .keyboardUpArrow:
            UpArrowPress()
        case .keyboardSpacebar:
            SpaceKeyPress()
        case .keyboardM:
            mKeyPress()
        case .keyboardX:
            xKeyPress()
        case .keyboardC:
            cKeyPress()
        case .keyboardZ:
            zKeyPress()
        case .keyboardOpenBracket:
            ABRepeatPress()
        case .keyboardCloseBracket:
            ABRepeatPress()
        case .keyboardBackslash:
            ABRepeatNonePress()
        case .keyboardP:
            ABRepeatNonePress()
        default:
            super.pressesBegan(presses, with: event)
        }
        }
    }

    func LeftArrowPress() {
        NLog("LeftArrowPress")
        if UIApplication.getTopViewController() is PlayerViewController {
            if isSeekable || nSeekableEnd > 0 {
                controlView.executeDoubleTapTouched(direction: .backward)
            }
        }
    }
    
    func RightArrowPress() {
        NLog("RightArrowPress")
        if UIApplication.getTopViewController() is PlayerViewController {
            if isSeekable || nSeekableEnd > 0 {
                controlView.executeDoubleTapTouched(direction: .forward)
            }
        }
    }

    func DownArrowPress() {
        NLog("DownArrowPress")
        let soundProgressView = controlView.soundProgressView
        controlView.soundProgressView.updateSoundUI(value: Float(soundProgressView!.volumeModifierValue))
    }
    
    func UpArrowPress() {
        NLog("UpArrowPress")
        let soundProgressView = controlView.soundProgressView
        controlView.soundProgressView.updateSoundUI(value: -Float(soundProgressView!.volumeModifierValue))
    }
    
    func SpaceKeyPress() {
        NLog("SpacePress")
        controlView.onPlayAndPauseTouched(controlView.playAndPauseButton)
    }

    func mKeyPress() {
        NLog("mKeyPress")
        controlView.soundProgressView.onMuteToggle()
    }

    func xKeyPress() {
        NLog("xKeyPress")
        guard let kollusContentEntity = kollusContentEntity else { return }
        let currentSpeedIndex = Int(kollusContentEntity.playSpeed)
        onPlaySpeedChanged(playSpeed: currentSpeedIndex - 1)
        actionSheetViewController?.tableViewReloadData()
        actionSheetViewController_iPad?.tableViewReloadData()
    }
    
    func cKeyPress() {
        NLog("cKeyPress")
        guard let kollusContentEntity = kollusContentEntity else { return }
        let currentSpeedIndex = Int(kollusContentEntity.playSpeed)
        onPlaySpeedChanged(playSpeed: currentSpeedIndex + 1)
        actionSheetViewController?.tableViewReloadData()
        actionSheetViewController_iPad?.tableViewReloadData()
    }

    func zKeyPress() {
        NLog("zKeyPress")
        onPlaySpeedChanged(playSpeed: PlaySpeed.x1.rawValue)
        actionSheetViewController?.tableViewReloadData()
        actionSheetViewController_iPad?.tableViewReloadData()
    }

    func ABRepeatPress() {
        NLog("ABRepeatPress")
        controlView.bottomStackRepeatTouched(view: controlView.bottomButtonsStackView)
    }
    
    func ABRepeatNonePress() {
        NLog("ABRepeatNonePress")
        controlView.repeatMode = .none
        controlView.bottomButtonsStackView.repeatImageView.image = #imageLiteral(resourceName: "sectionRepeat18")
        onBottomStackRepeatTouched(view: controlView, repeatMode: .none)
    }

}

// MARK: - VideoControlViewDelegate
extension PlayerViewController: VideoControlViewDelegate {
    
    var currentPlayTime: TimeInterval {
        return playerView.currentPlaybackTime
    }
    
    var totalDuration: TimeInterval {
        if isLive && isSeekable {
            return playerView.liveDuration
        }
        
        if isSectionPlay {
            return TimeInterval(playerView.playSection.endTime - playerView.playSection.startTime)
        }
        else {
            return data?.duration ?? 0
        }
    }
    
    var playSectionStartTime: TimeInterval {
        TimeInterval(playerView.playSection.startTime)
    }
    
    // MARK: - PlayTimeSlider
    func onTimeSliderTouchBegan(slider: PlayerSliderView) {
        timelineTimer.invalidate()
        if playerView.isPlaying {
            isPlayed = true
            try? playerView.pause()
        }
        
        getThumnailImage(position: currentPlayTime) { [unowned self] (image) in
            let time = KollusUtil.durationToString(duration: currentPlayTime - playSectionStartTime)
            self.controlView.sliderThumbnailView.setThumbnail(image: image, time: time)
        }
    }
    
    func onTimeSliderValueChanged(slider: PlayerSliderView) {
        var sliderPosition = Double(slider.value) * totalDuration
        
        if isLive && isSeekable {
            let current = sliderPosition - totalDuration
            controlView.updateLivePlaySliderValue(currentTime: current, totalDuration: totalDuration)
            return
        }

        if nSeekableEnd > 0 && Int(sliderPosition) > nSeekableEnd {
            sliderPosition = Double(nSeekableEnd)
            controlView.updatePlaySliderView(currentTime: sliderPosition, totalDuration: totalDuration)
        }
        getThumnailImage(position: sliderPosition + playSectionStartTime) { [unowned self] (image) in
            let time = KollusUtil.durationToString(duration: sliderPosition)
            self.controlView.sliderThumbnailView.setThumbnail(image: image, time: time)
        }
    }
    
    func onTimeSliderTouchEnd(slider: PlayerSliderView) {
        var currentTime = Double(slider.value) * totalDuration
        
        if isLive && isSeekable {
            let current = currentTime - totalDuration
            playerView.currentPlaybackTime = current
            try? playerView.play()
            
            controlView.updateLivePlaySliderView(currentTime: current, totalDuration: totalDuration)
            isPlayed = false
            resetTimelineTimer()
            return
        }

        if nSeekableEnd > 0 && Int(currentTime) > slider.seekableEnd {
//            playerView.currentPlaybackTime = playerView.currentPlaybackTime
            playerView.currentPlaybackTime = TimeInterval(slider.seekableEnd)
            try? playerView.play()
            return
        }
        
        if(currentTime >= totalDuration){
            currentTime = totalDuration-1.0
        }
        playerView.currentPlaybackTime = currentTime + playSectionStartTime
        if controlView.isDisplayBookmark != true {
            try? playerView.play()
        }
        
        if controlView.repeatMode == .end {
            if currentTime + playSectionStartTime < repeatStartPoint || currentTime + playSectionStartTime > repeatEndPoint {
                controlView.bottomStackRepeatTouched(view: controlView.bottomButtonsStackView)
            }
        }
        
        controlView.updatePlaySliderView(currentTime: currentTime, totalDuration: totalDuration)

        isPlayed = false
        resetTimelineTimer()
    }
    
    // MARK: - VideoSlider
    func onVideoSliderTouchBegan(view: VideoControlView) {
        timelineTimer.invalidate()
        if playerView.isPlaying {
            try? playerView.pause()
            isPlayed = true
        }

        getThumnailImage(position: currentPlayTime) { [unowned self] (image) in
            let time = KollusUtil.durationToString(duration: currentPlayTime - playSectionStartTime)
            self.controlView.videoSliderThumbnailView.setThumbnail(image: image, currentTimeText: time, addedTime: 0)
        }
    }
    
    func onVideoSliderValueChanged(view: VideoControlView, deltaX: CGFloat) {
        let diffDuration = Double(deltaX / self.view.frame.size.width * 90)
        
        var changedDuration = currentPlayTime + diffDuration
  
        if changedDuration < playSectionStartTime {
            changedDuration = playSectionStartTime
        }
        else if changedDuration > totalDuration + playSectionStartTime {
            changedDuration = totalDuration + playSectionStartTime
        }
        
        if nSeekableEnd > 0 && Int(changedDuration) > nSeekableEnd {
            changedDuration = Double(nSeekableEnd)
        }

        getThumnailImage(position: changedDuration) { [unowned self] (image) in
            let time = KollusUtil.durationToString(duration: changedDuration)
            self.controlView.videoSliderThumbnailView.setThumbnail(image: image, currentTimeText: time, addedTime: Int(diffDuration))
        }
    }
    
    func onVideoSliderTouchEnd(view: VideoControlView, deltaX: CGFloat) {
        let diffDuration = Double(deltaX / self.view.frame.size.width * 90)
        
        var changedDuration = currentPlayTime + diffDuration
                
        if changedDuration < playSectionStartTime {
            changedDuration = playSectionStartTime
        }
        else if changedDuration > totalDuration + playSectionStartTime {
            changedDuration = totalDuration + playSectionStartTime
        }
        
        if nSeekableEnd > 0 && Int(changedDuration) > nSeekableEnd {
            changedDuration = Double(nSeekableEnd)
        }

        playerView.currentPlaybackTime = changedDuration
        try? playerView.play()
        
        if controlView.repeatMode == .end {
            if changedDuration < repeatStartPoint || changedDuration > repeatEndPoint {
                controlView.bottomStackRepeatTouched(view: controlView.bottomButtonsStackView)
            }
        }
        
        isPlayed = false
        resetTimelineTimer()
    }
    
    // MARK: - BottomStackView
    func onBottomStackRepeatTouched(view: VideoControlView, repeatMode: VideoControlView.RepeatMode) {
        switch repeatMode {
        case .none:
            controlView.sliderView.removeRepeatPoint()
        case .start:
            controlView.sliderView.setRepeatStartPoint()
            repeatStartPoint = playerView.currentPlaybackTime
        case .end:
            controlView.sliderView.setRepeatEndPoint()
            repeatEndPoint = playerView.currentPlaybackTime
            
            if repeatStartPoint > repeatEndPoint {
                let swapTemp = repeatStartPoint
                repeatStartPoint = repeatEndPoint
                repeatEndPoint = swapTemp
            }
            
            playerView.currentPlaybackTime = repeatStartPoint
        }
    }
    
    func onBottomStackPlaySpeedTouched(view: VideoControlView) {
        NLog("onBottomStackPlaySpeedTouched")
        controlView.displayControlView(false)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [weak self] in
            guard let self = self else { return }
            self.playSpeedPopupView.isHidden = false
            
            UIView.animate(withDuration: 0.2) {
                self.playSpeedPopupView.alpha = 1
                self.controlView.controlUIButton.isHidden = true
                UIAccessibility.post(notification: .layoutChanged, argument: self.playSpeedPopupView.closeSpeedSetting)
                self.playSpeedPopupView.layoutIfNeeded()
            }
        }
    }
    
    
    func onBottomStackPlaySpeedLongPressed(view: VideoControlView) {
        playSpeedPopupView.selectedPlaySpeed = .x1
        onPlaySpeedChanged(playSpeed: PlaySpeed.x1.rawValue)
        controlView.bottomButtonsStackView.playSpeed = .x1
        
        
    }
    
    
    func onBottomStackMoreTouched(view: VideoControlView) {
        controlView.displayControlView(false)
        let storyBoard = UIStoryboard.storyboard(name: "ActionSheetViewController")
        
        let navigationViewController = storyBoard.instantiateInitialViewController() as! UINavigationController
        
        if let actionSheetPhone = navigationViewController.children.first as? ActionSheetViewController {
            actionSheetViewController = actionSheetPhone
            actionSheetViewController?.mode = .setting
            actionSheetViewController?.isLiveMode = controlView.bottomButtonsStackView.isLiveMode

            actionSheetViewController?.canModifyPlaybackRate = !playerView.disablePlayRate
            actionSheetViewController?.subtitleList = subtitleList
            actionSheetViewController?.selectedSubtitleIndex = selectedSubtitleIndex
            actionSheetViewController?.isAudioWatermarkingPlay = playerView.isWaterMark

            actionSheetViewController?.streamInfoList = streamInfoList
            actionSheetViewController?.selectedStreamInfoIndex = selectedStreamInfoindex

            actionSheetViewController?.kollusContentEntity = kollusContentEntity

            ActionSheetData.kollusContentEntity = kollusContentEntity

            actionSheetViewController?.delegate = self
        }
        else if let actionSheetPad = navigationViewController.children.first as? ActionSheetViewController_iPad {
            actionSheetViewController_iPad = actionSheetPad
            actionSheetViewController_iPad?.mode = .setting
            actionSheetViewController_iPad?.isLiveMode = controlView.bottomButtonsStackView.isLiveMode

            actionSheetViewController_iPad?.canModifyPlaybackRate = !playerView.disablePlayRate
            actionSheetViewController_iPad?.subtitleList = subtitleList
            actionSheetViewController_iPad?.selectedSubtitleIndex = selectedSubtitleIndex
            actionSheetViewController_iPad?.isAudioWatermarkingPlay = playerView.isWaterMark

            actionSheetViewController_iPad?.streamInfoList = streamInfoList
            actionSheetViewController_iPad?.selectedStreamInfoIndex = selectedStreamInfoindex

            actionSheetViewController_iPad?.kollusContentEntity = kollusContentEntity

            ActionSheetData.kollusContentEntity = kollusContentEntity

            actionSheetViewController_iPad?.delegate = self
        }
        
        present(navigationViewController, animated: false, completion: nil)
    }
    
    
    // MARK: - Event
    func onVideoCloseTouched(view: VideoControlView?) {
        NLog("onVideoCloseTouched +++")
        timelineTimer.invalidate()
//        playerView.delegate = nil
//        playerView.lmsDelegate = nil
//        playerView.drmDelegate = nil
//        playerView.bookmarkDelegate = nil
        NLog("onVideoCloseTouched playerView : \(playerView)")
        if playerView != nil {
            try? playerView.stop()
        } 
//         playerView = nil
        
//        controlView.chattingWebView.onChattingClose()
//
//        self.view.alpha = 0
//
//        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.dismiss(animated: false, completion: nil)
        NLog("onVideoCloseTouched ---")
    }
    
    func onVideoPlayAndPauseTouched(view: VideoControlView) {
        if playerView.isPlaying {
            try? playerView.pause()
        }
        else {
            try? playerView.play()
        }
    }
    
    func onVideoSeekerTouched(view: VideoControlView, direction: SeekerButton.Direction, isContinues: Bool, intervalTime: Double) {
        NLog("onVideoSeekerTouched +++")
        let seekRange = SeekRange(rawValue: PreferenceManager.seekRange)!.toInteger
        
        if !isContinues {
            savedCurrentTime = playerView.currentPlaybackTime
        }
        
                
        switch direction {
        case .backward:
            if isLive && isSeekable {
                if savedCurrentTime - intervalTime < -totalDuration {
                    playerView.currentPlaybackTime = -totalDuration
                }
                else {
                    playerView.currentPlaybackTime = savedCurrentTime - intervalTime
                }
            }
            else if isSectionPlay && (currentPlayTime - playSectionStartTime) - Double(seekRange) < 0 {
                playerView.currentPlaybackTime = playSectionStartTime
            }
            else {
                if savedCurrentTime - intervalTime < 0 {
                    playerView.currentPlaybackTime = 0
                }
                else {
                    playerView.currentPlaybackTime = savedCurrentTime - intervalTime
                }
            }
            
        case .forward:
            NLog("onVideoSeekerTouched forward +++")
            NLog("onVideoSeekerTouched forward currentPlayTime : \(currentPlayTime),playSectionStartTime : \(playSectionStartTime), totalDuration : \(totalDuration)")
            NLog("onVideoSeekerTouched forward savedCurrentTime : \(savedCurrentTime)")
            NLog("onVideoSeekerTouched forward intervalTime : \(intervalTime)")
            if isLive && isSeekable {
                if Int(savedCurrentTime + intervalTime) > 0 {
                    playerView.currentPlaybackTime = 0
                }
                else {
                    playerView.currentPlaybackTime = savedCurrentTime + intervalTime
                }
            }
            else if isSectionPlay {
                if (currentPlayTime - playSectionStartTime) + Double(seekRange) >= totalDuration {
                    playerView.currentPlaybackTime = totalDuration + playSectionStartTime-1
                }
                else {
                    playerView.currentPlaybackTime = currentPlayTime + Double(seekRange)
                }
            }
            else {
                if !isSeekable && nSeekableEnd > 0 {
                    if Int(savedCurrentTime + intervalTime) > nSeekableEnd {
                        playerView.currentPlaybackTime = TimeInterval(nSeekableEnd)
                    }
                    else {
                        playerView.currentPlaybackTime = savedCurrentTime + intervalTime
                    }
                }
                else if savedCurrentTime + intervalTime >= totalDuration {
                    playerView.currentPlaybackTime = totalDuration-1
                }
                else {
                    playerView.currentPlaybackTime = savedCurrentTime + intervalTime
                }
            }
            NLog("onVideoSeekerTouched forward ---")
        }
        
        
        if controlView.repeatMode == .end {
            if currentPlayTime - Double(seekRange) < repeatStartPoint && direction == .backward {
                controlView.bottomStackRepeatTouched(view: controlView.bottomButtonsStackView)
            }
            else if currentPlayTime + Double(seekRange) > repeatEndPoint && direction == .forward {
                controlView.bottomStackRepeatTouched(view: controlView.bottomButtonsStackView)
            }
        }
        
//        updateTimeLine()
        try? playerView.play()
        NLog("onVideoSeekerTouched ---")
    }
    
    // MARK: - inner function
    func releaseVideo() {
        NLog("releaseVideo +++")
        timelineTimer.invalidate()
        playerView.delegate = nil
        playerView.lmsDelegate = nil
        playerView.drmDelegate = nil
        playerView.bookmarkDelegate = nil
//        try? playerView.stop()
        if(playerView != nil){
            playerView.removeFromSuperview()
        }
         playerView = nil
        
        controlView.chattingWebView.onChattingClose()
        
        self.view.alpha = 0
        
//        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
//        self.dismiss(animated: false, completion: nil)
        self.dismiss(animated: false) { [unowned self] in
            AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            self.delegate?.onVideoViewClosed()
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.onVideoViewClosed()
        }
        NLog("releaseVideo ---")
    }

    
    // MARK: - PinchGesture
    func onVideoPinchGestured(view: VideoControlView, recognizer: UIPinchGestureRecognizer) {
        
        if playerView == nil {
            NLog("onVideoPinchGestured playerView nil")
            return
        }

        if playerView.scalingMode == .scaleFill {return}
        if playerView.isAudioOnly { return }
        
        do {
            try playerView.zoom(recognizer)
        }
        catch {
            print(error.localizedDescription)
        }
    }

    #if false
    func onVideoPinchGestured(view: VideoControlView, recognizer: UIPinchGestureRecognizer) {
        if playerView.scalingMode == .scaleFill {return}
        if playerView.isAudioOnly { return }
        if UIApplication.shared.statusBarOrientation == .portrait {
            portraitPinchGestured(sender: recognizer)
        }
        else {
            landscapePinchGestured(sender: recognizer)
        }
    }
    #endif

    func portraitPinchGestured(sender: UIPinchGestureRecognizer) {
        let currentScale = self.containerView.frame.width / self.containerView.bounds.size.width

        var newScale = sender.scale
        
        if currentScale * sender.scale < 1 {
            newScale = 1 / currentScale
        }
        else if currentScale * sender.scale > 2 {
            newScale = 2 / currentScale
        }
       
            
        self.containerView.transform = self.containerView.transform.scaledBy(x: newScale, y: newScale)
        sender.scale = 1
        
        let imageOriginY: CGFloat = containerView.frame.origin.y
        
        var safeAreaInsetTop: CGFloat = 0
//        var safeAreaInsetBottom: CGFloat = 0
        
        if #available(iOS 11.0, *) {
            safeAreaInsetTop = view.safeAreaInsets.top
//            safeAreaInsetBottom = view.safeAreaInsets.bottom
        }
        
        let superViewInset = safeAreaInsetTop + containerView.frame.origin.y
        
        if imageOriginY > -safeAreaInsetTop {
            scrollView.contentInset = UIEdgeInsets(top: -containerView.frame.origin.y + imageOriginY, left: -containerView.frame.origin.x, bottom: containerView.frame.origin.y - imageOriginY, right: containerView.frame.origin.x)
        }
        else {
            scrollView.contentInset = UIEdgeInsets(top: -containerView.frame.origin.y  + imageOriginY - superViewInset, left: -containerView.frame.origin.x, bottom: containerView.frame.origin.y  - imageOriginY - superViewInset, right: containerView.frame.origin.x)
        }
        
        scrollView.contentSize = CGSize(width: containerView.frame.width, height: containerView.frame.height  + (imageOriginY * 2))
    }
    
    func landscapePinchGestured(sender: UIPinchGestureRecognizer) {
        let currentScale = self.containerView.frame.width / self.containerView.bounds.size.width
        var newScale = sender.scale
        
        if currentScale * sender.scale < 1 {
            newScale = 1 / currentScale
        }
        else if currentScale * sender.scale > 2 {
            newScale = 2 / currentScale
        }
       
        self.containerView.transform = self.containerView.transform.scaledBy(x: newScale, y: newScale)
        sender.scale = 1
                
        var safeAreaInsetLeft: CGFloat = 0
//        var safeAreaInsetRight: CGFloat = 0
        
        if #available(iOS 11.0, *) {
            safeAreaInsetLeft = view.safeAreaInsets.left
//            safeAreaInsetRight = view.safeAreaInsets.right
        }
        
        let imageLeftInset: CGFloat = containerView.frame.origin.x
//        let imageRightInset: CGFloat = containerView.frame.origin.x

        let superViewInset = safeAreaInsetLeft + containerView.frame.origin.x
        
        if imageLeftInset > -safeAreaInsetLeft {
            scrollView.contentInset = UIEdgeInsets(top: -containerView.frame.origin.y, left: -containerView.frame.origin.x + imageLeftInset, bottom: containerView.frame.origin.y, right: containerView.frame.origin.x - imageLeftInset)
        }
        else {
            scrollView.contentInset = UIEdgeInsets(top: -containerView.frame.origin.y, left: -containerView.frame.origin.x + imageLeftInset - superViewInset, bottom: containerView.frame.origin.y, right: containerView.frame.origin.x - imageLeftInset - superViewInset)
        }
        
        scrollView.contentSize = CGSize(width: containerView.frame.width + (imageLeftInset * 2), height: containerView.frame.height)
    }
    
    
    func onPreviousVideoTouched(view: VideoControlView) {
        guard let previousVideo = getPreviousVideoData(), previousVideo.fileType != 1 else { return }
        self.data = previousVideo
        
        playVideoItem()
    }
    
    func onReplayVideoTouched(view: VideoControlView) {
        playVideoItem()
    }
    
    func onNextVideoTouched(view: VideoControlView) {
        guard let nextVideo = getNextVideoData() else { return }
        self.data = nextVideo
        
        playVideoItem()
    }
    
}

//MARK: - KollusPlayerDelegate
extension PlayerViewController: KollusPlayerDelegate {
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, buffering: Bool, prepared: Bool, error: Error!) {
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, prepareToPlayWithError error: Error!) {
        NLog("prepareToPlayWithError +++")
        playListIndex += 1
        cntPlay = 0
        view.isUserInteractionEnabled = true
        isSectionPlay = false
        isLive = playerView.isLive
        isSeekable = playerView.seekable
        nSeekableEnd = playerView.nSeekableEnd
//        isSeekable = false
//        nSeekableEnd = 300
        if let actionSheetViewController = actionSheetViewController {
            actionSheetViewController.dismiss(animated: false)
        }

        if let actionSheetViewController = actionSheetViewController_iPad {
            actionSheetViewController.dismiss(animated: false)
        }

        subtitleBackgroundView.isHidden = true
        subtitleLabel.isHidden = true

        data = playerView.content
        CoreDataManager.shared.saveKollusContentData(data: data!)
        kollusContentEntity = CoreDataManager.shared.fetchKollusContentData(data: data!)
        if playListIndex == 1 {
            kollusContentEntity!.playSpeed = 5
            playSpeed = 5
        }
        else {
            kollusContentEntity!.playSpeed = playSpeed
        }
        controlView.closeBookmarkView()

        controlView.setIndicatorView(isBuffering: false)
        
        controlView.titleLabel.text = data?.title
        controlView.sliderView.totalDuration = data?.duration ?? 0
        
        if kollusPlayerView.isAudioOnly {
            audioImageView.isHidden = false
//            audioImageView.image = UIImage(named: data!.snapshot)
            audioImageView.image = UIImage(contentsOfFile: data!.snapshot)
        }
        else {
            audioImageView.isHidden = true
        }
        updateRotation()
        
        if playerView.muteOnStart {
            controlView.soundProgressView.setMute()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [unowned self] in
            NLog("prepareToPlayWithError DispatchQueue.main.asyncAfter")
            if PreferenceManager.isFirstWatched {
                let storyBoard = UIStoryboard(name: "VideoAlphaGuideViewController", bundle: nil)
                let videoAlphaGuideViewController = storyBoard.instantiateInitialViewController() as! VideoAlphaGuideViewController
                videoAlphaGuideViewController.delegate = self
                present(videoAlphaGuideViewController, animated: false, completion: nil)
            }
            else if playerView.playSection.startTime >= 0 && playerView.playSection.endTime != 0 {
                isSectionPlay = true
                playerView.currentPlaybackTime = TimeInterval(playerView.playSection.startTime)
                controlView.sliderView.totalDuration = totalDuration
                
                initialPlay()
            }
            else if playerView.forceNScreen {
                playerView.currentPlaybackTime = data?.position ?? 0
            }
            else if data!.position > 30 && (data!.duration - data!.position) > 30 {
                let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
                let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
                confirmPopupViewController.delegate = self
                confirmPopupViewController.data = data
                confirmPopupViewController.type = .continueView
                
                present(confirmPopupViewController, animated: false)
            }
            else {
                initialPlay()
            }
        }
        
        
        resetTimelineTimer()
        updateThumbnailImages()
        resetABRepeatNone()
        
        if kollusPlayerView.isLive && isSeekable == false {
            controlView.updateLiveModeUI()
        }
        else if isSeekable == false && nSeekableEnd <= 0 {
            controlView.updateNoSeekModeUI()
        }
        else {
            controlView.updateSeekModeUI()
        }
        
        updateAudioWatermark()
        controlView.updateNSecSkipView(second: playerView.nSecSkip)
        
        // Chatting
        if let kollusChat = playerView.kollusChat, var chatURL = URL(string: kollusChat.chatUrl){
//            chatURL = URL(string: "\(chatURL)?font_color=00ff00")!
            #if DEBUG
            let strURL = "\(chatURL)"
            if strURL.range(of: "?") != nil {
                chatURL = URL(string: "\(chatURL)&chat_debug_mode")!
            }
            else {
                chatURL = URL(string: "\(chatURL)?chat_debug_mode")!
            }
            #endif
            controlView.chattingWebView.setURL(chatURL)
            controlView.bottomButtonsStackView.isChattingMode = true
        }
        else {
            controlView.bottomButtonsStackView.isChattingMode = false
        }
        
        // bookmark
        var isSection = false
        if playerView.playSection.startTime >= 0 && playerView.playSection.endTime != 0 {
            isSection = true
        }
        controlView.bottomButtonsStackView.removeBookmarkViewFromSuperView(isRemove: playerView.intro || !isSeekable || isSection)
        
        controlView.forwardSeekButton.isUserInteractionEnabled = isSeekable || nSeekableEnd > 0
        controlView.backwardSeekButton.isUserInteractionEnabled = isSeekable || nSeekableEnd > 0
        controlView.sliderView.isUserInteractionEnabled = isSeekable || nSeekableEnd > 0
                
        controlView.forwardSeekButton.updateSeekRange() // seekButton의 currentInterval 초기화
        controlView.backwardSeekButton.updateSeekRange()
        savedCurrentTime = 0

        // PlaySpeed
        controlView.bottomButtonsStackView.disablePlayRate = playerView.disablePlayRate
        
        // PlaySpeed popupView
        updatePlaySpeedDatas()
        
        if let mck = playerView.mediaContentKey {
            PreferenceManager.playedContents[mck] = false
        }
        
        playerView.setDisableZoomOut(true) // zoom 축소 기능 Disable
        NLog("prepareToPlayWithError ---")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, play userInteraction: Bool, error: Error!) {
        controlView.setPlayAndPauseButtonImage(isPlay: true)
        if cntPlay == 0 && playListIndex == 1 {
            controlView.displayControlView(true, resetTimer: 3)
        }
        else {
            controlView.resetControlHideTimer()
        }
        cntPlay += 1
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, pause userInteraction: Bool, error: Error!) {
        controlView.setPlayAndPauseButtonImage(isPlay: false)
        controlView.resetControlHideTimer()
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, buffering: Bool, error: Error!) {
        print("buffering")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, stop userInteraction: Bool, error: Error!) {
        NLog("kollusPlayerView stop error : \(error) +++")
        NLog("kollusPlayerView stop userInteraction : \(userInteraction)")
        NLog("kollusPlayerView stop conteytType : \(data?.contentType)");

        if let actionSheetViewController = actionSheetViewController {
            actionSheetViewController.dismiss(animated: false)
        }

        if let actionSheetViewController = actionSheetViewController_iPad {
            actionSheetViewController.dismiss(animated: false)
        }
        
        controlView.removeGestureEvent()

        if error != nil {
            self.dismiss(animated: false) { [unowned self] in
                
//                try? playerView.stop()
                UIApplication.presentErrorViewController(title: "Code : \((error as NSError).code)", errorDescription: "Can't play.".localized(), errorReason: (error as NSError).localizedDescription)
            }
            onVideoCloseTouched(view: nil)
            return
        }
        
        var isDownloaded = false
        
        if let data = data {
            if data.contentType == .downloading || data.contentType == .adaptiveDownload {
                if data.fileSize > 0 {
                    isDownloaded = data.downloadSize / data.fileSize == 1 ? true : false
                }
            }
        }
        
        if userInteraction == false && (data?.contentType == .downloading || data?.contentType == .adaptiveDownload || isDownloaded == true) {
            if let mck = playerView.mediaContentKey {
                PreferenceManager.playedContents[mck] = true
            }
        }
        

        #if false // 재생 종료시 리스트 화면으로
        if userInteraction == false && (data?.contentType == .downloading || data?.contentType == .adaptiveDownload || isDownloaded == true || data?.contentType == .streaming) {
            if let actionSheetViewController = actionSheetViewController {
                actionSheetViewController.onCloseTouched(nil)
            }

            if let actionSheetViewController = actionSheetViewController_iPad {
                actionSheetViewController.onCloseTouched(nil)
            }

            
            playerView.delegate = nil
            playerView.lmsDelegate = nil
            playerView.drmDelegate = nil
            playerView.bookmarkDelegate = nil
            
            
            controlView.isLocked = false
            controlView.updateLockModeUI(includeProgressView: false)
            controlView.bottomButtonsStackView.isLockMode = false
            controlView.backgroundView.alpha = 1
            controlView.displayPlayReachEndTimeView(willShow: true)
            playSpeedPopupView.onCloseTouched(nil)
            
            if let mck = playerView.mediaContentKey {
                PreferenceManager.playedContents[mck] = true
            }
        }
        else
        {
            onVideoCloseTouched(view: nil)
        }
        #endif
//        onVideoCloseTouched(view: nil)
        releaseVideo()
        playListIndex = 0
        NLog("kollusPlayerView stop ---")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, position: TimeInterval, error: Error!) {
        if data!.contentType == .streaming {
            controlView.setIndicatorView(isBuffering: playerView.isSeeking)
        }
        
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, scroll distance: CGPoint, error: Error!) {
        print("scroll")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, zoom recognizer: UIPinchGestureRecognizer!, error: NSErrorPointer) {
        print(recognizer!)
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, naturalSize: CGSize) {
        print("naturalSize")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playerContentMode: KollusPlayerContentMode, error: Error!) {
//        print("playerContentMode")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playerContentFrame contentFrame: CGRect, error: Error!) {

    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, playbackRate: Float, error: Error!) {
//        print("playbackRate")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, repeat: Bool, error: Error!) {
        print("repeat")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, enabledOutput: Bool, error: Error!) {
        print("enabledOutput")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, unknownError error: Error!) {
        if error != nil {
            self.dismiss(animated: false) { [unowned self] in
                try? playerView.stop()
                UIApplication.presentErrorViewController(title: "Code : \((error as NSError).code)", errorDescription: "Can't play.".localized(), errorReason: (error as NSError).localizedDescription)
            }
        }
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, framerate: Int32) {
//        print("framerate : \(framerate)")
    }
    
    func kollusPlayerView(_ view: KollusPlayerView!, height: Int32) {
    }
    
    func kollusPlayerView(_ view: KollusPlayerView, bitrate: Int32) {
        
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, lockedPlayer playerType: KollusPlayerType) {
        print("lockedPlayer")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, charset: UnsafeMutablePointer<Int8>!, caption: UnsafeMutablePointer<Int8>!) {
        currentSubtitleText = String(cString: caption)
        NLog("currentSubtitleText : \(currentSubtitleText)")
        if currentSubtitleText.hasSuffix("<BR>") {
            currentSubtitleText = String(currentSubtitleText.dropLast(4))
        }
        
        if selectedSubtitleIndex == 0 {
            subtitleBackgroundView.isHidden = true
            subtitleLabel.isHidden = true
        }
        else {
            updateSubtitleLabel(text: currentSubtitleText)
        }
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, thumbnail isThumbnail: Bool, error: Error!) {
        print("isThumbnail")
    }
    
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, mck: String!) {
        print("mck \(mck ?? "")")
    }
    
    
}

//MARK: - KollusPlayerBookmarkDelegate
extension PlayerViewController: KollusPlayerBookmarkDelegate {
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, bookmark bookmarks: [Any]!, enabled: Bool, error: Error!) {
        if bookmarks == nil {
            controlView.bookmarkView.state = .networkError
            return
        }
        
        let kollusBookmarks = bookmarks as! [KollusBookmark]
        
        for bookmark in kollusBookmarks {
            if bookmark.kind == .index {
                controlView.bottomButtonsStackView.bookmarkIndexExistView.isHidden = false
                break
            }
        }
        
        if enabled {
            controlView.bottomButtonsStackView.bookmarkImageView.tintColor = .white
        }
        else {
            controlView.bottomButtonsStackView.bookmarkImageView.tintColor = UIColor(hexFromString: "#AAA9AA")
        }
        
        controlView.bookmarkView.state = enabled ? .enable : .unable
        controlView.bookmarkView.data = kollusBookmarks
    }
    
}

//MARK: - KollusPlayerLMSDelegate
extension PlayerViewController: KollusPlayerLMSDelegate {
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, json: [AnyHashable : Any]!, error: Error!) {
        print("lms")
    }
}

//MARK: - KollusPlayerDRMDelegate
extension PlayerViewController: KollusPlayerDRMDelegate {
    func kollusPlayerView(_ kollusPlayerView: KollusPlayerView!, request: [AnyHashable : Any]!, json: [AnyHashable : Any]!, error: Error!) {
        print("drm")
        print(json ?? "")
    }
    
    
}

// MARK: - ActionSheetViewDelegate
extension PlayerViewController: ActionSheetViewDelegate {
    func onVideoScaleChanged(videoScale: Int) {
        let videoScale = VideoScale(rawValue: videoScale)!
        switch videoScale {
        case .aspectFit:
            playerView.scalingMode = .scaleAspectFit
        case .scaleToFill:
            playerView.scalingMode = .scaleFill
        case .scaleToFit:
            playerView.scalingMode = .scaleAspectFill
        }
        
        kollusContentEntity?.scaleMode = Int16(videoScale.rawValue)
        
        self.containerView.transform = .identity
        self.scrollView.contentInset = .zero
        
        
        if data?.contentType != .streaming {
            CoreDataManager.shared.saveContext()
        }
    }
    
    func onPlaySpeedChanged(playSpeed: Int) {
        NLog("onPlaySpeedChanged : \(playSpeed)")
        let playSpeedValue = PlaySpeed(rawValue: playSpeed)
        switch playSpeedValue {
        case .x0_5:
            playerView.currentPlaybackRate = 0.5
        case .x0_6:
            playerView.currentPlaybackRate = 0.6
        case .x0_7:
            playerView.currentPlaybackRate = 0.7
        case .x0_8:
            playerView.currentPlaybackRate = 0.8
        case .x0_9:
            playerView.currentPlaybackRate = 0.9
//        case .x0_75:
//            playerView.currentPlaybackRate = 0.75
        case .x1:
            playerView.currentPlaybackRate = 1
        case .x1_1:
            playerView.currentPlaybackRate = 1.1
        case .x1_2:
            playerView.currentPlaybackRate = 1.2
        case .x1_3:
            playerView.currentPlaybackRate = 1.3
        case .x1_4:
            playerView.currentPlaybackRate = 1.4
//        case .x1_25:
//            playerView.currentPlaybackRate = 1.25
        case .x1_5:
            playerView.currentPlaybackRate = 1.5
//        case .x1_75:
//            playerView.currentPlaybackRate = 1.75
        case .x1_6:
            playerView.currentPlaybackRate = 1.6
        case .x1_7:
            playerView.currentPlaybackRate = 1.7
        case .x1_8:
            playerView.currentPlaybackRate = 1.8
        case .x1_9:
            playerView.currentPlaybackRate = 1.9
        case .x2:
            playerView.currentPlaybackRate = 2
        case .none:
            return
        }
        
        kollusContentEntity?.playSpeed = Int16(playSpeedValue!.rawValue)
        self.playSpeed = Int16(playSpeedValue!.rawValue)
        controlView.bottomButtonsStackView.playSpeed = playSpeedValue ?? .x1
        updatePlaySpeedDatas()
        if data?.contentType != .streaming {
            CoreDataManager.shared.saveContext()
        }
    }

    func onActionSheetClose() {
        if UIAccessibility.isVoiceOverRunning {
            controlView.displayControlView(true)
        }
    }

    func onScreenRotation() {
        if UIAccessibility.isVoiceOverRunning {
            controlView.displayControlView(true)
        }
    }

    func onSeekRangeChanged(seekRange: Int) {
        PreferenceManager.seekRange = SeekRange(rawValue: seekRange)!.rawValue
        controlView.forwardSeekButton.updateSeekRange()
        controlView.backwardSeekButton.updateSeekRange()
    }
    
    func onVideoResolutionChanged(videoResolution: Int) {
        selectedStreamInfoindex = videoResolution
        let bandWidth = Int32(streamInfoList[videoResolution].bandwidth) ?? 0
        playerView.changeBandWidth(bandWidth)
    }
    
    func onSubtitleLanguageChanged(subtitleLanguage: Int) {
        selectedSubtitleIndex = subtitleLanguage
        
        if selectedSubtitleIndex == 0 { // 감추기
            subtitleBackgroundView.isHidden = true
            subtitleLabel.isHidden = true
        }
        else {
            if let listSubList = playerView.listSubTitle, listSubList.count > 0 {
                let selectedSubTitle = playerView.listSubTitle[subtitleLanguage - 1] as! SubTitleInfo
                playerView.setSubTitlePath(strdup(selectedSubTitle.strUrl))
            }
        }

        #if false
        kollusContentEntity?.subtitle = Int16(selectedSubtitleIndex)
        if data?.contentType != .streaming {
            CoreDataManager.shared.saveContext()
        }
        #endif
    }
    
    func onSubtitleStyleChanged() {
        if selectedSubtitleIndex != 0 {
            updateSubtitleLabel(text: currentSubtitleText)
        }
    }
    
    func onSubtitleBgChanged() {
        if selectedSubtitleIndex != 0 {
            updateSubtitleBg(text: currentSubtitleText)
        }
    }

}

//MARK: - VideoAlphaGuideViewDelegate
extension PlayerViewController: VideoAlphaGuideViewDelegate {
    func onAlphaGuideClosed(controller: VideoAlphaGuideViewController) {
        NLog("onAlphaGuideClosed +++")
        if playerView.playSection.startTime >= 0 && playerView.playSection.endTime != 0 {
            NLog("onAlphaGuideClosed playSection")
            isSectionPlay = true
            playerView.currentPlaybackTime = TimeInterval(playerView.playSection.startTime)
            controlView.sliderView.totalDuration = totalDuration
            
            initialPlay()
        }
        else if playerView.forceNScreen {
            NLog("onAlphaGuideClosed forceNScreen")
            playerView.currentPlaybackTime = data?.position ?? 0
        }
        else if data!.position > 30 && (data!.duration - data!.position) > 30 {
            NLog("onAlphaGuideClosed data.position : \(data!.position)")
            let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
            let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
            confirmPopupViewController.delegate = self
            confirmPopupViewController.data = data
            confirmPopupViewController.type = .continueView
            
            present(confirmPopupViewController, animated: false)
        }
        else {
            NLog("onAlphaGuideClosed initialPlay")
            initialPlay()
        }
//        initialPlay()
        NLog("onAlphaGuideClosed ---")
    }
}

extension PlayerViewController: ConfirmPopupViewDelegate {
    func onConfirmTouched(viewController: ConfirmPopupViewController) {
        guard let data = viewController.data else { return }
        playerView.currentPlaybackTime = data.position
        reconnectAVPlayer()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.reconnectAVPlayer()
        }
        
        initialPlay()
    }
    
    func onCancelTouched(viewController: ConfirmPopupViewController) {
        reconnectAVPlayer()
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.reconnectAVPlayer()
        }
        
        initialPlay()
    }
}

//MARK: -  PlaySpeedViewDelegate
extension PlayerViewController: PlaySpeedViewDelegate {
    func onPlaySpeedSliderClosed(View: PlaySpeedView) {
        if UIAccessibility.isVoiceOverRunning {
            controlView.displayControlView(true)
        }
    }
    
    func onPlaySpeedSliderIndexChanged(view: PlaySpeedView, playSpeed: PlaySpeed) {
        onPlaySpeedChanged(playSpeed: playSpeed.rawValue)
        controlView.bottomButtonsStackView.playSpeed = playSpeed
    }
}


//MARK: - AVPictureInPictureControllerDelegate
extension PlayerViewController: AVPictureInPictureControllerDelegate {
    
    func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
        //Update video controls of main player to reflect the current state of the video playback.
        //You may want to update the video scrubber position.
        print("restoreUserInterfaceForPictureInPictureStopWithCompletionHandler")
    }
    
    func pictureInPictureControllerWillStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        print("Handle PIP will start event")
        //Handle PIP will start event
    }
    
    func pictureInPictureControllerDidStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        print("Handle PIP did start event")
        //Handle PIP did start event
    }
    
    func pictureInPictureController(_ pictureInPictureController: AVPictureInPictureController, failedToStartPictureInPictureWithError error: Error) {
        print("pictureInPictureController")
        //Handle PIP failed to start event
    }
    
    func pictureInPictureControllerWillStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
//        print("pictureInPictureControllerWillStopPictureInPicture")
        //Handle PIP will stop event
    }
    
    func pictureInPictureControllerDidStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
        print("pictureInPictureControllerDidStopPictureInPicture")
        
        if controlView.isDisplayBookmark {
            controlView.bottomStackBookmarkTouched(view: controlView.bottomButtonsStackView)
        }
        
        //Handle PIP did start event
    }
}

extension UIView {
    func makeSecure() {
    if #available(iOS 13.0, *) {
        DispatchQueue.main.async {
            let field = UITextField()
            field.isSecureTextEntry = true
            self.addSubview(field)
            self.layer.superlayer?.addSublayer(field.layer)
            field.layer.sublayers?.first?.addSublayer(self.layer)
        }
    }
    }
}
