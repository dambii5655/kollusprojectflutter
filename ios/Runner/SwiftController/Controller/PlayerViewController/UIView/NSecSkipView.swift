//
//  NSecSkipView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/12/15.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation


protocol NSecSkipViewDelgate: class {
    func onSecSkipViewTouched(view: NSecSkipView)
}


class NSecSkipView: UIView {
    weak var delegate: NSecSkipViewDelgate?
    
    var second = 0
    
    @IBOutlet weak var countView: UIView!
    @IBOutlet weak var countLabel: UILabel!
    
    @IBOutlet weak var skipView: UIView!
    
    var timer = Timer()
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("NSecSkipView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    private func setEvent() {
        
    }
    
    func setCountDownLabel(second: Int) {
        timer.invalidate()
        
        countView.isHidden = false
        skipView.isHidden = true
        
        self.second = second
        countLabel.text = "\(self.second)"
    }
    
    func startCountDownTimer() {
        if second <= 0 { return }
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateSecondSkipLabel), userInfo: nil, repeats: true)
    }
    
    //MARK: - Event
    
    @objc func updateSecondSkipLabel() {
        second = second - 1
        if second == 0 {
            countView.isHidden = true
            skipView.isHidden = false
            timer.invalidate()
        }
        else {
            countLabel.text = "\(self.second)"
        }
    }
    
    
    @IBAction func onSkipTouched(_ sender: UIButton) {
        delegate?.onSecSkipViewTouched(view: self)
    }
}
