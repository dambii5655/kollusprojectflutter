//
//  BottomButtonsStackView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/28.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit
import Lottie

protocol BottomStackViewDelgate: class {
    func bottomStackLockTouched(view: BottomButtonsStackView, isLocked: Bool)
    func bottomStackChattingTouched(view: BottomButtonsStackView, isHidden: Bool)
    func bottomStackSeriesTouched(view: BottomButtonsStackView)
    func bottomStackRepeatTouched(view: BottomButtonsStackView)
    func bottomStackBookmarkTouched(view: BottomButtonsStackView)
    func bottomStackPlaySpeedTouched(view: BottomButtonsStackView)
    func bottomStackPlaySpeedLongPressed(view: BottomButtonsStackView)
}


class BottomButtonsStackView: UIView {
    weak var delegate: BottomStackViewDelgate?
    
    var isLockMode = false {
        didSet {
            updateLockModeUI(isLocked: isLockMode)
        }
    }
    
    var isHiddenChattingView = false {
        didSet {
            updateChattingView(isHidden: isHiddenChattingView)
        }
    }
    
    let blurView = UIVisualEffectView(effect: UIBlurEffect(style: .dark))
    @IBOutlet weak var stackView: UIStackView!
    
    @IBOutlet weak var lockAnimationView: AnimationView!
    @IBOutlet weak var lockButton: UIButton!
    @IBOutlet weak var lockLabel: UILabel!
    
    @IBOutlet weak var seriesView: UIView!
    @IBOutlet weak var seriesButton: UIButton!
    
    @IBOutlet weak var chattingView: UIView!
    @IBOutlet weak var chattingImageView: UIImageView!
    @IBOutlet weak var chattingButton: UIButton!
    
    @IBOutlet weak var repeatView: UIView!
    @IBOutlet weak var repeatImageView: UIImageView!
    @IBOutlet weak var repeatButton: UIButton!
    
    @IBOutlet weak var bookmarkView: UIView!
    @IBOutlet weak var bookmarkImageView: UIImageView!
    @IBOutlet weak var bookmarkIndexExistView: UIView!
    @IBOutlet weak var bookmarkButton: UIButton!
    
    @IBOutlet weak var playSpeedView: UIView!
    @IBOutlet weak var playSpeedLabel: UILabel!
    @IBOutlet weak var playSpeedButton: UIButton!
    
    var isLiveMode = false {
        didSet {
            setLiveMode()
        }
    }
    
    var noSeekMode = false {
        didSet {
            setNoSeekMode()
        }
    }
    
    var disablePlayRate = false {
        didSet {
            updatePlaySpeedUI()
        }
    }

    
    var isChattingMode = false {
        didSet {
            setChattingMode()
        }
    }
    
    var playSpeed: PlaySpeed = .x1 {
        didSet {
            updatePlaySpeedLabel()
        }
    }
    
    var isRemoveBookmark = false
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        NLog("setNib")
        let view = Bundle.main.loadNibNamed("BottomButtonsStackView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
        NLog("setUI")
        self.layer.cornerRadius = 6
        self.layer.masksToBounds = true
        
        if #available(iOS 10.0, *) {
            lockAnimationView.animation = Animation.named("lock")
        }
        else {
            lockButton.setImage(#imageLiteral(resourceName: "lockOpen18"), for: .normal)
        }
        lockButton.accessibilityLabel = "Lock screen".localized()
        chattingButton.accessibilityLabel = "View chat".localized()
        repeatButton.accessibilityLabel = "AB Repeat".localized()
        bookmarkButton.accessibilityLabel = "Bookmark".localized()
        playSpeedButton.accessibilityLabel = "Playback speed".localized()
        
        bookmarkIndexExistView.layer.cornerRadius = bookmarkIndexExistView.frame.size.height / 2
        
        self.insertSubview(blurView, at: 0)
        
        blurView.snp.makeConstraints { (maker) in
            maker.top.leading.trailing.bottom.equalToSuperview()
        }
        
        lockLabel.text = "Tap to unlock".localized()
        
        // add gesture recognizer
        let longPress = UILongPressGestureRecognizer(target: self, action: #selector(onPlaySpeedLongPressed))
        playSpeedButton.addGestureRecognizer(longPress)
        
        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(onPlaySpeedLongPressed))
        doubleTap.numberOfTapsRequired = 2
        playSpeedButton.addGestureRecognizer(doubleTap)

//        UITapGestureRecognizer *tapTwice = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapTwice:)];
//         UITapGestureRecognizer *tapTrice = [[UITapGestureRecognizer alloc] initWithTarget:self  action:@selector(tapTrice:)];
//
//         tapOnce.numberOfTapsRequired = 1;
//         tapTwice.numberOfTapsRequired = 2;
//         tapTrice.numberOfTapsRequired = 3;
//         //stops tapOnce from overriding tapTwice
//         [tapOnce requireGestureRecognizerToFail:tapTwice];
//         [tapTwice requireGestureRecognizerToFail:tapTrice];

        
//        let tap = UITapGestureRecognizer(target: self, action: #selector(onPlaySpeedTouched(_:)))
        
//        let doubleTap = UITapGestureRecognizer(target: self, action: #selector(onPlaySpeedLongPressed))
//        playSpeedButton.addGestureRecognizer(doubleTap)
        
    }
    
    private func setEvent() {
        NLog("setEvent")
        lockButton.addTarget(self, action: #selector(onLockTouched(_:)), for: .touchUpInside)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        NLog("traitCollectionDidChange")
        super.traitCollectionDidChange(previousTraitCollection)
        
        setChattingMode()
    }
    
    //MARK: - UI
    
    public func updatePlaySpeedUI() {
        NLog("updatePlaySpeedUI")
        if isLiveMode || disablePlayRate || isLockMode {
            playSpeedView.isHidden = true
        }
        else {
            playSpeedView.isHidden = false
        }
    }
    
    public func updatePlaySpeedLabel() {
        NLog("playSpeed : \(playSpeed)")
        if playSpeed.toString == PlaySpeed.x1.toString {
            playSpeedLabel.isHidden = true
        }
        else {
            playSpeedLabel.isHidden = false
            playSpeedLabel.text = playSpeed.toBottomString
        }
    }
    
    private func setLiveMode() {
        NLog("setLiveMode")
        seriesView.isHidden = isLiveMode
        repeatView.isHidden = isLiveMode
        bookmarkView.isHidden = isLiveMode
        
    }
    
    private func setNoSeekMode() {
        NLog("setNoSeekMode")
        if isLiveMode || noSeekMode || isLockMode{
            repeatView.isHidden = true
            bookmarkView.isHidden = true
        }
        else {
            repeatView.isHidden = false
            if isRemoveBookmark == false {
                bookmarkView.isHidden = false
            }
        }
    }
    
    private func setChattingMode() {
        NLog("setChattingMode")
        NLog("setChattingMode isChattingMode : \(isChattingMode)")
        if isChattingMode {
            chattingImageView.isHidden = false
            chattingButton.isHidden = false
            let position = 1
            stackView.insertArrangedSubview(chattingView, at: position)

        }
        else {
            chattingButton.isHidden = true
            chattingView.removeFromSuperview()
            chattingImageView.isHidden = true
        }
    }
    
    func updateLockModeUI(isLocked: Bool) {
        NLog("updateLockModeUI")
        if isLocked {
            self.layer.cornerRadius = self.bounds.size.height / 2
            lockLabel.isHidden = false
            lockAnimateView()
        }
        else {
            self.layer.cornerRadius = 6
            lockLabel.isHidden = true
            unlockAnimateView()
        }
    }
    
    
    func lockAnimateView() {
        NLog("lockAnimateView")

        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
//            self.seriesView.isHidden = true
            self.chattingView.isHidden = true
            self.repeatView.isHidden = true
            self.bookmarkView.isHidden = true
            self.playSpeedView.isHidden = true
        }) { [unowned self] (completion) in
            
            if #available(iOS 10.0, *) {
                self.lockAnimationView.animation = Animation.named("lock")
                self.lockAnimationView.play()
            }
            else {
                self.lockButton.setImage(#imageLiteral(resourceName: "lock18"), for: .normal)
            }
            lockButton.accessibilityLabel = "Unlock screen".localized()
        }
    }
    
    func unlockAnimateView() {
        NLog("unlockAnimateView")
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            guard let self = self else { return }
            if !self.isLiveMode && !self.noSeekMode {
                self.repeatView.isHidden = false
                if self.isRemoveBookmark == false {
                    self.bookmarkView.isHidden = false
                }
            }
            if !self.disablePlayRate {
                self.playSpeedView.isHidden = false
            }

            self.chattingView.isHidden = false
         }) { [weak self] (completion) in
            if #available(iOS 10.0, *) {
                self?.lockAnimationView.animation = Animation.named("unlock")
                self?.lockAnimationView.play()
            }
            else {
                self?.lockButton.setImage(#imageLiteral(resourceName: "lockOpen18"), for: .normal)
            }
            self?.lockButton.accessibilityLabel = "Lock screen".localized()
        }
    }
    
    func isExistBookmark(isExist: Bool) {
        NLog("isExistBookmark")
        bookmarkIndexExistView.isHidden = !isExist
    }
    
    
    func removeBookmarkViewFromSuperView(isRemove: Bool) {
        isRemoveBookmark = isRemove
        NLog("isRemoveBookmark : \(isRemoveBookmark)")
        if isRemoveBookmark {
            bookmarkView.isHidden = true
            bookmarkView.removeFromSuperview()
        }
        else {
            if isLockMode == false {
                bookmarkView.isHidden = false
            }
            let position = isChattingMode ? 3 : 2
            stackView.insertArrangedSubview(bookmarkView, at: position)
        }
    }
    
    func updateChattingView(isHidden: Bool) {
        NLog("updateChattingView")
        if isHiddenChattingView {
            chattingImageView.image = #imageLiteral(resourceName: "chatOff18.png")
            chattingButton.accessibilityLabel = "Hide chat".localized()
        }
        else {
            chattingImageView.image = #imageLiteral(resourceName: "chat18.png")
            chattingButton.accessibilityLabel = "View chat".localized()
        }
    }
    
    
    //MARK: - Event
    @objc func onLockTouched(_ sender: UIButton) {
        NLog("onLockTouched")
        isLockMode = !isLockMode
        delegate?.bottomStackLockTouched(view: self, isLocked: isLockMode)
    }
    
    
    @IBAction func onChattingTouched(_ sender: UIButton) {
        NLog("onChattingTouched")
        isHiddenChattingView = !isHiddenChattingView
        delegate?.bottomStackChattingTouched(view: self, isHidden: isHiddenChattingView)
    }
    
    @IBAction func onRepeatTouched(_ sender: UIButton) {
        NLog("onRepeatTouched")
        delegate?.bottomStackRepeatTouched(view: self)
    }
    
    @IBAction func onBookmarkTouched(_ sender: UIButton) {
        NLog("onBookmarkTouched")
        delegate?.bottomStackBookmarkTouched(view: self)
    }
    
    @IBAction func onPlaySpeedTouched(_ sender: UIButton) {
        NLog("onPlaySpeedTouched")
        delegate?.bottomStackPlaySpeedTouched(view: self)
    }
    
    @objc func onPlaySpeedLongPressed() {
        NLog("onPlaySpeedLongPressed")
        delegate?.bottomStackPlaySpeedLongPressed(view: self)
    }
}

