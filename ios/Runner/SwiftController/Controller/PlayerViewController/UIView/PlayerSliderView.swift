//
//  PlayerSliderView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/28.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol PlayerSliderViewDelegate : class {
    func sliderTouchBegan(slider: PlayerSliderView, thumbXPoint: CGFloat)
    func sliderValueChanged(slider: PlayerSliderView, thumbXPoint: CGFloat)
    func sliderTouchEnd(slider: PlayerSliderView)
    
}

class PlayerSliderView: UISlider {
    
    // MARK: - variable
    var totalDuration: Double = 0
    var repeatStartValue: CGFloat = 0
    var repeatEndValue: CGFloat = 0
    
    weak var delegate: PlayerSliderViewDelegate?
    
    public var progress: Float = 0 {
        didSet {
            self.setProgress(progress, animated: false)
        }
    }
    
    var currentDuration: Double {
        get {
            return totalDuration * Double(self.value)
        }
        
        set {
            let cd = (newValue) / totalDuration
            self.value = Float(cd)
            self.progressView.setProgress(Float(cd), animated: false)
        }
    }
    
    var thumbCenterX: CGFloat {
        return thumbBounds.midX
    }
    
    var bookmarkIndicators: [UIView] = []
    private var progressView = UIProgressView()
    
    //MARK: - Repeat View
    
    private var repeatStartView = SliderRepeatView()
    
    private var repeatEndView = SliderRepeatView()
    
    
    lazy var unSeekableView: UIView = {
        var view = UIView()
        view.backgroundColor = UIColor(hexFromString: "#343a40")
        return view
    }()
    
    var isLiveMode = false {
        didSet {
            if isLiveMode {
                setLiveMode()
            }            
        }
    }
    
    var isSeekable: Bool = true
    var seekableEnd: Int = 0 {
        didSet {
            updateSeekableRange()
        }
    }
    
    // MARK: - function
    
    public override init(frame: CGRect) {
        super.init(frame: frame)
        configureSlider()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        configureSlider()
    }
    
    private func configureSlider() {
        self.insertSubview(progressView, at: 0)
        self.setVODPlayMode()
        
        progressView.snp.makeConstraints { (make) in
            make.leading.equalTo(1)
            make.trailing.equalTo(-1)
            make.center.equalToSuperview().priorityHigh()
        }
        //        let thumbImage = #imageLiteral(resourceName: "thumb")
        //        setThumbImage(thumbImage, for: .normal)
        //        setThumbImage(thumbImage, for: .highlighted)
        
        
        
        progressView.clipsToBounds = true
        self.addTarget(self, action: #selector(onSliderValueChanged(slider:event:)), for: .valueChanged)
    }
    
    func setProgress(_ progress: Float, animated: Bool) {
        DispatchQueue.main.async { [weak self] in
            self?.value = progress
            self?.progressView.setProgress(progress, animated: animated)
        }
    }
    
    
    
    //     MARK: - UI
    private func setLiveMode() {
        //        progressView.tintColor = .red
        progressView.progressTintColor = .clear
        progressView.trackTintColor = UIColor(hexFromString: "#fff", alpha: 0.2)
        self.thumbTintColor = .clear
        self.isUserInteractionEnabled = false
    }
    
    private func setVODPlayMode() {
        progressView.progressTintColor = .white
        progressView.trackTintColor = UIColor(hexFromString: "#fff", alpha: 0.2)
        progressView.backgroundColor = .clear
    }
    
    private func updateSeekableRange() {
        if isSeekable { return }
        if seekableEnd <= 0 {
//            self.setLiveMode()
        }
        else {
            let seekableEndValue = ( CGFloat(seekableEnd) / CGFloat(totalDuration) ) * progressView.frame.size.width
            unSeekableView.frame = CGRect(x: seekableEndValue, y: 0, width: progressView.frame.width - seekableEndValue, height: progressView.frame.size.height)
            progressView.insertSubview(unSeekableView, aboveSubview: progressView)
        }
    }
    
    // MARK: - Event
    
    @objc func onSliderValueChanged(slider: UISlider, event: UIEvent) {
        guard let controlView = self.superview?.superview?.superview as? VideoControlView, controlView.isDisplayControl == true else { return }
        
        
        if let touchEvent = event.allTouches?.first {
            switch touchEvent.phase {
            case .began:
                delegate?.sliderTouchBegan(slider: self, thumbXPoint: thumbCenterX)
            case .moved:
                delegate?.sliderValueChanged(slider: self, thumbXPoint: thumbCenterX)
            case .ended:
                setProgress(slider.value, animated: false)
                delegate?.sliderTouchEnd(slider: self)
            default:
                setProgress(slider.value, animated: false)
                delegate?.sliderTouchEnd(slider: self)
                break
            }
        }
    }
    
    func updateRepeatViewFrame() {
        let startCenter = repeatStartValue * progressView.frame.size.width
        repeatStartView.frame = CGRect(x: 0, y: 0, width: startCenter, height: progressView.frame.height)
        repeatStartView.status = .start
        
        let endCenter = repeatEndValue * progressView.frame.size.width
        repeatEndView.frame = CGRect(x: endCenter, y: 0, width: progressView.frame.width - endCenter, height: progressView.frame.size.height)
        repeatEndView.status = .end
        
    }
    
    func setRepeatStartPoint() {
        repeatStartValue = CGFloat(self.value)
        
        let center = CGFloat(self.value) * progressView.frame.size.width
        repeatStartView.frame = CGRect(x: center, y: 0, width: 2, height: progressView.frame.size.height)
        progressView.insertSubview(repeatStartView, aboveSubview: progressView)
        repeatStartView.status = .start
        
        progressView.progressTintColor = .clear
        
    }
    
    func setRepeatEndPoint() {
        repeatEndValue = CGFloat(self.value)
        
        if repeatStartValue > repeatEndValue {
            let temp = repeatStartValue
            repeatStartValue = repeatEndValue
            repeatEndValue = temp
        }
        
        let startCenter = repeatStartValue * progressView.frame.size.width
        repeatStartView.frame = CGRect(x: 0, y: 0, width: startCenter, height: progressView.frame.height)
        repeatStartView.status = .start
        
        let endCenter = repeatEndValue * progressView.frame.size.width
        repeatEndView.frame = CGRect(x: endCenter, y: 0, width: progressView.frame.width - endCenter, height: progressView.frame.size.height)
        progressView.insertSubview(repeatEndView, aboveSubview: progressView)
        repeatEndView.status = .end
        
        progressView.progressTintColor = .white
        progressView.trackTintColor = UIColor(hexFromString: "#fff", alpha: 0.7)
    }
    
    
    func removeRepeatPoint() {
        repeatStartView.removeFromSuperview()
        repeatEndView.removeFromSuperview()
        setVODPlayMode()
    }
    
    //    private func setRepeatModeUI() {
    //        guard let controlView = self.superview as? VideoControlView else { return }
    //        let repeatMode = controlView.repeatMode
    //
    //        switch repeatMode {
    //        case .start:
    //            progressView.progressTintColor = .red
    //            progressView.trackTintColor = .red
    //
    //        case .end:
    //            let center = repeatStartValue * progressView.frame.size.width
    //            repeatStartView.frame = CGRect(x: 0, y: 0, width: center, height: progressView.frame.height)
    //
    //            progressView.progressTintColor = .clear
    //            progressView.trackTintColor = .clear
    //        case .none:
    //            break
    ////            setVODPlayMode()
    //        }
    //
    //
    //    }
    
    func addBookmarkIndicator() {
        let center = CGFloat(progressView.progress) * progressView.frame.size.width
        let bookmarkView = UIView(frame: CGRect(x: center, y: progressView.frame.origin.y, width: 2, height: progressView.frame.size.height))
        bookmarkView.backgroundColor = .orange
        self.insertSubview(bookmarkView, aboveSubview: progressView)
        self.bookmarkIndicators.append(bookmarkView)
    }
}


extension PlayerSliderView: UIGestureRecognizerDelegate {
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailBy otherGestureRecognizer: UIGestureRecognizer) -> Bool {

        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRequireFailureOf otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return false
    }
}

