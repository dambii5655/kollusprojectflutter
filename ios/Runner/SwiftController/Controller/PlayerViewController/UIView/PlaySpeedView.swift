//
//  PlaySpeedView.swift
//  Kollus Player
//
//  Created by 이기완 on 2021/04/27.
//  Copyright © 2021 kiwan. All rights reserved.
//

import Foundation


protocol PlaySpeedViewDelegate: class {
    func onPlaySpeedSliderIndexChanged(view: PlaySpeedView, playSpeed: PlaySpeed)
    func onPlaySpeedSliderClosed(View: PlaySpeedView)
    
}


class PlaySpeedView: UIView {
    weak var delegate: PlaySpeedViewDelegate?
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var stepSlider: StepSlider!
    @IBOutlet weak var closeSpeedSetting: UIButton!
    
    
    var selectedPlaySpeed: PlaySpeed = .x1 {
        didSet {
            updateSpeedLabel()
        }
    }
    
    var playSpeed: [PlaySpeed] = []
    
    var isWatermark = false
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("PlaySpeedView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    private func setUI() {
        stepSlider.tintColor = .white
        stepSlider.trackCircleRadius = 2
        stepSlider.stepSliderType = SPEEDRATE_TYPE
        stepSlider.addSpeedRateLabel()
        
        closeSpeedSetting.accessibilityLabel = "Close playback speed".localized()
    }
    
    private func setEvent() {
        stepSlider.enableHapticFeedback = true
        stepSlider.delegate = self
    }
    
    private func updateSpeedLabel() {
        stepSlider.maxCount = UInt(playSpeed.count)
        stepSlider.labels = playSpeed.enumerated().map({
            
            if $1 == selectedPlaySpeed {
                stepSlider.setIndex(UInt($0), animated: false)
                
                if $1 == .x1 {
                    let attributedString = NSMutableAttributedString(string: $1.toString, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)])
                    
                    let attributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11)]
                    attributedString.addAttributes(attributes, range: ($1.toString as NSString).range(of: "기본"))
                    
                    
                    return attributedString
                }
                else {
                    return NSMutableAttributedString(string: $1.toString, attributes: [NSAttributedString.Key.font : UIFont.boldSystemFont(ofSize: 13)])
                }
            }
            else {
                if $1 == .x1 {
                    let attributedString = NSMutableAttributedString(string: $1.toString, attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#ffffff", alpha: 0.7)])
                    
                    let attributes = [NSAttributedString.Key.foregroundColor : UIColor.white, NSAttributedString.Key.font : UIFont.systemFont(ofSize: 11)]
                    attributedString.addAttributes(attributes, range: ($1.toString as NSString).range(of: "기본"))
                    
                    return attributedString
                    
                }
                else {
                    return NSAttributedString(string: $1.toString, attributes: [NSAttributedString.Key.foregroundColor: UIColor(hexFromString: "#ffffff", alpha: 0.7)])
                }
                
            }
        })
        
        stepSlider.adjustLabel = true
    }
    
    //MARK: - Event
    
    @IBAction func onCloseTouched(_ sender: UIButton?) {
        NLog("onCloseTouched")
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.alpha = 0
            self?.layoutIfNeeded()
        } completion: { [weak self] (completion) in
            self?.isHidden = true
            self?.delegate?.onPlaySpeedSliderClosed(View: self!)
        }
        
    }
}

extension PlaySpeedView: StepSliderDelegate {
    func onIndexChanged(_ view: StepSlider!, index: Int) {
        NLog("onIndexChanged")
        if isWatermark {
            if let speed = PlaySpeed(rawValue: index + 2) {
                selectedPlaySpeed = speed
                delegate?.onPlaySpeedSliderIndexChanged(view: self, playSpeed: speed)
            }
        }
        else {
            if let speed = PlaySpeed(rawValue: index) {
                selectedPlaySpeed = speed
                delegate?.onPlaySpeedSliderIndexChanged(view: self, playSpeed: speed)
            }
            
        }
        
        
    }
}
