//
//  SliderRepeatView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/28.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class SliderRepeatView: UIView {
    
    enum Status {
        case start
        case end
    }
    
    var status: Status = .start {
        didSet {
            updateUI()
        }
    }
    
    
    let dotView = UIView()
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        
    }
    

    private func setUI() {
//        self.backgroundColor = UIColor(hexFromString: "#fff", alpha: 0.8)
        self.backgroundColor = UIColor(hexFromString: "#343a40", alpha: 0.8)
        
        dotView.frame = CGRect(x: 0, y: 0, width: 2, height: self.frame.size.height)
        dotView.backgroundColor = .white
        self.addSubview(dotView)
    }
    
    func updateUI() {
        switch status {
        case .start:
            dotView.frame = CGRect(x: self.frame.width - 2, y: 0, width: 2, height: self.frame.size.height)
            backgroundColor = UIColor.darkGray
            
        case .end:
            dotView.frame = CGRect(x: 0, y: 0, width: 2, height: self.frame.size.height)
            backgroundColor = UIColor.darkGray
        }
        
//        startView.frame = CGRect(x: 0, y: 0, width: 2, height: self.frame.size.height)
//        endView.frame = CGRect(x: self.frame.size.width - 2, y: 0, width: 2, height: self.frame.size.height)
    }
    
//    override func draw(_ rect: CGRect) {
//        super.draw(rect)
//        print("fff")
//
//    }
    
    private func setEvent() {
    }
    
}
