//
//  VideoSliderThumbnailView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/19.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

class VideoSliderThumbnailView: UIView {
    
    var isShown: Bool = false {
        didSet {
            self.setShown(isShown)
        }
    }
    
    var thumbnailImageView = UIImageView()
    
    var stackView = UIStackView()
    var currentTimeLabel = ShadowLabel()
    
    var addedTimeLabel = ShadowLabel()
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        addSubview(thumbnailImageView)
        addSubview(stackView)
        
        thumbnailImageView.snp.makeConstraints { (maker) in
            maker.top.equalToSuperview()
            maker.trailing.equalToSuperview()
            maker.leading.equalToSuperview()
            maker.bottom.equalToSuperview()
        }
        
        stackView.addArrangedSubview(currentTimeLabel)
        stackView.addArrangedSubview(addedTimeLabel)
        
        
        stackView.snp.makeConstraints { (maker) in
            maker.centerX.centerY.equalToSuperview()
        }
    }
    
    private func setUI() {
        self.backgroundColor = .black
        
        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.white.cgColor
        
        thumbnailImageView.backgroundColor = .black
        thumbnailImageView.contentMode = .scaleAspectFill
        thumbnailImageView.layer.masksToBounds = true
        
        
        stackView.alignment = .center
        stackView.axis = .vertical
        stackView.spacing = 5
        
        
        currentTimeLabel.textColor = .white
        currentTimeLabel.font = UIFont.systemFont(ofSize: 25, weight: .bold)
        currentTimeLabel.text = "1:23:19"
        currentTimeLabel.adjustsFontSizeToFitWidth = true
        
        addedTimeLabel.textColor = .white
        addedTimeLabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        addedTimeLabel.text = "+1:02"
        addedTimeLabel.adjustsFontSizeToFitWidth = true
    }
    
    private func setEvent() {
        
    }
    
    private func setShown(_ shown: Bool) {
        shown ? show() : hide()
    }
    
    private func show() {
        guard let controlView = self.superview?.superview as? VideoControlView else { return }
        controlView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15) {
            self.alpha = 1
            controlView.layoutIfNeeded()
        }
    }
    
    private func hide() {
        guard let controlView = self.superview?.superview as? VideoControlView else { return }
        controlView.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.15) {
            self.alpha = 0
            controlView.layoutIfNeeded()
        }
    }
    
    
    func setThumbnail(image: UIImage? = nil, currentTimeText: String, addedTime: Int) {
        if image != nil {
            thumbnailImageView.image = image
            thumbnailImageView.isHidden = false
        }
        else {
            thumbnailImageView.isHidden = true
        }
        
        
        currentTimeLabel.text = currentTimeText
        
        var absoluteTime = addedTime
        
        if absoluteTime < 0 {
            absoluteTime = -absoluteTime
        }
                
        let text = KollusUtil.durationToString(duration: TimeInterval(absoluteTime))
        
        
        if addedTime > 0 {
            addedTimeLabel.text = "+\(text)"
        }
        else if addedTime == 0 {
            addedTimeLabel.text = "\(text)"
        }
        else {
            addedTimeLabel.text = "-\(text)"
        }
    }
    

}

