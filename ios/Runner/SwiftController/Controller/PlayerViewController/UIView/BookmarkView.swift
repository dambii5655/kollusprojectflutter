//
//  BookmarkView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/09.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit


//protocol BookmarkViewDelegate: class {
//    func onAddBookmarkTouched(view: BookmarkView)
//    func onDeleteBookmarkTouched(view: BookmarkView)
//    func onModifyBookmarkTouched(view: BookmarkView)
//}



class BookmarkView: UIView {
    //    weak var delegate: BookmarkViewDelegate?
    
    enum State {
        case enable
        case unable
        case networkError
    }
    
    
    enum Mode {
        case normal
        case delete
        case modify
    }
    
    var data: [KollusBookmark] = [] {
        didSet {
            createCollectionViewData()
        }
    }
    
    var state: State = .networkError

    var selectedItemIndexPath: IndexPath?
    
    var collectionViewData: [Bookmark] = []
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var layoutButton: UIButton!
    @IBOutlet weak var addView: UIView!
    @IBOutlet weak var deleteView: UIView!
    @IBOutlet weak var modifyView: UIView!
    @IBOutlet weak var cancelView: UIView!
    @IBOutlet weak var completeView: UIView!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var bookmarkEditStackView: UIStackView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    var mode: Mode = .normal {
        didSet {
            updateCollectionView()
        }
    }
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setEvent()
        layoutButton.isHidden = true
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("BookmarkView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(view)
    }
    
    private func setEvent() {
        collectionView.register(UINib(nibName: "BookmarkCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BookmarkCell")
    }
    
    func createCollectionViewData() {
        NLog("createCollectionViewData +++")
        guard let viewController = self.parentViewController as? PlayerViewController else { return }
        self.collectionViewData.removeAll()
        
        bookmarkEditStackView.isHidden = !viewController.playerView.bookmarkModifyEnabled
        
        for bookmark in data {
            viewController.getThumnailImage(position: bookmark.position) { [weak self] (image) in
                guard let self = self else { return }

                let bookmark = Bookmark(isSelected: false, position: bookmark.position, thumnailImage: image, time: NSDate(), title: bookmark.title ?? "", value: bookmark.value, kind: bookmark.kind)
                self.collectionViewData.append(bookmark)

                if self.data.count == self.collectionViewData.count {
                    self.sortCollectionViewData()
                    self.collectionView.reloadData()
                }
            }
        }
        NLog("createCollectionViewData ---")
    }
    
    func sortCollectionViewData() {
        collectionViewData.sort {
            return $0.position < $1.position
        }
    }
    
    func updateCollectionView() {
        NLog("updateCollectionView +++")
        switch mode {
        case .normal:
            titleLabel.text = "Bookmark".localized()
            addView.isHidden = false
            modifyView.isHidden = false
            cancelView.isHidden = true
            deleteView.isHidden = false
            completeView.isHidden = true
            deleteButton.isEnabled = true
        case .delete:
            titleLabel.text = "Delete bookmark".localized()
            addView.isHidden = true
            modifyView.isHidden = true
            cancelView.isHidden = false
            deleteView.isHidden = false
            completeView.isHidden = true
            checkEnableDeleteButton()
        case .modify:
            titleLabel.text = "Modify bookmark".localized()
            addView.isHidden = true
            modifyView.isHidden = true
            cancelView.isHidden = true
            deleteView.isHidden = true
            completeView.isHidden = false
        }
        
        collectionView.reloadData()
        NLog("updateCollectionView ---")
    }
    
    func checkEnableDeleteButton() {
        if mode == .normal {
            deleteButton.isEnabled = true
        }
        else if mode == .delete {
            for data in collectionViewData {
                if data.isSelected {
                    deleteButton.isEnabled = true
                    return
                }
            }
            deleteButton.isEnabled = false
        }
    }
    
    func isExistBookmark(position: TimeInterval) -> Bool {
        
        for i in 0 ..< collectionViewData.count {
            if Int(collectionViewData[i].position) == Int(position) {
                self.collectionView.scrollToItem(at: IndexPath(row: i, section: 0), at: .centeredHorizontally, animated: true)
                self.makeToast(String(format: "The bookmark is already at %@.".localized() , KollusUtil.durationToString(duration: position)))                
                return true
            }
        }
        
        return false
    }
    
    // MARK: - Event
    
    @IBAction func onAddTouched(_ sender: UIButton) {
        NLog("onAddTouched +++")
        guard let viewController = self.parentViewController as? PlayerViewController else { return }
        guard let playerView = viewController.playerView else { return }
        if isExistBookmark(position: playerView.currentPlaybackTime) { return }
        
        do {
            try playerView.addBookmark(playerView.currentPlaybackTime, value: "")
            
            viewController.getThumnailImage(position: playerView.currentPlaybackTime) { [unowned self] (image) in
                let bookmark = Bookmark(isSelected: false, position: playerView.currentPlaybackTime, thumnailImage: image, time: Date().toLocalTime() as NSDate, title: "", value: "", kind: .user)
                
                let copiedData = self.collectionViewData
                
                self.collectionViewData.append(bookmark)
                self.sortCollectionViewData()
                
                for i in 0 ..< self.collectionViewData.count {
                    if i == copiedData.count {
                        self.collectionView.insertItems(at: [IndexPath(row: i, section: 0)])
                        self.collectionView.scrollToItem(at: IndexPath(row: i, section: 0), at: .centeredHorizontally, animated: true)
                        break
                    }
                    if copiedData[i].position != self.collectionViewData[i].position {
                        self.collectionView.insertItems(at: [IndexPath(row: i, section: 0)])
                        self.collectionView.scrollToItem(at: IndexPath(row: i, section: 0), at: .centeredHorizontally, animated: true)
                        break
                    }
                }
                let position = KollusUtil.durationToString(duration: bookmark.position)
                self.makeToast(String(format: "The %@ bookmark has been added".localized(), position))
                
                checkEnableDeleteButton()
            }
        }
        catch {
            print(error.localizedDescription)
        }
        NLog("onAddTouched ---")
    }
    
    @IBAction func onDeleteTouched(_ sender: UIButton) {
        if mode == .normal {
            mode = .delete
            checkEnableDeleteButton()
        }
        else {
            guard let viewController = self.parentViewController as? PlayerViewController else { return }
            
            mode = .normal
            for cell in collectionView.visibleCells {
                (cell as! BookmarkCollectionViewCell).mode = mode
            }
            
            var deleteIndexPaths: [IndexPath] = []
            
            for i in 0 ..< collectionViewData.count  {
                if collectionViewData[i].isSelected {
                    let indexPath = IndexPath(row: i, section: 0)
                    deleteIndexPaths.append(indexPath)
                }
            }
            
            deleteIndexPaths.sort(by: >)
            
            collectionView.performBatchUpdates({ [unowned self] in
                do {
                    for indexPath in deleteIndexPaths {
                        try viewController.playerView.removeBookmark(collectionViewData[indexPath.row].position)
                        self.collectionViewData.remove(at: indexPath.row)
                    }
                    self.collectionView.deleteItems(at: deleteIndexPaths)
                }
                catch {
                    print(error.localizedDescription)
                }
                
                self.checkEnableDeleteButton()
                
            }) { (completion) in
                
            }
        }
    }
    
    @IBAction func onCancelTouched(_ sender: UIButton) {
        mode = .normal
        
        for i in 0 ..< collectionViewData.count {
            collectionViewData[i].isSelected = false
        }
        
    }
    
    @IBAction func onModifyTouched(_ sender: UIButton) {
        mode = .modify
    }
    
    @IBAction func onCompleteTouched(_ sender: UIButton) {
        mode = .normal
    }
    
}


extension BookmarkView: UICollectionViewDataSource, UICollectionViewDelegate, BookmarkCollectionViewCellDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let thumbnailHeight = collectionView.frame.size.height - 39
        let width = thumbnailHeight / 9 * 16
        return CGSize(width: width, height: collectionView.frame.size.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = collectionViewData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "BookmarkCell", for: indexPath) as! BookmarkCollectionViewCell
        
        cell.mode = mode
        cell.bookmarkData = data
        cell.delegate = self
        return cell
    }
    
    func onCollectionViewCellTouched(view: BookmarkCollectionViewCell) {
        guard let indexPath = collectionView.indexPath(for: view) else { return }
                
        switch mode {
        case .normal:
            guard let viewController = self.parentViewController as? PlayerViewController else { return }
            
            var position = collectionViewData[indexPath.row].position
            if viewController.controlView.repeatMode == .end {
                if position < viewController.repeatStartPoint || position > viewController.repeatEndPoint {
                    viewController.controlView.bottomStackRepeatTouched(view: viewController.controlView.bottomButtonsStackView)
                }
            }

            viewController.playerView.currentPlaybackTime = collectionViewData[indexPath.row].position
            
        case .delete:
            if collectionViewData[indexPath.row].kind == .index {
                self.makeToast("This is where the content provider has bookmarked.\nIt cannot be deleted.".localized())
            }
            else {
                collectionViewData[indexPath.row].isSelected = !collectionViewData[indexPath.row].isSelected
                collectionView.reloadItems(at: [indexPath])
                checkEnableDeleteButton()
            }
        case .modify:
            if collectionViewData[indexPath.row].kind == .index {
                self.makeToast("This is where the content provider has bookmarked.\nIt cannot be modified.".localized())
            }
            else {
                guard let viewController = self.parentViewController as? PlayerViewController else { return }
                let storyBoard = UIStoryboard(name: "TextFieldInputPopupViewController", bundle: nil)
                let textFieldInputPopupViewController = storyBoard.instantiateInitialViewController() as! TextFieldInputPopupViewController
                textFieldInputPopupViewController.delegate = self
                viewController.present(textFieldInputPopupViewController, animated: false, completion: nil)
                textFieldInputPopupViewController.type = .bookmarkChangeTitle
                textFieldInputPopupViewController.textField.text = collectionViewData[indexPath.row].value
                selectedItemIndexPath = indexPath
            }
        }
    }
}

extension BookmarkView: TextFieldInputPopupViewDelegate {
    func onCreateFolderTouched(viewController: TextFieldInputPopupViewController, name: String) {
        guard let parentVC = self.parentViewController as? PlayerViewController else { return }
        do {
            try parentVC.playerView.addBookmark(collectionViewData[selectedItemIndexPath!.row].position, value: name)
            collectionViewData[selectedItemIndexPath!.row].value = name
            
            viewController.dismiss(animated: false) { [weak self] in
                self?.mode = .normal
            }
            self.mode = .normal
        }
        catch {
            print(error.localizedDescription)
        }
    }
}
