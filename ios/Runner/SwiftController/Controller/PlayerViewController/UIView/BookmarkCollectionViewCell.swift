//
//  BookmarkCollectionViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/09.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit


protocol BookmarkCollectionViewCellDelegate: class {
    func onCollectionViewCellTouched(view: BookmarkCollectionViewCell)
}


class BookmarkCollectionViewCell: UICollectionViewCell {
    weak var delegate: BookmarkCollectionViewCellDelegate?
    
    var mode: BookmarkView.Mode = .normal {
        didSet {
            updateCollectionViewCell()
        }
    }
    
    var bookmarkData: Bookmark? = nil {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var gradationImageView: UIImageView!
    @IBOutlet weak var alphaView: UIView!
    @IBOutlet weak var dotView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var selectImageView: UIImageView!
    @IBOutlet weak var modifyImageView: UIImageView!
    @IBOutlet weak var bookmarkButton: UIButton!
    @IBOutlet weak var timeLabel: ShadowLabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        gradationImageView.layer.cornerRadius = 4
        gradationImageView.layer.masksToBounds = true
    }
    
    
    func updateCollectionViewCell() {
        switch mode {
        case .normal:
            selectImageView.isHidden = true
            modifyImageView.isHidden = true
        case .delete:
            selectImageView.isHidden = false
            modifyImageView.isHidden = true
        case .modify:
            selectImageView.isHidden = true
            modifyImageView.isHidden = false
        }
    }

    func updateUI() {
        guard let data = bookmarkData else { return }
        alphaView.isHidden = true
        bookmarkButton.accessibilityLabel = "Bookmark".localized()
        imageView.image = data.thumnailImage
        titleLabel.text = data.value
        timeLabel.text = KollusUtil.durationToString(duration: data.position)
        timeLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: data.position)
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.onCollectionViewButton))
        timeLabel.isUserInteractionEnabled = true
        timeLabel.addGestureRecognizer(tap)
        dotView.isHidden = data.kind == .user
        
        if data.kind == .user {
            selectImageView.image = data.isSelected ? #imageLiteral(resourceName: "icChecked24") : #imageLiteral(resourceName: "unchecked24")
            alphaView.isHidden = !data.isSelected
        }
        else {
            selectImageView.image = #imageLiteral(resourceName: "uncheckedDisabled24")
        }
        
        if data.kind == .user {
            modifyImageView.image = #imageLiteral(resourceName: "edit24")
        }
        else {
            modifyImageView.image = #imageLiteral(resourceName: "disEdit2")
        }
        
    }
    
    @IBAction func onCollectionViewCellTouched(_ sender: UIButton) {
        delegate?.onCollectionViewCellTouched(view: self)
    }
        
    @objc func onCollectionViewButton(sender:UITapGestureRecognizer){
        delegate?.onCollectionViewCellTouched(view: self)
    }
}
