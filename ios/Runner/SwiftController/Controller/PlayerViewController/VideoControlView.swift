//
//  VideoControlView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/28.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit
// import MarqueeLabel

protocol VideoControlViewDelegate: class {
    func onVideoCloseTouched(view: VideoControlView?)
    func onVideoPlayAndPauseTouched(view: VideoControlView)
    func onVideoSeekerTouched(view: VideoControlView, direction: SeekerButton.Direction, isContinues: Bool, intervalTime: Double)
    
    
    func onTimeSliderTouchBegan(slider: PlayerSliderView)
    func onTimeSliderValueChanged(slider: PlayerSliderView)
    func onTimeSliderTouchEnd(slider: PlayerSliderView)
    
    func onVideoSliderTouchBegan(view: VideoControlView)
    func onVideoSliderValueChanged(view: VideoControlView, deltaX: CGFloat)
    func onVideoSliderTouchEnd(view: VideoControlView, deltaX: CGFloat)
    
    func onVideoPinchGestured(view: VideoControlView, recognizer: UIPinchGestureRecognizer)
    
    func onBottomStackRepeatTouched(view: VideoControlView, repeatMode: VideoControlView.RepeatMode)
    func onBottomStackMoreTouched(view: VideoControlView)
    func onBottomStackPlaySpeedTouched(view: VideoControlView)
    func onBottomStackPlaySpeedLongPressed(view: VideoControlView)
    
    func onPreviousVideoTouched(view: VideoControlView)
    func onReplayVideoTouched(view: VideoControlView)
    func onNextVideoTouched(view: VideoControlView)
    
}


class VideoControlView: PassthroughView {
    
    enum RepeatMode {
        case none
        case start
        case end
    }

    var repeatMode: RepeatMode = .none

    enum ScreenMode {
        case aspectFit
        case scaleToFill
        case scaletoFit
    }
    
    var screenMode: ScreenMode = .aspectFit

    weak var delegate: VideoControlViewDelegate?
    
    var isLocked: Bool = false
    
    var isDisplayBookmark: Bool = false {
        didSet {
            updateBookmarkPlayButton()
        }
    }
    
    var isAspectFit: Bool {
        if playerView.isZoomedIn {
            return false
        }
        else {
            return true
        }
    }

    #if false
    var isAspectFit: Bool {
        let parentViewController = self.parentViewController as! PlayerViewController
        if parentViewController.containerView.transform.a < 1.01  && parentViewController.containerView.transform.a > 0.99 {
            return true
        }
        
        return false
    }
    #endif

    var playerView: KollusPlayerView {
        let viewController = self.parentViewController as! PlayerViewController
        return viewController.playerView
    }
    
    var playerViewController: PlayerViewController {
        let viewController = self.parentViewController as! PlayerViewController
        return viewController
    }
    
    var isDisplayControl: Bool = true
    var controlViewTimer: Timer = Timer()
    var playSliderViewTimer: Timer = Timer()

    
    @IBOutlet var view: UIView!
//    @IBOutlet weak var titleLabel: MarqueeLabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var brightnessProgressView: VerticalProgressView!
    @IBOutlet weak var soundProgressView: VerticalProgressView!
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var topViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var screenButton: UIButton!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var centerView: UIStackView!
    @IBOutlet weak var indicatorView: UIActivityIndicatorView!
    @IBOutlet weak var playAndPauseButton: UIButton!
    
    @IBOutlet weak var bottomViewBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomView: UIView!
    
    @IBOutlet weak var timeStackView: UIStackView!
    @IBOutlet weak var currentDurationLabel: UILabel!
    @IBOutlet weak var totalDurationLabel: UILabel!
    
    @IBOutlet weak var liveStackView: UIStackView!
    
    @IBOutlet weak var bottomButtonsStackView: BottomButtonsStackView!
    
    
    @IBOutlet weak var videoSliderThumbnailView: VideoSliderThumbnailView!
    @IBOutlet weak var videoSliderYConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoSliderXConstraint: NSLayoutConstraint!    
    @IBOutlet weak var videoThumbnailWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var videoThumbnailAspectRatioConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var sliderView: PlayerSliderView!
    
    @IBOutlet weak var sliderThumbnailView: SliderThumbnailView!
    @IBOutlet weak var thumbnailLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var thumbnailBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var thumbnailWidthConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var backwardSeekButton: SeekerButton!
    @IBOutlet weak var forwardSeekButton: SeekerButton!
    @IBOutlet weak var controlUIButton: UIButton!
    
    @IBOutlet weak var bookmarkPlayButtonWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var bookmarkPlayAndPauseButton: UIButton!
    @IBOutlet weak var bookmarkCoverView: PassthroughView!
    @IBOutlet weak var bookmarkView: BookmarkView!
    @IBOutlet weak var bookmarkCloseButton: UIButton!
    
    @IBOutlet weak var bookmarkViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var playReachEndTimeView: UIView!
    @IBOutlet weak var previousVideoPlayButton: ShadowButton!
    @IBOutlet weak var nextVideoPlayButton: ShadowButton!
  
    @IBOutlet weak var nSecSkipView: NSecSkipView!
    @IBOutlet weak var chattingWebView: ChattingWebView!
        
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("VideoControlView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
        indicatorView.layer.shadowColor = UIColor.black.cgColor
        indicatorView.layer.shadowOffset = .zero
        indicatorView.layer.shadowOpacity = 0.4
        indicatorView.layer.shadowRadius = 3.0
        indicatorView.layer.masksToBounds = false
        
        
        brightnessProgressView.mode = .brightness
        brightnessProgressView.accessibilityElementsHidden = true
        soundProgressView.mode = .sound
        
        backwardSeekButton.directionMode = .backward
        forwardSeekButton.directionMode = .forward
        
        closeButton.accessibilityLabel = "Close".localized()
        screenButton.accessibilityLabel = "Full screen".localized()
        moreButton.accessibilityLabel = "More options".localized()
        backwardSeekButton.accessibilityLabel = "Rewind".localized()
        forwardSeekButton.accessibilityLabel = "Forward".localized()
        playAndPauseButton.accessibilityLabel = "Pause".localized()
        
        bookmarkCloseButton.accessibilityLabel = "Close bookmark".localized()
        controlUIButton.isHidden = true
        controlUIButton.accessibilityLabel = "Show controls".localized()
    }
    
    private func setEvent() {
        bottomButtonsStackView.delegate = self
        brightnessProgressView.delegate = self
        soundProgressView.delegate = self
        sliderView.delegate = self
        
        backwardSeekButton.delegate = self
        forwardSeekButton.delegate = self
        
        nSecSkipView.delegate = self
        chattingWebView.delegate = self
        bookmarkCoverView.touchDelegate = sliderView
        
        let singleTapGesture = UIShortTapGestureRecognizer(target: self, action: #selector(onVideoViewTouched(_:)))
        singleTapGesture.numberOfTapsRequired = 1
        singleTapGesture.delegate = self
        view.addGestureRecognizer(singleTapGesture)
        
        let doubleTapGesture = UITapGestureRecognizer(target: self, action: #selector(onVideoViewDoubleTapTouched(_:)))
        doubleTapGesture.numberOfTapsRequired = 2
        doubleTapGesture.delegate = self
        view.addGestureRecognizer(doubleTapGesture)
        
        singleTapGesture.require(toFail: doubleTapGesture)
        
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(onVideoViewPanGestured(_:)))
        panGesture.maximumNumberOfTouches = 1
        view.addGestureRecognizer(panGesture)
        
        let pinchGesture = UIPinchGestureRecognizer(target: self, action: #selector(onVideoViewPinchGestured(_:)))
        view.addGestureRecognizer(pinchGesture)
    }


    //MARK: - UpdateUI
    
    private func updateBookmarkPlayButton() {
        if isDisplayBookmark {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.bookmarkPlayButtonWidthConstraint.constant = 40
                self.layoutIfNeeded()
            }
        }
        else {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.bookmarkPlayButtonWidthConstraint.constant = 0
                self.layoutIfNeeded()
            }
        }
    }
    
    func updateLockModeUI(includeProgressView isIncluded: Bool = true) {
        topView.isHidden = isLocked
        centerView.isHidden = isLocked
        sliderView.isHidden = isLocked
        
        if bottomButtonsStackView.isLiveMode {
            liveStackView.isHidden = isLocked
        }
        else {
            timeStackView.isHidden = isLocked
        }
        
        if isIncluded {
            soundProgressView.isShown = !isLocked
            brightnessProgressView.isShown = !isLocked
        }
    }
    
    
    func updateLiveModeUI() {
        timeStackView.isHidden = true
        liveStackView.isHidden = false
        sliderView.isLiveMode = true
        bottomButtonsStackView.isLiveMode = true
        backwardSeekButton.isHidden = true
        forwardSeekButton.isHidden = true
    }
    
    func updateNoSeekModeUI() {
        bottomButtonsStackView.noSeekMode = true
        backwardSeekButton.isHidden = true
        forwardSeekButton.isHidden = true
    }
    
    func updateSeekModeUI() {
        bottomButtonsStackView.noSeekMode = false
        backwardSeekButton.isHidden = false
        forwardSeekButton.isHidden = false
    }
    
    func updateNSecSkipView(second: Int) {
        if second <= 0 {
            nSecSkipView.isHidden = true
        }
        else {
            nSecSkipView.isHidden = false
            nSecSkipView.setCountDownLabel(second: second)
        }
    }
    
    // MARK: - Public
    func removeGestureEvent() {
        if let recognizers = self.view.gestureRecognizers {
            for recognizer in recognizers {
                  self.view.removeGestureRecognizer(recognizer)
            }
        }
    }

    func setPlayAndPauseButtonImage(isPlay: Bool) {
        if isPlay {
            playAndPauseButton.setImage(#imageLiteral(resourceName: "pause32"), for: .normal)
            playAndPauseButton.accessibilityLabel = "Pause".localized()
            bookmarkPlayAndPauseButton.setImage(#imageLiteral(resourceName: "pause24"), for: .normal)
            bookmarkPlayAndPauseButton.accessibilityLabel = "Pause".localized()
        }
        else {
            playAndPauseButton.setImage(#imageLiteral(resourceName: "playArrow32"), for: .normal)
            playAndPauseButton.accessibilityLabel = "Play".localized()
            bookmarkPlayAndPauseButton.setImage(#imageLiteral(resourceName: "playArrow24"), for: .normal)
            bookmarkPlayAndPauseButton.accessibilityLabel = "Play".localized()
        }
    }

    func updateLivePlaySliderValue(currentTime: Double, totalDuration: Double) {
        var current = currentTime
        if current < 0 {
            current *= -1
        }
        self.currentDurationLabel.text = KollusUtil.durationToString(duration: totalDuration)
        self.currentDurationLabel.text = "-" + self.currentDurationLabel.text!
        self.currentDurationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: totalDuration)
        self.currentDurationLabel.accessibilityLabel = "-" + self.currentDurationLabel.accessibilityLabel!

        self.totalDurationLabel.text = KollusUtil.durationToString(duration: current)
        self.totalDurationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: current)
        if currentTime < 0 {
            self.totalDurationLabel.text = "-" + self.totalDurationLabel.text!
            self.totalDurationLabel.accessibilityLabel = "-" + self.totalDurationLabel.accessibilityLabel!
        }
    }

    func updateLivePlaySliderView(currentTime: Double, totalDuration: Double) {
        NLog("updateLivePlaySliderView currentTime : \(currentTime)")
        let timeSliderValue = (totalDuration + currentTime) / totalDuration
        self.sliderView.setProgress(Float(timeSliderValue), animated: false)
        
        var current = currentTime
        if current < 0 {
            current *= -1
        }
        self.currentDurationLabel.text = KollusUtil.durationToString(duration: totalDuration)
        self.currentDurationLabel.text = "-" + self.currentDurationLabel.text!
        self.currentDurationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: totalDuration)
        self.currentDurationLabel.accessibilityLabel = "-" + self.currentDurationLabel.accessibilityLabel!

        self.totalDurationLabel.text = KollusUtil.durationToString(duration: current)
        self.totalDurationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: current)
        if currentTime < 0 {
            self.totalDurationLabel.text = "-" + self.totalDurationLabel.text!
            self.totalDurationLabel.accessibilityLabel = "-" + self.totalDurationLabel.accessibilityLabel!
        }

        thumbnailLeadingConstraint.constant = sliderView.thumbCenterX
    }

    func updatePlaySliderView(currentTime: Double, totalDuration: Double) {
 //       NLog("updatePlaySliderView currentTime : \(currentTime)")
       let timeSliderValue = currentTime / totalDuration
        self.sliderView.setProgress(Float(timeSliderValue), animated: false)
        
        self.currentDurationLabel.text = KollusUtil.durationToString(duration: currentTime)
        self.currentDurationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: currentTime)

        self.totalDurationLabel.text = KollusUtil.durationToString(duration: totalDuration)
        self.totalDurationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: totalDuration)

        thumbnailLeadingConstraint.constant = sliderView.thumbCenterX
    }
    
    func updateSliderThumnailView(width: CGFloat, height: CGFloat) {
        if width == 0 || height == 0 { return }
        
        videoThumbnailAspectRatioConstraint = videoThumbnailAspectRatioConstraint.setMultiplier(multiplier: width / height)
        
        if width > height {
            sliderThumbnailView.maximumWidth = thumbnailPortraitViewWidth
            videoThumbnailWidthConstraint.constant = thumbnailPortraitViewWidth
        }
        else {
            sliderThumbnailView.maximumWidth = thumbnailLandScapeViewWidth
            videoThumbnailWidthConstraint.constant = thumbnailLandScapeViewWidth
        }
        
        sliderThumbnailView.updateConstraint(thumbnailWidth: width, thumbnailHeight: height)
    }
    
    func setIndicatorView(isBuffering: Bool) {        
        indicatorView.isHidden = !isBuffering
//        playAndPauseButton.isHidden = isBuffering
//        if(isDisplayControl) {
//            playAndPauseButton.alpha = isBuffering ? 0 : 1
//        }
    }
    
    func closeBookmarkView() {
        self.bottomButtonsStackView.isHidden = false
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            self.bookmarkViewBottomConstraint.constant = -220
            self.layoutIfNeeded()
        }) { [unowned self] (completion) in
            self.bookmarkCoverView.isHidden = true
            self.bookmarkView.mode = . normal
        }
        
        isDisplayBookmark = false
    }
    
    func displayPlayReachEndTimeView(willShow: Bool) {
        controlViewTimer.invalidate()
        
        titleLabel.isHidden = willShow
        bottomView.isHidden = willShow
        centerView.isHidden = willShow
        moreButton.isHidden = willShow
        
        topView.alpha = 1
        playReachEndTimeView.isHidden = !willShow
        
        self.bottomButtonsStackView.isHidden = false
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.bookmarkViewBottomConstraint.constant = -220
            self?.layoutIfNeeded()
        }) { [weak self] (completion) in
            self?.bookmarkCoverView.isHidden = true
            self?.bookmarkView.mode = . normal
        }
        
        isDisplayBookmark = false
        
        
        if willShow {
            soundProgressView.isShown = false
            brightnessProgressView.isShown = false
        }
        else {
            displayControlView(true)
        }
    }
    
    func updateReachToEndTimeView() {
        let viewController = self.parentViewController as! PlayerViewController

        let nextVideo = viewController.getNextVideoData()
        nextVideoPlayButton.isEnabled = nextVideo == nil ? false : true
        
        let previousVideo = viewController.getPreviousVideoData()
                
        if previousVideo == nil || previousVideo?.fileType == 1 {
            previousVideoPlayButton.isEnabled = false
        }
        else {
            previousVideoPlayButton.isEnabled = true
        }
        
    }
    
    // MARK: - Event
    
    @IBAction func onPlayAndPauseTouched(_ sender: UIButton) {
        delegate?.onVideoPlayAndPauseTouched(view: self)
        
//        if sender == playAndPauseButton {
//            resetControlHideTimer()
//        }
    }
    
    @objc func onVideoViewTouched(_ sender: UITapGestureRecognizer?) {
        NLog("onVideoViewTouched")
        isDisplayControl = !isDisplayControl
        NLog("onVideoViewTouched isDisplayControl :\(isDisplayControl)")
        displayControlView(isDisplayControl)
    }
    
    @objc func onVideoViewDoubleTapTouched(_ sender: UITapGestureRecognizer) {
        if isLocked || bottomButtonsStackView.isLiveMode { return }
        let location = sender.location(in: self)
        
        if location.x > bounds.width / 2 {
            executeDoubleTapTouched(direction: .forward)
        }
        else {
            executeDoubleTapTouched(direction: .backward)
        }        
    }
    
    @objc func hidePlaySliderView() {
        NLog("bottomView.alpha : \(bottomView.alpha)")
        NLog("playAndPauseButton.alpha : \(playAndPauseButton.alpha)")
        if(playAndPauseButton.isHidden == true){
            bottomView.isHidden = true
        }
        NLog("bottomView.alpha : \(bottomView.alpha)")
        timeStackView.alpha = 1
        bottomButtonsStackView.alpha = 1
        sliderView.thumbTintColor = .white
    }
    
    func executeDoubleTapTouched(direction: SeekerButton.Direction) {
        if !playerView.seekable { return }
        NLog("bottomView.alpha : \(bottomView.alpha)")
        
        if playSliderViewTimer.isValid {
            playSliderViewTimer.invalidate()
            playSliderViewTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(hidePlaySliderView), userInfo: nil, repeats: false)
        }
        else if bottomView.isHidden == true {
            bottomView.isHidden = false
            timeStackView.alpha = 0
            bottomButtonsStackView.alpha = 0
            sliderView.thumbTintColor = .clear
            playSliderViewTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(hidePlaySliderView), userInfo: nil, repeats: false)
        }
        switch direction {
        case .forward:
            NLog("executeDoubleTapTouched forward")
            if !isDisplayControl || (backgroundView.isHidden == true  && soundProgressView.isShown) || (backgroundView.isHidden == true && brightnessProgressView.isShown) {
                 centerView.isHidden = false
                playAndPauseButton.isHidden = true
                backwardSeekButton.isHidden = true
                forwardSeekButton.isHidden = false
                forwardSeekButton.resetDoubleTapTimer()
            }
            forwardSeekButton.onButtonTouched(forwardSeekButton.seekButton)
        case .backward:
            NLog("executeDoubleTapTouched backward")
            if !isDisplayControl || (backgroundView.isHidden == true  && soundProgressView.isShown) || (backgroundView.isHidden == true && brightnessProgressView.isShown) {
                centerView.isHidden = false
                playAndPauseButton.isHidden = true
                backwardSeekButton.isHidden = false
                forwardSeekButton.isHidden = true
                backwardSeekButton.resetDoubleTapTimer()
            }
            backwardSeekButton.onButtonTouched(backwardSeekButton.seekButton)
        }
    }
    
    
    var firstLocation = CGPoint()
    var scrollViewContentOffset: CGPoint = .zero
    
    @objc func onVideoViewPanGestured(_ sender: UIPanGestureRecognizer) {
        guard let viewController = self.parentViewController as? PlayerViewController else {
            NLog("onVideoViewPanGestured PlayerViewController nil")
            return
        }
        
        if viewController.playerView == nil {
            NLog("onVideoViewPanGestured viewController.playerView nil")
            return
        }

        if isLocked || !indicatorView.isHidden || !playerView.seekable { return }
       
//        let location = sender.location(in: sender.view)
        let location = sender.location(in: playerView)
        let scrollView = (self.parentViewController as! PlayerViewController).scrollView!
        
        if isAspectFit {
            if bottomButtonsStackView.isLiveMode { return }
            if !playerViewController.isSeekable && playerViewController.nSeekableEnd <= 0 {
                return
            }

            switch sender.state {
            case .began:
                firstLocation = location
                videoSliderXConstraint.constant = location.x - videoSliderThumbnailView.frame.size.width / 2
                videoSliderYConstraint.constant = location.y - videoSliderThumbnailView.frame.size.height - 30
                videoSliderThumbnailView.isShown = true
                delegate?.onVideoSliderTouchBegan(view: self)
                hideControlView()
                onBookmarkCloseTouched(nil)
            case .changed:
                let deltaX = location.x - firstLocation.x
                videoSliderXConstraint.constant = location.x - videoSliderThumbnailView.frame.size.width / 2
                delegate?.onVideoSliderValueChanged(view: self, deltaX: deltaX)
                
            case .ended:
                let deltaX = location.x - firstLocation.x
                videoSliderThumbnailView.isShown = false
                delegate?.onVideoSliderTouchEnd(view: self, deltaX: deltaX)
            default:
                let deltaX = location.x - firstLocation.x
                videoSliderThumbnailView.isShown = false
                delegate?.onVideoSliderTouchEnd(view: self, deltaX: deltaX)
            }
        }
        else {
            switch sender.state {
            case .began:
                firstLocation = location
                scrollViewContentOffset = scrollView.contentOffset
                hideControlView()
                onBookmarkCloseTouched(nil)
            case .changed:
                var deltaX = (location.x - firstLocation.x)
                var deltaY = (location.y - firstLocation.y)
                
                
                let distance = CGPoint(x: deltaX, y: deltaY)
 //               NLog("onVideoViewPinchGestured distance : \(distance)")
                do {
                    try playerView.scroll(distance)
                }
                catch {
                    print(error.localizedDescription)
                }
            case .ended:
                do {
                    try playerView.scrollStop()
                }
                catch {
                    print(error.localizedDescription)
                }
            default:
                break
            }
        }
    }

    #if false
    @objc func onVideoViewPanGestured(_ sender: UIPanGestureRecognizer) {
        if isLocked || !indicatorView.isHidden || !playerView.seekable { return }
       
        let location = sender.location(in: sender.view)
        let scrollView = (self.parentViewController as! PlayerViewController).scrollView!
        
        if isAspectFit {
            if bottomButtonsStackView.isLiveMode { return }
            if !playerViewController.isSeekable && playerViewController.nSeekableEnd <= 0 {
                return
            }

            switch sender.state {
            case .began:
                firstLocation = location
                videoSliderXConstraint.constant = location.x - videoSliderThumbnailView.frame.size.width / 2
                videoSliderYConstraint.constant = location.y - videoSliderThumbnailView.frame.size.height - 30
                videoSliderThumbnailView.isShown = true
                delegate?.onVideoSliderTouchBegan(view: self)
                hideControlView()
                onBookmarkCloseTouched(nil)
            case .changed:
                let deltaX = location.x - firstLocation.x
                videoSliderXConstraint.constant = location.x - videoSliderThumbnailView.frame.size.width / 2
                delegate?.onVideoSliderValueChanged(view: self, deltaX: deltaX)
                
            case .ended:
                let deltaX = location.x - firstLocation.x
                videoSliderThumbnailView.isShown = false
                delegate?.onVideoSliderTouchEnd(view: self, deltaX: deltaX)
            default:
                let deltaX = location.x - firstLocation.x
                videoSliderThumbnailView.isShown = false
                delegate?.onVideoSliderTouchEnd(view: self, deltaX: deltaX)
            }
        }
        else {
            switch sender.state {
            case .began:
                firstLocation = location
                scrollViewContentOffset = scrollView.contentOffset
                hideControlView()
                onBookmarkCloseTouched(nil)
            case .changed:
                var deltaX = -scrollViewContentOffset.x + (location.x - firstLocation.x)
                var deltaY = -scrollViewContentOffset.y + (location.y - firstLocation.y)
                
                if -scrollView.minContentOffset.x < deltaX {
                    deltaX = -scrollView.minContentOffset.x
                }
                
                if scrollView.maxContentOffset.x < -deltaX {
                    deltaX = -scrollView.maxContentOffset.x
                }
                
                if -scrollView.minContentOffset.y < deltaY {
                    deltaY = -scrollView.minContentOffset.y
                }
                
                if scrollView.maxContentOffset.y < -deltaY {
                    deltaY = -scrollView.maxContentOffset.y
                }
                
                scrollView.contentOffset = CGPoint(x: -deltaX, y: -deltaY)
            default:
                break
            }
        }
    }
    #endif

    @objc func onVideoViewPinchGestured(_ sender: UIPinchGestureRecognizer) {
        NLog("onVideoViewPinchGestured")
        if isLocked || !playReachEndTimeView.isHidden { return }
        if sender.state == .began {
            self.hideControlView()
            self.onBookmarkCloseTouched(nil)
        }
        
        delegate?.onVideoPinchGestured(view: self, recognizer: sender)
    }
    
    
    @IBAction func onCloseTouched(_ sender: UIButton) {
        delegate?.onVideoCloseTouched(view: self)
    }
    
    @IBAction func onBookmarkCloseTouched(_ sender: UIButton?) {
        NLog("onBookmarkCloseTouched")
        if UIAccessibility.isVoiceOverRunning {
            self.displayControlView(true)
        }
        else {
            self.hideControlView()
        }
        self.bottomButtonsStackView.isHidden = false
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            self.bookmarkViewBottomConstraint.constant = -220
            self.layoutIfNeeded()
        }) { [unowned self] (completion) in
            self.bookmarkCoverView.isHidden = true
            self.bookmarkView.mode = . normal
        }
        
        isDisplayBookmark = false
    }
    
    @IBAction func onPreviousVideoTouched(_ sender: UIButton) {
        displayPlayReachEndTimeView(willShow: false)
        delegate?.onPreviousVideoTouched(view: self)
    }
    
    @IBAction func onReplayTouched(_ sender: UIButton) {
        displayPlayReachEndTimeView(willShow: false)
        delegate?.onReplayVideoTouched(view: self)        
    }
    
    @IBAction func onNextVideoTouched(_ sender: UIButton) {
        displayPlayReachEndTimeView(willShow: false)
        delegate?.onNextVideoTouched(view: self)
    }
    
    @IBAction func onScreenModeTouched(_ sender: UIButton) {
        view.hideAllToasts()
        if screenMode == .aspectFit {
            screenButton.setImage(#imageLiteral(resourceName: "icZoomScreen"), for: .normal)
//            screenButton.setImage(#imageLiteral(resourceName: "icFullScreen"), for: .normal)
            screenButton.accessibilityLabel = "Enlarged screen".localized()
            screenMode = .scaleToFill
            playerView.scalingMode = .scaleFill
            view.makeToast("Full screen".localized())
        }
        else if screenMode == .scaleToFill {
            screenButton.setImage(#imageLiteral(resourceName: "icFitScreen"), for: .normal)
 //           screenButton.setImage(#imageLiteral(resourceName: "icZoomScreen"), for: .normal)
            screenButton.accessibilityLabel = "Optimal screen".localized()
            screenMode = .scaletoFit
            playerView.scalingMode = .scaleAspectFill
            view.makeToast("Enlarged screen".localized())
        }
        else if screenMode == .scaletoFit {
            screenButton.setImage(#imageLiteral(resourceName: "icFullScreen"), for: .normal)
//            screenButton.setImage(#imageLiteral(resourceName: "icFitScreen"), for: .normal)
            screenButton.accessibilityLabel = "Full screen".localized()
            screenMode = .aspectFit
            playerView.scalingMode = .scaleAspectFit
            view.makeToast("Optimal screen".localized())
        }
    }
    
    @IBAction func onMoreTouched(_ sender: UIButton) {
        resetControlHideTimer()
        delegate?.onBottomStackMoreTouched(view: self)
    }
    
    @IBAction func onControlUITouched(_ sender: UIButton) {
        displayControlView(true)
    }
    
    deinit {
        print("VideoControlerView Deinitted")
        controlViewTimer.invalidate()
    }
}

// MARK: - ControlView Show && Hide
extension VideoControlView {
    func displayControlView(_ willShow: Bool, resetTimer: Double = 5) {
        NLog("displayControlView willShow : \(willShow)")
        print("displayControlView willShow : \(willShow)")
        forwardSeekButton.doubleTapHideTimer.invalidate()
        backwardSeekButton.doubleTapHideTimer.invalidate()
        willShow ? showControlAnimation(resetTimer: resetTimer) : hiddenControlAnimation()
    }
    
    internal func showControlAnimation(resetTimer: Double = 5) {
        isDisplayControl = true
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            self.backgroundView.isHidden = false

            if #available(iOS 11.0, *) {
                self.topViewTopConstraint.constant = 0
            }
            else {
                self.topViewTopConstraint.constant = 10
            }
            
            if isLocked == false {
                self.topView.isHidden = false
                
                self.centerView.isHidden = false
                if bottomButtonsStackView.noSeekMode == false {
                    self.backwardSeekButton.isHidden = false
                    self.forwardSeekButton.isHidden = false
                }
                
                self.playAndPauseButton.isHidden = false
            }

            self.bottomViewBottomConstraint.constant = 0
            self.bottomView.isHidden = false
            
            self.soundProgressView.isShown = true
            self.brightnessProgressView.isShown = true
            self.controlUIButton.isHidden = true

            (self.parentViewController as? PlayerViewController)?.statusBarHidden = false
            
            self.layoutIfNeeded()
        }) { (completion) in
            self.resetControlHideTimer(second: resetTimer)
            self.chattingWebView.isTouchableWebView(isEnable: false)
        }
    }
    
    private func hiddenControlAnimation(progressHidden: Bool = true) {
        NLog("hiddenControlAnimation")
        sliderThumbnailView.isShown = false
        controlViewTimer.invalidate()
        isDisplayControl = !progressHidden
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            self.backgroundView.isHidden = true
            self.topViewTopConstraint.constant = -7
            self.topView.isHidden = true
            
            self.centerView.isHidden = true
            self.bottomViewBottomConstraint.constant = -10
            
            self.bottomView.isHidden = true
            
            guard let parentViewController = self.parentViewController as? PlayerViewController else { return }
//            var parentViewController = self.parentViewController as! PlayerViewController
//            if parentViewController.playerView == nil {return }
            
//            if parentViewController.selectedSubtitleIndex == 0 ||  parentViewController.playerView == nil || parentViewController.playerView.listSubTitle.count == 0 {
//                self.controlUIButton.isHidden = false
//                UIAccessibility.post(notification: .layoutChanged, argument: self.controlUIButton)
//            }
//            else if parentViewController.playerView.listSubTitle.count > 0 {
//                UIAccessibility.post(notification: .layoutChanged, argument: parentViewController.subtitleLabel)
//            }
            
            self.controlUIButton.isHidden = false
            UIAccessibility.post(notification: .layoutChanged, argument: self.controlUIButton)

            if progressHidden {
                self.soundProgressView.isShown = false
                self.brightnessProgressView.isShown = false
            }
            
            (self.parentViewController as? PlayerViewController)?.statusBarHidden = true
            
            self.layoutIfNeeded()
        }) { (completion) in
            self.chattingWebView.isTouchableWebView(isEnable: true)
        }
    }
    
    func resetControlHideTimer(forceReset: Bool = false, second: TimeInterval = 5) {
        NLog("resetControlHideTimer")
        controlViewTimer.invalidate()
        guard let viewController = self.parentViewController as? PlayerViewController else { return }
        
        if viewController.playerView == nil {
            return
        }
        
        var waitTime = second
        if UIAccessibility.isVoiceOverRunning {
            waitTime = 10
        }
        
        NLog("resetControlHideTimer viewController.playerView.isPlaying : \(viewController.playerView.isPlaying)")
        if UIAccessibility.isVoiceOverRunning || viewController.playerView.isPlaying || forceReset {
            controlViewTimer = Timer.scheduledTimer(timeInterval: waitTime, target: self, selector: #selector(hideControlView), userInfo: nil, repeats: false)
        }
    }
    
    @objc private func hideControlView() {
        self.displayControlView(false)
    }
}

//MARK: - VerticalProgressViewDelegate
extension VideoControlView: VerticalProgressViewDelegate {
    func verticalSliderTouchBegan(slider: VerticalProgressView) {
        controlViewTimer.invalidate()
    }
    
    func verticalSliderValueChanged(slider: VerticalProgressView) {
        if UIAccessibility.isVoiceOverRunning == false {
            hiddenControlAnimation(progressHidden: false)
        }
        if slider == brightnessProgressView {
            brightnessProgressView.isShown = true
            soundProgressView.isShown = false
        }
        else {
            if UIAccessibility.isVoiceOverRunning == false {
                brightnessProgressView.isShown = false
            }
            soundProgressView.isShown = true
        }
    }
    
    func verticalSliderTouchEnd(slider: VerticalProgressView) {
        resetControlHideTimer(forceReset: true)
    }
}
//MARK: - PlayerSliderViewDelegate
extension VideoControlView: PlayerSliderViewDelegate {
    func sliderTouchBegan(slider: PlayerSliderView, thumbXPoint: CGFloat) {
        NLog("sliderTouchBegan")
        if !playerViewController.isSeekable && playerViewController.nSeekableEnd <= 0 {
            return
        }
        thumbnailLeadingConstraint.constant = thumbXPoint
        sliderThumbnailView.isShown = true
        
        let isIntersected = isIntersectedVideoSliderView()
        displayTimeDurationStackView(isHidden: isIntersected)
        
        controlViewTimer.invalidate()
        
        delegate?.onTimeSliderTouchBegan(slider: slider)
    }
    
    func sliderValueChanged(slider: PlayerSliderView, thumbXPoint: CGFloat) {
        NLog("sliderValueChanged")
        if !playerViewController.isSeekable && playerViewController.nSeekableEnd <= 0 {
            return
        }
        thumbnailLeadingConstraint.constant = thumbXPoint
        
        let isIntersected = isIntersectedVideoSliderView()
        displayTimeDurationStackView(isHidden: isIntersected)
        
        delegate?.onTimeSliderValueChanged(slider: slider)
    }
    
    func sliderTouchEnd(slider: PlayerSliderView) {
        NLog("sliderTouchEnd")
        if !playerViewController.isSeekable && playerViewController.nSeekableEnd <= 0 {
            return
        }
        sliderThumbnailView.isShown = false
        displayTimeDurationStackView(isHidden: false)
        
        if !isDisplayBookmark {
            resetControlHideTimer(forceReset: true)
        }
        
        delegate?.onTimeSliderTouchEnd(slider: slider)
    }
    
    func isIntersectedVideoSliderView() -> Bool {
        let timeStackFrame = timeStackView.convert(timeStackView.bounds, to: self.view)
        let thumbnailTimeLabelFrame = sliderThumbnailView.currentTimeLabel.convert(sliderThumbnailView.bounds, to: self.view)
        return thumbnailTimeLabelFrame.intersects(timeStackFrame)
    }
    
    func displayTimeDurationStackView(isHidden: Bool) {
        UIView.animate(withDuration: 0.1) { [unowned self] in
            timeStackView.alpha = isHidden ? 0 : 1
            timeStackView.layoutIfNeeded()
        }
    }
    
}

//MARK: - BottomStackViewDelgate
extension VideoControlView: BottomStackViewDelgate {
    func bottomStackLockTouched(view: BottomButtonsStackView, isLocked: Bool) {
        self.isLocked = isLocked
        updateLockModeUI()
        resetControlHideTimer()
    }
    
    func bottomStackSeriesTouched(view: BottomButtonsStackView) {
        resetControlHideTimer()
    }
    
    func bottomStackChattingTouched(view: BottomButtonsStackView, isHidden: Bool) {
        resetControlHideTimer()
        chattingWebView.isHiddenChattingView(isHidden)
    }
    
    func bottomStackRepeatTouched(view: BottomButtonsStackView) {
        resetControlHideTimer()
        
        if repeatMode == .none {
            repeatMode = .start
            bottomButtonsStackView.repeatImageView.image = #imageLiteral(resourceName: "sectionRepeatA18")
            bottomButtonsStackView.repeatButton.accessibilityLabel = "AB Repeat Start".localized()
            delegate?.onBottomStackRepeatTouched(view: self, repeatMode: .start)
        }
        else if repeatMode == .start {
            repeatMode = .end
            bottomButtonsStackView.repeatImageView.image = #imageLiteral(resourceName: "sectionRepeatAb18")
            bottomButtonsStackView.repeatButton.accessibilityLabel = "AB Repeat End".localized()
            delegate?.onBottomStackRepeatTouched(view: self, repeatMode: .end)
        }
        else if repeatMode == .end {
            repeatMode = .none
            bottomButtonsStackView.repeatImageView.image = #imageLiteral(resourceName: "sectionRepeat18")
            bottomButtonsStackView.repeatButton.accessibilityLabel = "AB Repeat".localized()
            delegate?.onBottomStackRepeatTouched(view: self, repeatMode: .none)
        }
        
    }
    
    func bottomStackBookmarkTouched(view: BottomButtonsStackView) {
        NLog("bottomStackBookmarkTouched +++")
        resetControlHideTimer()
        if bookmarkView.state == .unable {
            self.makeToast("This video does not support bookmarks.".localized())
        }
        else if bookmarkView.state == .networkError {
            self.makeToast("Failed to read bookmark data.".localized())
        }
        else {
            controlViewTimer.invalidate()
            bookmarkCoverView.isHidden = false
            UIAccessibility.post(notification: .layoutChanged, argument: bookmarkView.titleLabel)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.backgroundView.isHidden = false
                self.topViewTopConstraint.constant = -7
                self.topView.isHidden = true
                self.centerView.isHidden = true
                
                self.soundProgressView.isShown = false
                self.brightnessProgressView.isShown = false
                
                self.bookmarkViewBottomConstraint.constant = 0
                self.bottomView.isHidden = false
                self.bottomButtonsStackView.isHidden = true
                self.layoutIfNeeded()
            }
            isDisplayBookmark = true
        }
        NLog("bottomStackBookmarkTouched ---")
    }
    
    
    func bottomStackPlaySpeedTouched(view: BottomButtonsStackView) {
        resetControlHideTimer()
        delegate?.onBottomStackPlaySpeedTouched(view: self)
    }
    
    func bottomStackPlaySpeedLongPressed(view: BottomButtonsStackView) {
        resetControlHideTimer()
        delegate?.onBottomStackPlaySpeedLongPressed(view: self)
    }
}

//MARK: - SeekerButtonDelegate
extension VideoControlView: SeekerButtonDelegate {
    func onForwadTouched(view: SeekerButton, isContinues: Bool, intervaltime: Double) {
        isDisplayControl ? resetControlHideTimer() : view.resetDoubleTapTimer()
        self.delegate?.onVideoSeekerTouched(view: self, direction: .forward, isContinues: isContinues, intervalTime: intervaltime)
    }
    
    func onBackwardTouched(view: SeekerButton, isContinues: Bool, intervaltime: Double) {
        isDisplayControl ? resetControlHideTimer() : view.resetDoubleTapTimer()
        self.delegate?.onVideoSeekerTouched(view: self, direction: .backward, isContinues: isContinues, intervalTime: intervaltime)
    }
}

//MARK: - NSecSkipViewDelgate
extension VideoControlView: NSecSkipViewDelgate {
    func onSecSkipViewTouched(view: NSecSkipView) {
        playerView.setSkipPlay()
    }
}


//MARK: - UIGestureRecognizerDelegate
extension VideoControlView: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        if touch.view == view {
            return true
        }
        else {
            return false
        }
    }
}


extension VideoControlView: ChattingWebViewDelegate {
    func onByPasstouched(view: ChattingWebView) {
        onVideoViewTouched(nil)
    }
}


//MARK: - ControlViewSize
extension VideoControlView {
    
    var thumbnailPortraitViewWidth: CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 180
        }
        else {
            return 210
        }
    }
    
    var thumbnailLandScapeViewWidth: CGFloat {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return 80
        }
        else {
            return 110
        }
    }
    
}
