//
//  VideoAlphaGuideViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/01.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol VideoAlphaGuideViewDelegate : class {
    func onAlphaGuideClosed(controller: VideoAlphaGuideViewController)
    
}


class VideoAlphaGuideViewController: UIViewController {

    weak var delegate: VideoAlphaGuideViewDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        PreferenceManager.isFirstWatched = false
    }
    
    @IBAction func onCloseTouched(_ sender: UIButton) {
        
        self.dismiss(animated: false) { [unowned self] in
            self.delegate?.onAlphaGuideClosed(controller: self)
        }
    }
    
}
