//
//  DetailInformationPopupViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/27.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class DetailInformationPopupViewController: UIViewController {
    
    var data: KollusContent?
    
    @IBOutlet weak var pathLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var fileSizeLabel: UILabel!
    
    @IBOutlet weak var addedDateLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    @IBOutlet weak var expireDateView: UIView!
    @IBOutlet weak var expireDateLabel: UILabel!
    
    @IBOutlet weak var expireCountView: UIView!
    @IBOutlet weak var expireCountLabel: UILabel!
    
    @IBOutlet weak var expirePlayTimeView: UIView!
    @IBOutlet weak var expirePlayTimeLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        updateUI()
    }
    
    func updateUI() {
        guard let data = data else { return }
        
        
        var path = Path.shared.currentPath
        path.removeFirst()
        path.insert("Download".localized(), at: 0)
        
        pathLabel.text = path.joined(separator: " > ")
        
        
        titleLabel.text = data.title
        
        
        fileSizeLabel.text = KollusUtil.fileSizeToString(fileSize: data.fileSize)
        fileSizeLabel.accessibilityLabel = KollusUtil.fileSizeToAccessibility(fileSize: data.fileSize)
        
        let date = KollusUtil.integerToDate(int: data.downloadedTime)
        print(date)
        let addedDate = KollusUtil.dateToString(date: date)
        
        addedDateLabel.text = addedDate
        addedDateLabel.accessibilityLabel = KollusUtil.dateToAccesibility(date: date)
        
        let durationText = KollusUtil.durationToString(duration: data.duration)
        durationLabel.text = durationText
        durationLabel.accessibilityLabel = KollusUtil.durationToAccessibility(duration: data.duration)
        
        
        
        if let expireDate = data.drmExpireDate {
            expireDateLabel.text = "~\(KollusUtil.expireDateToString(date: expireDate))"
        }
        else {
            expireDateView.isHidden = true
        }
        
        if data.drmExpireCountMax > 0 {
            expireCountLabel.text = "\(data.drmExpireCount)/\(data.drmExpireCountMax)"
        }
        else {
            expireCountView.isHidden = true
        }
        
        if data.drmTotalExpirePlayTime > 0 && data.drmExpirePlayTime >= 0 {
            let drmTotalExpirePlayTime = KollusUtil.durationToDate(duration: Int(data.drmTotalExpirePlayTime))
            let drmExpirePlayTime = KollusUtil.durationToDate(duration: Int(data.drmExpirePlayTime))
            
            
            expirePlayTimeLabel.text = "\(drmExpirePlayTime)(\(drmTotalExpirePlayTime))"
        }
        else {
            expirePlayTimeView.isHidden = true
        }
        
        
    }
    
    @IBAction func onCloseTouched(_ sender: UIButton) {
        self.dismiss(animated: false, completion: nil)
    }
}
