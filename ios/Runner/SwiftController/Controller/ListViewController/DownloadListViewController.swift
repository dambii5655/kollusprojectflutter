//
//  DownloadListViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/14.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol DownloadListViewDelegate: class {
    func downloadFinished(viewController: DownloadListViewController, content: KollusContent?)
    func downloadCanceled(viewController: DownloadListViewController, content: KollusContent)
}


class DownloadListViewController: UIViewController {
    var secondTime = 0
    var downloadedSize = 0
    
    var lock = NSLock()
    
    weak var delegate: DownloadListViewDelegate?
    
    fileprivate var heightDictionary: [Int : CGFloat] = [:]
    
    var isFolded = true {
        didSet {
            tableView.isScrollEnabled = !isFolded
        }
    }
    
    var remainTimer = Timer()
    
    var mediaContentKeys: [String] = []
    var downloadContentsList: [KollusContent] = []
    
    
    @IBOutlet weak var popupView: UIView!
    @IBOutlet weak var panGestureView: UIView!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet var tableViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var alphaView: UIView!
    
    var tableViewCellHeight: CGFloat {
        if UIDevice.current.userInterfaceIdiom == .pad {
            return 126
        }
        else {
            return 120
        }
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return UIInterfaceOrientationMask.portrait
    }
    
    override var shouldAutorotate: Bool {
        return false
    }
    
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        NLog("viewDidLoad +++")
        super.viewDidLoad()
        tableViewHeightConstraint.constant = tableViewCellHeight
        
        StorageManager.shared.storage.delegate = self
        
        panGestureView.layoutIfNeeded()
        panGestureView.layer.applySketchShadow(alpha: 0.24, blur: 6, spread: 2)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(scrollTap(sender:)))
        panGestureView.addGestureRecognizer(tapGesture)
        panGestureView.accessibilityLabel = "Scroll".localized()
        
        if let delegatingView = self.view as? PassthroughView {
            delegatingView.touchDelegate = presentingViewController?.view
        }
        
//        reloadDownloadContentsList()
//        startDownloadContentsList()
        alphaView.alpha = 0.001
        var stringLocal = "You cannot play the video while downloading.".localized()
        alphaView.accessibilityLabel = stringLocal

        remainTimer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateRemainTimeLabel), userInfo: nil, repeats: true)
        
        UIAccessibility.post(notification: .announcement, argument: stringLocal)
        NLog("viewDidLoad ---")
    }
    
    private var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        } else {
            return UIApplication.shared.statusBarOrientation
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        panGestureView.layer.applySketchShadow(alpha: 0.24, blur: 6, spread: 2)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        coordinator.animate(alongsideTransition: { [weak self] (context) in
            self?.panGestureView.layer.applySketchShadow(alpha: 0.24, blur: 6, spread: 2)
        })        
    }
    
    
    @objc private func updateRemainTimeLabel() {
        secondTime += 1
        
        var totalSize: Int64  = 0
        var recieveSize: Int64 = 0
        var stopSize: Int64 = 0

        for content in downloadContentsList {
            if content.downloadStatus != DownloadStatus.paused.rawValue {
                totalSize += content.fileSize
                stopSize += content.downloadStopSize;
                recieveSize += content.downloadSize
            }
        }
        
        var receivingSize: Int64 = 0
        if recieveSize > stopSize {
            receivingSize = recieveSize - stopSize
        }
        let bitRate = receivingSize / Int64(secondTime)
        
        var remainTimeInt64: Int64 = 0
        if bitRate > 0 {
            remainTimeInt64 = (totalSize - recieveSize) / bitRate
        }
    
        let remainTime = Int(remainTimeInt64)
        
        if remainTime > 0 {
            guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? DownloadTableViewCell else { return }
            cell.remainTimeLabel.text = String(format: "%d:%02d:%02d", remainTime / 3600, remainTime % 3600 / 60, remainTime % 60)
        }
        else {
            if(downloadContentsList.count == 0) {
                dismissViewController()
            }
            
            guard let cell = self.tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? DownloadTableViewCell else { return }
            cell.remainTimeLabel.text = "0:00:00"
            secondTime = 0
        }
        
    }
    
    func reloadDownloadContentsList() {
        NLog("reloadDownloadContentsList +++")
        let newDownloadList = StorageManager.shared.getDownloadList()
        
        for oldContents in downloadContentsList {
            for newContents in newDownloadList {
                if newContents.mediaContentKey == oldContents.mediaContentKey {
                    if newContents.downloaded == false {
                        newContents.downloadStatus = oldContents.downloadStatus
//                        if newContents.downloadStatus == DownloadStatus.waiting.rawValue {
//                            newContents.downloadStatus = DownloadStatus.paused.rawValue
//                        }
                    }
                    break
                }
            }
        }
        
        downloadContentsList = newDownloadList
        sortDownloadContetsList()
        
        
        if(downloadContentsList.count == 0) {
            dismissViewController()
        }
        else {
            self.delegate?.downloadFinished(viewController: self, content: nil)
            tableView?.reloadData()
        }
        NLog("reloadDownloadContentsList ---")
    }
    
    func dismissViewController() {
        if UIApplication.getTopViewController() == self {
            
            UIView.animate(withDuration: 0.2) { [weak self] in
                self?.alphaView.alpha = 0
                self?.view.layoutIfNeeded()
            }
            
            self.dismiss(animated: true) { [weak self] in
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.downloadListViewController = nil
                self?.remainTimer.invalidate()
                self?.delegate?.downloadFinished(viewController: self!, content: nil)
            }
        }
    }
    
    func startDownloadContentsList() {
        NLog("startDownloadContentsList +++")
        for mck in mediaContentKeys {
            for i in 0 ..< downloadContentsList.count {
                let contents = downloadContentsList[i]
                if contents.mediaContentKey == mck && contents.downloadStatus == DownloadStatus.paused.rawValue {
                    
                    StorageManager.shared.startDownloadContent(mediaContentKey: mck)
                    print("start : " , mck)
                    contents.downloadStatus = DownloadStatus.waiting.rawValue
                }
            }
        }
        
        mediaContentKeys.removeAll()
        
        sortDownloadContetsList()
        NLog("startDownloadContentsList ---")
    }
    
    func sortDownloadContetsList() {
        downloadContentsList.sort {
            if $0.downloadStatus != $1.downloadStatus {
                if ($0.downloadStatus == DownloadStatus.paused.rawValue) && ($0.downloadStatus == DownloadStatus.waiting.rawValue) {
                    return $0.downloadStatus < $1.downloadStatus
                }
                else if ($0.downloadStatus == DownloadStatus.waiting.rawValue) && ($0.downloadStatus == DownloadStatus.paused.rawValue) {
                    return $0.downloadStatus < $1.downloadStatus
                }
                
                return $0.downloadStatus > $1.downloadStatus
            }
            else {
                return $0.downloadedTime > $1.downloadedTime
            }
        }
        
        tableView?.reloadData()
    }
    
    
    var firstLocation = CGPoint()
    
    // MARK: - Event
    @IBAction func onPanGestureRecognizer(_ sender: UIPanGestureRecognizer) {
        let location = sender.location(in: sender.view)
        let velocity = sender.velocity(in: sender.view)
        
        switch sender.state {
        case .began:
            firstLocation = location
        case .changed:
            let deltaY = location.y - firstLocation.y
            
            let minimumTableViewHeight: CGFloat = tableViewCellHeight
            let maximumTableViewHeight: CGFloat = self.view.frame.size.height - 27 - 50
            
            if tableViewHeightConstraint.constant < minimumTableViewHeight {
                tableViewHeightConstraint.constant = minimumTableViewHeight
                break
            }
            
            if tableViewHeightConstraint.constant > maximumTableViewHeight {
                tableViewHeightConstraint.constant = maximumTableViewHeight
                break
            }
            
            let alpha = (tableViewHeightConstraint.constant - minimumTableViewHeight) / (maximumTableViewHeight - minimumTableViewHeight)
            alphaView.alpha = alpha
            
            tableViewHeightConstraint.constant = CGFloat(Int(tableViewHeightConstraint.constant - deltaY))
        case .ended:
            if velocity.y > 100 {
                minimizeDownloadListView()
            }
            else if velocity.y < -100 {
                maximizeDownloadListView()
            }
            else if tableViewHeightConstraint.constant > view.frame.size.height / 2 {
                maximizeDownloadListView()
            }
            else {
                minimizeDownloadListView()
            }
        default:
            break
        }
    }
    
    func maximizeDownloadListView() {
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {  [unowned self] in
            tableViewHeightConstraint.constant = self.view.frame.size.height - 27 - 50
            self.alphaView.alpha = 1
//            self.alphaView.isHidden = false
            self.view.layoutIfNeeded()
        }) { [weak self] (completion) in
            self?.isFolded = false
        }
    }
    
    func minimizeDownloadListView() {
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: .curveEaseInOut, animations: { [unowned self] in
            self.tableView.setContentOffset(CGPoint.zero, animated: false)
            self.tableViewHeightConstraint.constant = self.tableViewCellHeight
            self.alphaView.alpha = 0.001
//            self.alphaView.isHidden = true
            self.view.layoutIfNeeded()
        }) { [weak self] (completion) in
            self?.isFolded = true
        }
        
    }

    @objc func scrollTap(sender: UITapGestureRecognizer) {
        if isFolded == true {
            maximizeDownloadListView()
        }
        else {
            minimizeDownloadListView()
        }
    }

    
    deinit {
        print("DownloadListViewController deinit")
    }
}


// MARK: - Table View
extension DownloadListViewController: UITableViewDataSource, UITableViewDelegate, DownloadTableViewCellDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return downloadContentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = downloadContentsList[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "DownloadCell", for: indexPath) as! DownloadTableViewCell
        cell.data = data        
        cell.delegate = self
        
        if indexPath.row == 0 {
            cell.hideRemainTimeView(isHidden: false)
        }
        else {
            cell.hideRemainTimeView(isHidden: true)
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath.row] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath.row]
        return height ?? UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    //MARK: - DownloadTableViewCellDelegate
    func onDownloadCancelTouched(cell: DownloadTableViewCell) {
        guard let data = cell.data else { return }
        delegate?.downloadCanceled(viewController: self, content: data)
    }
    
}

// MARK: - Kollus Storage
extension DownloadListViewController: KollusStorageDelegate {
    func kollusStorage(_ kollusStorage: KollusStorage!, downloadContent content: KollusContent!, error: Error!) {
        if let error = error {
            DispatchQueue.main.async {
                self.reloadDownloadContentsList()
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.6) {
                UIApplication.presentErrorViewController(title: "Code : \((error as NSError).code)" , errorDescription: "", errorReason: error.localizedDescription)
            }
            
            return
        }
        
        
        let totalSize: Float = Float(content.fileSize)
        let downloadedSize: Float = Float(content.downloadSize)
        let downloadPercentage = downloadedSize / totalSize
        
//        print("\(totalSize)" + "        " + "\(downloadedSize)" + "   " + "\(downloadPercentage)")
//                
        DispatchQueue.main.async {
        var index = 0

            for i in 0 ..< self.downloadContentsList.count {
                if self.downloadContentsList[i].contentIndex == content.contentIndex {
                index = i
                break
            }
        }

            guard let _ = self.downloadContentsList[safe: index] else { return }
        
            self.downloadContentsList[index] = content


            if self.downloadContentsList[index].downloadStatus == DownloadStatus.waiting.rawValue || self.downloadContentsList[index].downloadStatus == DownloadStatus.paused.rawValue {
                self.downloadContentsList[index].downloadStatus = DownloadStatus.downloading.rawValue
                self.sortDownloadContetsList()
        }



        if content.downloadProgress == 100 || downloadPercentage == 1 {
            NLog("downloadContent 100 +++")
 //           StorageManager.shared.addDownloadedFile(mediaContentKey: content.mediaContentKey, toPath: ["/"])
            self.reloadDownloadContentsList()
            NLog("downloadContent 100 ---")

        }
        else {
            NLog("downloadContent downloadProgress : \(content.downloadProgress)")
            guard let cell = self.tableView.cellForRow(at: IndexPath(row: index, section: 0)) as? DownloadTableViewCell else { return }
            cell.updateProgressView(value: downloadPercentage)
        }
        }
    }
    
    
    func kollusStorage(_ kollusStorage: KollusStorage!, request: [AnyHashable : Any]!, json: [AnyHashable : Any]!, error: Error!) {
        //        guard let responseJSON = json else { return }
        //        guard let result = responseJSON["kind"] as? Int else { return }
        //
        //        if result == 2 { // Download Success
        //            guard let mck = responseJSON["media_content_key"] as? String else { return }
        //            StorageManager.shared.addDownloadedFile(mediaContentKey: mck, toPath: ["/"])
        //
        //            reloadDownloadContentsList()
        //
        //            print("complete")
        //
        //        }
        
    }
    
    func kollusStorage(_ kollusStorage: KollusStorage!, cur: Int32, count: Int32, error: Error!) {
//        print(count)
    }
    
    
}
