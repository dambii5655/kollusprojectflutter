//
//  FileMoveViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/14.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol FileMoveViewDelegate: class {
    func onMoveConfirmTouched(viewController: FileMoveViewController, path: [String])
    func onMoveCancleTouched(viewController: FileMoveViewController)
}



class FileMoveViewController: UIViewController {
    weak var delegate: FileMoveViewDelegate?
    
    var currentPath = Path.shared.currentPath
    lazy var folderList: [KollusContent] = StorageManager.shared.getFolderList(path: currentPath)
    
    @IBOutlet var titleHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var confirmButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateTitleView()
    }
    
    func updateTitleView() {
        let isRoot = currentPath.count == 1 ? true : false
        titleHeightConstraint.isActive = isRoot
        titleLabel.text = currentPath.last
    }
    
    func reloadFolderDatas() {
        folderList = StorageManager.shared.getFolderList(path: currentPath)
        tableView.reloadData()
        updateTitleView()
    }
    
    //MARK: - Event
    @IBAction func onNewFolderTouched(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "TextFieldInputPopupViewController", bundle: nil)
        let textFieldInputPopupViewController = storyBoard.instantiateInitialViewController() as! TextFieldInputPopupViewController
        textFieldInputPopupViewController.delegate = self
        textFieldInputPopupViewController.currentPathByMoveViewController = currentPath
        self.present(textFieldInputPopupViewController, animated: false, completion: nil)
        textFieldInputPopupViewController.type = .newFolder
    }
    
    @IBAction func onConfirmTouched(_ sender: UIButton) {
        dismiss(animated: false) { [unowned self] in
            self.delegate?.onMoveConfirmTouched(viewController: self, path: self.currentPath)
        }
        
    }
    
    
    @IBAction func onCancleTouched(_ sender: UIButton) {
        dismiss(animated: false) { [unowned self] in
            self.delegate?.onMoveCancleTouched(viewController: self)
        }
    }
    
    @IBAction func onBackTouched(_ sender: UIButton) {
        currentPath.removeLast()
        reloadFolderDatas()
    }
    
}


extension FileMoveViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return folderList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = folderList[indexPath.row]
        let cell = tableView.dequeueReusableCell(withIdentifier: "FolderCell", for: indexPath) as! FolderTableViewCell
        
        cell.data = data
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data = folderList[indexPath.row]
        currentPath.append(data.title)
        reloadFolderDatas()
    }
    
}

extension FileMoveViewController: TextFieldInputPopupViewDelegate {
    func onCreateFolderTouched(viewController: TextFieldInputPopupViewController, name: String) {
        reloadFolderDatas()
    }
    
    
}
