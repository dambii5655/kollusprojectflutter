//
//  FileTableViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit


protocol FileTableViewCellDelegate: class {
    func onFileMoreTouched(cell: FileTableViewCell, button: UIButton)
}


class FileTableViewCell: UITableViewCell {
    var isEditingCell: Bool = false
    
    weak var delegate: FileTableViewCellDelegate?
    
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var checkBoxImageView: UIImageView!
    var data: KollusContent? {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var expireDescriptionLabel: UILabel!
    @IBOutlet weak var playRateProgressView: UIProgressView!
    @IBOutlet weak var durationButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        thumbnailImageView.layer.cornerRadius = 4
        moreButton.accessibilityLabel = "File options".localized()

    }
    
    private func updateUI() {
        guard let data = data else { return }
        
        titleLabel.text = data.title
//        thumbnailImageView.image = UIImage(named: data.snapshot)
        thumbnailImageView.image = UIImage(contentsOfFile: data.snapshot)

        if let expireDate = data.drmExpireDate {
            if data.drmExpired {
                expireDescriptionLabel.text = "Valid date of play has been expired".localized()
            }
            else {
                expireDescriptionLabel.text = "Expiry date to play".localized() + ":~\(KollusUtil.expireDateToString(date: expireDate))"
            }            
        }
        else {
            expireDescriptionLabel.text = ""
        }
        
        let duration = KollusUtil.durationToString(duration: data.duration)
        let durationAccesibility = KollusUtil.durationToAccessibility(duration: data.duration)
        if data.duration == 0 {
            durationButton.isHidden = true
        }
        else {
            durationButton.isHidden = false
            durationButton.setTitle(duration, for: .normal)
            durationButton.accessibilityLabel = durationAccesibility
        }
        
        
        if PreferenceManager.playedContents[data.mediaContentKey] ?? false {
            playRateProgressView.progress = 1
        }
        else {
            let playRate = data.position / data.duration
            playRateProgressView.progress = Float(playRate)
        }
        
        
    }
    
    @IBAction func onFileMoreTouched(_ sender: UIButton) {
        delegate?.onFileMoreTouched(cell: self, button: sender)
    }

    
    override func setEditing(_ editing: Bool, animated: Bool) {
        //        UIView.animate(withDuration: 0.15) {
        
        isEditingCell = editing
        
        self.checkBoxImageView.isHidden = !editing
        self.moreButton.isHidden = editing
        //        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        guard isEditingCell else { return }
        
        checkBoxImageView.image = selected ? #imageLiteral(resourceName: "checked18") : #imageLiteral(resourceName: "unchecked18")
        selectedView.isHidden = selected ? false : true
        
        
    }
    
}
