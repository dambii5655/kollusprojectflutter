//
//  FolderTableViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/04.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol FolderTableViewCellDelegate: class {
    func onFolderMoreTouched(cell: FolderTableViewCell, button: UIButton)
}

class FolderTableViewCell: UITableViewCell {
    var isEditingCell: Bool = false
    weak var delegate: FolderTableViewCellDelegate?
    
    var data: KollusContent? {
        didSet {
            updateUI()
        }
    }   

    @IBOutlet weak var moreButton: UIButton!
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var trailingMarginConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var countButton: UIButton!
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        guard let countButton = countButton else { return }
        countButton.layer.cornerRadius = countButton.frame.size.height / 2
        moreButton.accessibilityLabel = "File options".localized()
    }

    private func updateUI() {
        guard let data = data else { return }
        titleLabel.text = data.title
        
        
        guard let countButton = countButton else { return }
        
        if data.directoryFileNumber == 0 {
            countButton.isHidden = true
        }
        else {
            countButton.isHidden = false
            countButton.setTitle("\(data.directoryFileNumber)", for: .normal)
        }
        
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        guard let checkBoxImageView = checkBoxImageView, let moreButton = moreButton else { return }
            checkBoxImageView.isHidden = !editing
            moreButton.isHidden = editing
        isEditingCell = editing
        
        if editing {
            trailingMarginConstraint.constant = 24
        }
        else {
            trailingMarginConstraint.constant = 0
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        guard let checkBoxImageView = checkBoxImageView else { return }
        guard isEditingCell else { return }
        
        checkBoxImageView.image = selected ? #imageLiteral(resourceName: "checked18") : #imageLiteral(resourceName: "unchecked18")
        selectedView.isHidden = selected ? false : true
        
    }
    
    @IBAction func onFolderMoreTouched(_ sender: UIButton) {
        delegate?.onFolderMoreTouched(cell: self, button: sender)
    }
    
    
}
