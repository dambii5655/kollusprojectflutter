//
//  DirectoryTableHeaderView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/04.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class DirectoryTableHeaderView: UIView {
    
    @IBOutlet weak var titleLabel: UILabel!
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("DirectoryTableHeaderView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
//        shadowView.layer.cornerRadius = 8
//        shadowView.layer.borderWidth = 1
//        shadowView.layer.borderColor = UIColor(hexFromString: "#f6f6f6").cgColor
//
//        shadowView.layer.applySketchShadow()
    }
    
    private func setEvent() {
        //        lockButton.addTarget(self, action: #selector(onLockTouched(_:)), for: .touchUpInside)
    }
    
    
    @IBAction func onRecentlyAddedFileTouched(_ sender: UIButton) {
        
    }
}
