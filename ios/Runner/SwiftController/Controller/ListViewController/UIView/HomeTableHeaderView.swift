//
//  HomeTableHeaderView.swift
//  Kollus Player
//
//  Created by 이기완 on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

class HomeTableHeaderView: UIView {
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var recentlyAddedFileButton: UIButton!
    @IBOutlet weak var recentlyAddedFileLabel: UILabel!
    @IBOutlet weak var recentlyAddedFileCountLabel: UILabel!    
    
//    MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("HomeTableHeaderView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
        shadowView.layer.cornerRadius = 8
        shadowView.layer.borderWidth = 1
        shadowView.layer.borderColor = UIColor(hexFromString: "#f6f6f6").cgColor
        
        shadowView.layer.applySketchShadow()
        
        recentlyAddedFileButton.accessibilityLabel = "Recently added file".localized()
        recentlyAddedFileLabel.text = "Recently added file".localized()
    }
    
    private func setEvent() {
//        lockButton.addTarget(self, action: #selector(onLockTouched(_:)), for: .touchUpInside)
    }
    
    func setRecentlyAddedFile(count: Int) {
        recentlyAddedFileCountLabel.text = "\(count)"
    }
    
    @IBAction func onRecentlyAddedFileTouched(_ sender: UIButton) {
        guard let viewController = self.parentViewController as? ListViewController else { return }
        viewController.hideAllPopupView()
        guard recentlyAddedFileCountLabel.text != "0" else { return }
        
        
        
          
        let storyBoard = UIStoryboard(name: "RecentlyAddedFileViewController", bundle: nil)
        let recentlyAddedFileViewController = storyBoard.instantiateInitialViewController() as! RecentlyAddedFileViewController
        viewController.navigationController?.pushViewController(recentlyAddedFileViewController, animated: true)
    }
}
