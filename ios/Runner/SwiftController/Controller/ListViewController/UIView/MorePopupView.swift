//
//  MorePopupView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/26.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol ListMorePopupViewDelegate: class {
    func onDeleteTouched(view: MorePopupView)
    func onChangeTitleTouched(view: MorePopupView)
    func onDetailInformationTouched(view: MorePopupView)
    func onSavedFolderMoveTouched(view: MorePopupView)
}

class MorePopupView: UIView {
    weak var delegate: ListMorePopupViewDelegate?
    
    enum ViewType {
        case folder
        case file
        case recentylAddedFile
    }
    
    var viewType: ViewType = .file {
        didSet {
            updateUI()
        }
    }
    
    var data: KollusContent?
    
    @IBOutlet weak var savedFolderMoveButton: UIButton!
    @IBOutlet weak var changeTitleButton: UIButton!
    @IBOutlet weak var deleteButton: UIButton!
    
    @IBOutlet weak var detailInformationButton: UIButton!
    
    
    let backgroundView = UIView()
    
    //    MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("MorePopupView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
        backgroundView.backgroundColor = .clear
        backgroundView.frame = CGRect(x: -2000, y: -2000, width: 4000, height: 4000)
        self.insertSubview(backgroundView, at: 0)
        
        
        savedFolderMoveButton.titleLabel?.adjustsFontSizeToFitWidth = true;
        savedFolderMoveButton.titleLabel?.minimumScaleFactor = 0.5;
        savedFolderMoveButton.titleLabel?.baselineAdjustment = .alignCenters

    }
    
    private func setEvent() {
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(onBackgroundTouched(recognizer:)))
        backgroundView.addGestureRecognizer(tapGesture)
    }
    
    func updateUI() {
        if viewType == .folder {
            savedFolderMoveButton.isHidden = true
            changeTitleButton.isHidden = false
            deleteButton.isHidden = false
            detailInformationButton.isHidden = true
        }
        else if viewType == .recentylAddedFile {
            savedFolderMoveButton.isHidden = false
            changeTitleButton.isHidden = true
            deleteButton.isHidden = false
            detailInformationButton.isHidden = false
        }
        else {
            savedFolderMoveButton.isHidden = true
            changeTitleButton.isHidden = true
            deleteButton.isHidden = false
            detailInformationButton.isHidden = false
        }
    }
    
    @objc func onBackgroundTouched(recognizer: UITapGestureRecognizer) {
        UIView.animate(withDuration: 0.15) { [weak self] in
            self?.alpha = 0
            self?.layoutIfNeeded()
        }
    }
    
    // MARK:- Event
    @IBAction func onChangeTitleTouched(_ sender: UIButton) {
        delegate?.onChangeTitleTouched(view: self)
    }
    
    @IBAction func onDeleteTouched(_ sender: UIButton) {
        delegate?.onDeleteTouched(view: self)
    }
    
    @IBAction func onDetailInformationTouched(_ sender: UIButton) {
        delegate?.onDetailInformationTouched(view: self)
    }
    
    @IBAction func onSavedFolderMoveTouched(_ sender: UIButton) {
        delegate?.onSavedFolderMoveTouched(view: self)
    }
    
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        if clipsToBounds || isHidden || alpha == 0 {
            return nil
        }
        
        for subview in subviews.reversed() {
            let subPoint = subview.convert(point, from: self)
            if let result = subview.hitTest(subPoint, with: event) {
                return result
            }
        }
        
        return nil
    }
    
}
