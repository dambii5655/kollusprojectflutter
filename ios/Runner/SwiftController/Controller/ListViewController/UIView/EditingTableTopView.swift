//
//  EditingTableTopView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/13.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit


protocol EditingTableTopViewDelegate: class {
    func onEditingConfirmTouched(view: EditingTableTopView)
    func onEditingCancelTouched(view: EditingTableTopView)
}

class EditingTableTopView: UIView {
    weak var delegate: EditingTableTopViewDelegate?
    
    var editMode: EditMode = .delete {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var countLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("EditingTableTopView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
    }
    
    private func updateUI() {
        if editMode == .delete {
            deleteButton.setTitle("Delete".localized(), for: .normal)
        }
        else {
            deleteButton.setTitle("Move".localized(), for: .normal)
        }
    }
    
    private func setEvent() {
        
    }
    
    func setSelectedCountText(count: Int) {
        countLabel.text = String(format: "%@ selected".localized(), "\(count)")
        
        deleteButton.isEnabled = count > 0 ? true : false
    }
    
    
    //MARK: - Event
    @IBAction func onCancelTouched(_ sender: UIButton) {
        delegate?.onEditingCancelTouched(view: self)
        
    }
    
    @IBAction func onDeleteTouched(_ sender: UIButton) {
        
        delegate?.onEditingConfirmTouched(view: self)
    }
}
