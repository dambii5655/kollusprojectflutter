//
//  DownloadTableViewCell.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/18.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit


protocol DownloadTableViewCellDelegate: class {
    func onDownloadCancelTouched(cell: DownloadTableViewCell)
}


class DownloadTableViewCell: UITableViewCell {
    weak var delegate: DownloadTableViewCellDelegate?
    
    var data: KollusContent? {
        didSet {
            updateUI()
        }
    }
    
    @IBOutlet weak var downloadStatusButton: UIButton!
    
    @IBOutlet weak var thumbnailImageView: UIImageView!
    
    @IBOutlet weak var downloadProgressView: UIProgressView!
    @IBOutlet weak var progressStackView: UIStackView!
    
    @IBOutlet weak var remainTimeStackView: UIStackView!
    
    @IBOutlet weak var remainTimeLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var sizeUnitLabel: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            cancelButton.contentEdgeInsets = UIEdgeInsets(top: 19, left: 19, bottom: 19, right: 19)
        }
        else {
            cancelButton.contentEdgeInsets = UIEdgeInsets(top: 17, left: 17, bottom: 17, right: 17)
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func updateUI() {
        guard let data = data else { return }
        
//        thumbnailImageView.image = UIImage(named: data.snapshot)
        thumbnailImageView.image = UIImage(contentsOfFile: data.snapshot)

        titleLabel.text = data.title
        
        downloadProgressView.progress = Float(data.downloadProgress) / 100.0
        
        var size = Float(data.fileSize) / (1024 * 1024)
        
        if size > 1000 {
            size = size / 1024
            sizeUnitLabel.text = "GB"
        }
        else {
            sizeUnitLabel.text = "MB"
        }
        
        sizeLabel.text = String(format: "%.2f", size)
        
        switch data.downloadStatus {
        case DownloadStatus.paused.rawValue:
            downloadStatusButton.backgroundColor = UIColor(hexFromString: "#343a40")
            downloadStatusButton.setTitleColor(UIColor(hexFromString: "#e9ecef"), for: .normal)
            downloadStatusButton.setTitle("Stopped".localized(), for: .normal)
            downloadProgressView.tintColor = UIColor(hexFromString: "#343a40")
            cancelButton.setImage(#imageLiteral(resourceName: "delete24"), for: .normal)
            cancelButton.accessibilityLabel = "Delete".localized()
//            progressStackView.isHidden = false
        case DownloadStatus.waiting.rawValue:
            downloadStatusButton.backgroundColor = UIColor(hexFromString: "#f1f3f5")
            downloadStatusButton.setTitleColor(UIColor(hexFromString: "#343a40"), for: .normal)
            downloadStatusButton.setTitle("Preparing".localized(), for: .normal)
            downloadProgressView.tintColor = UIColor(hexFromString: "#007aff")
            cancelButton.setImage(#imageLiteral(resourceName: "stopCircle18"), for: .normal)
            cancelButton.accessibilityLabel = "Stop".localized()
//            progressStackView.isHidden = false

        case DownloadStatus.downloading.rawValue:
            downloadStatusButton.backgroundColor = UIColor(hexFromString: "#e4f3ff")
            downloadStatusButton.setTitleColor(UIColor(hexFromString: "#007aff"), for: .normal)
            downloadStatusButton.setTitle("Downloading".localized(), for: .normal)
            downloadProgressView.tintColor = UIColor(hexFromString: "#007aff")
            cancelButton.setImage(#imageLiteral(resourceName: "stopCircle18"), for: .normal)
            cancelButton.accessibilityLabel = "Stop".localized()
//            progressStackView.isHidden = false
        default:
            break
        }
        
    }
    
    func updateProgressView(value: Float) {
        guard let data = data, data.downloadStatus == DownloadStatus.downloading.rawValue else { return }
        downloadProgressView.progress = value
    }
    
    func hideRemainTimeView(isHidden: Bool) {
        remainTimeStackView.isHidden = isHidden
    }
    
    
    @IBAction func onCancelTouched(_ sender: UIButton) {
        guard let data = data else { return }
        NLog("onCancelTouched +++")
        if data.downloadStatus != DownloadStatus.paused.rawValue {
            DispatchQueue.global().async {
                StorageManager.shared.cancelDownloadContent(mediaContentKey: data.mediaContentKey)
            }
            
            data.downloadStatus = DownloadStatus.paused.rawValue
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.downloadListViewController?.reloadDownloadContentsList()
                
        }
        else {
            delegate?.onDownloadCancelTouched(cell: self)
        }
        NLog("onCancelTouched ---")

        
    }
    
}
