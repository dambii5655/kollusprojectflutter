//
//  EditingTableHeaderView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/06.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class EditingTableHeaderView: UIView {
    
    var isAllSelected = false {
        didSet {
            updateCheckBoxUI()
        }
    }
    
    
    @IBOutlet weak var checkBoxImageView: UIImageView!
    @IBOutlet weak var selectAllButton: UIButton!
    @IBOutlet weak var selectAllLabel: UILabel!
    
    
    // MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
    }
    
    private func setNib() {
        let view = Bundle.main.loadNibNamed("EditingTableHeaderView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
        selectAllLabel.text = "Select all".localized()
    }
    
    private func setEvent() {
        //        lockButton.addTarget(self, action: #selector(onLockTouched(_:)), for: .touchUpInside)
    }
    
    @IBAction func onCancelTouched(_ sender: UIButton) {
        if let viewController = self.parentViewController as? ListViewController {
            viewController.updateTableTopUI(isEditing: false)
            viewController.tableView.setEditing(false, animated: true)
        }
        else if let viewController = self.parentViewController as? RecentlyAddedFileViewController {
            viewController.updateTableTopUI(isEditing: false)
            viewController.tableView.setEditing(false, animated: true)
        }
        
    }
    
    @IBAction func onSelectAllTouched(_ sender: UIButton) {
        isAllSelected = !isAllSelected
        updateAllSelectUI()
    }
    
    func updateAllSelectUI() {
        //        guard let viewController = self.parentViewController as? ListViewController else { return }
        
        if let viewController = self.parentViewController as? ListViewController {
            let totalRows = viewController.tableView.numberOfRows(inSection: 0)
            
            if isAllSelected {
                for row in 0 ..< totalRows {
                    viewController.tableView.selectRow(at: IndexPath(row: row, section: 0), animated: false, scrollPosition: .none)
                }
            }
            else {
                for row in 0 ..< totalRows {
                    viewController.tableView.deselectRow(at: IndexPath(row: row, section: 0), animated: false)
                }
            }

            let count = viewController.tableView.indexPathsForSelectedRows?.count ?? 0
            viewController.updateEditingTableHeaderView(selectedCount: count)
            
        }
        else if let viewController = self.parentViewController as? RecentlyAddedFileViewController {
            let totalRows = viewController.tableView.numberOfRows(inSection: 0)
            
            if isAllSelected {
                for row in 0 ..< totalRows {
                    viewController.tableView.selectRow(at: IndexPath(row: row, section: 0), animated: false, scrollPosition: .none)
                }
            }
            else {
                for row in 0 ..< totalRows {
                    viewController.tableView.deselectRow(at: IndexPath(row: row, section: 0), animated: false)
                }
            }
            
            let count = viewController.tableView.indexPathsForSelectedRows?.count ?? 0
            viewController.updateEditingTableHeaderView(selectedCount: count)
        }
    }
    
    func updateCheckBoxUI() {
        checkBoxImageView.image = isAllSelected ? #imageLiteral(resourceName: "checked18") : #imageLiteral(resourceName: "unchecked18")
    }
    
}

