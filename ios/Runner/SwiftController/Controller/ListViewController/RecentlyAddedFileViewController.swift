//
//  RecentlyAddedFileViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/25.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation
import UIKit
//import Alamofire

class RecentlyAddedFileViewController: UIViewController {
    lazy var listMorePopupView = MorePopupView()
    var popupViewHideTimer = Timer()
    
    var contentsList: [KollusContent] = []
    lazy var editingTableHeaderView = EditingTableHeaderView()
    
    fileprivate var heightDictionary: [Int : CGFloat] = [:]
    
    @IBOutlet weak var editingTableTopView: EditingTableTopView!
    
    @IBOutlet weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var settingButton: HighlightedButton!
    @IBOutlet weak var moreOptionsButton: HighlightedButton!
    
    @IBOutlet weak var tableView: UIListTableView!
    @IBOutlet weak var morePopupView: UIView!
    
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var deleteButton: HighlightedButton!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "FileTableViewCell", bundle: nil), forCellReuseIdentifier: "FileTableViewCell")
        
        listMorePopupView.delegate = self
        setUI()
        
        tableView.touchesBegan = { [weak self] in
            self?.hideAllPopupView()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reloadContentsList()
    }
    
    func setUI() {
        backButton.accessibilityLabel = "back".localized()
        settingButton.accessibilityLabel = "Setting".localized()
        moreOptionsButton.accessibilityLabel = "More options".localized()
        
        morePopupView.layer.applySketchShadow(alpha: 0.24, blur: 4, spread: 0)
        listMorePopupView.layer.applySketchShadow(alpha: 0.24, blur: 4, spread: 0)
        navigationTitleLabel.text = "Recently added files".localized()
        deleteButton.setTitle("Delete".localized(), for: .normal)
    }
    
    func updateTableTopUI(isEditing: Bool) {
        if isEditing {
            updateEditingTableHeaderView(selectedCount: 0)
            tableView.setAndLayoutTableHeaderView(header: editingTableHeaderView)
            editingTableTopView.isHidden = false
        }
        else {
            tableView.tableHeaderView = nil
            editingTableTopView.isHidden = true
        }
        
        editingTableTopView.delegate = self
    }
    
    func updateEditingTableHeaderView(selectedCount count: Int) {
        editingTableTopView.setSelectedCountText(count: count)
        if count == contentsList.count {
            editingTableHeaderView.isAllSelected = true
        }
        else {
            editingTableHeaderView.isAllSelected = false
        }
    }
    
    func reloadContentsList() {
        contentsList.removeAll()
        contentsList.append(contentsOf: StorageManager.shared.getRecentlyAddedFileList())
        setTableViewContentInset()
        tableView.reloadData()
        
        if contentsList.count == 0 {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    // MARK: - UI
    func hideMorePopupView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.15) { [unowned self] in
                self.morePopupView.alpha = 0
            }
        }
        else {
//            resetPopupViewHideTimer()
            UIAccessibility.post(notification: .layoutChanged, argument: self.morePopupView)
            self.hideListMorePopupView(isHidden: true)
            UIView.animate(withDuration: 0.15) { [unowned self] in
                self.morePopupView.alpha = 1
            }
        }
    }
    
    func hideIndicatorView(isHidden: Bool) {
        DispatchQueue.main.async { [unowned self] in
            self.loadingView.isHidden = isHidden
        }
        
    }
    
    func setTableViewContentInset() {
        if UIApplication.getTopViewController() is DownloadListViewController {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 145, right: 0)
        }
        else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    // MARK: - Event
    
    func resetPopupViewHideTimer() {
        popupViewHideTimer.invalidate()
        popupViewHideTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(hideAllPopupView), userInfo: nil, repeats: false)
    }
    
    @objc private func hideAllPopupView() {
        hideMorePopupView(isHidden: true)
        self.hideListMorePopupView(isHidden: true)
    }
    
    @IBAction func onSettingTouched(_ sender: UIButton) {
        hideMorePopupView(isHidden: true)
        if let topViewController = UIApplication.getTopViewController() {
            let storyBoard = UIStoryboard(name: "SettingViewController", bundle: nil)
            let settingViewController = storyBoard.instantiateInitialViewController()
            topViewController.present(settingViewController!, animated: true, completion: nil)
        }
    }
    
    @IBAction func onBackTouched(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func onMoreTouched(_ sender: UIButton) {
        morePopupView.alpha == 0 ? hideMorePopupView(isHidden: false) : hideMorePopupView(isHidden: true)
    }
    
    
    @IBAction func onDeleteTouched(_ sender: UIButton) {
        hideMorePopupView(isHidden: true)
        editingTableTopView.editMode = .delete
        updateTableTopUI(isEditing: true)
        tableView.setEditing(true, animated: true)
    }
    
    deinit {
        print("RecentlyAddedFileViewController Deinitted")
    }
}

// MARK: - Delegate
extension RecentlyAddedFileViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = contentsList[indexPath.row]
        
        if data.fileType == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableViewCell", for: indexPath) as! FileTableViewCell
            cell.data = contentsList[indexPath.row]
            cell.delegate = self
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FolderTableViewCell", for: indexPath) as! FolderTableViewCell
            cell.data = contentsList[indexPath.row]
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            let count = tableView.indexPathsForSelectedRows?.count ?? 0
            updateEditingTableHeaderView(selectedCount: count)
        }
        else {
            let data = contentsList[indexPath.row]
            
            if data.fileType == 0 {
                if data.drmExpired && data.drmExpireRefreshPopup {
                    let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
                    let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
                    confirmPopupViewController.delegate = self
                    confirmPopupViewController.type = .drmRenew
                    confirmPopupViewController.data = data
                    UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
                    return
                }


                let storyBoard = UIStoryboard(name: "PlayerViewController", bundle: nil)
                let playerViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
                playerViewController.data = data
                playerViewController.playListItems = contentsList
                playerViewController.delegate = self

                
                _ = UIInterfaceOrientation.portrait.rawValue
//                UIDevice.current.setValue(value, forKey: "orientation")

                UIApplication.getTopViewController()?.present(playerViewController, animated: false, completion: nil)
            }
            else {
                let storyBoard = UIStoryboard(name: "ListViewController", bundle: nil)
                let listViewController  = storyBoard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
                listViewController.isRoot = false
                Path.shared.appendPath(directoryName: data.title)
                self.navigationController?.pushViewController(listViewController, animated: true)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            let count = tableView.indexPathsForSelectedRows?.count ?? 0
            updateEditingTableHeaderView(selectedCount: count)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath.row] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath.row]
        return height ?? UITableView.automaticDimension
    }
    
}



//MARK: - TableViewCellDelgate , ListMorePopupViewDelegate
extension RecentlyAddedFileViewController: FileTableViewCellDelegate, ListMorePopupViewDelegate {
    
    func onFileMoreTouched(cell: FileTableViewCell, button: UIButton) {         // file
        hideMorePopupView(isHidden: true)
        
        tableView.addSubview(listMorePopupView)
        
        if listMorePopupView.data == cell.data {        //hide
            hideListMorePopupView(isHidden: true, button: button)
        }
        else {
            listMorePopupView.data = cell.data         // appear
            hideListMorePopupView(isHidden: false, button: button)
            UIAccessibility.post(notification: .layoutChanged, argument: listMorePopupView.savedFolderMoveButton)
        }
    }
    
    func hideListMorePopupView(isHidden: Bool, button: UIButton! = nil) {
        if isHidden {
            UIView.animate(withDuration: 0.15) { [weak self] in
                UIView.animate(withDuration: 0.15, animations: { [weak self] in
                    self?.listMorePopupView.alpha = 0
                }) { [weak self] (completion)  in
                    self?.listMorePopupView.data = nil
                }
            }
        }
        else {
//            resetPopupViewHideTimer()
            hideMorePopupView(isHidden: true)
            UIView.animate(withDuration: 0.15, animations: { [unowned self] in
                self.listMorePopupView.alpha = 0
            }) { [unowned self] (completion)  in
                self.listMorePopupView.snp.remakeConstraints { (maker) in
                    self.listMorePopupView.viewType = .recentylAddedFile
                    maker.top.equalTo(button.snp.bottom).offset(-43)
                    maker.trailing.equalTo(button).offset(-10)
                }
            
                DispatchQueue.global().asyncAfter(deadline: .now() + 0.01) {
                    DispatchQueue.main.async {
                        
                        let moreFrame = listMorePopupView.convert(listMorePopupView.bounds, to: view)
                        if moreFrame.origin.y + moreFrame.size.height > view.frame.size.height {
                            self.listMorePopupView.snp.remakeConstraints { (maker) in
                                maker.top.equalTo(button.snp.bottom).offset(-123)
                                maker.trailing.equalTo(button).offset(-10)
                            }
                        }
                        else if UIApplication.getTopViewController() is DownloadListViewController {
                            if moreFrame.origin.y + moreFrame.size.height > view.frame.size.height - 145 {
                                self.listMorePopupView.snp.remakeConstraints { (maker) in
                                    maker.top.equalTo(button.snp.bottom).offset(-123)
                                    maker.trailing.equalTo(button).offset(-10)
                                }
                            }
                        }
                        
                        UIView.animate(withDuration: 0.15) { [unowned self] in
                            self.listMorePopupView.alpha = 1
                        }
                    }
                }
                
            }
        }
    }
    
    func onDeleteTouched(view: MorePopupView) {
        hideListMorePopupView(isHidden: true)
        let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
        let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
        confirmPopupViewController.delegate = self
        confirmPopupViewController.data = view.data
        confirmPopupViewController.type = view.data!.directoryFileNumber == 0 ? .delete : .deleteFilesinFolder
        UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
    }
    
    func onChangeTitleTouched(view: MorePopupView) {
        
    }
    
    func onDetailInformationTouched(view: MorePopupView) {
        hideListMorePopupView(isHidden: true)
        
        let storyBoard = UIStoryboard(name: "DetailInformationPopupViewController", bundle: nil)
        let detailInformationPopupViewController = storyBoard.instantiateInitialViewController() as! DetailInformationPopupViewController
        detailInformationPopupViewController.data = view.data
        UIApplication.getTopViewController()?.present(detailInformationPopupViewController, animated: false, completion: nil)
    }
    
    func onSavedFolderMoveTouched(view: MorePopupView) {
        guard let data = view.data else { return }
        
        guard var path = DirectoryJson.searchPath(data.mediaContentKey, originalJsonData: DirectoryJson.getData()) as? [String] else { return }
        
        navigationController?.popToRootViewController(animated: true)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            guard let rootViewController = UIApplication.getTopViewController() as? ListViewController else { return }
            
            var viewControllers = rootViewController.navigationController?.viewControllers
            
            if path.count > 1 {
                path.removeFirst()
                
                for directoryName in path {
                    let storyBoard = UIStoryboard(name: "ListViewController", bundle: nil)
                    let listViewController = storyBoard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
                    listViewController.isRoot = false
                    Path.shared.appendPath(directoryName: directoryName)
                    viewControllers?.append(listViewController)
                }
            }
            
            rootViewController.navigationController?.setViewControllers(viewControllers!, animated: true)
        }
        
    }
    
}



//MARK: - UIScrollViewDelegate
extension RecentlyAddedFileViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        setTableViewContentInset()
    }
}
// MARK:- EditingTableTopView
extension RecentlyAddedFileViewController: EditingTableTopViewDelegate {
    func onEditingConfirmTouched(view: EditingTableTopView) {
        
        if view.editMode == .delete {
            let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
            let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
            confirmPopupViewController.delegate = self
            confirmPopupViewController.type = .delete
            UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
        }
    }
    
    func onEditingCancelTouched(view: EditingTableTopView) {
        updateTableTopUI(isEditing: false)
        tableView.setEditing(false, animated: true)
    }
    
}


// MARK: - File Remove
extension RecentlyAddedFileViewController: ConfirmPopupViewDelegate {
    func onCancelTouched(viewController: ConfirmPopupViewController) {
        viewController.data = nil
    }
    
    func onConfirmTouched(viewController: ConfirmPopupViewController) {
        if viewController.type == .delete {
            if let data = viewController.data {         // 더보기 단일 선택에서 삭제시
                DispatchQueue.global().async { [unowned self] in
                    self.hideIndicatorView(isHidden: false)
                    
                    StorageManager.shared.removeContent(content: data)
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.hideIndicatorView(isHidden: true)
                        self.reloadContentsList()
                        
                        return
                    }
                }
            }
            // 다중 선택시
            guard let indexPaths = tableView.indexPathsForSelectedRows else {
                return
            }
            
            DispatchQueue.global().async { [unowned self] in
                self.hideIndicatorView(isHidden: false)
                
                for indexPath in indexPaths {
                    let content = self.contentsList[indexPath.row]
                    StorageManager.shared.removeContent(content: content)
                }
                
                DispatchQueue.main.async { [unowned self] in
                    self.hideIndicatorView(isHidden: true)
                    self.reloadContentsList()
                    
                    self.tableView.setEditing(false, animated: false)
                    self.updateTableTopUI(isEditing: false)
                }
            }
        }
        else if viewController.type == .drmRenew {
            let storyBoard = UIStoryboard(name: "PlayerViewController", bundle: nil)
            let playerViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
            playerViewController.data = viewController.data
            playerViewController.playListItems = contentsList
            playerViewController.delegate = self
            _ = UIInterfaceOrientation.portrait.rawValue
//            UIDevice.current.setValue(value, forKey: "orientation")
            UIApplication.getTopViewController()?.present(playerViewController, animated: false, completion: nil)
        }
        
    }
}

extension RecentlyAddedFileViewController: PlayerViewControllerDelegate {
    func onVideoViewClosed() {
        reloadContentsList()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isExistDownloadList()
    }
}
