//
//  ListViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/07/29.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

class ListViewController: UIViewController {
    var popupViewHideTimer = Timer()
    
    lazy var listMorePopupView = MorePopupView()
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    
    var isRoot = true
    var contentsList: [KollusContent] = []
    var headerView: UIView = HomeTableHeaderView()
    lazy var editingTableHeaderView = EditingTableHeaderView()
    
    fileprivate var heightDictionary: [Int : CGFloat] = [:]
    
    @IBOutlet weak var editingTableTopView: EditingTableTopView!
    
    @IBOutlet weak var navigationBarHeight: NSLayoutConstraint!
    @IBOutlet weak var navigationTitleLabel: UILabel!
    @IBOutlet weak var settingButton: HighlightedButton!
    @IBOutlet weak var sortButton: HighlightedButton!
    @IBOutlet weak var moreOptionsButton: HighlightedButton!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var tableView: UIListTableView!
    @IBOutlet weak var morePopupView: UIView!
    
    
    @IBOutlet weak var loadingView: LoadingView!
    @IBOutlet weak var noDataViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var noDataView: UIView!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if AppDelegate.startStorage == false {
            return
        }
        tableView.register(UINib(nibName: "FileTableViewCell", bundle: nil), forCellReuseIdentifier: "FileTableViewCell")
        tableView.register(UINib(nibName: "FolderTableViewCell", bundle: nil), forCellReuseIdentifier: "FolderTableViewCell")
        
        listMorePopupView.delegate = self
        
        setUI(isRoot: isRoot)
        
        updateTableTopUI(isEditing: false)
        
        if PreferenceManager.isFirstExecuted && isRoot {
            loadingView.isHidden = false
            StorageManager.shared.requestSampleContentsData { [unowned self] (success, error) in
                if success {
                    PreferenceManager.isFirstExecuted = false
                }
                else if let error = error {

                    UIApplication.presentErrorViewController(title: "Error Code : \((error as NSError).code)", errorDescription: nil, errorReason: error.localizedDescription)
                    DirectoryJson.make([])
                }
                
                self.reloadContentsList()
                self.loadingView.isHidden = true
            }
        }
        else if isRoot {
            appDelegate.isExistDownloadList()
        }
        
        tableView.touchesBegan = { [weak self] in
            self?.hideAllPopupView()
        }
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
//        NLog("viewWillAppear +++")
        super.viewWillAppear(animated)
        if AppDelegate.startStorage == false {
            return
        }
        AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
        appDelegate.currentListViewController = self
        //        if !isRoot {
        reloadContentsList()

        print(DirectoryJson.getData() as Any)
//        NLog("viewWillAppear ---")
    }
    
    
    func setUI(isRoot: Bool) {
        if isRoot {
            Path.shared.appendPath(directoryName: "/")
            headerView = HomeTableHeaderView()
            navigationTitleLabel.text = "Download".localized()
        }
        else {
            headerView = DirectoryTableHeaderView()
            navigationTitleLabel.text = Path.shared.getLastPath()
            navigationTitleLabel.isEnabled = true
            
        }
        settingButton.accessibilityLabel = "Setting".localized()
        sortButton.accessibilityLabel = "Sort".localized()
        moreOptionsButton.accessibilityLabel = "More options".localized()
        backButton.accessibilityLabel = "back".localized()
        
        morePopupView.layer.applySketchShadow(alpha: 0.24, blur: 4, spread: 0)
        listMorePopupView.layer.applySketchShadow(alpha: 0.24, blur: 4, spread: 0)
    }
    
    func updateTableTopUI(isEditing: Bool) {
        if isEditing {
            updateEditingTableHeaderView(selectedCount: 0)
            tableView.setAndLayoutTableHeaderView(header: editingTableHeaderView)
            editingTableTopView.isHidden = false
            settingButton.isHidden = true
        }
        else {
            editingTableTopView.isHidden = true
            settingButton.isHidden = false

            if isRoot {
                tableView.setAndLayoutTableHeaderView(header: headerView)
                backButton.isHidden = true
            }
            else {
                guard let headerView = headerView as? DirectoryTableHeaderView else { return }
                headerView.titleLabel.text = Path.shared.getLastPath()
                tableView.setAndLayoutTableHeaderView(header: headerView)
                backButton.isHidden = false
                
                noDataViewTopConstraint.constant = headerView.frame.size.height
            }
        }
        editingTableTopView.delegate = self
    }
    
    
    func updateEditingTableHeaderView(selectedCount count: Int) {
        editingTableTopView.setSelectedCountText(count: count)
        if count == contentsList.count {
            editingTableHeaderView.isAllSelected = true
        }
        else {
            editingTableHeaderView.isAllSelected = false
        }
    }
    
    func reloadContentsList() {
        NLog("reloadContentsList +++")
        editingTableTopView.setSelectedCountText(count: 0)
        
        if(isRoot) {
            if let headerView = headerView as? HomeTableHeaderView {
                let count = StorageManager.shared.getRecentlyAddedFileList().count
                headerView.setRecentlyAddedFile(count: count)
            }
            
            if let recentlyAddedFileViewController = UIApplication.getTopViewController() as? RecentlyAddedFileViewController {
                recentlyAddedFileViewController.reloadContentsList()
            }
            
        }
        
        contentsList.removeAll()
        contentsList.append(contentsOf: StorageManager.shared.getContentsList())
        setTableViewContentInset()
        tableView.reloadData()
        NLog("reloadContentsList ---")
    }
    
    deinit {
        print("listviewvc deinit")
    }
    
    // MARK: - UI
    func hideMorePopupView(isHidden: Bool) {
        if isHidden {
            UIView.animate(withDuration: 0.15) { [unowned self] in
                self.morePopupView.alpha = 0
            }
        }
        else {
//            resetPopupViewHideTimer()
            UIAccessibility.post(notification: .layoutChanged, argument: self.morePopupView)
            self.hideListMorePopupView(isHidden: true)
            UIView.animate(withDuration: 0.15) { [unowned self] in
                self.morePopupView.alpha = 1
            }
        }
    }
    
    func hideIndicatorView(isHidden: Bool) {
        DispatchQueue.main.async { [unowned self] in
            self.loadingView.isHidden = isHidden
        }
    }
    
    func setTableViewContentInset() {
        if UIApplication.getTopViewController() is DownloadListViewController {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 150, right: 0)
        }
        else {
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        }
    }
    
    // MARK: - Event
    @IBAction func onSettingTouched(_ sender: UIButton) {
        if AppDelegate.startStorage == false {
            return
        }
        hideMorePopupView(isHidden: true)
        if let topViewController = UIApplication.getTopViewController() {
            let storyBoard = UIStoryboard(name: "SettingViewController", bundle: nil)
            let settingViewController = storyBoard.instantiateInitialViewController()
            topViewController.present(settingViewController!, animated: true, completion: nil)
        }
    }
    
    @IBAction func onBackTouched(_ sender: UIButton) {
        if isRoot { return }
        Path.shared.removeLastPath()
        popupViewHideTimer.invalidate()
        self.navigationController?.popViewController(animated: true)
    }
        
    @IBAction func onOrderTouched(_ sender: UIButton) {
        if AppDelegate.startStorage == false {
            return
        }
        hideMorePopupView(isHidden: true)
        
        if let topViewController = UIApplication.getTopViewController() {
            
            let storyBoard = UIStoryboard.storyboard(name: "ActionSheetViewController")
            
            let navigationViewController = storyBoard.instantiateInitialViewController() as! UINavigationController
            
            if let actionSheetPhone = navigationViewController.children.first as? ActionSheetViewController {
                actionSheetPhone.currentListViewController = self
                actionSheetPhone.mode = .sort
            }
            else if let actionSheetPad = navigationViewController.children.first as? ActionSheetViewController_iPad {
                actionSheetPad.currentListViewController = self
                actionSheetPad.mode = .sort
                
            }
            
            topViewController.present(navigationViewController, animated: false, completion: nil)
        }
    }
    
    @IBAction func onMoreTouched(_ sender: UIButton) {
        if AppDelegate.startStorage == false {
            return
        }
        morePopupView.alpha == 0 ? hideMorePopupView(isHidden: false) : hideMorePopupView(isHidden: true)
    }
    
    
    @IBAction func onDeleteTouched(_ sender: UIButton) {
        hideMorePopupView(isHidden: true)
        editingTableTopView.editMode = .delete
        updateTableTopUI(isEditing: true)
        tableView.setEditing(true, animated: true)
    }
    
    @IBAction func onMoveTouched(_ sender: UIButton) {
        hideMorePopupView(isHidden: true)
        editingTableTopView.editMode = .move
        updateTableTopUI(isEditing: true)
        tableView.setEditing(true, animated: true)
    }
    
    @IBAction func onCreateNewFolderTouched(_ sender: UIButton) {
        hideMorePopupView(isHidden: true)
        let storyBoard = UIStoryboard(name: "TextFieldInputPopupViewController", bundle: nil)
        let textFieldInputPopupViewController = storyBoard.instantiateInitialViewController() as! TextFieldInputPopupViewController
        textFieldInputPopupViewController.delegate = self
        UIApplication.getTopViewController()?.present(textFieldInputPopupViewController, animated: false, completion: nil)
        textFieldInputPopupViewController.type = .newFolder
    }
    
    func resetPopupViewHideTimer() {
        popupViewHideTimer.invalidate()
        popupViewHideTimer = Timer.scheduledTimer(timeInterval: 3, target: self, selector: #selector(hideAllPopupView), userInfo: nil, repeats: false)
    }
    
    @objc func hideAllPopupView() {
        hideMorePopupView(isHidden: true)
        self.hideListMorePopupView(isHidden: true)
    }
    
    func isExpired(info:KollusContent) -> Bool {
        // check date
        if let expireDate = info.drmExpireDate {
        
            let now = Date()
            if now.compare(expireDate) == ComparisonResult.orderedDescending {
                return true
            }
        }
        
        // check count
        if info.drmExpireCountMax > 0 && info.drmExpireCount == 0 {
            return true
        }
        
        // check play time
        if info.drmTotalExpirePlayTime > 0 && info.drmExpirePlayTime == 0 {
            return true
        }

        // check flag
        if info.drmExpired {
            return true
        }

        return false
    }
    
}

// MARK: - UITableViewDataSource, UITableViewDelegate
extension ListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        noDataView.isHidden = contentsList.count == 0 ? false : true
        tableView.alwaysBounceVertical = contentsList.count == 0 ? false : true
        return contentsList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        NLog("cellForRowAt +++")
        let data = contentsList[indexPath.row]
        if data.fileType == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FileTableViewCell", for: indexPath) as! FileTableViewCell
            cell.delegate = self
            cell.data = contentsList[indexPath.row]
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "FolderTableViewCell", for: indexPath) as! FolderTableViewCell
            cell.delegate = self
            cell.data = contentsList[indexPath.row]
            return cell
        }
//        NLog("cellForRowAt ---")
    }
        
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        NLog("didSelectRowAt +++")
        if UIApplication.getTopViewController() != self {
            if UIApplication.getTopViewController() is DownloadListViewController {}
            else {
                return
            }
        }
        
        if tableView.isEditing {
            let count = tableView.indexPathsForSelectedRows?.count ?? 0
            updateEditingTableHeaderView(selectedCount: count)
        }
        else {
            let data = contentsList[indexPath.row]
            
            if data.fileType == 0 {
                if self.isExpired(info: data) && data.drmExpireRefreshPopup {
                    let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
                    let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
                    confirmPopupViewController.delegate = self
                    confirmPopupViewController.type = .drmRenew
                    confirmPopupViewController.data = data
                    UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
                    return
                }

                // TODO gggg video player
                let storyBoard = UIStoryboard(name: "PlayerViewController", bundle: nil)
                let playerViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
                playerViewController.data = data
                playerViewController.playListItems = contentsList
                playerViewController.delegate = self
                UIApplication.getTopViewController()?.present(playerViewController, animated: false, completion: nil)
            }
            else {
                let storyBoard = UIStoryboard(name: "ListViewController", bundle: nil)
                let listViewController = storyBoard.instantiateViewController(withIdentifier: "ListViewController") as! ListViewController
                listViewController.isRoot = false
                Path.shared.appendPath(directoryName: data.title)
                self.navigationController?.pushViewController(listViewController, animated: true)
            }
        }
        NLog("didSelectRowAt ---")
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            let count = tableView.indexPathsForSelectedRows?.count ?? 0
            updateEditingTableHeaderView(selectedCount: count)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        heightDictionary[indexPath.row] = cell.frame.size.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        let height = heightDictionary[indexPath.row]
        return height ?? UITableView.automaticDimension
    }
    
}

//MARK: - TableViewCellDelgate , ListMorePopupViewDelegate
extension ListViewController: FileTableViewCellDelegate, FolderTableViewCellDelegate, ListMorePopupViewDelegate {
    
    func onFileMoreTouched(cell: FileTableViewCell, button: UIButton) {         // file
        tableView.addSubview(listMorePopupView)
        
        if listMorePopupView.data == cell.data {        //hide
            hideListMorePopupView(isHidden: true, button: button)
        }
        else {
            listMorePopupView.data = cell.data         // appear
            hideListMorePopupView(isHidden: false, button: button)
            UIAccessibility.post(notification: .layoutChanged, argument: listMorePopupView.deleteButton)
        }
    }
    
    func onFolderMoreTouched(cell: FolderTableViewCell, button: UIButton) {       //folder
        tableView.addSubview(listMorePopupView)
        
        if listMorePopupView.data == cell.data {
            hideListMorePopupView(isHidden: true, button: button)
        }
        else {
            listMorePopupView.data = cell.data         // appear
            hideListMorePopupView(isHidden: false, button: button)
            UIAccessibility.post(notification: .layoutChanged, argument: listMorePopupView.changeTitleButton)
        }
    }
    
    func hideListMorePopupView(isHidden: Bool, button: UIButton! = nil) {
        if isHidden {
            UIView.animate(withDuration: 0.15) { [unowned self] in
                UIView.animate(withDuration: 0.15, animations: { [unowned self] in
                    self.listMorePopupView.alpha = 0
                }) { [unowned self] (completion)  in
                    self.listMorePopupView.data = nil
                }
            }
        }
        else {
//            resetPopupViewHideTimer()
            hideMorePopupView(isHidden: true)
            UIView.animate(withDuration: 0.15, animations: { [unowned self] in
                self.listMorePopupView.alpha = 0
            }) { [unowned self] (completion)  in
                self.listMorePopupView.snp.remakeConstraints { (maker) in
                    if self.listMorePopupView.data?.fileType == 1 {
                        self.listMorePopupView.viewType = .folder
                        maker.top.equalTo(button.snp.bottom).offset(-11)
                        maker.trailing.equalTo(button).offset(-15)
                    }
                    else {
                        self.listMorePopupView.viewType = .file
                        maker.top.equalTo(button.snp.bottom).offset(-43)
                        maker.trailing.equalTo(button).offset(-10)
                    }
                }
            
                DispatchQueue.global().asyncAfter(deadline: .now() + 0.01) {
                    DispatchQueue.main.async {
                        
                        let moreFrame = listMorePopupView.convert(listMorePopupView.bounds, to: view)
                        if moreFrame.origin.y + moreFrame.size.height > view.frame.size.height {
                            self.listMorePopupView.snp.remakeConstraints { (maker) in
                                maker.top.equalTo(button.snp.bottom).offset(-123)
                                maker.trailing.equalTo(button).offset(-10)
                            }
                        }
                        else if UIApplication.getTopViewController() is DownloadListViewController {
                            if moreFrame.origin.y + moreFrame.size.height > view.frame.size.height - 145 {
                                self.listMorePopupView.snp.remakeConstraints { (maker) in
                                    maker.top.equalTo(button.snp.bottom).offset(-123)
                                    maker.trailing.equalTo(button).offset(-10)
                                }
                            }
                        }
                        
                        UIView.animate(withDuration: 0.15) { [unowned self] in
                            self.listMorePopupView.alpha = 1
                        }
                    }
                }
            }
        }
    }
    
    func onDeleteTouched(view: MorePopupView) {
        NLog("onDeleteTouched +++")
        hideListMorePopupView(isHidden: true)
        let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
        let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
        confirmPopupViewController.delegate = self
        confirmPopupViewController.data = view.data
        
        var fromPath = Path.shared.currentPath
        NLog("onDeleteTouched fromPath : \(fromPath)")
        var count = 1
        if view.data?.fileType == 1{
            fromPath.append((view.data?.title)!)
            
            let curDict = DirectoryJson.getCurrentDict(fromPath, originalJsonData: DirectoryJson.getData()!)
            var jsonData : Data
            var curDictStr : String
            do {
                jsonData = try JSONSerialization.data(withJSONObject: curDict)
                curDictStr = String(data: jsonData, encoding: .utf8)!
                NLog(curDictStr)
                count = curDictStr.components(separatedBy: "\"key\":").count-1
                NLog("count : \(count)")
            } catch {
                print("something went wrong with parsing json")
            }
            confirmPopupViewController.type = count == 0 ? .delete : .deleteFilesinFolder
        }
        else {
            confirmPopupViewController.type = .delete
        }
        UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
        NLog("onDeleteTouched ---")
    }
    
    func onChangeTitleTouched(view: MorePopupView) {
        hideListMorePopupView(isHidden: true)
        let storyBoard = UIStoryboard(name: "TextFieldInputPopupViewController", bundle: nil)
        let textFieldInputPopupViewController = storyBoard.instantiateInitialViewController() as! TextFieldInputPopupViewController
        textFieldInputPopupViewController.delegate = self
        UIApplication.getTopViewController()?.present(textFieldInputPopupViewController, animated: false, completion: nil)
        textFieldInputPopupViewController.data = view.data
        textFieldInputPopupViewController.type = .changeTitle
        
    }

    func onDetailInformationTouched(view: MorePopupView) {
        hideListMorePopupView(isHidden: true)
        
        let storyBoard = UIStoryboard(name: "DetailInformationPopupViewController", bundle: nil)
        let detailInformationPopupViewController = storyBoard.instantiateInitialViewController() as! DetailInformationPopupViewController
        detailInformationPopupViewController.data = view.data
        UIApplication.getTopViewController()?.present(detailInformationPopupViewController, animated: false, completion: nil)
        
    }
    
    func onSavedFolderMoveTouched(view: MorePopupView) {
        
    }
}


// MARK: - UIScrollViewDelegate
extension ListViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if !isRoot {
            guard let headerView = headerView as? DirectoryTableHeaderView else { return }
            
            if scrollView.contentOffset.y > headerView.titleLabel.frame.size.height + 10 {
                UIView.animate(withDuration: 0.2) {
                    self.navigationTitleLabel.isEnabled = false
                }
            }
            else {
                UIView.animate(withDuration: 0.2) {
                    self.navigationTitleLabel.isEnabled = true
                }
            }
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        hideListMorePopupView(isHidden: true)
        hideMorePopupView(isHidden: true)
        
        setTableViewContentInset()
    }
    
}
// MARK:- Create Folder
extension ListViewController: TextFieldInputPopupViewDelegate {
    func onCreateFolderTouched(viewController: TextFieldInputPopupViewController, name: String) {
        if viewController.type == .newFolder {
            reloadContentsList()
        }
        else if viewController.type == .changeTitle {
            reloadContentsList()
        }
    }
}

// MARK:- EditingTableTopView
extension ListViewController: EditingTableTopViewDelegate {
    func onEditingConfirmTouched(view: EditingTableTopView) {
        
        if view.editMode == .delete {
            let storyBoard = UIStoryboard(name: "ConfirmPopupViewController", bundle: nil)
            let confirmPopupViewController = storyBoard.instantiateInitialViewController() as! ConfirmPopupViewController
            confirmPopupViewController.delegate = self
            
            let indexPaths = tableView.indexPathsForSelectedRows
            var fromPath = Path.shared.currentPath
            var count = 0
            for indexPath in indexPaths ?? [] {
                let content = self.contentsList[indexPath.row]
                if content.fileType == 1{
                    fromPath.append((content.title)!)
                    let curDict = DirectoryJson.getCurrentDict(fromPath, originalJsonData: DirectoryJson.getData()!)
                    var jsonData : Data
                    var curDictStr : String
                    do {
                        jsonData = try JSONSerialization.data(withJSONObject: curDict)
                        curDictStr = String(data: jsonData, encoding: .utf8)!
                        NLog(curDictStr)
                        count = curDictStr.components(separatedBy: "\"key\":").count-1
                        
                        NLog("count : \(count)")
                    } catch {
                        print("something went wrong with parsing json")
                    }
                    if count > 0 {
                        break
                    }
                }
            }
            confirmPopupViewController.type = count == 0 ? .delete : .deleteFilesinFolder

//            confirmPopupViewController.type = .delete
            UIApplication.getTopViewController()?.present(confirmPopupViewController, animated: false, completion: nil)
        }
        else {
            let storyBoard = UIStoryboard(name: "FileMoveViewController", bundle: nil)
            let fileMoveViewController = storyBoard.instantiateInitialViewController() as! FileMoveViewController
            fileMoveViewController.delegate = self
            present(fileMoveViewController, animated: false, completion: nil)
        }
    }
    
    func onEditingCancelTouched(view: EditingTableTopView) {
        
        updateTableTopUI(isEditing: false)
        tableView.setEditing(false, animated: true)
    }
    
}

// MARK: - File Remove
extension ListViewController: ConfirmPopupViewDelegate {
    func onConfirmTouched(viewController: ConfirmPopupViewController) {
        if viewController.type == .delete || viewController.type == .deleteFilesinFolder {
            if let data = viewController.data {         // 더보기 단일 선택에서 삭제시
                DispatchQueue.global().async { [unowned self] in
                    self.hideIndicatorView(isHidden: false)
                    
                    StorageManager.shared.removeContent(content: data)
                    
                    DispatchQueue.main.async { [unowned self] in
                        self.hideIndicatorView(isHidden: true)
                        self.reloadContentsList()
                        
                        return
                    }
                }
            }
            // 다중 선택시
            guard let indexPaths = tableView.indexPathsForSelectedRows else { return }
            
            DispatchQueue.global().async { [unowned self] in
                self.hideIndicatorView(isHidden: false)
                
                for indexPath in indexPaths {
                    let content = self.contentsList[indexPath.row]
                    StorageManager.shared.removeContent(content: content)
                }
                
                DispatchQueue.main.async { [unowned self] in
                    self.hideIndicatorView(isHidden: true)
                    self.reloadContentsList()
                    
                    self.tableView.setEditing(false, animated: false)
                    self.updateTableTopUI(isEditing: false)
                }
            }
        }
        else if viewController.type == .drmRenew {
            let storyBoard = UIStoryboard(name: "PlayerViewController", bundle: nil)
            let playerViewController = storyBoard.instantiateViewController(withIdentifier: "PlayerViewController") as! PlayerViewController
            playerViewController.data = viewController.data
            playerViewController.playListItems = contentsList
            playerViewController.delegate = self
            UIApplication.getTopViewController()?.present(playerViewController, animated: false, completion: nil)
        }
    }
    
    func onCancelTouched(viewController: ConfirmPopupViewController) {
        viewController.data = nil
    }
}

// MARK: - File Move
extension ListViewController: FileMoveViewDelegate {
    func onMoveConfirmTouched(viewController: FileMoveViewController, path: [String]) {
        guard let indexPaths = tableView.indexPathsForSelectedRows else {
            return
        }
        
        for indexPath in indexPaths {
            let contents = contentsList[indexPath.row]
            
            if contents.fileType == 0 {     // File
                let fromPath = Path.shared.currentPath
                DirectoryJson.moveFile(contents.mediaContentKey, from: fromPath, to: path, originalJsonData: DirectoryJson.getData())
                view.makeToast("The file has been moved".localized())
            }
            else {                          // Directory
                var fromPath = Path.shared.currentPath
                fromPath.append(contents.title)
                do {
                    try DirectoryJson.moveDirectory(fromPath, to: path, originalJsonData: DirectoryJson.getData())
                    view.makeToast("The file has been moved".localized())
                }
                catch {
                    view.makeToast(error.localizedDescription)
                }
                
            }
        }
        
        tableView.setEditing(false, animated: false)
        updateTableTopUI(isEditing: false)
        reloadContentsList()
        
    }
    
    func onMoveCancleTouched(viewController: FileMoveViewController) {
        reloadContentsList()
    }
}

// MARK: - Delegate Video View
extension ListViewController: PlayerViewControllerDelegate {
    func onVideoViewClosed() {
        reloadContentsList()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appDelegate.isExistDownloadList()
    }
}

