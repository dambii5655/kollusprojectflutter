//
//  ErrorPopupViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/15.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation


class ErrorPopupViewController: UIViewController {
    
    var titleText: String?
    var errorDescriptionText: String?
    var errorReasonText: String?
    
    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var popupView: UIView!
    
    
    @IBOutlet weak var errorDescriptionLabel: UILabel!
    @IBOutlet weak var errorReasonLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        updateUI()
    }
    
    func setUI() {
        popupView.layer.masksToBounds = true
        popupView.layer.cornerRadius = 8
        confirmButton.layer.cornerRadius = 4
    }
    
    func updateUI() {
        
        if let titleText = titleText {
            titleLabel.text = titleText
            
            if titleText.count == 0 {
                titleLabel.isHidden = true
            }
        }
        else {
            titleLabel.isHidden = true
        }

        if let errorDescriptionText = errorDescriptionText {
            errorDescriptionLabel.text = errorDescriptionText
            if errorDescriptionText.count == 0 {
                errorDescriptionLabel.isHidden = true
            }
//            UIAccessibility.post(notification: .screenChanged, argument: errorDescriptionLabel)
        }
        else {
            errorDescriptionLabel.isHidden = true
        }

        if let errorReasonText = errorReasonText {
            errorReasonLabel.text = errorDescriptionLabel.isHidden ? "\(errorReasonText)" : ": \(errorReasonText)"
            if errorReasonText.count == 0 {
                errorReasonLabel.isHidden = true
            }
            
        }
        else {
            errorReasonLabel.isHidden = true
        }
        
    }
    
    
    @IBAction func onConfirmTouched(_ sender: UIButton) {
        dismiss(animated: false, completion: nil)
    }
    
}

