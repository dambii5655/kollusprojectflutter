//
//  ConfirmPopupViewController.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/13.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

protocol ConfirmPopupViewDelegate: class {
    func onConfirmTouched(viewController: ConfirmPopupViewController)
    func onCancelTouched(viewController: ConfirmPopupViewController)
}



class ConfirmPopupViewController: UIViewController {
    enum ViewType {
        case delete
        case deleteFilesinFolder
        case contentsAllDelete
        case initializeAppData
        case downloadCancel
        case continueView
        
        case networkDataUseStreaming
        case networkDataUseDownload
        
        case drmRenew
    }
    
    var type: ViewType = .delete
//        didSet {
//            updateUI()
//        }
//    }
    
    var data: KollusContent?
    
    
    
    weak var delegate: ConfirmPopupViewDelegate?
    weak var currentListViewController: ListViewController?
    
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var contentsLabel: UILabel!
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var popupView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        updateUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        UIAccessibility.post(notification: .screenChanged, argument: contentsLabel)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func setUI() {
        popupView.layer.masksToBounds = true
        popupView.layer.cornerRadius = 8
        confirmButton.layer.cornerRadius = 4
    }
    
    func updateUI() {
        if type == .delete {
            contentsLabel.text = "Are you sure you want to delete?".localized()
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Delete".localized(), for: .normal)
            
            
            confirmButton.backgroundColor = UIColor(hexFromString: "#fce6e6")
            confirmButton.setTitleColor(UIColor(hexFromString: "#cb453a"), for: .normal)
        }
        else if type == .deleteFilesinFolder {
            titleLabel.isHidden = false
            
            titleLabel.text = "Are you sure you want to delete?".localized()
            contentsLabel.text = "All videos in the folder are deleted.\nThis operation cannot be undone.".localized()
            
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Delete".localized(), for: .normal)
            
            
            confirmButton.backgroundColor = UIColor(hexFromString: "#fce6e6")
            confirmButton.setTitleColor(UIColor(hexFromString: "#cb453a"), for: .normal)
        }
        else if type == .contentsAllDelete {
            contentsLabel.text = "All download content will be deleted.\nDo you want to proceed?".localized()
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Delete".localized(), for: .normal)
            
            
            confirmButton.backgroundColor = UIColor(hexFromString: "#fce6e6")
            confirmButton.setTitleColor(UIColor(hexFromString: "#cb453a"), for: .normal)
        }
        else if type == .initializeAppData {
            contentsLabel.text = "Do you want to reset all settings?".localized()
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Confirm".localized(), for: .normal)
            
            
            confirmButton.backgroundColor = UIColor(hexFromString: "#fce6e6")
            confirmButton.setTitleColor(UIColor(hexFromString: "#cb453a"), for: .normal)
            
        }
        else if type == .continueView {
            guard let data = data else { return }
            titleLabel.isHidden = false
            titleLabel.text = "Continued viewing".localized()
            contentsLabel.text = String(format: "Continue viewing from (%@)?".localized(), KollusUtil.durationToString(duration: data.position))
            contentsLabel.accessibilityLabel = String(format: "Continue viewing from (%@)?".localized(), KollusUtil.durationToAccessibility(duration: data.position))
//            cancelButton.setTitle("No".localized(), for: .normal)
//            confirmButton.setTitle("YES".localized(), for: .normal)
            cancelButton.setTitle("From the beginning".localized(), for: .normal)
            confirmButton.setTitle("Continued viewing".localized(), for: .normal)

        }
        else if type == .networkDataUseDownload {
            contentsLabel.text = "You are connected to a 3G/LTE network.\nDo you want to start downloading?".localized()
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Confirm".localized(), for: .normal)
            
            confirmButton.backgroundColor = UIColor(hexFromString: "#eef1fb")
            confirmButton.setTitleColor(UIColor(hexFromString: "#007aff"), for: .normal)
        }
        else if type == .networkDataUseStreaming {
            contentsLabel.text = "You are connected to a 3G/LTE network.\nDo you want to start Playing?".localized()
            cancelButton.setTitle("Cancel".localized(), for: .normal)
            confirmButton.setTitle("Confirm".localized(), for: .normal)
            
            confirmButton.backgroundColor = UIColor(hexFromString: "#eef1fb")
            confirmButton.setTitleColor(UIColor(hexFromString: "#007aff"), for: .normal)
        }
        else if type == .drmRenew {
            contentsLabel.text = "This content has expired.\nWould you like to renew DRM?".localized()
            cancelButton.setTitle("No".localized(), for: .normal)
            confirmButton.setTitle("YES".localized(), for: .normal)
        }
        else {
            contentsLabel.text = "Are you sure you want to cancel the download?".localized()
            cancelButton.setTitle("No".localized(), for: .normal)
            confirmButton.setTitle("YES".localized(), for: .normal)
            
//            confirmButton.backgroundColor = UIColor(hexFromString: "#f6f6f6")
//            confirmButton.setTitleColor(UIColor(hexFromString: "#495057"), for: .normal)
        }
    }
    
    @IBAction func onCancelTouched(_ sender: UIButton) {
        
        self.dismiss(animated: false) { [unowned self] in
            self.delegate?.onCancelTouched(viewController: self)
        }
    }
    
    @IBAction func onConfirmTouched(_ sender: UIButton) {
        
        self.dismiss(animated: false) { [unowned self] in
            self.delegate?.onConfirmTouched(viewController: self)
        }
    }
    
}
