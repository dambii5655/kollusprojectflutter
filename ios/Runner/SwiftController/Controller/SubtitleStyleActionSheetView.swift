//
//  SubtitleStyleActionSheetView.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/24.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

protocol SubtitleStyleActionSheetViewDelegate: class {
    
    func onSubtitleStyleChanged(view: SubtitleStyleActionSheetView)
    func onSubtitleBgChanged(view: SubtitleStyleActionSheetView)
}



class SubtitleStyleActionSheetView: UIView {
    weak var delegate: SubtitleStyleActionSheetViewDelegate?
    
    var collectionViewData: [SubtitleColorData] = ActionSheetData.createSubtitleColors()
    
    let stepSlider = StepSlider()
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var sliderCoverView: UIView!
    
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var subtitleBackgroundView: UIView!
    @IBOutlet weak var subtitleBackgroundSwitch: UISwitch!
        
    @IBOutlet weak var smallALabel: UILabel!
    @IBOutlet weak var bigALabel: UILabel!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeightConstraint: NSLayoutConstraint!
    
    //MARK: - Func
    override init(frame: CGRect) {
        super.init(frame: frame)
        setNib()
        setUI()
        setEvent()
        fetchDatas()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setNib()
        setUI()
        setEvent()
        fetchDatas()
    }

    private func setNib() {
        let view = Bundle.main.loadNibNamed("SubtitleStyleActionSheetView", owner: self, options: nil)?.first as! UIView
        view.frame = self.bounds
        self.addSubview(view)
    }
    
    private func setUI() {
        collectionView.register(UINib(nibName: "SubtitleColorCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "SubtitleColorCell")
        collectionView.frame.size.width = self.bounds.width - 32
        sliderCoverView.addSubview(stepSlider)
                
        stepSlider.snp.makeConstraints { (maker) in
            maker.centerY.equalTo(sliderCoverView)
            maker.leading.trailing.equalTo(sliderCoverView)
        }
        
        stepSlider.labelOffset = 0
        stepSlider.tintColor = UIColor(hexFromString: "#adb5bd")
        stepSlider.trackColor = UIColor(hexFromString: "#adb5bd")

        stepSlider.sliderCircleImage = #imageLiteral(resourceName: "slideThumb")
        stepSlider.trackHeight = 2
        stepSlider.maxCount = 5
        
        stepSlider.enableHapticFeedback = true

        let verticalView = UIView(frame: CGRect(x: 4, y: 0, width: 2, height: 10))
        verticalView.backgroundColor = UIColor(hexFromString: "#adb5bd")
        
        let backgroundView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: 10))
        backgroundView.backgroundColor = .clear
        backgroundView.addSubview(verticalView)

        let verticalIamge = backgroundView.asImage()

        stepSlider.setTrackCircleImage(verticalIamge, for: .normal)
        stepSlider.setTrackCircleImage(verticalIamge, for: .selected)
    }
    
    private func setEvent() {
        stepSlider.addTarget(self, action: #selector(onSliderValueChanged), for: .valueChanged)
        
        
    }
    
    func fetchDatas() {
        subtitleBackgroundSwitch.isOn = PreferenceManager.isUseSubtitleBackground
        
        subtitleBackgroundView.isHidden = !PreferenceManager.isUseSubtitleBackground
        
        stepSlider.index = UInt(PreferenceManager.subtitleSize)
        
        let color = SubtitleColor(rawValue: PreferenceManager.subtitleColor)!.toColor
        let size = SubtitleSize(rawValue: Int(stepSlider.index))!.toFloat

        subtitleLabel.setHTMLFromString(htmlText: "Subtitle style preview.".localized(), baseFont: color, baseFont: size)
        
        
        collectionView.reloadData()
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [unowned self] in
            collectionViewHeightConstraint.constant = collectionView.collectionViewLayout.collectionViewContentSize.height
            self.layoutIfNeeded()
//        }
        
    }
 
    
    //MARK: - Event
    @objc func onSliderValueChanged(slider: StepSlider) {
        PreferenceManager.subtitleSize = Int(slider.index)
        subtitleLabel.font = subtitleLabel.font.withSize(SubtitleSize(rawValue: Int(stepSlider.index))!.toFloat)
        delegate?.onSubtitleStyleChanged(view: self)
    }
    
    @IBAction func onSubtitleBackgroundSwitched(_ sender: UISwitch) {
        PreferenceManager.isUseSubtitleBackground = subtitleBackgroundSwitch.isOn
        subtitleBackgroundView.isHidden = !subtitleBackgroundSwitch.isOn
        delegate?.onSubtitleBgChanged(view: self)
    }
}



extension SubtitleStyleActionSheetView: UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return collectionViewData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = collectionViewData[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SubtitleColorCell", for: indexPath) as! SubtitleColorCollectionViewCell
        
        cell.data = data
        cell.layoutIfNeeded()        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        PreferenceManager.subtitleColor = indexPath.row
        subtitleLabel.textColor = SubtitleColor(rawValue: PreferenceManager.subtitleColor)?.toColor
        collectionViewData = ActionSheetData.createSubtitleColors()
        delegate?.onSubtitleStyleChanged(view: self)
        collectionView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.bounds.width / 5.0
        
        return CGSize(width: width, height: width)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        collectionView.collectionViewLayout.invalidateLayout()
    }
    
}
