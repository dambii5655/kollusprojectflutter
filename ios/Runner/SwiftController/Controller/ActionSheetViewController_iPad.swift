//
//  ActionSheetViewController_iPad.swift
//  Kollus Player
//
//  Created by kiwan on 2020/12/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

class ActionSheetViewController_iPad: UIViewController {
    weak var kollusContentEntity: KollusContentEntity?
    
    enum Mode {
        case sort
        case setting
        case videoScale
        case seekRange
        case videoResolution
        case subtitleLanguage
        case subtitleStyle
    }
    
    weak var delegate: ActionSheetViewDelegate?
    
    weak var currentListViewController: ListViewController?
    
    var sortDatas: [ActionSheetData] = []
    
    lazy var animationHeight = {
        return (sortDatas.count + 5) * 46
    }()
    
    var mode: Mode = .setting
    var isLiveMode = false
    var canModifyPlaybackRate = true
    var isAudioWatermarkingPlay = false
    
    
    var subtitleList: [SubTitleInfo] = []
    var streamInfoList: [StreamInfo] = []
    
    var selectedSubtitleIndex = 1
    var selectedStreamInfoIndex = 0
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var actionSheetView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var closeSettingButton: UIButton!
    
    @IBOutlet weak var subtitleStyleActionSheetView: SubtitleStyleActionSheetView!
    
    private var windowInterfaceOrientation: UIInterfaceOrientation? {
        if #available(iOS 13.0, *) {
            return UIApplication.shared.windows.first?.windowScene?.interfaceOrientation
        } else {
            return UIApplication.shared.statusBarOrientation
        }
    }
    
    override var prefersHomeIndicatorAutoHidden: Bool {
        return true
    }
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CheckTableViewCell", bundle: nil), forCellReuseIdentifier: "CheckTableViewCell")
        tableView.register(UINib(nibName: "DisclosureTableViewCell", bundle: nil), forCellReuseIdentifier: "DisclosureTableViewCell")
        
        fetchActionSheetDatas()
        subtitleStyleActionSheetView.delegate = self
        
        backButton.accessibilityLabel = "back".localized()
        closeButton.accessibilityLabel = "Close".localized()
        closeSettingButton.accessibilityLabel = "Close setting".localized()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.view.alpha = 1
            self?.view.layoutIfNeeded()
        }
        
        subtitleStyleActionSheetView.fetchDatas()
    }
        
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            self?.updateDeviceOrientationLabel()
            self?.subtitleStyleActionSheetView.fetchDatas()
        }
    }

    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        
        print("traitCollectionDidChange")
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.05) { [weak self] in
            self?.subtitleStyleActionSheetView.fetchDatas()
        }
    }
    
    //MARK: - Func
    
    func updateDeviceOrientationLabel() {
        guard let cell = tableView.cellForRow(at: IndexPath(row: 0, section: 0)) as? DisclosureTableViewCell else { return }
        if UIApplication.shared.statusBarOrientation == .portrait {
            cell.selectedItemLabel.text = "Vertical".localized()
        }
        else {
            cell.selectedItemLabel.text = "Horizontal".localized()
        }
    }
    
    func getTableViewHeight() -> Int{
        return sortDatas.count * 46
    }
    
    func fetchActionSheetDatas() {
        switch mode {
        case .sort:
            titleLabel.text = "Sort".localized()
            sortDatas = ActionSheetData.createSortDatas()
        case .setting:
            titleLabel.text = "Setting".localized()
            let isExistSubtitle = subtitleList.count > 1 ? true : false   // 감추기로 1 이상
            let isExistStreamInfo = streamInfoList.count > 1 ? true : false   // 감추기로 1 이상
            sortDatas = ActionSheetData.createSettingDatas(canModifyPlaybackRate, isLiveMode, isExistSubtitle, isExistStreamInfo)
            setSelectedSubtitleText()
            setStreamInfoText()
        case .videoScale:
            titleLabel.text = "Screen mode".localized()
            sortDatas = ActionSheetData.createVideoScaleDatas()
        case .seekRange:
            titleLabel.text = "Moving range".localized()
            sortDatas = ActionSheetData.createSeekRangeDatas()
        case .videoResolution:
            titleLabel.text = "Quality".localized()
            sortDatas = createStreamInfoDatas()
        case .subtitleLanguage:
            titleLabel.text = "Subtitle language".localized()
            sortDatas = createSubtitleDatas()
        
        case .subtitleStyle:
            titleLabel.text = "Subtitle style".localized()
        }
    }
    
    func reloadDataAnimation() {
        if mode == .subtitleStyle {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.subtitleStyleActionSheetView.alpha = 1
                self.tableView.alpha = 0
                self.subtitleStyleActionSheetView.layoutIfNeeded()
                UIAccessibility.post(notification: .screenChanged, argument: closeButton)
            }
        }
        else if mode == .setting {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.subtitleStyleActionSheetView.alpha = 0
                self.tableView.alpha = 1
                self.subtitleStyleActionSheetView.layoutIfNeeded()
            }
            
            UIView.transition(with: tableView, duration: 0.2, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() })
        }
        else {
            UIView.transition(with: tableView, duration: 0.2, options: .transitionCrossDissolve, animations: { self.tableView.reloadData() })
        }
        
        backButton.isHidden = mode == .setting ? true : false
    }
    
    
    func tableViewReloadData() {
        fetchActionSheetDatas()
        tableView.reloadData()
    }
    
    //MARK: - Datas
    private func createSubtitleDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        for i in 0 ..< subtitleList.count {
            let subtitle = subtitleList[i]
            let isSelected = i == selectedSubtitleIndex
        
            sortData.append(ActionSheetData(viewType: .language, index: i, isSelected: isSelected, title: subtitle.strName, selectedItem: subtitle.strLanguage))
        }
        return sortData
    }
    
    private func setSelectedSubtitleText() {
        let isExistSubtitle = subtitleList.count > 1 ? true : false
        if isExistSubtitle {
            for i in 0 ..< sortDatas.count {
                if sortDatas[i].title == "Subtitle language".localized() {
                    sortDatas[i].selectedItem = subtitleList[selectedSubtitleIndex].strName
                }
            }
        }
    }
    
    private func createStreamInfoDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        for i in 0 ..< streamInfoList.count {
            let streamInfo = streamInfoList[i]
            
            var title = ""
            
            if i == 0 {
                title = streamInfo.bandwidth
            }
            else if Int(streamInfo.streamHeight) != 0 {
                title = "\(streamInfo.streamHeight)P"
            }
            else {
                title = "\(Int(streamInfo.bandwidth) ?? 0 / 1000)Kbps"
            }
            
            let isSelected = i == selectedStreamInfoIndex
        
            sortData.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: isSelected, title: title, selectedItem: ""))
        }
        return sortData
    }
    
    private func setStreamInfoText() {
        let isExistStreamInfo = streamInfoList.count > 1 ? true : false
        if isExistStreamInfo {
            for i in 0 ..< sortDatas.count {
                if sortDatas[i].title == "Quality".localized() {
                    var title = ""
                    
                    if selectedStreamInfoIndex == 0 {
                        title = streamInfoList[selectedStreamInfoIndex].bandwidth
                    }
                    else if Int(streamInfoList[selectedStreamInfoIndex].streamHeight) != 0 {
                        title = "\(streamInfoList[selectedStreamInfoIndex].streamHeight)P"
                    }
                    else {
                        title = "\(Int(streamInfoList[selectedStreamInfoIndex].bandwidth) ?? 0 / 1000)Kbps"
                    }
                    
                    sortDatas[i].selectedItem = title
                }
            }
        }
    }
    
    //MARK: - Event
    @IBAction func onBackTouched(_ sender: UIButton) {
        mode = .setting
        fetchActionSheetDatas()
        reloadDataAnimation()
    }
    
    @IBAction func onCloseTouched(_ sender: UIButton?) {
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.view.alpha = 0
            self?.view.layoutIfNeeded()
        } completion: { [weak self] (completion) in
//            self?.dismiss(animated: false, completion: nil)
            self?.dismiss(animated: false) { [unowned self] in
                self?.currentListViewController?.reloadContentsList()
                self?.delegate?.onActionSheetClose()
            }
            
        }
    }
    
    @IBAction func onCloseSettingTouched(_ sender: UIButton) {
        UIView.animate(withDuration: 0.2) { [weak self] in
            self?.view.alpha = 0
            self?.view.layoutIfNeeded()
        } completion: { [weak self] (completion) in
//            self?.dismiss(animated: false, completion: nil)
            self?.dismiss(animated: false) { [unowned self] in
                self?.currentListViewController?.reloadContentsList()
                self?.delegate?.onActionSheetClose()
            }
            
        }
    }
    
    deinit {
        print("ActionSheetViewController Deinitted")
    }
}

extension ActionSheetViewController_iPad: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sortDatas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let data = sortDatas[indexPath.row]
        if mode == .setting {
            let cell = tableView.dequeueReusableCell(withIdentifier: "DisclosureTableViewCell", for: indexPath) as! DisclosureTableViewCell
            cell.setData(data: data)
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "CheckTableViewCell", for: indexPath) as! CheckTableViewCell
            cell.setData(data: data)
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if mode != .setting {
            let data = sortDatas[indexPath.row]
            
            if data.isSelected {
                tableView.selectRow(at: indexPath, animated: false, scrollPosition: .none)
            }
        }
        else {
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if mode == .sort {
            PreferenceManager.sortOrder = indexPath.row
            tableViewReloadData()
        }
        else if mode == .setting && indexPath.row == 0 {
            if UIApplication.shared.statusBarOrientation == .portrait {
                AppUtility.lockOrientation(.landscape, andRotateTo: .landscapeRight)
            }
            else {
                AppUtility.lockOrientation(.portrait, andRotateTo: .portrait)
            }
            delegate?.onScreenRotation()
            self.dismiss(animated: false)
            
//            updateDeviceOrientationLabel()
        }
        else if mode == .videoScale {
            delegate?.onVideoScaleChanged(videoScale: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .seekRange {
            PreferenceManager.seekRange = indexPath.row
            delegate?.onSeekRangeChanged(seekRange: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .videoResolution {
            selectedStreamInfoIndex = indexPath.row
            delegate?.onVideoResolutionChanged(videoResolution: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .subtitleLanguage {
            selectedSubtitleIndex = indexPath.row
            delegate?.onSubtitleLanguageChanged(subtitleLanguage: indexPath.row)
            tableViewReloadData()
        }
        else if mode == .subtitleStyle {
            
        }
        else {
            let title = sortDatas[indexPath.row].title
            
            if title == "Screen mode".localized() {
                mode = .videoScale
            }
            else if title == "Moving range".localized() {
                mode = .seekRange
            }
            else if title == "Quality".localized() {
                mode = .videoResolution
            }
            else if title == "Subtitle language".localized() {
                mode = .subtitleLanguage
            }
            else if title == "Subtitle style".localized() {
                mode = .subtitleStyle
            }
            
            fetchActionSheetDatas()
            reloadDataAnimation()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
}


extension ActionSheetViewController_iPad: SubtitleStyleActionSheetViewDelegate {
    func onSubtitleStyleChanged(view: SubtitleStyleActionSheetView) {
        self.delegate?.onSubtitleStyleChanged()
    }
    func onSubtitleBgChanged(view: SubtitleStyleActionSheetView) {
        self.delegate?.onSubtitleBgChanged()
    }
}
