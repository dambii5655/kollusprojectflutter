//
//  Bookmark.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/10.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

struct Bookmark {
    var isSelected: Bool
    var position: TimeInterval
    var thumnailImage: UIImage?
    var time: NSDate
    var title: String
    var value: String
    var kind: KollusBookmarkKind
    
}

/// 북마크 시간
//@property (nonatomic, unsafe_unretained, readonly) NSTimeInterval position;
///// 북마크 생성된 일시
//@property (nonatomic, unsafe_unretained, readonly) NSDate *time;
///// 북마크 타이틀(인덱스:강사용)
//@property (nonatomic, copy, readonly) NSString *title;
///// 북마크 타이틀(사용자)
//@property (nonatomic, copy, readonly) NSString *value;
///// 북마크 종류
//@property (nonatomic, readonly) KollusBookmarkKind kind;
