//
//  SortData.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/13.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

struct ActionSheetData {
    static var kollusContentEntity: KollusContentEntity?
    
    enum ViewType {
        case none
        case disclosureIndicator
        case selectCheck
        case language
    }
    
    var viewType: ViewType
    var index: Int
    var isSelected: Bool
    let title: String
    var selectedItem: String
    
    
    static func createSortDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        for i in 0 ..< Sort.count {
            let selected =  PreferenceManager.sortOrder == i
            sortData.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: selected, title: Sort(rawValue: i)!.toString, selectedItem: ""))           
        }
        
        return sortData
    }
    
    static func createSettingDatas(_ canModifyPlaybackRate: Bool, _ isLiveMode: Bool, _ isExistSubtitle: Bool, _ isExsitStreamInfo: Bool) -> [ActionSheetData] {
        print(kollusContentEntity?.mediaContentKey ?? "")
        print("----------------------------------------------------")
        var sortData: [ActionSheetData] = []
        for i in 0 ..< VideoSetting.count {
            switch i {
            case 0:
                if UIDevice.current.userInterfaceIdiom == .phone {
                    let orientation = UIApplication.shared.statusBarOrientation == .portrait ? "Vertical".localized() : "Horizontal".localized()
                    sortData.append(ActionSheetData(viewType: .none, index: i, isSelected: true, title: VideoSetting(rawValue: i)!.toString, selectedItem: orientation))
                }
            case 1:
                #if false // 화면 비율 퀵메뉴 배치로 삭제함
                let selectedItem = VideoScale(rawValue: Int(kollusContentEntity!.scaleMode))!.toString
                sortData.append(ActionSheetData(viewType: .disclosureIndicator, index: i, isSelected: true, title: VideoSetting(rawValue: i)!.toString, selectedItem: selectedItem))
                #endif
            case 2:
                if !isLiveMode {
                    let selectedItem = SeekRange(rawValue: PreferenceManager.seekRange)!.toString
                    sortData.append(ActionSheetData(viewType: .disclosureIndicator, index: i, isSelected: true, title: VideoSetting(rawValue: i)!.toString, selectedItem: selectedItem))
                }
            case 3:
                if isExsitStreamInfo {
                    sortData.append(ActionSheetData(viewType: .disclosureIndicator, index: i, isSelected: true, title: VideoSetting(rawValue: i)!.toString, selectedItem: ""))
                }
            case 4:
                if isExistSubtitle {
                    sortData.append(ActionSheetData(viewType: .disclosureIndicator, index: i, isSelected: true, title: VideoSetting(rawValue: i)!.toString, selectedItem: ""))
                }
            case 5:
                if isExistSubtitle {
                    sortData.append(ActionSheetData(viewType: .disclosureIndicator, index: i, isSelected: true, title: VideoSetting(rawValue: i)!.toString, selectedItem: ""))
                }
            default:
                break
            }
        }
        
        return sortData
    }
    
    static func createVideoScaleDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        
        for i in 0 ..< VideoScale.count {
            var selected: Bool = false
            if let scaleMode = kollusContentEntity?.scaleMode {
                selected = scaleMode == i
            }
            
            sortData.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: selected, title: VideoScale(rawValue: i)!.toString, selectedItem: ""))
        }
        
        return sortData
    }
    
    
    
    static func createPlaySpeedDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        
        
        for i in 0 ..< PlaySpeed.count {
            var selected: Bool = false
            if let scaleMode = kollusContentEntity?.playSpeed {
                selected = scaleMode == i
            }
            
            sortData.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: selected, title: PlaySpeed(rawValue: i)!.toString, selectedItem: ""))
        }
        
        return sortData
    }
    
    static func createSeekRangeDatas() -> [ActionSheetData] {
        var sortData: [ActionSheetData] = []
        
        for i in 0 ..< SeekRange.count {
            let selected =  PreferenceManager.seekRange == i
            
            sortData.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: selected, title: SeekRange(rawValue: i)!.toString, selectedItem: ""))
        }
        
        return sortData
    }
    
    static func createSubtitleColors() -> [SubtitleColorData] {
        var colorDatas: [SubtitleColorData] = []
        
        for i in 0 ..< SubtitleColor.count {
            let selected =  PreferenceManager.subtitleColor == i
            colorDatas.append(SubtitleColorData(isSelected: selected, color: SubtitleColor(rawValue: i)!.toColor))
        }
        
        return colorDatas
    }

    static func createPlayerCodecDatas() -> [ActionSheetData] {
        var playerCodecs: [ActionSheetData] = []
        
        for i in 0 ..< PlayerCodec.count {
            let selected =  PreferenceManager.playerCodec == i
            playerCodecs.append(ActionSheetData(viewType: .selectCheck, index: i, isSelected: selected, title: PlayerCodec(rawValue: i)!.toString, selectedItem: ""))
        }
        
        return playerCodecs
    }

}


