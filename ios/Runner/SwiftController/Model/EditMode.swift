//
//  EditMode.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/06.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

enum EditMode {
      case delete
      case move
  }
  
