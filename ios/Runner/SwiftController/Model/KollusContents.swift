//
//  KollusContents.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/26.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

class KollusContents: KollusContent {

    enum DownloadStatus: Int {
        case Stopped
        case Downloading
        case Ready
    }
    
    var fileCount: Int?
    var DownloadStatus: DownloadStatus?

}
