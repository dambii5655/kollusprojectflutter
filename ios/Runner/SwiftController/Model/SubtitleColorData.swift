//
//  SubtitleColor.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/25.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

struct SubtitleColorData {
    var isSelected: Bool
    var color: UIColor
    
}
