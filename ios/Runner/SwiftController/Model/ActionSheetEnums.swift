//
//  ActionSheetEnums.swift
//  Kollus Player
//
//  Created by kiwan on 2020/09/16.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation


enum Sort: Int {
    case name
    case downloadedTime
    case fileSize
    
    static var count: Int {
        return 3
    }
    
    var toString: String {
        switch self {
        case .name: return "Name order".localized()
        case .downloadedTime: return "Date order".localized()
        case .fileSize: return "File Size order".localized()
        }
    }
}


enum VideoSetting: Int {
    case screenRotate
    case videoScale
    case seekRange
    case videoResolution
    case subtitleLanguage
    case subtitleStyle
    
    static var count: Int {
        return 6
    }
    
    var toString: String {
        switch self {
        case .screenRotate: return "Screen rotation".localized()
        case .videoScale: return "Screen mode".localized()
        case .seekRange: return "Moving range".localized()
        case .videoResolution: return "Quality".localized()
        case .subtitleLanguage: return "Subtitle language".localized()
        case .subtitleStyle: return "Subtitle style".localized()
        }
    }
}



enum VideoScale: Int {
    case aspectFit
    case scaleToFill
    case scaleToFit
    
    
    static var count: Int {
        return 3
    }
    
    var toString: String {
        switch self {
        case .aspectFit: return "Optimal screen".localized()
        case .scaleToFill: return "Full screen".localized()
        case .scaleToFit: return "Enlarged screen".localized()
        }
    }
    
}

enum PlaySpeed: Int {
    case x0_5
    case x0_6
    case x0_7
    case x0_8
    case x0_9
//    case x0_75
    case x1
    case x1_1
    case x1_2
    case x1_3
    case x1_4
//    case x1_25
    case x1_5
    case x1_6
    case x1_7
    case x1_8
    case x1_9
//    case x1_75
    case x2
    
    static var count: Int {
        return 16
    }
    
    var toString: String {
        switch self {
        case .x0_5: return "0.5x"
//        case .x0_75: return "0.75x"
        case .x1: return "1x\n기본"
//        case .x1_25: return "1.25x"
        case .x1_5: return "1.5x"
//        case .x1_75: return "1.75x"
        case .x2: return "2x   "
        case .x0_6: return ""
        case .x0_7: return ""
        case .x0_8: return ""
        case .x0_9: return ""
        case .x1_1: return ""
        case .x1_2: return ""
        case .x1_3: return ""
        case .x1_4: return ""
        case .x1_6: return ""
        case .x1_7:return ""
        case .x1_8:return ""
        case .x1_9:return ""
        }
    }
    
    var toBottomString: String {
        switch self {
        case .x0_5: return "0.5x"
        case .x0_6: return "0.6x"
        case .x0_7: return "0.7x"
        case .x0_8: return "0.8x"
        case .x0_9: return "0.9x"
        case .x1: return "1x\n기본"
        case .x1_1: return "1.1x"
        case .x1_2: return "1.2x"
        case .x1_3: return "1.3x"
        case .x1_4: return "1.4x"
        case .x1_5: return "1.5x"
        case .x1_6: return "1.6x"
        case .x1_7: return "1.7x"
        case .x1_8: return "1.8x"
        case .x1_9: return "1.9x"
        case .x2: return "2x   "
        }
    }
}

enum SeekRange: Int {
    case r5
    case r10
    case r20
    case r30
    case r60
    case r300
    
    static var count: Int {
        return 6
    }
    
    var toString: String {
        switch self {
        case .r5: return "5" + "s".localized()
        case .r10: return "10" + "s".localized()
        case .r20: return "20" + "s".localized()
        case .r30: return "30" + "s".localized()
        case .r60: return "60" + "s".localized()
        case .r300: return "300" + "s".localized()
        }
    }
    
    var toInteger: Int {
        switch self {
        case .r5: return 5
        case .r10: return 10
        case .r20: return 20
        case .r30: return 30
        case .r60: return 60
        case .r300: return 300
        }
    }
}


enum VideoResolution: Int {
    case auto
    case p1080
    case p720
    case p360
    case p270
    case p144
    
    static var count: Int {
        return 6
    }
    
    var toString: String {
        switch self {
        case .auto: return "Auto"
        case .p1080: return "1080p"
        case .p720: return "720p"
        case .p360: return "360p"
        case .p270: return "270p"
        case .p144: return "144p"
        }
    }
}

enum SubtitleSize: Int {
    case verySmall
    case small
    case normal
    case big
    case veryBig
    
    static var count: Int {
        return 5
    }
    
    var toFloat: CGFloat {
        switch self {
        case .verySmall: return 12
        case .small: return 14
        case .normal: return 18
        case .big: return 20
        case .veryBig: return 24
        }
    }
}


enum SubtitleColor: Int {
    case white
    case black
    case lightGray
    case darkGary
    case red
    case pink
    case orange
    case yellow
    case green
    case blue

    static var count: Int {
        return 10
    }
    
    var toColor: UIColor {
        switch self {
        case .white: return UIColor(hexFromString: "#ffffff")
        case .black: return .black
        case .lightGray: return UIColor(hexFromString: "#e9ecef")
        case .darkGary: return UIColor(hexFromString: "#868e96")
        case .red: return UIColor(hexFromString: "#f84848")
        case .pink: return UIColor(hexFromString: "#f791f6")
        case .orange: return UIColor(hexFromString: "#f8a15f")
        case .yellow: return UIColor(hexFromString: "#ffe56c")
        case .green: return UIColor(hexFromString: "#b7e58b")
        case .blue: return UIColor(hexFromString: "#b4f6fb")
        
        }
    }
}

enum PlayerCodec: Int {
    case hardware
    case software
    case nativePlayer
    
    static var count: Int {
        return 3
    }
    
    var toString: String {
        switch self {
        case .hardware: return "Hardware".localized()
        case .software: return "Software".localized()
        case .nativePlayer: return "Native Player".localized()
        }
    }
}

