//
//  DirectoryInfo.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

struct DirectoryInfo: Codable {
    let name: String
    let files: [File]
    let dirs: [DirectoryInfo]
}

struct File: Codable {
    let key: String
}
