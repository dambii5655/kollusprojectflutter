//
//  StreamInfo.swift
//  Kollus Player
//
//  Created by kiwan on 2020/11/04.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation

struct StreamInfo: Codable {
    var bandwidth: String
    var streamWidth: String
    var streamHeight: String
    
}
