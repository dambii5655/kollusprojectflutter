//
//  DownloadStatus.swift
//  Kollus Player
//
//  Created by kiwan on 2020/10/12.
//  Copyright © 2020 kiwan. All rights reserved.
//

import Foundation


enum DownloadStatus: Int {
    case paused
    case waiting        
    case downloading
}
