//
//  SampleContents.swift
//  Kollus Player
//
//  Created by kiwan on 2020/08/03.
//  Copyright © 2020 kiwan. All rights reserved.
//

import UIKit

struct ResponseSampleContents: Codable {
    let error: Int
    let message: String?
    let result: [SampleContents]
}

struct SampleContents: Codable {
    let video_url: String
    let media_content_key: String
}
