import UIKit
import Flutter
import flutter_downloader

@UIApplicationMain
@objc class AppDelegate: FlutterAppDelegate {
    
    var orientationLock = UIInterfaceOrientationMask.all
    var currentListViewController: ListViewController?
    var downloadListViewController: DownloadListViewController?
    
    static var startStorage = false
    static var contentsList: [KollusContent] = []
    
  override func application(
    _ application: UIApplication,
    didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?
  ) -> Bool {

        GeneratedPluginRegistrant.register(with: self)

        FlutterDownloaderPlugin.setPluginRegistrantCallback(registerPlugins)

        StorageManager.shared.validateSDK()

        // 1
        let controller : FlutterViewController = window?.rootViewController as! FlutterViewController

        // 2
        let deviceChannel = FlutterMethodChannel(name: "media.methodchannel/iOS",
                                               binaryMessenger: controller.binaryMessenger)

        // 3
        prepareMethodHandler(deviceChannel: deviceChannel)
      
    return super.application(application, didFinishLaunchingWithOptions: launchOptions)
  }
    
    private func prepareMethodHandler(deviceChannel: FlutterMethodChannel) {
        print("gggg connection method channel ")
        
        // 4
        deviceChannel.setMethodCallHandler({
            (call: FlutterMethodCall, result: @escaping FlutterResult) -> Void in
            print("call ==> ")
            print(call.arguments)
            // 5
            if call.method == "getVideoList" {
                
                self.receiveDeviceModel(result: result)
                
            } else if call.method == "playVideo" {
                
                if let arguments = call.arguments as? [String: Any] {
                    // Access the individual arguments
                    if let mediaContentKey = arguments["mediaContentKey"] as? String {
                        print("Argument 1: \(mediaContentKey)")
                        StorageManager.shared.playAction(mediaContentKey: mediaContentKey)
                    }
                } else {
                    result(FlutterError(code: "INVALID_ARGUMENTS", message: "Invalid mediaContentKey", details: nil))
                }
                
            } else {
                result(FlutterMethodNotImplemented)
                return
            }
        })
    }
    
    private func receiveDeviceModel(result: FlutterResult) {
        // 7
        
        print("ddddd 1 ")
        // StorageManager.shared.validateSDK()
        print("ddddd 2 ")
        StorageManager.shared.requestContentsList()
        print("ddddd 3 ")
        AppDelegate.contentsList = StorageManager.shared.getContentsList()
        print("ddddd 4")
        print("ddddd 4 ", AppDelegate.contentsList)
        
        // let data = AppDelegate.contentsList[0]
        
        var contentArray = [] as [Any]
        
        AppDelegate.contentsList.forEach { content in
            print("mmmmmmm ", content.title, content.fileType)
            let serializedObject: [String: Any] = [
                "title": content.title ?? "",
                "snapshot": content.snapshot ?? "",
                "drmExpireDate": content.drmExpireDate ?? "",
                "drmExpired": content.drmExpired,
                "duration": content.duration,
                "mediaContentKey": content.mediaContentKey ?? "",
                "position": content.position,
                "fileType": content.fileType,
                "fileSize": content.fileSize,
                "contentID" : content.contentID ?? "",
                "contentIndex": content.contentIndex
               // "directoryFileNumber": content.directoryFileNumber
            ]
            
            contentArray.append(serializedObject)
        }
        
        let array = NSArray(array: contentArray)
        
        // 8
        result(array)
    }
    
    func isExistDownloadList() {
        NLog("isExistDownloadList")
        let downloadList = StorageManager.shared.getDownloadList()
        
        downloadList.forEach {
            $0.downloadStatus = DownloadStatus.paused.rawValue
        }
        
        if downloadList.count > 0 {
    //            if self.window?.rootViewController?.presentedViewController as? DownloadListViewController == nil {
    //                let storyBoard = UIStoryboard(name: "DownloadListViewController", bundle: nil)
    //                downloadListViewController = storyBoard.instantiateInitialViewController() as? DownloadListViewController
    //                downloadListViewController?.delegate = self
    //                self.window?.rootViewController?.present(downloadListViewController!, animated: true, completion: nil)
    //            }
    //            downloadListViewController?.reloadDownloadContentsList()
        }
    }

    func onVideoViewClosed() {
        NLog("onVideoViewClosed")
      //  self.semaAppDelegate.signal()
    }
}


private func registerPlugins(registry: FlutterPluginRegistry) {
    if (!registry.hasPlugin("FlutterDownloaderPlugin")) {
        FlutterDownloaderPlugin.register(with: registry.registrar(forPlugin: "FlutterDownloaderPlugin")!)
    }
}
