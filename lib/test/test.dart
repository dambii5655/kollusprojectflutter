import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AsModel extends StatefulWidget {
  @override
  _AsModelState createState() => _AsModelState();
}

class _AsModelState extends State<AsModel> {
  List<MyData> testMyList = [];
  final nameController = TextEditingController();
  final idController = TextEditingController();
  final numberController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadFromSharedPreferences();
  }

  Future<void> _loadFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final myListString = prefs.getStringList('testMyList') ?? [];

    setState(() {
      testMyList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return MyData.fromJson(jsonMap);
      }).toList();
    });
  }

  Future<void> _saveToSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();

    final myListString = prefs.getStringList('testMyList') ?? [];

    final name = nameController.text;
    final id = idController.text;
    final number = int.tryParse(numberController.text) ?? 0;

    final newData = MyData(name: name, id: id, number: number);
    final newDataJson = newData.toJson();

    myListString.add(json.encode(newDataJson));

    await prefs.setStringList('testMyList', myListString);

    nameController.clear();
    idController.clear();
    numberController.clear();

    setState(() {
      testMyList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return MyData.fromJson(jsonMap);
      }).toList();
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Shared Preferences Example',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Shared Preferences Example'),
        ),
        body: Column(
          children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(hintText: 'Enter Name'),
            ),
            TextField(
              controller: idController,
              decoration: InputDecoration(hintText: 'Enter ID'),
            ),
            TextField(
              controller: numberController,
              decoration: InputDecoration(hintText: 'Enter Number'),
              keyboardType: TextInputType.number,
            ),
            ElevatedButton(
              onPressed: _saveToSharedPreferences,
              child: Text('Save'),
            ),
            ElevatedButton(
              onPressed: _saveToSharedPreferences,
              child: Text('Save'),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: testMyList.length,
                itemBuilder: (BuildContext context, int index) {
                  final myData = testMyList[index];
                  return ListTile(
                    title: Text(myData.name),
                    subtitle: Text('ID: ${myData.id}, Number: ${myData.number}'),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MyData {
  final String name;
  final String id;
  final int number;

  MyData({required this.name, required this.id, required this.number});

  factory MyData.fromJson(Map<String, dynamic> json) {
    return MyData(
      name: json['name'],
      id: json['id'],
      number: json['number'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'id': id,
      'number': number,
    };
  }

  @override
  String toString() {
    return '$name - $id - $number';
  }
}
