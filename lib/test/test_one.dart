import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';



class AsModelA extends StatefulWidget {
  @override
  _AsModelAState createState() => _AsModelAState();
}

class _AsModelAState extends State<AsModelA> {
  final nameController = TextEditingController();
  final idController = TextEditingController();
  final numberController = TextEditingController();
  List<String> myList = [];

  @override
  void dispose() {
    nameController.dispose();
    idController.dispose();
    numberController.dispose();
    super.dispose();
  }

  Future<void> _saveToSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();

    // Read the current list from SharedPreferences
    final myListString = prefs.getStringList('myList') ?? [];

    // Get the values from the text fields
    final name = nameController.text;
    final id = idController.text;
    final number = numberController.text;

    // Create a new entry for the list
    final newEntry = '$name - $id - $number';

    // Add the new entry to the list
    myListString.add(newEntry);

    // Save the updated list to SharedPreferences
    await prefs.setStringList('myList', myListString);

    // Clear the text fields
    nameController.clear();
    idController.clear();
    numberController.clear();

    // Update the state to trigger a rebuild of the UI
    setState(() {
      myList = myListString;
    });
  }

  Future<void> _readFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();

    // Read the list from SharedPreferences
    final myListString = prefs.getStringList('myList') ?? [];

    // Update the state to trigger a rebuild of the UI
    setState(() {
      myList = myListString;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Shared Preferences Example'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextField(
              controller: nameController,
              decoration: InputDecoration(
                labelText: 'Name',
              ),
            ),
            TextField(
              controller: idController,
              decoration: InputDecoration(
                labelText: 'ID',
              ),
            ),
            TextField(
              controller: numberController,
              decoration: InputDecoration(
                labelText: 'Number',
              ),
            ),
            SizedBox(height: 20),
            ElevatedButton(
              onPressed: _saveToSharedPreferences,
              child: Text('Save to Shared Preferences'),
            ),
            ElevatedButton(
              onPressed: _readFromSharedPreferences,
              child: Text('Read from Shared Preferences'),
            ),
            SizedBox(height: 20),
            Text(
              'Saved Data:',
              style: TextStyle(fontSize: 20),
            ),
            Expanded(
              child: ListView.builder(
                itemCount: myList.length,
                itemBuilder: (BuildContext context, int index) {
                  return ListTile(
                    title: Text(myList[index]),
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
