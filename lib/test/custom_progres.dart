import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';



class CHeck extends StatefulWidget {
  @override
  _CHeckState createState() => _CHeckState();
}

class _CHeckState extends State<CHeck> {
  final _controller = TextEditingController();
  final _filePaths = <String>[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Flutter SQLite Demo'),
      ),
      body: Column(
        children: [
          TextField(
            controller: _controller,
            decoration: InputDecoration(
              hintText: 'Enter file path',
            ),
          ),
          ElevatedButton(
            onPressed: () {
              setState(() {
                _filePaths.add(_controller.text);
                _controller.clear();
              });
            },
            child: Text('Add'),
          ),
          ElevatedButton(
            onPressed: () async {
              final databasesPath = await getDatabasesPath();
              final path = join(databasesPath, 'my_database.db');
              final db = await openDatabase(path, version: 1,
                  onCreate: (db, version) async {
                    await db.execute('''
                  CREATE TABLE IF NOT EXISTS files (
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    file_path TEXT NOT NULL
                  )
                ''');
                  });
              final batch = db.batch();
              for (final filePath in _filePaths) {
                batch.insert('files', {'file_path': filePath});
              }
              await batch.commit();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(content: Text('Saved to SQLite')),
              );
            },
            child: Text('Save to SQLite'),
          ),
        ],
      ),
    );
  }
}
