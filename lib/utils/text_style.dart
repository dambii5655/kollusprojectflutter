import 'package:flutter/material.dart';

class CustomTextStyle {
  static const TextStyle titleStyle = TextStyle(fontWeight: FontWeight.bold, fontSize: 16, height: 1, );
  static const TextStyle subtitleTextStyle = TextStyle(color: Colors.black54, fontSize: 14);

}