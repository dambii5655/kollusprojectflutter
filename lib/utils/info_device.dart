import 'dart:io';
import 'package:device_info_plus/device_info_plus.dart';

  class InfoDevice{
  static Future<String> getModel() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;

      return "${iosDeviceInfo.utsname.machine}";
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;

      return "${androidDeviceInfo.brand}  ${androidDeviceInfo.model}";
    }
  }
  static Future<String> getProductionLevel() async {
    var deviceInfo = DeviceInfoPlugin();
    if (Platform.isIOS) {
      var iosDeviceInfo = await deviceInfo.iosInfo;

      return "${iosDeviceInfo.systemVersion}";
    } else {
      var androidDeviceInfo = await deviceInfo.androidInfo;

      return "${androidDeviceInfo.version.release}";
    }
  }
}