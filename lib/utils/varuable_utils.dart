import 'package:collus_player/model/video_local_model/video_model.dart';
import 'package:collus_player/service/routes/app_routes.dart';
import 'package:collus_player/service/routes/routes_name.dart';
import 'package:collus_player/view/function_view/functon_bottom_sheet.dart';
// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/src/widgets/framework.dart';

class UtilsVariable{

  static String freeSpace='';
  static String ID='';
  static String platformVersion='';
  static String filePath='';
  static String savePathName='';
  static String? sort;
  static bool switchBackground=false;
  static bool switchNotification=false;
  static bool showFunction=false;
  static int forward_duration=5;
  static bool  forwardSize=false;
  static bool  isFullScreen=false;
  static bool  showMoving=false;


  // for control full and zoom screen
  static bool fullScreen=false;
  static bool bigScreen=false;
  // for control  verticals full and zoom screen
  static bool verticalsFullScreen=false;
  static bool verticalsBigScreen=false;

  // for hide and show upper part
  static bool  showUpperPart=false;
  static  bool animated=false;

  // for video Information
static String  videoName='' ;
static String  videoSize='' ;
static String  dateAdded='' ;
static String  videoDuration='' ;
static String  videoPath='' ;

static bool download=false;
static bool selectAll=false;
static bool selectAllFolder=false;
static bool selectAllLocalVideo=false;

static bool selectFolder=false;
static bool deleteAllItems=false;
static bool watch=false;

static  bool allUnchecked = true;
static  bool allUncheckedLocalVideo = true;

static String deepLink='';
static  List<bool> FoldersBoolList = [];
static  List<bool> MargeBoolList = [];
static  List<bool> videoLocalList = [];
static  List<int> selectedIds = [];


static  TextEditingController folderController = TextEditingController();
 static  List<VideoData> localVideoList = [];



}
