class Book {

  String? name;
  String? imageMovie;
  String? videoUrl;
  String? duration;
  String? experyDate;
  String? endDate;
  String? addedData;
  String? size;
  int? id;

  Book({ this.name,this.id,this.imageMovie,this.videoUrl,this.duration,this.addedData,this.size,this.experyDate,this.endDate,});

  Book.fromJson(Map<String, dynamic> json) {

    name = json['name'];

    id = json['id'];
    videoUrl = json['videoUrl'];
    imageMovie = json['imageMovie'];
    duration = json['duration'];
    experyDate = json['experyDate'];
    endDate = json['endDate'];
    addedData = json['addedData'];
    size = json['size'];

  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();

    data['name'] = this.name;
    data['id'] = this.id;
    data['experyDate'] = this.experyDate;
    data['endDate'] = this.endDate;
    data['videoUrl'] = this.id;
    data['addedData'] = this.addedData;
    data['duration'] = this.duration;
    data['imageMovie'] = this.imageMovie;
    data['size'] = this.size;
    return data;
  }
}