import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
class AppLocalization{
  final Locale locale;
  AppLocalization(this.locale);
  static AppLocalization of (BuildContext context){
    return Localizations.of(context, AppLocalization);
  }
 late  Map<String, String> _localizedValue;
  Future loadJson() async{
   Future<String> jsonStringValue = rootBundle.loadString("assets/lang/${locale.languageCode}.json");
   Map<String,dynamic> mappedJson = json.decode(jsonStringValue.toString());
   _localizedValue = mappedJson.map((key, value) => MapEntry(key, value.toString()));
  }
  String getTranslatedValue(String key){
    return _localizedValue[key].toString();
  }
  static const Type delegate = AppLocalizationsDelegate;
}
class  AppLocalizationsDelegate extends LocalizationsDelegate <AppLocalization>{
  const AppLocalizationsDelegate();
  @override
  bool isSupported(Locale locale)
  {
    return['en','fr'].contains(locale.languageCode);
  }
  @override
  Future<AppLocalization> load(Locale locale)async{
    AppLocalization localization = AppLocalization(locale);
   await localization.loadJson();
   return localization;
  }
  @override
  bool shouldReload(LocalizationsDelegate<AppLocalization> old ){
    return false;
  }


}