import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class FunctionOfMainPage{
 static final folderList = <String>[];
 static Future<void> saveItems() async {
    final prefs = await SharedPreferences.getInstance();
    prefs.setStringList('folderList', folderList);
  }
}
