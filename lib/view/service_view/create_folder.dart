import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/service_view/functions.dart';
import 'package:flutter/material.dart';
class CreateFolder extends StatefulWidget {
  const CreateFolder({Key? key}) : super(key: key);

  @override
  State<CreateFolder> createState() => _CreateFolderState();
}

class _CreateFolderState extends State<CreateFolder> {


  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          height: 20,
        ),
        Text(
          "Create a new folder",
          style: CustomTextStyle.titleStyle,
        ),
        SizedBox(
          height: 15,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20),
          child: TextField(
            autofocus: true,
            controller:UtilsVariable. folderController,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(10.0),
              isDense: false,
              enabledBorder: OutlineInputBorder(
                borderSide: BorderSide(
                    width: 1, color: Colors.black54), //<-- SEE HERE
              ),
              focusedBorder: OutlineInputBorder(
                borderSide: BorderSide(width: 1, color: Colors.black54),
              ),
            ),
          ),
        ),
        SizedBox(
          height: 15,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Row(
            children: [
              Expanded(child: Container()),
              Container(
                  height: 40,
                  child: Center(
                      child: InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Text("Cancel")))),
              SizedBox(
                width: 20,
              ),
              Spacer(),
              InkWell(
                onTap: () {
                  setState(() {
                    FunctionOfMainPage.folderList.add(UtilsVariable. folderController
                        .text); // Add the entered item to the list
                    UtilsVariable.folderController.clear(); // Clear the text field
                    FunctionOfMainPage.saveItems(); // Save the updated list to SharedPreferences
                  });
                  Navigator.pop(context);
                },
                child: Container(
                    padding: EdgeInsets.only(left: 40, right: 40),
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.blueAccent,
                    ),
                    child: Center(
                        child: Text(
                          "Confirm",
                          style: TextStyle(color: Colors.white),
                        ))),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }
}
