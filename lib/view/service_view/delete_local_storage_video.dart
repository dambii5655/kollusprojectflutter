import 'dart:convert';

import 'package:collus_player/model/video_local_model/video_model.dart';
import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class DeleteVideoFromLocal extends StatefulWidget {


  DeleteVideoFromLocal({Key? key, required this.id,}) : super(key: key);

   final String id ;
  @override
  State<DeleteVideoFromLocal> createState() => _DeleteVideoFromLocalState();
}

class _DeleteVideoFromLocalState extends State<DeleteVideoFromLocal> {
  @override
  Widget build(BuildContext context) {
      return  Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          SizedBox(
            height: 20,
          ),
          Text(
            "Are you sure want to delete?",
            style: CustomTextStyle.titleStyle,
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Row(
              children: [
                Expanded(child: Container()),
                Container(
                    height: 40,
                    child: Center(
                        child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                            },
                            child: Text("Cancel")))),
                SizedBox(
                  width: 20,
                ),
                Spacer(),
                InkWell(
                  onTap: () {
                    deleteVideoInfo(widget.id);
                    Navigator.pop(context);
                  },
                  child: Container(
                      padding: EdgeInsets.only(left: 40, right: 40),
                      height: 40,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(4),
                        color: Colors.red.shade50,
                      ),
                      child: Center(
                          child: Text(
                            "Delete",
                            style: TextStyle(
                                color: Colors.red,
                                fontWeight: FontWeight.bold),
                          ))),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 20,
          )
        ],
      );
  }
  Future<void> deleteVideoInfo(String id) async {
    setState(() {
      deleteById(UtilsVariable.localVideoList, id);
      print("localVideoList.length");
      print(UtilsVariable.localVideoList.length);
    });
  }

  void deleteById(List<VideoData> myList, String id) {
    myList.removeWhere((item) => item.name == id);
    SharedPreferences.getInstance().then((prefs) {
      final myListString =
      myList.map((item) => jsonEncode(item.toJson())).toList();
      prefs.setStringList('videoInfo', myListString);
    });
  }

}
