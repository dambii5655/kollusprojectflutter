import 'dart:io';
import 'dart:math';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import '../video_page/video_view.dart';

class DownloadServiceView extends StatefulWidget {
  const DownloadServiceView({Key? key}) : super(key: key);

  @override
  State<DownloadServiceView> createState() => _DownloadServiceViewState();
}

class _DownloadServiceViewState extends State<DownloadServiceView> {
  CancelToken _cancelToken = CancelToken();

  @override
  void initState() {
    // TODO: implement initState

    downloadFileWithPercent();
    super.initState();
  }

  String downloadMessage = "Initializing...";
  double progress = 0;
  int downloadedBytes = 0;
  int totalBytes = 0;
  String deviceDirectory = '';
  String receivedBytes = '';
  String toByte = '';

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(8),
          topRight: Radius.circular(8),
        ),
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Container(
                margin: EdgeInsets.only(top: 40, left: 10),
                padding:
                    EdgeInsets.only(left: 14, right: 14, top: 8, bottom: 8),
                height: 40,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(0xffe9f4f3)),
                child: Text(
                  "Downloading",
                  style: TextStyle(
                      color: Color(0xff0db4fa), fontWeight: FontWeight.bold),
                ),
              ),
              Spacer(),
              Container(
                margin: EdgeInsets.only(top: 40, left: 10),
                padding:
                    EdgeInsets.only(left: 14, right: 14, top: 8, bottom: 8),
                height: 40,
                child: Text(downloadMessage),
              ),
            ],
          ),
          SizedBox(
            height: 5,
          ),
          InkWell(
            onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => ButterFlyAssetVideo(videoPath: deviceDirectory, fileName: 'fileName',),
                ),
              );
            },

            child: Container(
              margin: EdgeInsets.only(left: 10, right: 10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Color(0xffe9f4f3)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    child:
                        Image.asset("assets/content.webp", fit: BoxFit.fitWidth),
                    width: 100,
                    height: 60,
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text("Name Content"),
                  Spacer(),
                  InkWell(
                      onTap: () async {
                        _cancelToken.cancel("Download paused by user.");
                      },
                      child: Icon(Icons.stop_circle_outlined)),
                  SizedBox(
                    width: 20,
                  )
                ],
              ),
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10),
                child: SizedBox(
                  width: 250,
                  child: LinearProgressIndicator(
                    value: progress,
                    minHeight: 3,
                  ),
                ),
              ),
              Spacer(),
              Text(receivedBytes + " / "),
              Text(toByte),
            ],
          ),
        ],
      ),
    );
  }



  Future<String?> downloadFileWithPercent() async {
    try {
      // Create Dio instance and set up options
      final appDocDir = await getApplicationDocumentsDirectory();
      final fileTwo = File('${appDocDir.path}/fileName');
      deviceDirectory = "${fileTwo.path}";
      UtilsVariable.filePath= "${fileTwo.path}";
      UtilsVariable.savePathName= "${fileTwo.path}";
      Dio dio = Dio();
      dio.options.headers['user-agent'] = 'Dio';
      dio.options.followRedirects = true;
      dio.options.connectTimeout = Duration(seconds: 30); // 30 seconds
      dio.options.receiveTimeout = Duration(seconds: 30); // 30 seconds
      UtilsVariable.videoName='Sample ';
      UtilsVariable.videoSize='20.09';
      UtilsVariable.videoSize='2023.05.01';
      UtilsVariable.videoDuration='01:57';
      // Send a GET request with the specified URL
      Response response = await dio.get(

          "http://catenoid-sample.video.kr.kollus.com/kr/media-file.mp4?_s=bcd33378e14d44647772a692fa2079154fbab9feeff532e4f31c40974641006ebeff417761c90de0c018f953059a064b3fae15a065109d29d8331f43f8f7f0f3483164630295911901aa045a39c50683dd3c23231ff1c48a8663747f27fde76b8f4c495cc35d8599b6b6cbd7e0528a1db93467088031ada743ade3d4df3629e4d73525421ecb73b8a67f215182b0081b057f04f03b1ef5726677ba675ff7fb9bde28e30073d60855df7b9932f8d2e5df91b9beb8be5a8e242f4fc1543a4db802f348ada6ce2100240b022fe49255fe794fa0916efa5c9755f25047a2d9b578e690d82daa1737f7b384cfa764f57b0d34c2d0ddc611138dbdf34abeed25abdc7eced84ca2026991e347c8702eb3a9b1611f21f4f5b0e4de9b6f90b23b563919f60feeb6cab28962cdd2939c71a2525b97feb6d02ce86bc8f3608a730b6516a3b2c54685712217c15968182a041fe2dd33d4505e6959ae489bd273d2c60f256947e0021f7bc7aff622ac5569c0272bf2665ddb147c5d726d0178d9fe8ef1c6d90a&_ts=100000&_tr=100000&channelkey=9c0qcbqi",
          cancelToken: _cancelToken,
          onReceiveProgress: (received, total) {
        downloadedBytes = received;
        totalBytes = total;
        var percentage = downloadedBytes / totalBytes * 100;
        if (percentage < 100) {
          progress = percentage / 100;
          setState(() {
            downloadMessage = "Downloading... ${percentage.floor()} %";
          });
        } else {
          setState(() {
            downloadMessage = "Success ${percentage.floor()} %";
          });
        }

        // Calculate download progress
        if (total != -1) {

          print('Downloaded ${((received / total) * 100).toStringAsFixed(0)}%');
          print(
              'Downloaded ${_formatBytes(received)} of ${_formatBytes(total)}');
          receivedBytes = "${_formatBytes(received)}";
          toByte = "${_formatBytes(total)}";
          setState(() {});
          // print('Remaining time: ${_formatDuration(Duration(seconds: ((total - received) / (received / DateTime.now().difference(startTime).inSeconds)).floor()))}');
        }
      },
          options: Options(responseType: ResponseType.bytes,));

      // Write the response data to the specified file path
      File file = File(deviceDirectory);
      await file.writeAsBytes(response.data);
      Future.delayed(Duration.zero, () {
        this.popUp();
      });
      UtilsVariable.videoPath=deviceDirectory;

      var percentage = downloadedBytes / totalBytes * 100;
      // if (percentage > 99) {
      //   setState(() {
      //     downloadMessage = "Success ${percentage.floor()} %";
      //     Navigator.push(
      //       context,
      //       MaterialPageRoute(
      //         builder: (context) => ButterFlyAssetVideo(videoPath: deviceDirectory, fileName: 'fileName',),
      //       ),
      //     );
      //   });
      // }
      // Return the local file path
      return deviceDirectory;
    } catch (e) {
      print(e);
      return null;
    }
  }

  void popUp() async {
   Navigator.pop(context);
  }
  String _formatBytes(int bytes, [int decimals = 2]) {
    if (bytes <= 0) return '0 B';
    const suffixes = ['B', 'KB', 'MB', 'GB', 'TB'];
    var i = (log(bytes) / log(1024)).floor();
    return '${(bytes / pow(1024, i)).toStringAsFixed(decimals)} ${suffixes[i]}';
  }

  String _formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitSeconds = twoDigits(duration.inSeconds.remainder(60));
    return "${twoDigits(duration.inHours)}:$twoDigitMinutes:$twoDigitSeconds";
  }
}
