import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
class SortView extends StatefulWidget {
  const SortView({Key? key}) : super(key: key);

  @override
  State<SortView> createState() => _SortViewState();
}

class _SortViewState extends State<SortView> {
  late final SharedPreferences prefs;
  @override
  void initState() {
    // TODO: implement initState
    initialShare();
    super.initState();
  }
  void initialShare()async{
    prefs  = await SharedPreferences.getInstance();
  setState(() {

  });
  }


  @override
  Widget build(BuildContext context) {
    return  Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 16,right: 16),
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              Text(
                "Sort",
                style: TextStyle(
                    color: Colors.black54,
                    fontSize: 14,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 13,),
              InkWell(
                onTap: ()async{

                  prefs.setString("sortItem", "0");
                  UtilsVariable.sort= prefs.getString("sortItem");
                  print(UtilsVariable.sort);
                  setState(() {

                  });
                },
                child: Row(
                  children: [
                    Text("Name order",style: UtilsVariable.sort=="0"?CustomTextStyle.titleStyle:CustomTextStyle.subtitleTextStyle,),
                    Spacer(),
                    UtilsVariable.sort=="0"?
                    Icon(Icons.check):Text("")
                  ],
                ),
              ),
              SizedBox(height: 13,),
              InkWell(
                onTap: ()async{

                  prefs.setString("sortItem", "1");
                  UtilsVariable.sort= prefs.getString("sortItem");
                  print(UtilsVariable.sort);
                  setState(() {

                  });

                },
                child: Row(
                  children: [
                    Text("Date order",style: UtilsVariable.sort=="1"?CustomTextStyle.titleStyle:CustomTextStyle.subtitleTextStyle,),
                    Spacer(),
                    UtilsVariable.sort=="1"?
                    Icon(Icons.check):Text(""),

                  ],
                ),
              ),
              SizedBox(height: 13,),
              InkWell(
                onTap: ()async{

                  prefs.setString("sortItem", "2");
                  UtilsVariable.sort= prefs.getString("sortItem");
                  print(UtilsVariable.sort);
                  setState(() {

                  });

                },
                child: Row(
                  children: [
                    Text("File Size order",style: UtilsVariable.sort=="2"?CustomTextStyle.titleStyle:CustomTextStyle.subtitleTextStyle,),
                    Spacer(),
                    UtilsVariable.sort=="2"?
                    Icon(Icons.check):Text("")
                  ],
                ),
              ),

            ],
          ),
        ),
        SizedBox(height: 15,),
        Expanded(
          child: InkWell(

            child: Container(

              color: Colors.black12,
              child: Row(
                children: [
                  SizedBox(
                    width: 8,
                  ),
                  Icon(Icons.close),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Close",
                    style: TextStyle(
                        color: Colors
                            .grey,
                        fontWeight:
                        FontWeight
                            .w600),
                  )
                ],
              ),

            ),
            onTap: (){
              Navigator.pop(context);
            },
          ),
        ),
      ],
    );
  }
}
