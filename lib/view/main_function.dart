import 'package:easy_localization/easy_localization.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:system_info2/system_info2.dart';
import '../bloc/main_bloc/main_bloc.dart';
import '../bloc/switch_bloc/switch_bloc.dart';
import '../service/firebase/firebase_push_notification.dart';
import '../service/routes/app_routes.dart';
import '../service/routes/navigator_service.dart';
import '../utils/varuable_utils.dart';

class KollusPlayer extends StatefulWidget {
  static NavigationService navigationService = NavigationService();

  const KollusPlayer({Key? key}) : super(key: key);

  @override
  _KollusPlayerState createState() => _KollusPlayerState(navigationService);
}

class _KollusPlayerState extends State<KollusPlayer> {
  final NavigationService navigationService;

  _KollusPlayerState(this.navigationService);
  @override
  void initState() {
    // FirebaseMessaging.instance.getToken().then((value) {
    //   String? token = value;
    //   // print("token");
    //   // print(token);
    // });    // TODO: implement initState
    //
    getDiskSpaceInfo();
    super.initState();
  }

  static int megaByte = 1024 * 1024;
  void getDiskSpaceInfo() async{
    UtilsVariable.freeSpace=(SysInfo.getFreeVirtualMemory() ~/ megaByte/1000).toStringAsFixed(2);
    UtilsVariable.ID=SysInfo.userId;
    UtilsVariable.platformVersion=SysInfo.operatingSystemVersion;
    print(UtilsVariable.platformVersion);

    print('Total physical memory   '
        ': ${SysInfo.getTotalPhysicalMemory()~/ megaByte } MB');
  }


  // void _increment() async{
  //   String? fcmKey = await getPushToken();
  //   print("FCM key $fcmKey");
  // }
  @override
  Widget build(BuildContext context) {

    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => SwitchBloc()),
        BlocProvider(
          create: (context) => MainBloc(),
        )
      ],
      child: BlocBuilder<SwitchBloc, SwitchState>(
        builder: (contex, state) => MaterialApp(
          theme: ThemeData(
            // colorSchemeSeed: const Color(0xff5692ff),
            useMaterial3: false,

            popupMenuTheme: PopupMenuThemeData(
              color: Colors.white,
            ),
          ),
          title: 'Kollus Player',
          debugShowCheckedModeBanner: false,

          localizationsDelegates: context.localizationDelegates,
          supportedLocales: context.supportedLocales,
          locale: context.locale,
          // navigatorKey: navigatorKey,
          navigatorKey: navigationService.navigatorKey,
          // navigatorObservers: <NavigatorObserver>[KollusPlayer.application.observer],
          home: MainNavigator(),
        ),
      ),
    );
  }
}