import 'dart:convert';

import 'package:collus_player/model/video_local_model/video_model.dart';
import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/view/settings_page/settings_page.dart';
import 'package:collus_player/view/video_page/video_view.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RecentlyAdded extends StatefulWidget {
   RecentlyAdded({Key? key, required this.onRefresh}) : super(key: key);
  final VoidCallback onRefresh;
  @override
  State<RecentlyAdded> createState() => _RecentlyAddedState();
}

class _RecentlyAddedState extends State<RecentlyAdded> with TickerProviderStateMixin{
  List<VideoData> testMyList = [];
  @override
  void initState() {
    // TODO: implement initState
    loadFromSharedPreferences();
    super.initState();
  }
  Future<void> loadFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final myListString = prefs.getStringList('videoInfo') ?? [];

    setState(() {
      testMyList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return VideoData.fromJson(jsonMap);
      }).toList();
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading:IconButton(icon: Icon(Icons.arrow_back), onPressed: () {Navigator.pop(context);         widget.onRefresh(); },color: Colors.black,),
        backgroundColor: Colors.white,
        title: Row(
          children: [
            Text("Recently added files",style: TextStyle(color: Colors.black,fontSize: 20,fontWeight: FontWeight.w600),),
            Spacer(),
            InkWell(
                onTap: () {
                  showModalBottomSheet<void>(
                    transitionAnimationController: AnimationController(
                        duration: const Duration(milliseconds: 400),
                        vsync: this),
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    isScrollControlled: true,
                    backgroundColor: Color(0xfff7f7f7),
                    context: context,
                    builder: (BuildContext context) {
                      return FractionallySizedBox(
                          heightFactor: 0.9,
                          child: SettingsPage(
                            k: context,
                          ));
                    },
                  ).then((value) => {setState(() {})});
                },
                child: Icon(Icons.settings_outlined,color: Colors.black87,)),
            SizedBox(
              width: 5,
            ),



            Container(


              child: PopupMenuButton(
                offset: Offset(0, 30),
                itemBuilder: (BuildContext context) => [
                  PopupMenuItem(
                    child: Text(
                      'Delete',
                      style: TextStyle(fontSize: 14),
                    ),
                    value: 1,
                  ),
                  PopupMenuItem(
                    child: Text(
                      'Move',
                      style: TextStyle(fontSize: 14),
                    ),
                    value: 2,
                  ),
                  PopupMenuItem(
                    onTap: () {


                    },
                    child: Text(
                      'Create folder',
                      style: TextStyle(fontSize: 14),
                    ),
                    value: 3,
                  ),
                ],
                child: Icon(Icons.more_vert,size: 26,color: Colors.black87,),

              ),
            ),

          ],
        ),
        elevation: 0,
      ),
      body: Column(
          children: List.generate(
            testMyList.length,
                (index) =>
                InkWell(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ButterFlyAssetVideo(
                          // videoPath: UtilsVariable.filePath,
                          videoPath: testMyList[index].videoPath,

                          fileName: testMyList[index].name.toString(),
                          // videoUrl: movie.videoUrl,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.only(left: 10, right: 10,bottom: 8),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8),
                        color: Color(0xffe9f4f3)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Stack(
                          children: [
                            Container(
                              child: Image.asset("assets/first_pic.png",
                                  fit: BoxFit.fitWidth),
                              width: 100,
                              height: 75,
                            ),
                            Container(
                              margin: EdgeInsets.only(left: 60, top: 53),
                              width: 35,
                              height: 15,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(4),
                                  color: Colors.black87),
                              child: Center(
                                  child: Text(
                                    testMyList[index].videoDuration,
                                    style: TextStyle(color: Colors.white, fontSize: 10),
                                  )),
                            )
                          ],
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Container(
                            width: 200,
                            child: Text(testMyList[index].name)),


                        Spacer(),

                        PopupMenuButton(
                          offset: Offset(0, 30),
                          itemBuilder: (BuildContext context) => [
                            PopupMenuItem(
                              onTap: (){
                                deleteVideoPath(context,testMyList[index].name);
                              },
                              child: Text(
                                'Delete',
                                style: TextStyle(fontSize: 14),
                              ),
                              value: 1,
                            ),
                            PopupMenuItem(
                              child: Text(
                                'Detail Information',
                                style: TextStyle(fontSize: 14),
                              ),
                              value: 2,
                            ),

                          ],
                          child: Icon(Icons.more_vert),
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 20,
                        )
                      ],
                    ),
                  ),
                ),)

      ),
    );
  }
  void deleteVideoPath(BuildContext context,String id) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.white,
            elevation: 0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(height: 20,),
                Text("Are you sure want to delete?",style: CustomTextStyle.titleStyle,),
                SizedBox(height: 20,),

                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      Expanded(child: Container()),
                      Container(
                          height: 40,
                          child: Center(
                              child: InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("Cancel")))),
                      SizedBox(
                        width: 20,
                      ),
                      Spacer(),
                      InkWell(
                        onTap: () {
                          deleteVideoInfo(id);
                          Navigator.pop(context);
                        },
                        child: Container(
                            padding: EdgeInsets.only(left: 40, right: 40),
                            height: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.red.shade50,
                            ),
                            child: Center(
                                child: Text(
                                  "Delete",
                                  style: TextStyle(color: Colors.red,fontWeight: FontWeight.bold),
                                ))),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20,)
              ],
            ),
          );
        }).then((value) => {setState(() {})});
  }
  Future<void> deleteVideoInfo(String id) async {
    setState(() {
      deleteById(testMyList,id);
      print("testMyList.length");
      print(testMyList.length);
    });

  }

  void deleteById(List<VideoData> myList, String id) {
    myList.removeWhere((item) => item.name == id);
    SharedPreferences.getInstance().then((prefs) {
      final myListString = myList.map((item) => jsonEncode(item.toJson())).toList();
      prefs.setStringList('videoInfo', myListString);
    });
  }

}
