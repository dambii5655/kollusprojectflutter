import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/service_view/functions.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FolderView extends StatefulWidget {
  final VoidCallback onRefresh;

  FolderView({Key? key, required this.onRefresh}) : super(key: key);

  @override
  State<FolderView> createState() => _FolderViewState();
}

class _FolderViewState extends State<FolderView> {
  @override
  void initState() {
    loadItems();
    super.initState();
  }

  void updateAllUnchecked() {
    setState(() {
      UtilsVariable.allUnchecked = UtilsVariable.FoldersBoolList.every(
          (isChecked) => isChecked == false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: List.generate(FunctionOfMainPage.folderList.length, (index) {
      return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          children: [
            UtilsVariable.deleteAllItems
                ? Checkbox(
                    value: UtilsVariable.FoldersBoolList[index],
                    onChanged: (value) {
                      setState(() {
                        UtilsVariable.FoldersBoolList[index] = value ?? false;


                        UtilsVariable.selectAll = UtilsVariable.FoldersBoolList.every(
                                (value) => value == true);
                              UtilsVariable.selectAllFolder=UtilsVariable.FoldersBoolList.every(
                                      (value) => value == true);
                        updateAllUnchecked();
                      });
                      // if(UtilsVariable.selectAllLocalVideo==true){
                        widget.onRefresh();

                      // }
                      // setState(() {});
                    })
                : Container(),
            Container(
                padding: EdgeInsets.all(6),
                margin: EdgeInsets.all(6),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8),
                  color: Color(0xffCDE8E8FF),
                ),
                child: Icon(
                  Icons.folder_open,
                  color: Colors.grey,
                  size: 35,
                )),
            Text(FunctionOfMainPage.folderList[index]),
            Spacer(),
            UtilsVariable.deleteAllItems
                ? Container()
                : PopupMenuButton(
                    offset: Offset(0, 30),
                    itemBuilder: (BuildContext context) => [
                      PopupMenuItem(
                        onTap: () {
                          deleteFolder(context, index);
                        },
                        child: Text(
                          'Delete',
                          style: TextStyle(fontSize: 14),
                        ),
                        value: 1,
                      ),
                      PopupMenuItem(
                        child: Text(
                          'Detail Information',
                          style: TextStyle(fontSize: 14),
                        ),
                        value: 2,
                      ),
                    ],
                    child: Icon(Icons.more_vert),
                    color: Colors.white,
                  ),
          ],
        ),
      );
    }));
  }

  void deleteFolder(BuildContext context, int id) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.white,
            elevation: 0,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                SizedBox(
                  height: 20,
                ),
                Text(
                  "Are you sure want to delete?",
                  style: CustomTextStyle.titleStyle,
                ),
                SizedBox(
                  height: 20,
                ),
                Padding(
                  padding: const EdgeInsets.only(left: 20, right: 20),
                  child: Row(
                    children: [
                      Expanded(child: Container()),
                      Container(
                          height: 40,
                          child: Center(
                              child: InkWell(
                                  onTap: () {
                                    Navigator.pop(context);
                                  },
                                  child: Text("Cancel")))),
                      SizedBox(
                        width: 20,
                      ),
                      Spacer(),
                      InkWell(
                        onTap: () {
                          deleteFolderById(id);
                          Navigator.pop(context);
                        },
                        child: Container(
                            padding: EdgeInsets.only(left: 40, right: 40),
                            height: 40,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(4),
                              color: Colors.red.shade50,
                            ),
                            child: Center(
                                child: Text(
                              "Delete",
                              style: TextStyle(
                                  color: Colors.red,
                                  fontWeight: FontWeight.bold),
                            ))),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 20,
                )
              ],
            ),
          );
        }).then((value) => {setState(() {})});
  }

  Future<void> deleteFolderById(int id) async {
    setState(() {
      FunctionOfMainPage.folderList.removeAt(id);
    });
    await FunctionOfMainPage.saveItems();
  }

  void loadItems() async {
    final prefs = await SharedPreferences.getInstance();
    final items = prefs.getStringList('folderList') ?? [];
    print("folderList.length");
    print(items.length);

    UtilsVariable.FoldersBoolList =
        List.generate(items.length, (index) => index % 1 == 1);



    print(UtilsVariable.MargeBoolList.length);

    setState(() {
      FunctionOfMainPage.folderList
          .addAll(items); // Add the saved items to the list
    });
  }
}
