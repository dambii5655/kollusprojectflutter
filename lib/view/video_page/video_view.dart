import 'dart:io';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/vertical_video/change_vertical.dart';
import 'package:collus_player/view/video_page/size_video/default_screen.dart';
import 'package:collus_player/view/video_page/size_video/full_screen.dart';
import 'package:collus_player/view/video_page/size_video/zoom_screen.dart';
import 'package:collus_player/view/video_page/upper_part.dart';
import 'package:collus_player/view/video_page/video_controller/forward_controller.dart';
import 'package:collus_player/view/video_page/video_controller/forward_duration_controller.dart';
import 'package:collus_player/view/video_page/video_controller/forward_horizontal_view.dart';
import 'package:collus_player/view/video_page/video_controller/slide_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:subtitle_wrapper_package/subtitle_wrapper_package.dart';
import 'package:video_player/video_player.dart';
class ButterFlyAssetVideo extends StatefulWidget {
  final String? videoPath;
  final String? videoUrl;
  final String fileName;

  ButterFlyAssetVideo({required this.videoPath, required this.fileName, this.videoUrl});

  @override
  ButterFlyAssetVideoState createState() => ButterFlyAssetVideoState();
}

class ButterFlyAssetVideoState extends State<ButterFlyAssetVideo> {
  late VideoPlayerController _controller;
  late SubtitleController _subtitleController;
  bool fullScreen =false;
  bool defaultScreen =false;
  @override
  void initState() {
    super.initState();

    UtilsVariable. showUpperPart=false;
    if(widget.videoPath==''){
      _controller = VideoPlayerController.network(
         widget.videoUrl.toString(),
          videoPlayerOptions: VideoPlayerOptions(
              allowBackgroundPlayback: UtilsVariable.switchBackground))
        ..addListener(() {
          setState(() {
            _value = _controller.value.position.inSeconds.toDouble();

          });
        })
        ..initialize().then((_) {
          setState(() {});
        });
    }
    else{
      _controller = VideoPlayerController.file(
          File(
            widget.videoPath!,
          ),
          videoPlayerOptions: VideoPlayerOptions(
              allowBackgroundPlayback: UtilsVariable.switchBackground))
        ..addListener(() {
          setState(() {
            _value = _controller.value.position.inSeconds.toDouble();

          });
        })
        ..initialize().then((_) {
          setState(() {});
        });
    }
    _subtitleController = SubtitleController(
      subtitleUrl: "https://pastebin.com/raw/ZWWAL7fK",
      subtitleType: SubtitleType.webvtt,


    );
    _value = 0.0;

    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize().then((_) => setState(() {}));
    _controller.play();
    getForward();
    changeScreen();

  }

  void changeScreen(){
    UtilsVariable.isFullScreen = !UtilsVariable.isFullScreen;
    UtilsVariable.forwardSize=true;
    UtilsVariable. showUpperPart=true;
    SystemChrome
        .setEnabledSystemUIMode(
        SystemUiMode
            .manual,
        overlays:
        SystemUiOverlay
            .values);
    SystemChrome
        .setPreferredOrientations([
      DeviceOrientation
          .landscapeLeft,
      DeviceOrientation
          .landscapeRight,
    ]);
    setState(() {

    });
  }

  void getForward() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.getInt("forward_dur");
    UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
    print("UtilsVariable.forward_duration");
    print(UtilsVariable.forward_duration);
  }

  @override
  void dispose() {
    _controller.dispose();
    UtilsVariable.bigScreen=false;
    UtilsVariable.isFullScreen=false;
    UtilsVariable.fullScreen=false;
    moveRange();
    UtilsVariable.forwardSize=false;
    super.dispose();
  }

  late double _value;
  bool showFunction = false;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        showFunction = !showFunction;
        setState(() {});
      },
      child: Scaffold(
        backgroundColor: Colors.black54,
        endDrawer: Drawer(child: HorizontalView(),),
        body:OrientationBuilder(
          builder: (context, orientation) {
            return

              Stack(children: [
                (UtilsVariable.fullScreen)?
                FullScreen(controller: _controller, value: _value, showFunction: showFunction,):
                UtilsVariable.bigScreen?
                ZoomScreen(controller: _controller, value: _value, fileName: widget.fileName, showFunction: showFunction,):
                Stack(
                  fit: orientation == Orientation.landscape
                      ? StackFit.expand
                      : StackFit.loose,
                  children: [
                    DefaultScreen(controller: _controller, fileName: widget.fileName, value: _value, showFunction: showFunction, subtitleController: _subtitleController,),
                    // SubtitleWrapper(videoChild: Text(''),
                    //
                    //   subtitleController: _subtitleController, videoPlayerController: _controller,subtitleStyle: SubtitleStyle(
                    //       textColor: Colors.white
                    //   ),),
                  ],
                ),
                UtilsVariable. showUpperPart==false?
                Padding(
                  padding:  EdgeInsets.only(top:UtilsVariable.forwardSize==true?0: 35),
                  child: (showFunction&&UtilsVariable.animated==false)?Row(
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      InkWell(
                          onTap: () {
                            moveRange();
                          },
                          child: Icon(
                            Icons.close,
                            color: Colors.white,
                          )),
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        widget.fileName,
                        style: TextStyle(color: Colors.white),
                      ),
                      Spacer(),
                      (UtilsVariable.fullScreen==false&&UtilsVariable.bigScreen==false)?
                      InkWell(
                        onTap: (){
                          UtilsVariable.bigScreen=false;
                          UtilsVariable.fullScreen=true;
                          setState(() {

                          });
                        },
                        child: Container(
                            width: 40,
                            height: 40,
                            child: Image.asset("assets/full_screen.png")),
                      ):
                      (UtilsVariable.bigScreen==false&&UtilsVariable.fullScreen==true)?
                      InkWell(

                        onTap: (){
                          UtilsVariable.bigScreen=true;
                          UtilsVariable.fullScreen=false;
                          print("1");

                          setState(() {

                          });
                        },
                        child: Container(
                          width: 40,
                          height: 40,
                          child: Icon(
                            Icons.zoom_out_map_rounded,
                            color: Colors.white,
                            size: 14,
                          ),
                        ),
                      ):
                      InkWell(

                        onTap: (){
                          UtilsVariable.bigScreen=false;
                          UtilsVariable.fullScreen=false;
                          print("--------------");
                          setState(() {

                          });
                        },
                        child: Container(
                          width: 40,
                          height: 40,
                          child: Icon(
                            Icons.fit_screen_outlined,
                            color: Colors.white,
                            size: 14,
                          ),
                        ),
                      ),

                      InkWell(
                        onTap: () {
                          showModalBottomSheet<void>(
                            shape: RoundedRectangleBorder(
                              borderRadius:
                              BorderRadius.circular(0),
                            ),
                            backgroundColor: Color(0xfff7f7f7),
                            context: context,
                            builder: (BuildContext context) {
                              return ChangeVertical();
                            },
                          ).then((value) => {setState(() {})});
                        },
                        child: Icon(
                          Icons.more_vert,
                          color: Colors.white,
                          size: 18,
                        ),
                      ),
                      SizedBox(
                        width: 10,
                      )
                    ],
                  ):Text(""),
                ):Text("",style: TextStyle(),),



              ],);

          },
        ),






      ),
    );
  }
void moveRange(){
  UtilsVariable.forwardSize = false;
  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
      overlays: SystemUiOverlay.values);
  SystemChrome.setPreferredOrientations([
    DeviceOrientation.portraitUp,
  ]);
  UtilsVariable.isFullScreen = false;
  Navigator.pop(context);
}
}
