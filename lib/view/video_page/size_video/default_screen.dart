import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/vertical_video/change_vertical.dart';
import 'package:collus_player/view/video_page/upper_part.dart';
import 'package:collus_player/view/video_page/vertical_video/vertical_screen.dart';
import 'package:collus_player/view/video_page/video_controller/forward_duration_controller.dart';
import 'package:collus_player/view/video_page/video_controller/slide_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:subtitle_wrapper_package/data/models/style/subtitle_style.dart';
import 'package:subtitle_wrapper_package/subtitle_wrapper_package.dart';
import 'package:video_player/video_player.dart';

import '../video_controller/forward_controller.dart';

class DefaultScreen extends StatefulWidget {
  DefaultScreen(
      {Key? key,
      required this.controller,
      required this.fileName,
      required this.value,
      required this.showFunction, required this.subtitleController})
      : super(key: key);
  final VideoPlayerController controller;
  final String fileName;
  final double value;
  final bool showFunction;
  final SubtitleController subtitleController;

  @override
  State<DefaultScreen> createState() => _DefaultScreenState();
}


class _DefaultScreenState extends State<DefaultScreen> {
  double speedPlay = 1;
  bool half = false;
  bool one = false;
  bool oneHalf = false;
  bool two = false;

  @override
  Widget build(BuildContext context) {
    return !UtilsVariable.isFullScreen
        ? Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(height: 65),
                Expanded(child: Container()),
                InteractiveViewer(
                  child: AspectRatio(
                    aspectRatio: widget.controller.value.aspectRatio,
                    child: Stack(
                      alignment: Alignment.bottomCenter,
                      children: <Widget>[
                        VideoPlayer(widget.controller),
                        (widget.showFunction&&UtilsVariable.animated==false)?
                        ForwardController(
                          controller: widget.controller,
                          showFunction: widget.showFunction,
                        ):Container(),
                      ],
                    ),
                  ),
                ),

                Container(
                  color: Colors.greenAccent,
                  child: SubtitleWrapper(videoChild: Text(''),

                    subtitleController: widget.subtitleController, videoPlayerController: widget.controller,subtitleStyle: SubtitleStyle(
                        textColor: Colors.red
                    ),),
                ),
                Expanded(child: Container()),
                (widget.showFunction&&UtilsVariable.animated==false)
                    ? Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          InkWell(
                              onTap: () {
                                if (widget.controller.value.playbackSpeed >
                                    0.6) {
                                  widget.controller.setPlaybackSpeed(
                                      widget.controller.value.playbackSpeed -
                                          0.1);
                                  speedPlay = double.parse(widget
                                      .controller.value.playbackSpeed
                                      .toString());
                                  print(widget.controller.value.playbackSpeed);
                                  setState(() {});
                                }
                              },
                              child: Icon(
                                Icons.remove,
                                color: Colors.white,
                                size: 20,
                              )),
                          SizedBox(
                            width: 40,
                          ),
                          Column(
                            children: [
                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                      shape: RoundedRectangleBorder(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                      // isDismissible: false,
                                      enableDrag: true,
                                      isScrollControlled: false,
                                      backgroundColor: Colors.black,
                                      context: context,
                                      builder: (BuildContext c) {
                                        return Container(
                                          color: Color(0xff151618),
                                          child: FractionallySizedBox(
                                            heightFactor: 0.4,
                                            child: StatefulBuilder(
                                              builder: (BuildContext context,
                                                  void Function(void Function())
                                                      setState) {
                                                return Column(
                                                  children: [
                                                    SizedBox(
                                                      height: 40,
                                                    ),
                                                    SliderTheme(
                                                      data: SliderThemeData(
                                                        showValueIndicator:
                                                            ShowValueIndicator
                                                                .always,
                                                        valueIndicatorShape:
                                                            RectangularSliderValueIndicatorShape(),
                                                        valueIndicatorColor:
                                                            Colors.transparent,
                                                        valueIndicatorTextStyle:
                                                            TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontSize: 14,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold),
                                                      ),
                                                      child: Column(
                                                        children: [
                                                          Padding(
                                                            padding:
                                                                const EdgeInsets
                                                                        .only(
                                                                    left: 10,
                                                                    right: 10),
                                                            child: Container(
                                                              child: Slider(
                                                                label: speedPlay
                                                                        .toStringAsFixed(
                                                                            1) +
                                                                    "x",
                                                                activeColor:
                                                                    Colors
                                                                        .white,
                                                                value:
                                                                    speedPlay,
                                                                min: 0.5,
                                                                max: 2,
                                                                onChanged:
                                                                    (value) {
                                                                  if (value ==
                                                                      0.5) {
                                                                    half = true;
                                                                    one = false;
                                                                    oneHalf =
                                                                        false;
                                                                    two = false;
                                                                    setState(
                                                                        () {});
                                                                  }
                                                                  if (value >
                                                                          1 &&
                                                                      value <
                                                                          1.1) {
                                                                    half =
                                                                        false;
                                                                    one = true;
                                                                    oneHalf =
                                                                        false;
                                                                    two = false;
                                                                    setState(
                                                                        () {});
                                                                  } else {
                                                                    half =
                                                                        false;
                                                                    one = false;
                                                                    oneHalf =
                                                                        false;
                                                                    two = false;
                                                                    setState(
                                                                        () {});
                                                                  }
                                                                  if (value >
                                                                          1.5 &&
                                                                      value <
                                                                          1.6) {
                                                                    half =
                                                                        false;
                                                                    one = false;
                                                                    oneHalf =
                                                                        true;
                                                                    two = false;
                                                                    setState(
                                                                        () {});
                                                                  }
                                                                  if (value ==
                                                                      2) {
                                                                    half =
                                                                        false;
                                                                    one = false;
                                                                    oneHalf =
                                                                        false;
                                                                    two = true;

                                                                    setState(
                                                                        () {});
                                                                  }
                                                                  setState(() {
                                                                    speedPlay =
                                                                        value;
                                                                    widget
                                                                        .controller
                                                                        .setPlaybackSpeed(
                                                                            speedPlay);
                                                                  });
                                                                },
                                                              ),
                                                            ),
                                                          ),
                                                          Row(
                                                            mainAxisAlignment:
                                                                MainAxisAlignment
                                                                    .end,
                                                            crossAxisAlignment:
                                                                CrossAxisAlignment
                                                                    .end,
                                                            children: [
                                                              Text(
                                                                'Speed: ' +
                                                                    widget
                                                                        .controller
                                                                        .value
                                                                        .playbackSpeed
                                                                        .toStringAsFixed(
                                                                            1) +
                                                                    " x",
                                                                style: TextStyle(
                                                                    color: Colors
                                                                        .white),
                                                              ),
                                                              SizedBox(
                                                                width: 30,
                                                              )
                                                            ],
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    SizedBox(
                                                      height: 20,
                                                    ),
                                                    Row(
                                                      mainAxisAlignment:
                                                          MainAxisAlignment
                                                              .spaceAround,
                                                      children: [
                                                        InkWell(
                                                          child: Text(
                                                            "0.5x",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight: half
                                                                    ? FontWeight
                                                                        .bold
                                                                    : FontWeight
                                                                        .w500,
                                                                fontSize: half
                                                                    ? 16
                                                                    : 14),
                                                          ),
                                                          onTap: () {
                                                            half = true;
                                                            one = false;
                                                            oneHalf = false;
                                                            two = false;
                                                            widget.controller
                                                                .setPlaybackSpeed(
                                                                    0.5);
                                                            speedPlay = 0.5;
                                                            setState(() {});
                                                          },
                                                        ),
                                                        InkWell(
                                                          child: Text(
                                                            "1x",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight: one
                                                                    ? FontWeight
                                                                        .bold
                                                                    : FontWeight
                                                                        .w500,
                                                                fontSize: one
                                                                    ? 16
                                                                    : 14),
                                                          ),
                                                          onTap: () {
                                                            half = false;
                                                            one = true;
                                                            oneHalf = false;
                                                            two = false;
                                                            widget.controller
                                                                .setPlaybackSpeed(
                                                                    1);
                                                            speedPlay = 1;
                                                            setState(() {});
                                                          },
                                                        ),
                                                        InkWell(
                                                          child: Text(
                                                            "1.5x",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight: oneHalf
                                                                    ? FontWeight
                                                                        .bold
                                                                    : FontWeight
                                                                        .w500,
                                                                fontSize:
                                                                    oneHalf
                                                                        ? 16
                                                                        : 14),
                                                          ),
                                                          onTap: () {
                                                            half = false;
                                                            one = false;
                                                            oneHalf = true;
                                                            two = false;
                                                            widget.controller
                                                                .setPlaybackSpeed(
                                                                    1.5);
                                                            speedPlay = 1.5;
                                                            setState(() {});
                                                          },
                                                        ),
                                                        InkWell(
                                                          child: Text(
                                                            "2x",
                                                            style: TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight: two
                                                                    ? FontWeight
                                                                        .bold
                                                                    : FontWeight
                                                                        .w500,
                                                                fontSize: two
                                                                    ? 16
                                                                    : 14),
                                                          ),
                                                          onTap: () {
                                                            half = false;
                                                            one = false;
                                                            oneHalf = false;
                                                            two = true;
                                                            widget.controller
                                                                .setPlaybackSpeed(
                                                                    2);
                                                            speedPlay = 2;
                                                            setState(() {});
                                                          },
                                                        ),
                                                      ],
                                                    )
                                                  ],
                                                );
                                              },
                                            ),
                                          ),
                                        );
                                      });
                                },
                                child: Container(
                                    width: 20,
                                    child: Image.asset("assets/speed.png")),
                              ),
                              Text(
                                widget.controller.value.playbackSpeed
                                    .toStringAsFixed(1),
                                style: TextStyle(color: Colors.white),
                              ),
                            ],
                          ),
                          SizedBox(
                            width: 40,
                          ),
                          InkWell(
                              onTap: () {
                                if (widget.controller.value.playbackSpeed <
                                    1.9) {
                                  widget.controller.setPlaybackSpeed(
                                      widget.controller.value.playbackSpeed +
                                          0.1);
                                  speedPlay = double.parse(widget
                                      .controller.value.playbackSpeed
                                      .toString());
                                  print(widget.controller.value.playbackSpeed);
                                  setState(() {});
                                }
                              },
                              child: Icon(
                                Icons.add,
                                color: Colors.white,
                                size: 20,
                              )),
                        ],
                      )
                    : Container(
                        width: 0,
                        height: 40,
                      ),
                (widget.showFunction&&UtilsVariable.animated==false)
                    ? Row(
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Text(
                            widget.controller.value.position
                                    .toString()
                                    .replaceAll("0:", '')
                                    .split('.')[0] +
                                ' / ' +
                                widget.controller.value.duration
                                    .toString()
                                    .replaceAll("0:", '')
                                    .split('.')[0],
                            style: TextStyle(
                              color: Colors.white,
                            ),
                          ),
                        ],
                      )
                    : Row(
                        children: [
                          SizedBox(
                            width: 20,
                          ),
                          Text(""),
                        ],
                      ),
                (widget.showFunction&&UtilsVariable.animated==false)
                    ? SlideController(
                        controller: widget.controller, value: widget.value)
                    : Container(
                        height: 40,
                      ),
                widget.showFunction?
                AnimatedContainer(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    color: Color(0xff151618),
                  ),
                  width: UtilsVariable.animated?150:250,
                  height: 40,
                  duration: Duration(milliseconds: 200),
                  child: Row(
                    mainAxisAlignment: UtilsVariable.animated?MainAxisAlignment.center: MainAxisAlignment.spaceEvenly,
                    children: [
                      InkWell(


                        child: Icon(
                          Icons.lock_open_outlined,
                          color: Colors.white,
                        ),
                        onTap: (){UtilsVariable.animated=!UtilsVariable.animated;setState(() {

                        });},
                      ),
                      UtilsVariable.animated?InkWell(
                           onTap: (){UtilsVariable.animated=!UtilsVariable.animated;setState(() {

                           });},
                           child: Text("Tap to unlock",style: TextStyle(color: Colors.white),)):
                      Icon(
                        Icons.repeat_one,
                        color: Colors.white,
                      ),
                      UtilsVariable.animated?Container():  Image.asset("assets/img.png"),
                      UtilsVariable.animated?Container():Icon(
                        Icons.star_border,
                        color: Colors.white,
                      ),
                    ],
                  ),
                ):
                Container(height: 36,),
                SizedBox(
                  height: 30,
                ),
              ],
            ),
          )
        : VerticalScreen(
            controller: widget.controller,
            fileName: widget.fileName,
            value: widget.value,
            showFunction: widget.showFunction,
          );
  }

  void moveRange() {
    UtilsVariable.forwardSize = false;
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    UtilsVariable.isFullScreen = false;
    Navigator.pop(context);
  }
}
