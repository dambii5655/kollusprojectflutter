import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/upper_part.dart';
import 'package:collus_player/view/video_page/video_controller/forward_controller.dart';
import 'package:collus_player/view/video_page/video_controller/slide_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';

import '../video_controller/forward_duration_controller.dart';
class ZoomScreen extends StatefulWidget {
  ZoomScreen({Key? key, required this.controller, required this.value, required this.fileName, required this.showFunction}) : super(key: key);
  final VideoPlayerController controller;
  final String fileName;
  final bool showFunction;
  final double value;


  @override
  State<ZoomScreen> createState() => _ZoomScreenState();
}

class _ZoomScreenState extends State<ZoomScreen> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: double.infinity,
          child: Transform.scale(
            scale: 1.7,
            child: AspectRatio(
              aspectRatio: widget.controller.value.aspectRatio,
              child: VideoPlayer(widget.controller),
            ),
          ),
        ),

        ForwardController(
          controller: widget.controller,
          showFunction: widget.showFunction,
        ),
        widget.showFunction?
        Column(mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            Padding(
              padding: const EdgeInsets.only(bottom: 40),
              child: Row(
                children: [
                  SizedBox(width: 20,),
                  Text(
                    widget.controller.value.position
                        .toString().replaceAll("0:", '')
                        .split('.')[0] +
                        ' / ' +
                        widget.controller.value.duration
                            .toString().replaceAll("0:", '')
                            .split('.')[0],
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30,)

          ],
        ):
        Row(
          children: [
            SizedBox(width: 20,),
            Text(
                ""
            ),
          ],
        ),
        widget.showFunction?
        Column(
          mainAxisAlignment: MainAxisAlignment.end,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: [
            SlideController(
                controller: widget.controller, value: widget.value),
            SizedBox(height: 60,)
          ],
        ):Container(height: 40,),
      ],
    );
  }
  void moveRange(){
    UtilsVariable.forwardSize = false;
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    UtilsVariable.isFullScreen = false;
    Navigator.pop(context);
  }
}
