import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/video_controller/forward_duration_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
class UpperPartVideo extends StatefulWidget {
   UpperPartVideo({Key? key, required this.fileName, }) : super(key: key);
  final String fileName ;


  @override
  State<UpperPartVideo> createState() => _UpperPartVideoState();
}

class _UpperPartVideoState extends State<UpperPartVideo> {
  @override
  Widget build(BuildContext context) {
    return  Padding(
      padding:  EdgeInsets.only(top:UtilsVariable.forwardSize==true?0: 30),
      child: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          InkWell(
              onTap: () {
                moveRange();
              },
              child: Icon(
                Icons.close,
                color: Colors.white,
              )),
          SizedBox(
            width: 20,
          ),
          Text(
            widget.fileName,
            style: TextStyle(color: Colors.white),
          ),
          Spacer(),



          (UtilsVariable.fullScreen==false&&UtilsVariable.bigScreen==false)?
          InkWell(
            onTap: (){
              UtilsVariable.bigScreen=false;
              UtilsVariable.fullScreen=true;
              setState(() {

              });
            },
            child: Container(
                width: 40,
                height: 40,
                child: Image.asset("assets/full_screen.png")),
          ):
          (UtilsVariable.bigScreen==false&&UtilsVariable.fullScreen==true)?
          InkWell(

            onTap: (){
              UtilsVariable.bigScreen=true;
              UtilsVariable.fullScreen=false;
              print("1");

              setState(() {

              });
            },
            child: Container(
              width: 40,
              height: 40,
              child: Icon(
                Icons.zoom_out_map_rounded,
                color: Colors.white,
                size: 14,
              ),
            ),
          ):
          InkWell(

            onTap: (){
              UtilsVariable.bigScreen=false;
              UtilsVariable.fullScreen=false;

              setState(() {

              });
            },
            child: Container(
              width: 40,
              height: 40,
              child: Icon(
                Icons.fit_screen_outlined,
                color: Colors.white,
                size: 14,
              ),
            ),
          ),

          InkWell(
            onTap: () {
              showModalBottomSheet<void>(
                shape: RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius.circular(0),
                ),
                backgroundColor: Color(0xfff7f7f7),
                context: context,
                builder: (BuildContext context) {
                  return Container(
                    height: 200,
                    width: double.infinity,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        Text(
                          "Setting",
                          style: TextStyle(
                              color: Colors.grey,
                              fontWeight:
                              FontWeight.w600),
                        ),
                        InkWell(
                          onTap: () {
                            UtilsVariable.isFullScreen = !UtilsVariable.isFullScreen;
                            UtilsVariable.forwardSize=true;
                            SystemChrome
                                .setEnabledSystemUIMode(
                                SystemUiMode
                                    .manual,
                                overlays:
                                SystemUiOverlay
                                    .values);
                            SystemChrome
                                .setPreferredOrientations([
                              DeviceOrientation
                                  .landscapeLeft,
                              DeviceOrientation
                                  .landscapeRight,
                            ]);
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding:
                            const EdgeInsets
                                .all(8.0),
                            child: Row(
                              children: [
                                Text(
                                  "Screen rotation",
                                  style: TextStyle(
                                      color: Colors
                                          .black,
                                      fontWeight:
                                      FontWeight
                                          .bold),
                                ),
                                Spacer(),
                                Text("Vertical")
                              ],
                            ),
                          ),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);

                            showModalBottomSheet<
                                void>(
                              shape:
                              RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius
                                    .circular(
                                    0),
                              ),
                              // isScrollControlled: true,
                              backgroundColor:
                              Color(0xfff7f7f7),
                              context: context,
                              builder: (BuildContext
                              context) {
                                return ForwardView();
                              },
                            ).then((value) =>
                            {setState(() {})});
                          },
                          child: Padding(
                            padding:
                            const EdgeInsets
                                .all(8.0),
                            child: Row(
                              children: [
                                Text(
                                  "Moving range",
                                  style: TextStyle(
                                      color: Colors
                                          .black,
                                      fontWeight:
                                      FontWeight
                                          .bold),
                                ),
                                Spacer(),
                                Text("Vertical")
                              ],
                            ),
                          ),
                        ),
                        Spacer(),
                        Container(
                          height: 1,
                          width: double.infinity,
                          color:
                          Color(0xff1C1C1CDD),
                        ),
                        InkWell(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 80,
                            color: Colors.black12,
                            child: Row(
                              children: [
                                SizedBox(
                                  width: 8,
                                ),
                                Icon(Icons.close),
                                SizedBox(
                                  width: 10,
                                ),
                                Text(
                                  "Close",
                                  style: TextStyle(
                                      color: Colors
                                          .grey,
                                      fontWeight:
                                      FontWeight
                                          .w600),
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  );
                },
              ).then((value) => {setState(() {})});
            },
            child: Icon(
              Icons.more_vert,
              color: Colors.white,
              size: 18,
            ),
          ),
          SizedBox(
            width: 10,
          )
        ],
      ),
    );
  }
  void moveRange(){
    UtilsVariable.forwardSize = false;
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    UtilsVariable.isFullScreen = false;
    Navigator.pop(context);
  }
}
