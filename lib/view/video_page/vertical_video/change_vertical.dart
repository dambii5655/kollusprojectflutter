import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/video_controller/forward_duration_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
class ChangeVertical extends StatefulWidget {
  const ChangeVertical({Key? key}) : super(key: key);

  @override
  State<ChangeVertical> createState() => _ChangeVerticalState();
}

class _ChangeVerticalState extends State<ChangeVertical> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 200,
      width: double.infinity,
      child: Column(
        children: [
          SizedBox(
            height: 10,
          ),
          Text(
            "Setting",
            style: TextStyle(
                color: Colors.grey,
                fontWeight:
                FontWeight.w600),
          ),
          InkWell(
            onTap: () {
              UtilsVariable.isFullScreen = !UtilsVariable.isFullScreen;
              UtilsVariable.forwardSize=true;
              UtilsVariable. showUpperPart=true;
              SystemChrome
                  .setEnabledSystemUIMode(
                  SystemUiMode
                      .manual,
                  overlays:
                  SystemUiOverlay
                      .values);
              SystemChrome
                  .setPreferredOrientations([
                DeviceOrientation
                    .landscapeLeft,
                DeviceOrientation
                    .landscapeRight,
              ]);
              UtilsVariable.bigScreen=false;
              UtilsVariable.fullScreen=false;
              Navigator.pop(context);
            },
            child: Padding(
              padding:
              const EdgeInsets
                  .all(8.0),
              child: Row(
                children: [
                  Text(
                    "Screen rotation",
                    style: TextStyle(
                        color: Colors
                            .black,
                        fontWeight:
                        FontWeight
                            .bold),
                  ),
                  Spacer(),
                  Text("Vertical",style: TextStyle(color: Colors.grey))
                ],
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);

              showModalBottomSheet<
                  void>(
                shape:
                RoundedRectangleBorder(
                  borderRadius:
                  BorderRadius
                      .circular(
                      0),
                ),
                // isScrollControlled: true,
                backgroundColor:
                Color(0xfff7f7f7),
                context: context,
                builder: (BuildContext
                context) {
                  return ForwardView();
                },
              ).then((value) =>
              {setState(() {})});
            },
            child: Padding(
              padding:
              const EdgeInsets
                  .all(8.0),
              child: Row(
                children: [
                  Text(
                    "Moving range",
                    style: TextStyle(
                        color: Colors
                            .black,
                        fontWeight:
                        FontWeight
                            .bold),
                  ),
                  Spacer(),
                  Text("${UtilsVariable.forward_duration}"+'s  ',style: TextStyle(color: Colors.grey),),Icon(Icons.arrow_forward_ios,size: 10,color: Colors.grey,)
                ],
              ),
            ),
          ),
          Spacer(),
          Container(
            height: 1,
            width: double.infinity,
            color:
            Color(0xff1C1C1CDD),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
            },
            child: Container(
              height: 80,
              color: Colors.black12,
              child: Row(
                children: [
                  SizedBox(
                    width: 8,
                  ),
                  Icon(Icons.close),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Close",
                    style: TextStyle(
                        color: Colors
                            .grey,
                        fontWeight:
                        FontWeight
                            .w600),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );;
  }
}
