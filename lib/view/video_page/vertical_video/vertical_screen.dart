import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/size_video/full_screen.dart';
import 'package:collus_player/view/video_page/size_video/zoom_screen.dart';
import 'package:collus_player/view/video_page/size_video/zoom_screen_vertical.dart';
import 'package:collus_player/view/video_page/video_controller/forward_controller.dart';
import 'package:collus_player/view/video_page/video_controller/slide_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:video_player/video_player.dart';
class VerticalScreen extends StatefulWidget {
   VerticalScreen({Key? key, required this.controller, required this.fileName, required this.value, required this.showFunction}) : super(key: key);
   final VideoPlayerController controller;
   final String fileName;
   final double value;
   final bool showFunction;
  @override
  State<VerticalScreen> createState() => _VerticalScreenState();
}

class _VerticalScreenState extends State<VerticalScreen> {
  @override
  Widget build(BuildContext context) {
    return      OrientationBuilder(
      builder: (context, orientation) {
        return

          Stack(
            children: [
              (UtilsVariable.verticalsFullScreen)?
              ZoomScreenVertical(controller: widget.controller, value: widget.value, showFunction: widget.showFunction, ):
              UtilsVariable.verticalsBigScreen?
              ZoomScreen(controller: widget.controller, value: widget.value, fileName:widget.fileName, showFunction: widget.showFunction,):
              Container(
                width: double.infinity,
                child: AspectRatio(
                  aspectRatio: widget.controller.value.aspectRatio,
                  child: Stack(
                    alignment: Alignment.bottomCenter,
                    children: <Widget>[
                      VideoPlayer(widget.controller),
                      (widget.showFunction&&UtilsVariable.animated==false)?
                      ForwardController(
                        controller: widget.controller,
                        showFunction: widget.showFunction,
                      ):Container(width: 0,height: 0,),

                      Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          (widget.showFunction&&UtilsVariable.animated==false)? Row(
                            children: [
                              SizedBox(width: 20,),
                              Text(
                                widget.controller.value.position
                                    .toString().replaceAll("0:", '')
                                    .split('.')[0] +
                                    ' / ' +
                                    widget.controller.value.duration
                                        .toString().replaceAll("0:", '')
                                        .split('.')[0],
                                style: TextStyle(
                                  color: Colors.white,
                                ),
                              ),
                            ],
                          ):Text(''),
                          (widget.showFunction&&UtilsVariable.animated==false)?
                          SlideController(
                              controller: widget.controller, value: widget.value):Container(height: 40,),
                          SizedBox(height: 30,),

                          SizedBox(height: 30,),

                        ],
                      ),

                    ],
                  ),
                ),
              ),

              Column(
                children: [
                  SizedBox(height: 20,),

                  UtilsVariable. showUpperPart==true?
                  Padding(
                    padding:  EdgeInsets.only(top: 0),
                    child:  (widget.showFunction&&UtilsVariable.animated==false)?Row(
                      children: [
                        SizedBox(
                          width: 20,
                        ),
                        InkWell(
                            onTap: () {
                              moveRange();
                            },
                            child: Icon(
                              Icons.arrow_back_ios,
                              color: Colors.white,
                            )),
                        SizedBox(
                          width: 20,
                        ),
                        Text(
                          widget.fileName,
                          style: TextStyle(color: Colors.white),
                        ),
                        Spacer(),



                        (UtilsVariable.verticalsFullScreen==false&&UtilsVariable.verticalsBigScreen==false)?
                        InkWell(
                          onTap: (){
                            UtilsVariable.verticalsBigScreen=false;
                            UtilsVariable.verticalsFullScreen=true;
                            setState(() {

                            });
                          },
                          child: Container(
                              width: 40,
                              height: 40,
                              child: Image.asset("assets/full_screen.png")),
                        ):
                        (UtilsVariable.verticalsBigScreen==false&&UtilsVariable.verticalsFullScreen==true)?
                        InkWell(

                          onTap: (){
                            UtilsVariable.verticalsBigScreen=true;
                            UtilsVariable.verticalsFullScreen=false;
                            print("1");

                            setState(() {

                            });
                          },
                          child: Container(
                            width: 40,
                            height: 40,
                            child: Icon(
                              Icons.zoom_out_map_rounded,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ):
                        InkWell(

                          onTap: (){
                            UtilsVariable.verticalsBigScreen=false;
                            UtilsVariable.verticalsFullScreen=false;

                            setState(() {

                            });
                          },
                          child: Container(
                            width: 40,
                            height: 40,
                            child: Icon(
                              Icons.fit_screen_outlined,
                              color: Colors.white,
                              size: 14,
                            ),
                          ),
                        ),

                        InkWell(
                          onTap: () {
                            UtilsVariable.showMoving=true;
                            print("______________");
                            Scaffold.of(context).openEndDrawer();
                          },
                          child: Icon(
                            Icons.more_vert,
                            color: Colors.white,
                            size: 18,
                          ),
                        ),
                        SizedBox(
                          width: 10,
                        )
                      ],
                    ):Text(""),
                  ):Text('')

                ],
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      widget.showFunction?  AnimatedContainer(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8),
                          color: Color(0xff151618),
                        ),
                        width: UtilsVariable.animated?150:250,
                        height: 40,
                        duration: Duration(milliseconds: 200),
                        child: Row(
                          mainAxisAlignment: UtilsVariable.animated?MainAxisAlignment.center: MainAxisAlignment.spaceEvenly,
                          children: [
                            InkWell(


                              child: Icon(
                                Icons.lock_open_outlined,
                                color: Colors.white,
                              ),
                              onTap: (){UtilsVariable.animated=!UtilsVariable.animated;setState(() {

                              });},
                            ),
                            UtilsVariable.animated?InkWell(
                                onTap: (){UtilsVariable.animated=!UtilsVariable.animated;setState(() {

                                });},
                                child: Text("Tap to unlock",style: TextStyle(color: Colors.white),)):
                            Icon(
                              Icons.repeat_one,
                              color: Colors.white,
                            ),
                            UtilsVariable.animated?Container():  Image.asset("assets/img.png"),
                            UtilsVariable.animated?Container():Icon(
                              Icons.star_border,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ):Container(height: 40,),

                    ],
                  ),
                  SizedBox(height: 20,)
                ],
              ),            ],
          );

      },
    );

  }
  void moveRange(){
    UtilsVariable.forwardSize = false;
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
        overlays: SystemUiOverlay.values);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    UtilsVariable.isFullScreen = false;
    Navigator.pop(context);
  }
}
