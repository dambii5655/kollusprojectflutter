import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/video_page/video_controller/forward_duration_controller.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class HorizontalView extends StatefulWidget {
  const HorizontalView({Key? key}) : super(key: key);

  @override
  State<HorizontalView> createState() => _HorizontalViewState();
}

class _HorizontalViewState extends State<HorizontalView> {
  @override
  Widget build(BuildContext context) {
    return UtilsVariable.showMoving
        ? Column(
            children: [
              Text(
                "Setting",
                style:
                    TextStyle(color: Colors.grey, fontWeight: FontWeight.w600),
              ),
              InkWell(
                onTap: () {
                  UtilsVariable.forwardSize = false;
                  SystemChrome.setEnabledSystemUIMode(SystemUiMode.manual,
                      overlays: SystemUiOverlay.values);
                  SystemChrome.setPreferredOrientations([
                    DeviceOrientation.portraitUp,
                  ]);
                  UtilsVariable.isFullScreen = false;
                  UtilsVariable. showUpperPart=false;
                  Navigator.pop(context);
                  setState(() {});
                },
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Row(
                    children: [
                      Text(
                        "Screen rotation",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      Text("Horizontal")
                    ],
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  onTap: () {
                    UtilsVariable.showMoving = false;
                  },
                  child: Row(
                    children: [
                      Text(
                        "Moving Range",
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                      Spacer(),
                      Text(
                        "${UtilsVariable.forward_duration}" + 's  ',
                        style: TextStyle(color: Colors.grey),
                      ),
                      Icon(
                        Icons.arrow_forward_ios,
                        size: 10,
                        color: Colors.grey,
                      )
                    ],
                  ),
                ),
              )
            ],
          )
        : ForwardView();
  }
}
