import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/varuable_utils.dart';
class ForwardView extends StatefulWidget {
  const ForwardView({Key? key}) : super(key: key);

  @override
  State<ForwardView> createState() => _ForwardViewState();
}

class _ForwardViewState extends State<ForwardView> {
  @override
  Widget build(BuildContext context) {
    return  Container(
      color: Colors.white,
      height: 400,
      width: double
          .infinity,
      child: Column(
        children: [
          Padding(
            padding:
            const EdgeInsets
                .only(
                left:
                16,
                right:
                16),
            child: Column(
              children: [
                SizedBox(
                  height:
                  10,
                ),
                Text(
                  "Moving range",
                  style:
                  TextStyle(
                    color: Colors
                        .grey,
                    fontSize:
                    22,
                    fontWeight:
                    FontWeight.bold,
                  ),
                ),
                InkWell(
                    onTap:
                        () async {
                      final SharedPreferences
                      prefs =
                      await SharedPreferences.getInstance();
                      prefs.setInt(
                          "forward_dur",
                          5);
                      prefs.getInt("forward_dur");
                      UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
                      setState(() {

                      });
                    },
                    child:
                    Row(
                      children: [
                        Text("5s",style: TextStyle(color:  (UtilsVariable.forward_duration == 5)? Colors.black:Colors.grey,fontWeight: FontWeight.bold),),
                        Spacer(),
                        (UtilsVariable.forward_duration == 5)
                            ? Icon(Icons.check)
                            : Container(
                          width: 0,
                          height: 0,
                        )
                      ],
                    )),
                SizedBox(
                  height:
                  25,
                ),
                InkWell(
                    onTap:
                        () async {
                      final SharedPreferences
                      prefs =
                      await SharedPreferences.getInstance();
                      prefs.setInt(
                          "forward_dur",
                          10);
                      prefs.getInt("forward_dur");
                      UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
                      setState(() {

                      });
                    },
                    child:
                    Row(
                      children: [
                        Text("10s",style: TextStyle(color:  (UtilsVariable.forward_duration == 10)? Colors.black:Colors.grey,fontWeight: FontWeight.bold),),
                        Spacer(),
                        (UtilsVariable.forward_duration == 10)
                            ? Icon(Icons.check)
                            : Container(
                          width: 0,
                          height: 0,
                        )
                      ],
                    )),
                SizedBox(
                  height:
                  25,
                ),
                InkWell(
                    onTap:
                        () async {
                      final SharedPreferences
                      prefs =
                      await SharedPreferences.getInstance();
                      prefs.setInt(
                          "forward_dur",
                          20);
                      prefs.getInt("forward_dur");
                      UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
                      setState(() {

                      });
                    },
                    child:
                    Row(
                      children: [
                        Text("20s",style: TextStyle(color:  (UtilsVariable.forward_duration == 20)? Colors.black:Colors.grey,fontWeight: FontWeight.bold),),
                        Spacer(),
                        (UtilsVariable.forward_duration == 20)
                            ? Icon(Icons.check)
                            : Container(
                          width: 0,
                          height: 0,
                        )
                      ],
                    )),
                SizedBox(
                  height:
                  25,
                ),
                InkWell(
                    onTap:
                        () async {
                      final SharedPreferences
                      prefs =
                      await SharedPreferences.getInstance();
                      prefs.setInt(
                          "forward_dur",
                          30);
                      prefs.getInt("forward_dur");
                      UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
                      setState(() {

                      });
                    },
                    child:
                    Row(
                      children: [
                        Text("30s",style: TextStyle(color:  (UtilsVariable.forward_duration == 30)? Colors.black:Colors.grey,fontWeight: FontWeight.bold),),
                        Spacer(),
                        (UtilsVariable.forward_duration == 30)
                            ? Icon(Icons.check)
                            : Container(
                          width: 0,
                          height: 0,
                        )
                      ],
                    )),
                SizedBox(
                  height:
                  25,
                ),
                InkWell(
                    onTap:
                        () async {
                      final SharedPreferences
                      prefs =
                      await SharedPreferences.getInstance();
                      prefs.setInt(
                          "forward_dur",
                          60);
                      prefs.getInt("forward_dur");
                      UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
                      setState(() {

                      });
                    },
                    child:
                    Row(
                      children: [
                        Text("60s",style: TextStyle(color:  (UtilsVariable.forward_duration == 60)? Colors.black:Colors.grey,fontWeight: FontWeight.bold),),
                        Spacer(),
                        (UtilsVariable.forward_duration == 60)
                            ? Icon(Icons.check)
                            : Container(
                          width: 0,
                          height: 0,
                        )
                      ],
                    )),
                SizedBox(
                  height:
                  25,
                ),
                InkWell(
                    onTap:
                        () async {
                      final SharedPreferences
                      prefs =
                      await SharedPreferences.getInstance();
                      prefs.setInt(
                          "forward_dur",
                          300);
                      prefs.getInt("forward_dur");
                      UtilsVariable.forward_duration = prefs.getInt("forward_dur") ?? 5;
                      setState(() {

                      });
                    },
                    child:
                    Row(
                      children: [
                        Text("300s",style: TextStyle(color:  (UtilsVariable.forward_duration == 300)? Colors.black:Colors.grey,fontWeight: FontWeight.bold),),
                        Spacer(),
                        (UtilsVariable.forward_duration == 300)
                            ? Icon(Icons.check)
                            : Container(
                          width: 0,
                          height: 0,
                        )
                      ],
                    )),

              ],
            ),
          ),
          Spacer(),
          Container(
            height: 1,
            width: double.infinity,
            color:
            Color(0xff1C1C1CDD),
          ),
          InkWell(
            onTap: () {
              Navigator.pop(context);
              UtilsVariable.showMoving=true;
              print("object");
            },
            child: Container(
              height: 80,
              color: Colors.black12,
              child: Row(
                children: [
                  SizedBox(
                    width: 8,
                  ),
                  Icon(Icons.close),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Close",
                    style: TextStyle(
                        color: Colors
                            .grey,
                        fontWeight:
                        FontWeight
                            .w600),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
