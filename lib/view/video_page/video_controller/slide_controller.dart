import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
class SlideController extends StatefulWidget {

      SlideController({Key? key, required this.controller, required this.value}) : super(key: key);
  final VideoPlayerController controller;
  late  double value;

  @override
  State<SlideController> createState() => _SlideControllerState();
}

class _SlideControllerState extends State<SlideController> {
  @override
  Widget build(BuildContext context) {
    return  SliderTheme(
      data: SliderTheme.of(context).copyWith(
        activeTrackColor: Colors.white,
        inactiveTrackColor: Colors.white10,
        thumbColor: Colors.white,
        thumbShape: RoundSliderThumbShape(enabledThumbRadius: 15.0),
        overlayShape: RoundSliderOverlayShape(overlayRadius: 20.0),
      ),
      child: Slider(
        value: widget.value,
        max: widget.controller.value.duration.inSeconds.toDouble(),
        onChanged: (value) {
          setState(() {
            widget.value = value;
            widget.controller.seekTo(Duration(seconds: value.toInt()));
          });
        },
      ),
    );
  }
}
