import 'dart:async';
import 'dart:math';

import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';
import 'package:volume_control/volume_control.dart';
import 'package:screen_brightness/screen_brightness.dart';

import '../../../utils/varuable_utils.dart';
class ForwardController extends StatefulWidget {

  ForwardController({Key? key, required this.controller, required this.showFunction, }) : super(key: key);
  final VideoPlayerController controller;
  final bool showFunction;


  @override
  State<ForwardController> createState() => _ForwardControllerState();
}

class _ForwardControllerState extends State<ForwardController>
    with TickerProviderStateMixin{


  Future<void> initVolumeState() async {
    if (!mounted) return;

    //read the current volume
    _val = await VolumeControl.volume;
    setState(() {
    });
  }



  double _val = 0.5;
  Timer? timer;
  double _brightness = 0.5;

  void _setBrightness(double brightness) async{
    // Set the screen brightness to the new value
   await ScreenBrightness().setScreenBrightness(brightness);

    // Update the state with the new brightness value
    setState(() {
      _brightness = brightness;
    });
  }

@override
  void initState() {
    // TODO: implement initState
  initVolumeState();
  super.initState();
  }

  bool mute=false;
  bool showDurationReturn=false;
  bool showDurationForward=false;
  double width = 0;
  @override
  Widget build(BuildContext context) {
    return   widget.showFunction
        ? AnimatedSwitcher(
      duration: const Duration(milliseconds: 50),
      reverseDuration: const Duration(milliseconds: 200),
      child:
          Center(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              // color: Colors.red,
              height: 250,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(height: 15,),
                  Container(
                      height: 35,
                      child: Image.asset("assets/brightness.png",fit: BoxFit.fill,),

                  ),

                  Container(
                    height: 150,
                    width: 20,

                    child: RotatedBox(
                      quarterTurns: 3,
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: Colors.white,
                          inactiveTrackColor: Colors.black54,
                          thumbColor: Colors.white,
                          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0.0),

                          trackShape: RectangularSliderTrackShape(),
                          trackHeight: 4.0,

                          overlayColor: Colors.transparent.withAlpha(32),
                          overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),
                        ),
                        child: Slider(

                          min:0,max:1,divisions: 100,
                          value: _brightness,
                          onChanged: _setBrightness,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            showDurationReturn? Container(
              // color: Colors.red,
              width: 50,
              child: AnimatedTextKit(

                animatedTexts: [
                  FadeAnimatedText("- "+"${UtilsVariable.forward_duration}",textStyle: TextStyle(color: Colors.white)),

                ],),
            ):
            SizedBox(width: 50,),


            UtilsVariable.forwardSize?Spacer():            SizedBox(width: 0,),


            Container(
              // color: Colors.green,
              margin: EdgeInsets.only(top: 10),

              width: 50,
              height: 50,

              child: InkWell(
                onTap: () async{




                  forwardBackVideo();
                  showDurationReturn=true;
                  await Future.delayed(Duration(seconds: 2));{
                    showDurationReturn=false;
                  }
                  setState(() {});
                },
                child:  Stack(
                  children: [
                    Center(
                      child: Image.asset('assets/btn_replay_sec.png',fit: BoxFit.cover,),
                    ),
                    Center(child: Text(showDurationReturn?"":"${UtilsVariable.forward_duration}",style: TextStyle(color: Colors.white,fontSize: 12),)),

                  ],
                ),
              ),
            ),
            SizedBox(width: 40,),
            widget.controller.value.isPlaying?
            Container(


              width: 40,
              height: 40,
              child: InkWell(
                onTap: () {
                  widget.controller.pause();

                  setState(() {});
                },
                child: const Center(
                  child: Icon(
                    Icons.pause,
                    color: Colors.white,
                    size: 50.0,
                    semanticLabel: 'Play',
                  ),
                ),
              ),
            ):Container(


              width: 40,
              height: 40,
              child: InkWell(
                onTap: () {
                  widget.controller.play();
                  setState(() {});
                },
                child: const Center(
                  child: Icon(
                    Icons.play_arrow_outlined,
                    color: Colors.white,
                    size: 50.0,
                    semanticLabel: 'Play',
                  ),
                ),
              ),
            ),
            SizedBox(width: 40,),

            Container(

              margin: EdgeInsets.only(top: 10),
              // color: Colors.red,
              width: 50,
              height: 50,
              child: InkWell(
                onTap: () async{

                  forwardVideo();
                  showDurationForward=true;
                  await Future.delayed(Duration(seconds: 2));{
                    showDurationForward=false;
                  }
                  setState(() {});

                },
                child:  Stack(
                  children: [

                    Center(
                        child: Image.asset('assets/btn_forward_sec.png')
                    ),
                    Center(child: Text(showDurationForward?"":"${UtilsVariable.forward_duration}",style: TextStyle(color: Colors.white,fontSize: 12),)),

                  ],
                ),
              ),
            ),

            UtilsVariable.forwardSize?Spacer():SizedBox(width: 0,),
            showDurationForward? Container(
              // color: Colors.amber,
              width: 50,
              child: AnimatedTextKit(

                animatedTexts: [
                  FadeAnimatedText("+ "+"${UtilsVariable.forward_duration}",textStyle: TextStyle(color: Colors.white),textAlign: TextAlign.end),

                ],),
            ):
            SizedBox(width: 50,),

            Container(
              height: 250,

              child: Column(

                children: [
                  SizedBox(height: 15,),

                  Container(
                      height: 35,
                      width: 35,
                      child: mute?Image.asset("assets/mute.png"):Icon(Icons.volume_down_sharp,color: Colors.white,)

                  ),
                  Container(
                    height: 150,
                    width: 20,
                    child: RotatedBox(
                      quarterTurns: 3,
                      child: SliderTheme(
                        data: SliderTheme.of(context).copyWith(
                          activeTrackColor: Colors.white,
                          inactiveTrackColor: Colors.black54,
                          thumbColor: Colors.white,
                          thumbShape: RoundSliderThumbShape(enabledThumbRadius: 0.0),

                          trackShape: RectangularSliderTrackShape(),
                          trackHeight: 4.0,

                          overlayColor: Colors.transparent.withAlpha(32),
                          overlayShape: RoundSliderOverlayShape(overlayRadius: 28.0),

                        ),

                        child: Slider(value:_val,min:0,max:1,divisions: 100,onChanged:(val){
                          _val = val;
                          setState(() {});
                          if (timer!=null){
                            timer?.cancel();
                          }
                           if(val==0){
                             mute=true;
                             setState(() {

                             });
                           }else{
                             mute=false;
                             setState(() {

                             });
                           }
                          //use timer for the smoother sliding
                          timer = Timer(Duration(milliseconds: 200), (){VolumeControl.setVolume(val);});

                          print("val:$val");
                        }),
                      ),
                    ),
                  ),

                ],
              ),
            )

          ],
        ),
      ),
    )
        :    Text('');
  }

  void forwardVideo() {
    Duration currentPosition = widget.controller.value.position;
    Duration newPosition = Duration(seconds: currentPosition.inSeconds + UtilsVariable.forward_duration);
    widget.controller.seekTo(newPosition);

    print(UtilsVariable.forward_duration);
  }
  void forwardBackVideo() {
    Duration currentPosition = widget.controller.value.position;
    Duration newPosition = Duration(seconds: currentPosition.inSeconds - UtilsVariable.forward_duration);
    widget.controller.seekTo(newPosition);
  }
}
