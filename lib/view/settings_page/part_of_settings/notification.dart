import 'package:collus_player/utils/varuable_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:collus_player/utils/text_style.dart';

class NotificationSettings extends StatefulWidget {
  const NotificationSettings({Key? key}) : super(key: key);

  @override
  State<NotificationSettings> createState() => _NotificationSettingsState();
}

class _NotificationSettingsState extends State<NotificationSettings> {


  @override
  void initState() {
    getStatusNotification();
    super.initState();
  }
  void getStatusNotification()async{
    SharedPreferences? preferences = await SharedPreferences.getInstance();
    UtilsVariable.switchNotification=preferences.getBool("notification")??false;
    setState(() {
    });

  }

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,

          borderRadius: BorderRadius.circular(4)),
      child: Padding(
        padding: const EdgeInsets.only(left: 20,right: 20,bottom: 10,top: 10),
        child: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  "Notification when using\ndata network",
                  style: CustomTextStyle.titleStyle,
                ),
                SizedBox(height: 5,),
                Text("Show Notification when downloading\nfrom data networks like 3G/LTE",style: CustomTextStyle.subtitleTextStyle,)
              ],
            ),Spacer(),
            Container(
              margin: EdgeInsets.only(right: 10),
              height: 30,
              width: 60,
              child: FlutterSwitch(
                inactiveColor: Colors.black26,
                value: UtilsVariable.switchNotification,
                activeColor: Color(0xff0db4fa),
                onToggle: (val) async{
                  SharedPreferences? preferences = await SharedPreferences.getInstance();
                  preferences.setBool("notification", val);
                  setState(() {
                    UtilsVariable.switchNotification = val;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
