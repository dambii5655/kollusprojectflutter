import 'package:collus_player/utils/info_dialog.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/settings_page/part_of_settings/part_of_storage/unused_size.dart';
import 'package:flutter/material.dart';

import '../../../utils/text_style.dart';

class Storage extends StatefulWidget {
  const Storage({Key? key}) : super(key: key);

  @override
  State<Storage> createState() => _StorageState();
}

class _StorageState extends State<Storage> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        downloadContent(),
         SizedBox(height: 30,),
        UnusedSize(),
        SizedBox(height: 10,),

        Container(
          color: Colors.white,
          child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20,),
            child: Column(
              children: [
                SizedBox(height: 10,),

                Row(
                  children: [
                    Text(
                      "Temporary file (streaming)",
                      style: CustomTextStyle.titleStyle,
                    ),
                    Spacer(),
                    Text(
                      "0.00 MB",
                      style: CustomTextStyle.subtitleTextStyle,
                    )
                  ],
                ),
                SizedBox(height: 10,),
                Row(mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [

                    InkWell(
                      onTap: () {
                        deleteDialog(context);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(6),
                            color: Color(0xffCDCCCCFF)),
                        width: 70,
                        child: Center(child: Text("Delete")),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),

      ],
    );
  }
  Widget downloadContent(){
    return Container(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(left: 20, right: 20,),
        child: Column(
          children: [
            SizedBox(height: 10,),

            Row(
              children: [
                Text(
                  "Download content",
                  style: CustomTextStyle.titleStyle,
                ),
                Spacer(),
                Text(
                  "0.00 MB",
                  style: CustomTextStyle.subtitleTextStyle,
                )
              ],
            ),
            SizedBox(height: 10,),
            Row(mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [

                InkWell(
                  onTap: () {
                    deleteDialog(context);
                  },
                  child: Container(
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(6),
                        color: Color(0xffCDCCCCFF)),
                    width: 70,
                    child: Center(child: Text("Delete")),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
}
  void deleteDialog(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.white,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "All download content will be deleted.\nDo you want to proceed?",
                    style: CustomTextStyle.titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                          height: 40, child: Center(child: InkWell(
                          onTap: (){
                                  Navigator.pop(context);
                          },
                          child: Text("Cancel")))),
                      Container(
                        padding: EdgeInsets.only(left: 8,right: 8),
                          height: 40,
                          decoration: BoxDecoration(borderRadius: BorderRadius.circular(8),                          color: Colors.red.shade50,
                          ),
                          child: Center(
                              child: Text(
                            "Delete",
                            style: TextStyle(color: Colors.red),
                          ))),
                    ],
                  ),
                  SizedBox(height: 20,)
                ],
              ),
            ),
          );
        }).then((value) => {
    setState(() {})
        });
  }
}
