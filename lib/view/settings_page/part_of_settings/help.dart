import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/view/settings_page/part_of_settings/decoder_settings.dart';
import 'package:collus_player/view/settings_page/part_of_settings/gesture_ui/gesture_view.dart';
import 'package:collus_player/view/settings_page/part_of_settings/shotcase/shortcuts_view.dart';
import 'package:flutter/material.dart';

import 'frequency_asked/frequency_asked_web.dart';

class Help extends StatefulWidget {
  const Help({Key? key}) : super(key: key);

  @override
  State<Help> createState() => _HelpState();
}

class _HelpState extends State<Help> {
  @override
  Widget build(BuildContext context) {
    return  Column(children: [
      SizedBox(height: 30,),
      InkWell(

        onTap: (){
          Navigator.pushReplacement(context, MaterialPageRoute(builder: (context)=>WebViewExample()));

        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 20,top: 10,bottom: 10,right: 10),
          child: Row(
            children: [
              Text("Frequently Asked Questions",style: CustomTextStyle.titleStyle,),Spacer(),
              Icon(Icons.arrow_forward_ios,color: Colors.black54,)
            ],
          ),
        ),
      ),SizedBox(height: 20,),
      InkWell(
        onTap: (){
          Navigator.pop(context);
          showModalBottomSheet<void>(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            isScrollControlled: true,
            backgroundColor: Color(0xfff7f7f7),
            context: context,
            builder: (BuildContext context) {
              return FractionallySizedBox (
                  heightFactor: 0.9,
                  child: GestureView());
            },
          );
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 20,top: 10,bottom: 10,right: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Gesture",style: CustomTextStyle.titleStyle,),Spacer(),
                  Icon(Icons.arrow_forward_ios,color: Colors.black54,)
                ],
              ),
              Text("Description of gesture action the\nplayer uses",style: CustomTextStyle.subtitleTextStyle,)
            ],
          ),
        ),
      ),
      SizedBox(height: 20,),
      InkWell(
        onTap: (){
          showModalBottomSheet<void>(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            isScrollControlled: true,
            backgroundColor: Color(0xfff7f7f7),
            context: context,
            builder: (BuildContext context) {
              return FractionallySizedBox (
                  heightFactor: 0.9,
                  child: ShortCutsView());
            },
          );
        },
        child: Container(
          color: Colors.white,
          padding: EdgeInsets.only(left: 20,top: 10,bottom: 10,right: 10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Shortcuts",style: CustomTextStyle.titleStyle,),Spacer(),
                  Icon(Icons.arrow_forward_ios,color: Colors.black54,)
                ],
              ),
              Text("List of shortcuts that can be used when\nconnecting a keyboard",style: CustomTextStyle.subtitleTextStyle,)
            ],
          ),
        ),
      )

    ],);
  }
}
