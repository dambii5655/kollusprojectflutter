import 'package:collus_player/utils/info_dialog.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/text_style.dart';
import 'decoder_settings.dart';
import 'inside_settings/view_device_info.dart';
class Advanced extends StatefulWidget {
   Advanced({Key? key, required this.cxt}) : super(key: key);
   final BuildContext cxt;

  @override
  State<Advanced> createState() => _AdvancedState();
}

class _AdvancedState extends State<Advanced> {
  @override
  Widget build(BuildContext context) {
    return  Column(children: [
      SizedBox(height: 20,),
      Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 20,top: 10,bottom: 10,right: 10),
        child: InkWell(
          onTap: (){
            // Navigator.of(context).push(
            //   PageRouteBuilder(
            //     transitionDuration: Duration(milliseconds: 500),
            //     transitionsBuilder: (context, animation, secondaryAnimation, child) {
            //       var begin = Offset(1.0, 0.0);
            //       var end = Offset.zero;
            //       var curve = Curves.easeInOutQuart;
            //
            //       var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
            //
            //       return SlideTransition(
            //         position: animation.drive(tween),
            //         child: child,
            //       );
            //     },
            //     pageBuilder: (context, animation, secondaryAnimation) {
            //       return DetailPage();
            //     },
            //   ),
            // );
            Navigator.pop(widget.cxt);
            showModalBottomSheet<void>(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              isScrollControlled: true,
              backgroundColor: Color(0xfff7f7f7),
              context: context,
              builder: (BuildContext context) {
                return FractionallySizedBox (
                    heightFactor: 0.9,
                    child: ViewDeviceInfo());
              },
            );

          },
          child: Row(mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("Device information",style: CustomTextStyle.titleStyle,),Spacer(),
              Icon(Icons.arrow_forward_ios,color: Colors.black54,)
            ],
          ),
        ),
      )
,
      SizedBox(height: 10,),
      Container(
        color: Colors.white,
        padding: EdgeInsets.only(left: 20,top: 10,bottom: 10,right: 10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text("Reset settings",style: CustomTextStyle.titleStyle,),
            SizedBox(height: 10,),
            Row(
              children: [
                Text("Reset all settings",style: CustomTextStyle.subtitleTextStyle,),
                Spacer(),
                InkWell(
                    onTap: (){
                      resetDialog(context);
                    },
                    child: Text("Reset",style: TextStyle(color: Colors.red),)),
              ],
            )
          ],
        ),
      )

    ],);
  }
  void resetDialog(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.white,
            elevation: 0,
            child: Padding(
              padding: const EdgeInsets.only(left: 8, right: 8),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    height: 20,
                  ),
                  Text(
                    "Do you want to reset all settings?",
                    style: CustomTextStyle.titleStyle,
                    textAlign: TextAlign.center,
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                          height: 40, child: Center(child: InkWell(
                          onTap: (){
                            Navigator.pop(context);


                          },
                          child: Text("Cancel")))),
                      InkWell(
                        onTap: ()async{
                          Navigator.pop(context);
                          SharedPreferences? preferences = await SharedPreferences.getInstance();
                          preferences.setBool("background", false);
                          DialogInfo.changeFunction(false);
                          // print(val);
                          setState(() {
                            UtilsVariable.switchBackground = false;
                          });
                          ScaffoldMessenger.of(context).showSnackBar(
                            SnackBar(content: Text('Initialization is complete.')),
                          );
                        },
                        child: Container(
                            padding: EdgeInsets.only(left: 8,right: 8),
                            height: 40,
                            decoration: BoxDecoration(borderRadius: BorderRadius.circular(8),                          color: Colors.red.shade50,
                            ),
                            child: Center(
                                child: Text(
                                  "Confirm",
                                  style: TextStyle(color: Colors.red),
                                ))),
                      ),
                    ],
                  ),
                  SizedBox(height: 20,)
                ],
              ),
            ),
          );
        }).then((value) => {
      getStatusBackground(),
      setState(() {})
    });
  }
  void getStatusBackground()async{
    SharedPreferences? preferences = await SharedPreferences.getInstance();
    UtilsVariable.switchBackground=preferences.getBool("background")??false;
    setState(() {
    });

  }

}
// class DetailPage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Detail Page'),
//       ),
//       body: Center(
//         child: ElevatedButton(
//           child: Text('Go Back'),
//           onPressed: () {
//             Navigator.of(context).pop();
//           },
//         ),
//       ),
//     );
//   }
// }
// class HomePage extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Home Page'),
//       ),
//       body: Center(
//         child: ElevatedButton(
//           child: Text('Open Bottom Sheet'),
//           onPressed: () {
//             showModalBottomSheet(
//               context: context,
//               isScrollControlled: true,
//               builder: (BuildContext context) {
//                 return AnimatedPadding(
//                   duration: Duration(milliseconds: 500),
//                   curve: Curves.easeInOutQuart,
//                   padding: EdgeInsets.only(
//                       bottom: MediaQuery.of(context).viewInsets.bottom),
//                   child: Container(
//                     height: 200.0,
//                     child: Center(
//                       child: Text(
//                         'This is a Bottom Sheet!',
//                         style: TextStyle(
//                           fontSize: 24.0,
//                         ),
//                       ),
//                     ),
//                   ),
//                 );
//               },
//             );
//           },
//         ),
//       ),
//     );
//   }
// }
