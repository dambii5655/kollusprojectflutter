import 'package:collus_player/utils/text_style.dart';
import 'package:flutter/material.dart';

class ShortCutsView extends StatelessWidget {
  const ShortCutsView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(8), color: Colors.white),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(
                width: 15,
              ),
              IconButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  icon: Icon(Icons.arrow_back_ios)),
              Text(
                "ShortCuts",
                style:TextStyle(fontWeight: FontWeight.bold,fontSize: 22),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              SizedBox(
                width: 30,
              ),
              Container(
                padding:
                    EdgeInsets.only(left: 12, right: 12, bottom: 6, top: 6),
                // padding: EdgeInsets.all(4),

                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Color(0xfff1f2f7)),
                child: Text(
                  "Space",
                  style: CustomTextStyle.titleStyle,
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Play/Pause")
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              icon(Icon(Icons.play_arrow), 4),
              SizedBox(
                width: 10,
              ),
              Text("Fast Forward")
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              icon(Icon(Icons.play_arrow), 2),
              SizedBox(
                width: 10,
              ),
              Text("Rewind")
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              icon(Icon(Icons.play_arrow), 3),
              SizedBox(
                width: 10,
              ),
              Text("Volume up"),
              SizedBox(
                height: 20,
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              icon(Icon(Icons.play_arrow), 3),
              SizedBox(
                width: 10,
              ),
              Text("Volume down")
            ],
          ),
          SizedBox(
            height: 20,
          ),
          text("L", "Mute/Unmute"),
          SizedBox(
            height: 20,
          ),
          text("X", "Slower"),
          SizedBox(
            height: 20,
          ),
          text("C", "Facter"),
          SizedBox(
            height: 20,
          ),
          text("Z", "Normal speed"),
          SizedBox(
            height: 20,
          ),
          text("[", "A-B Repeat Start"),
          SizedBox(
            height: 20,
          ),
          text("]", "A-B Repeat end"),
          SizedBox(
            height: 20,
          ),
          Row(
            children: [
              SizedBox(
                width: 20,
              ),
              Container(
                width: 34,
                height: 34,
                padding: EdgeInsets.only(left: 4, right: 4, bottom: 4, top: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Color(0xfff1f2f7)),
                child: Center(
                  child: Text(
                    "\\",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: 34,
                height: 34,
                padding: EdgeInsets.only(left: 4, right: 4, bottom: 4, top: 4),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(4),
                    color: Color(0xfff1f2f7)),
                child: Center(
                  child: Text(
                    "P",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Text("Undo A-B Repeat")
            ],
          ),
        ],
      ),
    );
  }

  Widget icon(Widget icon, int box) {
    return Container(
        padding: EdgeInsets.only(left: 4, right: 4, bottom: 4, top: 4),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(4), color: Color(0xfff1f2f7)),
        child: RotatedBox(quarterTurns: box, child: icon));
  }

  Widget text(String text, String description) {
    return Row(
      children: [
        SizedBox(
          width: 20,
        ),
        Container(
          width: 34,
          height: 34,
          padding: EdgeInsets.only(left: 4, right: 4, bottom: 4, top: 4),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(4), color: Color(0xfff1f2f7)),
          child: Center(
            child: Text(
              text,
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
          ),
        ),
        SizedBox(
          width: 10,
        ),
        Text(description)
      ],
    );
  }
}
