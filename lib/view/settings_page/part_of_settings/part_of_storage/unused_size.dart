import 'package:flutter/material.dart';

import '../../../../utils/text_style.dart';
import '../../../../utils/varuable_utils.dart';
class UnusedSize extends StatelessWidget {
  const UnusedSize({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      padding: EdgeInsets.only(left: 20,right: 20,top: 5,bottom: 5),

      child: Row(
        children: [
          Text(
            "Unused Size",
            style: CustomTextStyle.titleStyle,
          ),
          Spacer(),
          Text(UtilsVariable.freeSpace+" GB", style: CustomTextStyle.subtitleTextStyle,),


        ],
      ),
    );
  }
}
