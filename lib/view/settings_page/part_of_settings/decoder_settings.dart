import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/view/settings_page/settings_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DecoderSettings extends StatefulWidget {
  const DecoderSettings({Key? key}) : super(key: key);

  @override
  State<DecoderSettings> createState() => _DecoderSettingsState();
}

class _DecoderSettingsState extends State<DecoderSettings> {
  @override
  void initState() {
    getDecode();
    super.initState();
  }
  void getDecode()async{
    final SharedPreferences
    prefs =
        await SharedPreferences.getInstance();


    decodeSettings=prefs.getString('decode')!;
    setState(() {

    });
  }
  String decodeSettings='';
  @override
  Widget build(BuildContext context) {
    return   Column(children: [
      SizedBox(height: 20,),

      Row(children: [
        SizedBox(width: 20,),
        InkWell(
            onTap: (){
              Navigator.pop(context);
              showModalBottomSheet<void>(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                isScrollControlled: true,
                backgroundColor: Color(0xfff7f7f7),
                context: context,
                builder: (BuildContext context) {
                  return FractionallySizedBox (
                      heightFactor: 0.9,
                      child: SettingsPage(k:context));
                },
              );
            },
            child: Icon(Icons.arrow_back_ios_new)),
    SizedBox(width: 20,),
    Text("Decoder settings",style: CustomTextStyle.titleStyle,)

      ],),
      SizedBox(height: 20,),

      Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: ()async{
            final SharedPreferences
            prefs =
                await SharedPreferences.getInstance();
                prefs.setString("decode", "hardware");
            decodeSettings=prefs.getString('decode')!;
            print(decodeSettings);
            setState(() {

            });
          },


          child: Row(
            children: [
              Text("Hardware",style: decodeSettings=='hardware'?CustomTextStyle.titleStyle:CustomTextStyle.subtitleTextStyle,),Spacer(),
              decodeSettings=='hardware'? Icon(Icons.check):Container()
            ],
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: ()async{
            final SharedPreferences
            prefs =
            await SharedPreferences.getInstance();
            prefs.setString("decode", "software");

            decodeSettings=prefs.getString('decode')!;
            setState(() {

            });
          },
          child: Row(
            children: [
              Text("Software",style: decodeSettings=='software'?CustomTextStyle.titleStyle:CustomTextStyle.subtitleTextStyle,),Spacer(),
              decodeSettings=='software'? Icon(Icons.check):Container()
            ],
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: InkWell(
          onTap: ()async{
            final SharedPreferences
            prefs =
            await SharedPreferences.getInstance();
            prefs.setString("decode", "native");
            decodeSettings=prefs.getString('decode')!;
       setState(() {

       });
          },
          child: Row(
            children: [
              Text("Native Player",style: decodeSettings=='native'?CustomTextStyle.titleStyle:CustomTextStyle.subtitleTextStyle,),Spacer(),
              decodeSettings=='native'? Icon(Icons.check):Container()
            ],
          ),
        ),
      ),

    ],);
  }
}
