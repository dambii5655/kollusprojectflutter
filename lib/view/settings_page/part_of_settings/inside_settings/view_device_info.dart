import 'dart:io';

import 'package:collus_player/utils/text_style.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';

import '../../../../utils/info_device.dart';
import '../../../../utils/varuable_utils.dart';
import '../../settings_page.dart';

class ViewDeviceInfo extends StatefulWidget {
  const ViewDeviceInfo({Key? key}) : super(key: key);

  @override
  State<ViewDeviceInfo> createState() => _ViewDeviceInfoState();
}

class _ViewDeviceInfoState extends State<ViewDeviceInfo> {
  String modelName='';
  String clientVersion='';
  @override
  void initState() {
    getInfo();
    super.initState();
  }
  void getInfo()async{
    modelName=await InfoDevice.getModel();
    clientVersion=await InfoDevice.getProductionLevel();

    setState(() {

    });

  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          backgroundColor: Colors.white,
          elevation: 0,
          title:Text('Device information',style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16, height: 1,color: Colors.black ),)
          , leading:  InkWell(
          onTap: (){
            Navigator.pop(context);
            showModalBottomSheet<void>(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(10.0),
              ),
              isScrollControlled: true,
              backgroundColor: Color(0xfff7f7f7),
              context: context,
              builder: (BuildContext context) {
                return FractionallySizedBox (
                    heightFactor: 0.9,
                    child: SettingsPage(k:context));
              },
            );
          },
          child: Icon(Icons.arrow_back_ios_new,color: Colors.black,))),
      body: Padding(
        padding: const EdgeInsets.only(left: 16,right: 16),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
          Text("Player",style: CustomTextStyle.titleStyle,),
          SizedBox(height: 15,),
          Row(
            children: [
              Text("Version"),
              Spacer(),
              Text("version: 2.2.3, build: 5",style:CustomTextStyle.subtitleTextStyle ,),
            ],
          ),
            SizedBox(height: 10,),
            Divider(),
            SizedBox(height: 10,),

            Row(
            children: [
              Text("ID"),
              Spacer(),
              Text(UtilsVariable.ID,style:CustomTextStyle.subtitleTextStyle ,),
            ],
          ),
            SizedBox(height: 30,),
            Text("Device",style: CustomTextStyle.titleStyle,),


            SizedBox(height: 20,),

            Row(
            children: [
              Text("Version"),
              Spacer(),
              Text(clientVersion,style:CustomTextStyle.subtitleTextStyle ,),
            ],
          ),
            SizedBox(height: 10,),

            Divider(),
            SizedBox(height: 10,),

            Row(
              children: [
                Text("Model name"),
                Spacer(),
                Text(modelName,style:CustomTextStyle.subtitleTextStyle ),
              ],
            )
        ],),
      ),
    );
  }

}
