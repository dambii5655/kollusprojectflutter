import 'package:flutter/material.dart';

import '../../../utils/text_style.dart';
import '../../../utils/varuable_utils.dart';
class Renew extends StatefulWidget {
  const Renew({Key? key}) : super(key: key);

  @override
  State<Renew> createState() => _RenewState();
}

class _RenewState extends State<Renew> {

  bool switchOne = true;

  @override
  Widget build(BuildContext context) {
    return       Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(6),
        color: Colors.white,
      ),
      padding: EdgeInsets.only(left: 10, right: 10),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Text("Renew DRM info",style: CustomTextStyle.titleStyle,),
            ],
          ),
          Text(
            "Update the validity period of content",
            style: CustomTextStyle.subtitleTextStyle,
          ),
          Row(
            children: [
              SizedBox(
                child: Transform.scale(
                    scale: 0.7,
                    child: Checkbox(
                        activeColor: Color(0xff0db4fa),
                        value: switchOne,
                        onChanged: (value) {
                          switchOne = !switchOne;
                          print(switchOne);
                          setState(() {});
                        })),
                width: 10,
                height: 20,
              ),
              SizedBox(
                width: 5,
              ),
              Text(
                "Renew expired content only",
                style:
                TextStyle(color: Colors.black, fontSize: 12),
              ),
              Spacer(),
              InkWell(
                onTap: () {
                  showSnackBar();
                },
                child: Container(
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Color(0xffCDCCCCFF)),
                  width: 70,
                  child: Center(child: Text("Renew")),
                ),
              )
            ],
          ),
          SizedBox(height: 10,),

        ],
      ),
    );
  }
  void showSnackBar() {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: const Text('DRM renewal is complete'),
      action: SnackBarAction(
        label: 'Undo',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    ));
  }
}
