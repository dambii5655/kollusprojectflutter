import 'package:collus_player/utils/varuable_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../utils/info_dialog.dart';
import '../../../utils/text_style.dart';

class BackGroundPlayer extends StatefulWidget {

   BackGroundPlayer({Key? key,}) : super(key: key);

  @override
  State<BackGroundPlayer> createState() => _BackGroundPlayerState();
}

class _BackGroundPlayerState extends State<BackGroundPlayer> {


@override
  void initState() {
  getStatusBackground();
    super.initState();
  }
void getStatusBackground()async{
  SharedPreferences? preferences = await SharedPreferences.getInstance();
  UtilsVariable.switchBackground=preferences.getBool("background")??false;
  setState(() {
  });

}
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
          color: Colors.white,

          borderRadius: BorderRadius.circular(4)),
      child: Padding(
        padding: const EdgeInsets.only(left: 20,right: 20,bottom: 10),
        child: Row(
          children: [
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(height: 10,),
                Text(
                  "Play background audio",
                  style: CustomTextStyle.titleStyle,
                ),
                Text("Allow background audio",style: CustomTextStyle.subtitleTextStyle,)
              ],
            ),Spacer(),

            Container(
              margin: EdgeInsets.only(right: 10),
              height: 30,
              width: 60,
              child: FlutterSwitch(
                inactiveColor: Colors.black26,
                value: UtilsVariable.switchBackground,
                activeColor: Color(0xff0db4fa),
                onToggle: (val) async{
                  SharedPreferences? preferences = await SharedPreferences.getInstance();
                  preferences.setBool("background", val);
                  DialogInfo.changeFunction(val);
                  print(val);
                  setState(() {
                    UtilsVariable.switchBackground = val;
                  });
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
