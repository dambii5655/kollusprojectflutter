import 'package:flutter/material.dart';
import 'package:flutter/material.dart';

class BottomSheetExample extends StatefulWidget {
  @override
  _BottomSheetExampleState createState() => _BottomSheetExampleState();
}

class _BottomSheetExampleState extends State<BottomSheetExample> {
      bool _innerSheetVisible = false;
      double _innerSheetHeight = 0.0;

      void _toggleInnerSheet() {
        setState(() {
          _innerSheetVisible = !_innerSheetVisible;
          _innerSheetHeight = _innerSheetVisible ? 200.0 : 0.0;
        });
      }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Nested Bottom Sheet Example'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: () {
            showModalBottomSheet(
              context: context,
              builder: (BuildContext context) {
                return Container(
                  height: 400.0,
                  child: Column(
                    children: [
                      SizedBox(height: 20.0),
                      Text(
                        'Outer Bottom Sheet',
                        style: TextStyle(fontSize: 24.0),
                      ),
                      SizedBox(height: 20.0),
                      ElevatedButton(
                        child: Text('Open Inner Bottom Sheet'),
                        onPressed: () {
                          _toggleInnerSheet();
                        },
                      ),
                      AnimatedContainer(
                        height: _innerSheetHeight,
                        duration: Duration(milliseconds: 500),
                        curve: Curves.easeInOutQuart,
                        child: Container(
                          color: Colors.grey[200],
                          child: Center(
                            child: Text(
                              'Inner Bottom Sheet',
                              style: TextStyle(fontSize: 24.0),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                );
              },
            );
          },
          child: Text('Open Bottom Sheet'),
        ),
      ),
    );
  }
}
