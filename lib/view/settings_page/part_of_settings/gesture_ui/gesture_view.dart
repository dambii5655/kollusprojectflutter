import 'package:collus_player/utils/text_style.dart';
import 'package:flutter/material.dart';

class GestureView extends StatelessWidget {
  const GestureView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(8)),
      child: Column(
        children: [
          Row(
            children: [
              SizedBox(width: 10,),
              IconButton(onPressed: (){

                Navigator.pop(context);

              }, icon: Icon(Icons.arrow_back_ios)),
              Text(
                "Gesture",
                style: CustomTextStyle.titleStyle,
              ),
            ],
          ),
          gesturePart('Fine movement of the video section',"Left and right slide","assets/gesture_main.png" ),
          gesturePart('Zoom In/Out',"Spread / narrow two fingers","assets/img_gesture_info_pinch.png" ),
          gesturePart('Move forward',"Double tap left","assets/img_2.png" ),
          gesturePart('Move backward',"Double tap right","assets/img_1.png" ),
        ],
      ),
    );
  }
  Widget gesturePart(String mainText,String secondText,String imageGesture,  ){
    return        Container(
      padding: EdgeInsets.all(14),
      margin: EdgeInsets.all(8),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(4),
        border: Border.all(color: Colors.black12),
      ),
      child: Column(
        children: [
          Row(
            children: [
              Expanded(
                child: Text(
                  mainText,
                  style: CustomTextStyle.titleStyle,
                ),
              ),
              Text(
                secondText,
                style: CustomTextStyle.subtitleTextStyle,
              )
            ],
          ),
          SizedBox(height: 10,),
          Image.asset(imageGesture),
          // SizedBox(height: 5,),


        ],
      ),
    );
  }


}
