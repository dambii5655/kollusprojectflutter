import 'package:collus_player/view/settings_page/part_of_settings/advanced.dart';
import 'package:collus_player/view/settings_page/part_of_settings/help.dart';
import 'package:collus_player/view/settings_page/part_of_settings/play_background.dart';
import 'package:collus_player/view/settings_page/part_of_settings/renew.dart';
import 'package:collus_player/view/settings_page/part_of_settings/storage.dart';
import 'package:flutter/material.dart';
// import 'package:fluttertoast/fluttertoast.dart';
import '../../utils/varuable_utils.dart';
import 'part_of_settings/decoder_settings.dart';
import 'part_of_settings/notification.dart';

class SettingsPage extends StatefulWidget {
  
  
   SettingsPage({Key? key, required this.k,}) : super(key: key);
  final BuildContext k;

  @override
  State<SettingsPage> createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {

  @override
  void initState() {

    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return

      Scaffold(
      backgroundColor: Color(0xfff7f7f7),
      appBar: AppBar(
        backgroundColor: Colors.white,
        title:             Container(
        height: 40,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(8),
            topRight: Radius.circular(8),
          ),
          // color: Colors.transparent,
        ),
        child: Row(
          children: [
            Expanded(child: Text('')),
            Text(
              "Settings",
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 18,
                  fontWeight: FontWeight.bold),
            ),
            Spacer(),
            InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(Icons.close)),
            SizedBox(
              width: 10,
            )
          ],
        ),
      ),
      leading: Icon(Icons.arrow_back_ios_new,color: Colors.transparent,),
        elevation: 0,
          ),

      body: SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 5,
                  ),
                  Text(
                    "General",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: Row(
                      children: [
                        Text("Player info"),
                        Spacer(),
                        Text("2.2.3",style: TextStyle(color: Colors.blueAccent),),
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Renew(),
                  SizedBox(height: 30,),
                  Container(
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(6),
                      color: Colors.white,
                    ),
                    padding: EdgeInsets.only(left: 10, right: 10),
                    child: InkWell(
                      onTap: () {
                        Navigator.pop(context);
                        showModalBottomSheet<void>(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          isScrollControlled: true,
                          backgroundColor: Color(0xfff7f7f7),
                          context: context,
                          builder: (BuildContext context) {
                            return FractionallySizedBox (
                                heightFactor: 0.9,
                                child: DecoderSettings());
                          },
                        );
                      },
                      child: Row(
                        children: [
                          Text("Decoder settings",style: TextStyle(fontWeight: FontWeight.bold),),
                          Spacer(),
                         Icon(Icons.arrow_forward_ios,size: 16,)
                        ],
                      ),
                    ),
                  ),
                  SizedBox(height: 30,),
                  Text(
                    "Play",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),SizedBox(height: 10,),
                  BackGroundPlayer(),
                  SizedBox(height: 10,),
                  NotificationSettings(),
                  SizedBox(height: 30,),
                  Text(
                    "Storage",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),SizedBox(height: 10,),
                  Storage(),
                  SizedBox(height: 30,),
                  Text(
                    "Help",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                  Help(),

                  SizedBox(height: 30,),
                  Text(
                    "Advanced",
                    style: TextStyle(
                        color: Colors.black54,
                        fontSize: 16,
                        fontWeight: FontWeight.w500),
                  ),
                          Advanced(cxt: widget.k,),
                  SizedBox(height: 30,),



                ],
              ),
            )
          ],
        ),
      ),
    );
  }


}
