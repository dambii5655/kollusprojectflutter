import 'dart:convert';

import 'package:collus_player/model/video_local_model/video_model.dart';
import 'package:collus_player/utils/text_style.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/service_view/functions.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DeleteFolderSelected extends StatefulWidget {
  const DeleteFolderSelected({Key? key}) : super(key: key);

  @override
  State<DeleteFolderSelected> createState() => _DeleteFolderSelectedState();
}

class _DeleteFolderSelectedState extends State<DeleteFolderSelected> {

  @override
  void initState() {
    // TODO: implement initState

    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return  Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        SizedBox(
          height: 20,
        ),
        Text(
          "Are you sure want to delete?",
          style: CustomTextStyle.titleStyle,
        ),
        SizedBox(
          height: 20,
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Row(
            children: [
              Expanded(child: Container()),
              Container(
                  height: 40,
                  child: Center(
                      child: InkWell(
                          onTap: () {
                          Navigator.pop(context);
                          },
                          child: Text("Cancel")))),
              SizedBox(
                width: 20,
              ),
              Spacer(),
              InkWell(
                onTap: ()async {

                    deleteSelectedItems();
                 await Future.delayed(Duration(seconds: 1));
                  Navigator.pop(context);
                },
                child: Container(
                    padding: EdgeInsets.only(left: 40, right: 40),
                    height: 40,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(4),
                      color: Colors.red.shade50,
                    ),
                    child: Center(
                        child: Text(
                          "Delete",
                          style: TextStyle(
                              color: Colors.red,
                              fontWeight: FontWeight.bold),
                        ))),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 20,
        )
      ],
    );
  }

       void deleteSelectedItems() async{
    setState(() {
      for (int i = UtilsVariable.FoldersBoolList.length - 1; i >= 0; i--) {
        if (UtilsVariable.FoldersBoolList[i]) {
          FunctionOfMainPage.folderList.removeAt(i);
          UtilsVariable.FoldersBoolList.removeAt(i);


        }
      }
      for (int i = UtilsVariable.videoLocalList.length - 1; i >= 0; i--) {
        if (UtilsVariable.videoLocalList[i]) {
          UtilsVariable.localVideoList.removeAt(i);
          UtilsVariable.videoLocalList.removeAt(i);


        }
      }
    });
    await FunctionOfMainPage.saveItems();
     deleteById(UtilsVariable.localVideoList);
  }


   void deleteById(List<VideoData> myList,) {

    SharedPreferences.getInstance().then((prefs) {
      final myListString =
      myList.map((item) => jsonEncode(item.toJson())).toList();
      prefs.setStringList('videoInfo', myListString);
    });
  }
}
