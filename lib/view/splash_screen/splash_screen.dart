import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../../service/routes/routes_name.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with TickerProviderStateMixin {
  late AnimationController _animatedController;
  late CurvedAnimation _curvedAnimation;
  bool visibleButton = false;
  var isRegistered = false;
  var isPinEnabled = false;
  var isFirstEntered = false;

  @override
  void initState() {
    super.initState();
    init();
  }

  init() {
    _animatedController =
    AnimationController(duration: const Duration(seconds: 3), vsync: this)
      ..forward()
      ..addStatusListener((status) {
        if (status == AnimationStatus.completed) {
          Navigator.of(context)
              .pushReplacementNamed(MainRoutes.main);
          // if (isPinEnabled) {
          //   Navigator.of(context)
          //       .pushReplacementNamed(MainRoutes.main);
          // } else {
          //   Navigator.of(context).pushReplacementNamed(isFirstEntered
          //       ? (isRegistered
          //       ? MainRoutes.sign_in_screen
          //       : MainRoutes.sign_up_screen)
          //       : MainRoutes.selectLanguage);
          // }
        }
      });
    _curvedAnimation =
        CurvedAnimation(parent: _animatedController, curve: Curves.elasticOut);
  }

  @override
  void dispose() {
    super.dispose();
    _animatedController.dispose();
  }

  void restartAnimation() {
    if (!_animatedController.isAnimating) {
      _animatedController.reset();
      _animatedController.forward();
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Align(
              child: ScaleTransition(
                scale: Tween(begin: 0.5, end: 1.0).animate(_curvedAnimation),
                child: SizedBox(
                  width: 500,
                  height: 300,
                  child: Image.asset("assets/logo.png"),
                ),
              ),
            ),
            visibleButton
                ? Container(
              margin: const EdgeInsets.symmetric(
                  horizontal: 48, vertical: 18),
              child: TextButton(
                onPressed: () {},
                child: const Text("Choose Language"),
              ),
            )
                : Container(),
          ],
        ),
      ),
    );
  }
}