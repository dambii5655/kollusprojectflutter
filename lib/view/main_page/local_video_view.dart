import 'dart:convert';

import 'package:collus_player/model/video_local_model/video_model.dart';
import 'package:collus_player/utils/varuable_utils.dart';
import 'package:collus_player/view/service_view/delete_local_storage_video.dart';
import 'package:collus_player/view/video_page/video_view.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalVideoView extends StatefulWidget {
  final VoidCallback onRefresh;
  final VoidCallback showDelete;
  // final VoidCallback playAction(String);
  final void Function(String) playAction;

  const LocalVideoView(
      {Key? key,
      required this.onRefresh,
      required this.showDelete,
      required this.playAction})
      : super(key: key);

  @override
  State<LocalVideoView> createState() => _LocalVideoViewState();
}

class _LocalVideoViewState extends State<LocalVideoView> {
  @override
  void initState() {
    loadFromSharedPreferences();
    super.initState();
  }

  void updateAllUnchecked() {
    setState(() {
      UtilsVariable.allUnchecked =
          UtilsVariable.videoLocalList.every((isChecked) => isChecked == false);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
        children: List.generate(
      UtilsVariable.localVideoList.length,
      (index) => Padding(
        padding: const EdgeInsets.only(left: 8.0),
        child: Row(
          children: [
            UtilsVariable.deleteAllItems
                ? Checkbox(
                    value: UtilsVariable.videoLocalList[index],
                    onChanged: (value) {
                      setState(() {
                        UtilsVariable.videoLocalList[index] = value ?? false;

                        UtilsVariable.selectAll = UtilsVariable.videoLocalList
                            .every((value) => value == true);
                        UtilsVariable.selectAllLocalVideo = UtilsVariable
                            .videoLocalList
                            .every((value) => value == true);

                        updateAllUnchecked();
                      });
                      // if(UtilsVariable.selectAllFolder==true){
                      //
                      //
                      // }
                      widget.onRefresh();

                      // setState(() {});
                    })
                : Container(),
            Expanded(
              child: InkWell(
                onTap: () {
                  // TODO IOS add code
                  widget.playAction(UtilsVariable.localVideoList[index].size);

                  // Navigator.push(
                  //   context,
                  //   MaterialPageRoute(
                  //     builder: (context) => ButterFlyAssetVideo(
                  //       // videoPath: UtilsVariable.filePath,
                  //       videoPath:
                  //           UtilsVariable.localVideoList[index].videoPath,

                  //       fileName:
                  //           UtilsVariable.localVideoList[index].name.toString(),
                  //       // videoUrl: movie.videoUrl,
                  //     ),
                  //   ),
                  // );

                  // setState(() {});
                },
                child: Container(
                  margin: EdgeInsets.only(left: 10, right: 10, bottom: 8),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Stack(
                        children: [
                          Container(
                            child: Image.asset("assets/first_pic.png",
                                fit: BoxFit.fitWidth),
                            width: 100,
                            height: 75,
                          ),
                          Container(
                            margin: EdgeInsets.only(left: 60, top: 53),
                            width: 35,
                            height: 15,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(4),
                                color: Colors.black87),
                            child: Center(
                                child: Text(
                              UtilsVariable.localVideoList[index].videoDuration,
                              style:
                                  TextStyle(color: Colors.white, fontSize: 10),
                            )),
                          )
                        ],
                      ),
                      SizedBox(
                        width: 5,
                      ),
                      Container(
                          padding: EdgeInsets.only(top: 5),
                          width: 200,
                          child:
                              Text(UtilsVariable.localVideoList[index].name)),
                      Spacer(),
                      UtilsVariable.deleteAllItems
                          ? Container()
                          : PopupMenuButton(
                              offset: Offset(0, 30),
                              itemBuilder: (BuildContext context) => [
                                PopupMenuItem(
                                  onTap: () {
                                    deleteVideoPath(
                                        context,
                                        UtilsVariable
                                            .localVideoList[index].name);
                                  },
                                  child: Text(
                                    'Delete',
                                    style: TextStyle(fontSize: 14),
                                  ),
                                  value: 1,
                                ),
                                PopupMenuItem(
                                  child: Text(
                                    'Detail Information',
                                    style: TextStyle(fontSize: 14),
                                  ),
                                  value: 2,
                                ),
                              ],
                              child: Icon(Icons.more_vert),
                              color: Colors.white,
                            ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    ));
  }

  void deleteVideoPath(BuildContext context, String id) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.white,
            elevation: 0,
            child: DeleteVideoFromLocal(
              id: id,
            ),
          );
        }).then((value) => {setState(() {})});
  }

  Future<void> loadFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final myListString = prefs.getStringList('videoInfo') ?? [];
    UtilsVariable.videoLocalList =
        List.generate(myListString.length, (index) => index % 1 == 1);
    setState(() {
      UtilsVariable.localVideoList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return VideoData.fromJson(jsonMap);
      }).toList();
      prefs.getString("sortItem");
      UtilsVariable.sort = prefs.getString("sortItem");

      if (UtilsVariable.sort == "0") {
        UtilsVariable.localVideoList.sort((a, b) => a.name.compareTo(b.name));
      } else {
        if (UtilsVariable.sort == "1") {
          UtilsVariable.localVideoList.sort((b, a) => b.name.compareTo(a.name));
        }
      }
      if (UtilsVariable.sort == "2") {}
    });
  }
}
