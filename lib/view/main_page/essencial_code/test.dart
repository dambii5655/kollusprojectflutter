import 'package:flutter/material.dart';

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  List<Item> itemList1 = [
    Item(name: 'Item 1'),
    Item(name: 'Item 2'),
    Item(name: 'Item 3'),
    // Add more items to itemList1 as needed
  ];

  List<Item> itemList2 = [
    Item(name: 'Item A'),
    Item(name: 'Item B'),
    Item(name: 'Item C'),
    // Add more items to itemList2 as needed
  ];

  List<Item> mergedList = [];

  bool selectAll = false;

  @override
  void initState() {
    super.initState();
    mergedList.addAll(itemList1);
    mergedList.addAll(itemList2);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Item Selection'),
      ),
      body: Column(
        children: [
          CheckboxListTile(
            title: Text('Select All'),
            value: selectAll,
            onChanged: (value) {
              setState(() {
                selectAll = value ?? false;
                mergedList.forEach((item) {
                  item.isSelected = selectAll;
                });
              });
            },
          ),
          Expanded(
            child: ListView.builder(
              itemCount: mergedList.length,
              itemBuilder: (context, index) {
                return CheckboxListTile(
                  title: Text(mergedList[index].name),
                  value: mergedList[index].isSelected,
                  onChanged: (value) {
                    setState(() {
                      mergedList[index].isSelected = value ?? false;
                      if (!mergedList[index].isSelected) {
                        selectAll = false;
                      } else {
                        selectAll = mergedList.every((item) => item.isSelected);
                      }
                    });
                  },
                );
              },
            ),
          ),
          ElevatedButton(
            onPressed: () {
              int selectedCount =
                  mergedList.where((item) => item.isSelected).length;
              showDialog(
                context: context,
                builder: (context) {
                  return AlertDialog(
                    title: Text('Selected Count'),
                    content: Text('Total selected checkboxes: $selectedCount'),
                    actions: [
                      TextButton(
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Text('OK'),
                      ),
                    ],
                  );
                },
              );
            },
            child: Text('Check Selection'),
          ),
        ],
      ),
    );
  }
}

class Item {
  String name;
  bool isSelected;

  Item({required this.name, this.isSelected = false});
}
