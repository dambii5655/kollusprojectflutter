import 'package:collus_player/utils/varuable_utils.dart';
import 'package:flutter/material.dart';
class Recently extends StatefulWidget {
  const Recently({Key? key}) : super(key: key);

  @override
  State<Recently> createState() => _RecentlyState();
}

class _RecentlyState extends State<Recently> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Material(
        elevation: 2,
        borderRadius: BorderRadius.circular(4),
        color: Colors.white,
        shadowColor: Colors.grey.shade200,
        child: Container(
          decoration: BoxDecoration(
              border: Border.all(color: Colors.grey.shade300),
              borderRadius: BorderRadius.circular(4)),
          child: Row(
            children: [
              SizedBox(
                width: 0,
              ),
              Container(
                  padding: EdgeInsets.all(6),
                  margin: EdgeInsets.all(6),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                  ),
                  child: Image.asset("assets/recently_added.png")),
              Text(
                "Recently added file  ${UtilsVariable.localVideoList.length}",
                style: TextStyle(fontWeight: FontWeight.w600),
              ),
              Spacer(),
              Icon(
                Icons.arrow_forward_ios,
                color: Colors.grey,
              ),
              SizedBox(
                width: 10,
              )
            ],
          ),
        ),
      ),
    );
  }


}
