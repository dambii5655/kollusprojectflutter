// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:collus_player/api/firebase/firebase_repo.dart';
// import 'package:collus_player/utils/text_style.dart';
// import 'package:collus_player/utils/varuable_utils.dart';
// import 'package:collus_player/view/video_page/video_view.dart';
// import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
// import 'package:flutter/material.dart';
//
// class MovieView extends StatefulWidget {
//   const MovieView({Key? key}) : super(key: key);
//
//   @override
//   State<MovieView> createState() => _MovieViewState();
//
//   static Future<String?> getDynamicUrl(String link) async {
//     FirebaseDynamicLinks dynamicLinks = FirebaseDynamicLinks.instance;
//     try {
//       String? _linkMessage;
//
//       final DynamicLinkParameters parameters = DynamicLinkParameters(
//         uriPrefix: "https://kollus.page.link",
//         link: Uri.parse(link),
//         androidParameters: AndroidParameters(
//           packageName: 'com.example.collus_player',
//         ),
//
//         iosParameters:
//             // IosParameters(bundleId: 'com.xmed', appStoreId: '1580909766'),
//             IOSParameters(
//                 bundleId: 'com.xmed',
//                 appStoreId: '1580909766',
//                 minimumVersion: "1"),
//         // navigationInfoParameters:NavigationInfoParameters(forcedRedirectEnabled: true),
//       );
//
//       Uri url;
//
//       ShortDynamicLink shortDynamicLink =
//           await dynamicLinks.buildShortLink(parameters);
//       url = shortDynamicLink.shortUrl;
//
//       _linkMessage = Uri.decodeFull(url.toString());
//       print("_linkMessage");
//       print(_linkMessage);
//
//       return _linkMessage;
//     } catch (e) {
//       return null;
//     }
//   }
// }
//
// class _MovieViewState extends State<MovieView> {
//   @override
//   Widget build(BuildContext context) {
//     final moviesRef = FirebaseFirestore.instance
//         .collection('kollus')
//         .withConverter<Book>(
//           fromFirestore: (snapshots, index) => Book.fromJson(snapshots.data()!),
//           toFirestore: (movie, index) => movie.toJson(),
//         );
//
//     return Scaffold(
//       backgroundColor: Colors.white,
//       body: StreamBuilder<QuerySnapshot<Book>>(
//           stream: moviesRef.snapshots(),
//           builder: (context, snapshot) {
//             if (!snapshot.hasData) {
//               return const Text('Loading...');
//             }
//             if (snapshot.hasError) {
//               return const Text('Something went wrong.');
//             }
//             final data = snapshot.requireData;
//             return ListView.separated(
//                 separatorBuilder: (context, index) {
//                   return Container();
//                 },
//                 itemCount: data.size,
//                 itemBuilder: (BuildContext context, int index) {
//                   return buildUIMovie(
//                       snapshot.data!.docs[index].data(), context);
//                 });
//           }),
//     );
//   }
//
//   @override
//   Widget buildUIMovie(Book movie, BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(left: 8.0),
//       child: InkWell(
//         onTap: () {
//           Navigator.push(
//             context,
//             MaterialPageRoute(
//               builder: (context) => ButterFlyAssetVideo(
//                 videoPath: UtilsVariable.filePath,
//                 fileName: movie.name.toString(),
//                 videoUrl: movie.videoUrl,
//               ),
//             ),
//           );
//           print(movie.videoUrl);
//           // MovieView.getDynamicUrl("https://kollus.page.link/${movie.videoUrl}");
//         },
//         child: Container(
//
//           margin: EdgeInsets.only(left: 10, right: 10),
//           decoration: BoxDecoration(
//               borderRadius: BorderRadius.circular(8),color: Colors.white ),
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.start,
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: [
//               Stack(
//                 children: [
//                   Container(
//                       height: 80,
//                       width: 100,
//                       child: Image.network(movie.imageMovie.toString())),
//                   Container(
//                     margin: EdgeInsets.only(left: 60, top: 55),
//                     width: 35,
//                     height: 15,
//                     decoration: BoxDecoration(
//                         borderRadius: BorderRadius.circular(4),
//                         color: Colors.black87),
//                     child: Center(
//                         child: Text(
//                       movie.duration.toString(),
//                       style: TextStyle(color: Colors.white, fontSize: 10),
//                     )),
//                   )
//                 ],
//               ),
//               SizedBox(
//                 width: 5,
//               ),
//               Container(
//                   padding: EdgeInsets.only(top: 5),
//
//                   child: Text(movie.name.toString())),
//               Spacer(),
//               PopupMenuButton(
//                 offset: Offset(0, 30),
//                 itemBuilder: (BuildContext context) => [
//                   PopupMenuItem(
//                     child: Text(
//                       'Delete',
//                       style: TextStyle(fontSize: 14),
//                     ),
//                     value: 1,
//                   ),
//                   PopupMenuItem(
//                     onTap: () {
//                       aboutData(
//                         context,
//                         movie.name.toString(),
//                         movie.addedData.toString(),
//                         movie.size.toString(),
//                         movie.duration.toString(),
//                         movie.endDate.toString(),
//                       );
//                     },
//                     child: Text(
//                       'Detailed information',
//                       style: TextStyle(fontSize: 14),
//                     ),
//                     value: 2,
//                   ),
//                 ],
//                 child: Icon(Icons.more_vert),
//                 color: Colors.white,
//               ),
//
//             ],
//           ),
//         ),
//       ),
//     );
//   }
//
//   void aboutData(BuildContext context, String fileName, String addedData,
//       String size, String duration,String endDate,) async {
//     showDialog(
//         context: context,
//         builder: (BuildContext context) {
//           return Dialog(
//             shape: RoundedRectangleBorder(
//                 borderRadius: BorderRadius.all(Radius.circular(8))),
//             backgroundColor: Colors.white,
//             elevation: 0,
//             child: Padding(
//               padding: const EdgeInsets.only(left: 16, right: 16),
//               child: Column(
//                 mainAxisSize: MainAxisSize.min,
//                 children: [
//                   SizedBox(
//                     height: 20,
//                   ),
//                   Row(
//                     children: [
//                       Text(
//                         "Download",
//                         style: CustomTextStyle.subtitleTextStyle,
//                       ),
//                     ],
//                   ),
//                   SizedBox(
//                     height: 17,
//                   ),
//                   Row(
//                     children: [
//                       Text(
//                         fileName,
//                         style: CustomTextStyle.titleStyle,
//                       ),
//                     ],
//                   ),
//                   SizedBox(
//                     height: 14,
//                   ),
//                   Divider(),
//                   SizedBox(
//                     height: 10,
//                   ),
//                   row("File size", size),
//
//                   SizedBox(
//                     height: 17,
//                   ),
//                   row("Date added", addedData),
//
//                   SizedBox(
//                     height: 17,
//                   ),
//                   row("Video length", duration),
//                   SizedBox(
//                     height: 17,
//                   ),
//                   row("Expiry date", endDate),
//                   SizedBox(
//                     height: 40,
//                   ),
//                   Row(
//                     mainAxisAlignment: MainAxisAlignment.center,
//                     crossAxisAlignment: CrossAxisAlignment.center,
//                     children: [
//                       InkWell(
//                           onTap: () {
//                             Navigator.pop(context);
//                           },
//                           child: Text(
//                             "Close",
//                             style: TextStyle(
//                                 color: Colors.black54,
//                                 fontSize: 20,
//                                 fontWeight: FontWeight.w600),
//                           ))
//                     ],
//                   ),
//                   SizedBox(
//                     height: 30,
//                   ),
//                 ],
//               ),
//             ),
//           );
//         }).then((value) => {setState(() {})});
//   }
//
//   Widget row(String description, String infoVideo) {
//     return Row(
//       children: [
//         Text(
//           description,
//           style: TextStyle(
//               color: Colors.black87, fontSize: 16, fontWeight: FontWeight.bold),
//         ),
//         Spacer(),
//         Text(infoVideo,style: TextStyle(
//             color: Colors.black38, fontSize: 16, fontWeight: FontWeight.w500),
//         ),
//       ],
//     );
//   }
// }
