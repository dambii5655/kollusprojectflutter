import 'dart:async';
import 'dart:convert';
import 'dart:isolate';
import 'dart:ui';
import 'package:collus_player/model/video_local_model/video_model.dart';
import 'package:collus_player/view/create_folder/create_folder_view.dart';
import 'package:collus_player/view/create_folder/recently_added.dart';
import 'package:collus_player/view/delete/delete%20folder.dart';
import 'package:collus_player/view/main_page/essencial_code/test.dart';

import 'package:collus_player/view/main_page/local_video_view.dart';
import 'package:collus_player/view/main_page/movie_view.dart';
import 'package:collus_player/view/main_page/recently_added.dart';
import 'package:collus_player/view/service_view/create_folder.dart';
import 'package:collus_player/view/service_view/download_service.dart';
import 'package:collus_player/view/service_view/functions.dart';
import 'package:collus_player/view/service_view/sort_view.dart';
import 'package:collus_player/view/video_page/video_view.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
import 'package:flutter_windowmanager/flutter_windowmanager.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uni_links/uni_links.dart';
import '../../utils/varuable_utils.dart';
import '../settings_page/settings_page.dart';
import 'dart:io';
// import 'package:device_info_plus/device_info_plus.dart';

class MainPage extends StatefulWidget {
  const MainPage({Key? key}) : super(key: key);

  @override
  State<MainPage> createState() => _MainPageState();
}

class _MainPageState extends State<MainPage> with TickerProviderStateMixin {
  @override
  ReceivePort _port = ReceivePort();

  // TODO IOS add code
  static const platformChannel = MethodChannel('media.methodchannel/iOS');
  late List<dynamic> contentList = [];
  late String _resultText;
  // ===========

  String link = '';

  void initState() {
    initUniLinks();
    IsolateNameServer.registerPortWithName(
        _port.sendPort, 'downloader_send_port');
    _port.listen((dynamic data) {
      DownloadTaskStatus status = data[1];

      if (status == DownloadTaskStatus.complete) {
        print("Download Complate");
      }
      setState(() {});
    });
    FlutterDownloader.registerCallback(downloadCallback);

    // secureScreen();
    super.initState();
    Future.delayed(Duration(seconds: 2));
    {
      print("link");
      print(link);
    }
    // Future.delayed(Duration.zero, () {
    //   this.watchVideo();
    // });
    loadFromSharedPreferences();
  }

  // TODO IOS add code
  void _getKollusContent() async {
    try {
      final result = await platformChannel.invokeMethod('getVideoList');
      print("return content list =>> ");

      setState(() {
        contentList = List<dynamic>.from(result);
      });

      print(contentList.length);
      print(contentList[0]);
      saveToKollusVideoSharedPreferences();
    } catch (e) {
      _resultText = "Can't fetch the method: '$e'.";
    }

    print("gggggg => $_resultText");
  }

  void _playVideoAction(mediaContentKey) async {
    try {
      print("_playVideoAction Play content mediaContentKey ");
      print(mediaContentKey);
      final result = await platformChannel
          .invokeMethod('playVideo', {"mediaContentKey": mediaContentKey});
    } catch (e) {
      _resultText = " Can't fetch the method: '$e'.";
    }
    print("gggggg _playVideoAction => $_resultText");
  }

  // TODO IOS add code
  Future<void> saveToKollusVideoSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    await prefs.setStringList('videoInfo', []);
    final myListString = prefs.getStringList('videoInfo') ?? [];

    contentList.forEach((element) {
      print("contetntList foreach");
      print(element["mediaContentKey"]);
      final name = element["title"];
      final videoSize = element["mediaContentKey"];
      final dateAdded = element["drmExpireDate"];
      final duration = element["contentID"];
      final videoPath = element["snapshot"];

      final newData = VideoData(
          name: name,
          size: videoSize,
          dateAdded: dateAdded,
          videoDuration: duration,
          videoPath: videoPath);
      final newDataJson = newData.toJson();

      myListString.add(json.encode(newDataJson));
    });

    await prefs.setStringList('videoInfo', myListString);

    UtilsVariable.videoName = '';
    UtilsVariable.videoSize = '';
    UtilsVariable.dateAdded = '';
    UtilsVariable.videoDuration = '';
    UtilsVariable.videoPath = '';

    setState(() {
      UtilsVariable.localVideoList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return VideoData.fromJson(jsonMap);
      }).toList();
    });
    loadFromSharedPreferences();
  }

  // ===================

  void giveContext() async {
    {
      showModalBottomSheet<void>(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        // isDismissible: false,
        enableDrag: true,

        isScrollControlled: false,
        backgroundColor: Colors.white,
        context: context,

        builder: (BuildContext context) {
          return FractionallySizedBox(
              heightFactor: 0.8, child: DownloadServiceView());
        },
      ).then((value) => {
            setState(() {
              saveToSharedPreferences(); // Save the updated list to SharedPreferences
            }),
          });
    }
    ;
  }

  initUniLinks() async {
// Platform messages may fail, so we use a try/catch PlatformException.
    print("testPrint");
    try {
      final initialLink = await getInitialLink();
      // initialLinkTest=initialLink.toString();
      setState(() {});
      print("initialLink");
      print(initialLink);
      // link=initialLink??'';
      print(initialLink!.length);
      if (initialLink.length == 291) {
        print('4444444444');
        Future.delayed(Duration.zero, () {
          this.giveContext();
        });
      }
      if (initialLink.length == 287) {
        print('5555555555');

        Future.delayed(Duration.zero, () {
          this.watchVideo();
        });
      }
      setState(() {});
// Parse the link and warn the user, if it is not correct,
// but keep in mind it could be `null`.
    } on PlatformException {
// Handle exception by warning the user their action did not succeed
// return?
    }
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    setState(() {
      if (state == AppLifecycleState.inactive) print("111111");
      {
        print(state);
      }
      if (state == AppLifecycleState.resumed) print("22222");
      {
        print(state);
      }
      if (state == AppLifecycleState.paused) print("333333");
      {
        print(state);
      }
    });
  }

  void watchVideo() async {
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ButterFlyAssetVideo(
          // videoPath: UtilsVariable.filePath,
          videoPath: "",

          fileName: "Video name",
          videoUrl:
              "https://catenoid-sample.video.kr.kollus.com/kr/media-file.mp4?_s=bfb0f9d0d9b875d05eac609e4a9369f626414a4c55f3e6dfb75becff3fec7bc2a0a0f331391cb8445a3a23384499603aa8d3a8786635534ddd3870e132c35bef9dc406c372762257d44486ca4d3d3caf5c6739c4b06c73006f0666a6535399311cefa9cd56de63108f49d88d150473352920ff5b373b1608c5c02fe55216ed92a509d25c79bdc359e3af5197c3590f5d68667f3d83791e732db304b8e11905f623972250c633aafba35bb2e94bea094fefb0de8d2936c414d5a57b35ad1311a1738cbfd404456f7b08a5edd6c243516098d3145261eb7d214ef91c810700987b268912ca13a8dfc358052744b4110c5212f75b110c8033cdecf68ef3058811a370096f1ad0c5cae9fe7094d7464f8a7004a19c3399909a8648c3f5a9999c4ceeef0ed4fbf6a7cb3e5266ea55279666415e3f99d12f2bacc882afa530adcc9e69ccd71b766f3bcaf69469d154447717858f5c8c30e814f72d824d5897544f7afa19d385a30c579108f0a35d1d8f8cd2aa&_ts=100000&_tr=100000&channelkey=9c0qcbqi",
        ),
      ),
    );
  }

  @override
  void dispose() {
    IsolateNameServer.removePortNameMapping('downloader_send_port');
    super.dispose();
    UtilsVariable.download = false;
    UtilsVariable.watch = false;
  }

  @pragma('vm:entry-point')
  static void downloadCallback(
      String id, DownloadTaskStatus status, int progress) {
    final SendPort? send =
        IsolateNameServer.lookupPortByName('downloader_send_port');
    send!.send([id, status, progress]);
  }

  String selectedOption = '';
  String deviceDirectory = '';
  bool loc = false;
  bool fol = true;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: UtilsVariable.deleteAllItems
            ? Row(
                children: [
                  Text(
                    "    selected",
                    style: TextStyle(color: Colors.black),
                  ),
                  Spacer(),
                  InkWell(
                      onTap: () {
                        UtilsVariable.deleteAllItems = false;
                        setState(() {});
                      },
                      child: Text(
                        "Cancel",
                        style: TextStyle(color: Colors.black, fontSize: 14),
                      )),
                  SizedBox(
                    width: 20,
                  ),
                  ((UtilsVariable.allUnchecked &&
                          UtilsVariable.selectAll == false)
                      // ||(UtilsVariable.allUnchecked && UtilsVariable.selectAll == false)
                      )
                      ? Text(
                          "Delete",
                          style: TextStyle(color: Colors.grey, fontSize: 14),
                        )
                      : InkWell(
                          onTap: () {
                            deleteFolderSelected(context);
                          },
                          child: Text(
                            "Delete",
                            style: TextStyle(color: Colors.black, fontSize: 14),
                          ),
                        )
                ],
              )
            : Row(
                children: [
                  Text(
                    "Download",
                    style: TextStyle(color: Colors.black),
                  ),
                  Spacer(),
                  InkWell(
                      onTap: () {
                        showModalBottomSheet<void>(
                          transitionAnimationController: AnimationController(
                              duration: const Duration(milliseconds: 400),
                              vsync: this),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          isScrollControlled: true,
                          backgroundColor: Color(0xfff7f7f7),
                          context: context,
                          builder: (BuildContext context) {
                            return FractionallySizedBox(
                                heightFactor: 0.9,
                                child: SettingsPage(
                                  k: context,
                                ));
                          },
                        ).then((value) => {setState(() {})});
                      },
                      child: Icon(
                        Icons.settings_outlined,
                        color: Colors.black87,
                      )),
                  SizedBox(
                    width: 5,
                  ),
                  InkWell(
                      onTap: () async {
                        final SharedPreferences prefs =
                            await SharedPreferences.getInstance();
                        prefs.getString("sortItem");
                        UtilsVariable.sort = prefs.getString("sortItem");

                        showModalBottomSheet<void>(
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(10.0),
                          ),
                          // isDismissible: false,
                          enableDrag: true,

                          isScrollControlled: false,
                          backgroundColor: Colors.white,
                          context: context,

                          builder: (BuildContext context) {
                            return StatefulBuilder(builder: (BuildContext
                                    context,
                                StateSetter setState /*You can rename this!*/) {
                              return FractionallySizedBox(
                                  heightFactor: 0.4, child: SortView());
                            });
                          },
                        ).then((value) => {
                              loadFromSharedPreferences(),
                              setState(() {}),
                            });
                      },
                      child: Container(
                          width: 50,
                          height: 50,
                          child: Image.asset(
                            "assets/sort_original .png",
                            color: Colors.black87,
                          ))),
                  SizedBox(
                    width: 5,
                  ),
                  Container(
                    child: PopupMenuButton(
                      offset: Offset(0, 30),
                      itemBuilder: (BuildContext context) => [
                        PopupMenuItem(
                          child: Text(
                            'Delete',
                            style: TextStyle(fontSize: 14),
                          ),
                          onTap: () async {
                            WidgetsBinding.instance
                                .addPostFrameCallback((_) async {
                              final prefs =
                                  await SharedPreferences.getInstance();
                              final items =
                                  prefs.getStringList('folderList') ?? [];
                              UtilsVariable.FoldersBoolList = List.generate(
                                  items.length, (index) => index % 1 == 1);
                              UtilsVariable.deleteAllItems = true;
                              setState(() {});
                            });
                          },
                          value: 1,
                        ),
                        PopupMenuItem(
                          child: Text(
                            'Move',
                            style: TextStyle(fontSize: 14),
                          ),
                          value: 2,
                        ),
                        PopupMenuItem(
                          onTap: () {
                            UtilsVariable.folderController.text = "New folder";
                            createFolder(context);
                          },
                          child: Text(
                            'Create folder',
                            style: TextStyle(fontSize: 14),
                          ),
                          value: 3,
                        ),
                      ],
                      child: Icon(
                        Icons.more_vert,
                        size: 26,
                        color: Colors.black87,
                      ),
                    ),
                  ),
                ],
              ),
        elevation: 0,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.only(left: 8.0, right: 8),
          child: Column(
            children: [
              UtilsVariable.deleteAllItems
                  ? Row(
                      children: [
                        (UtilsVariable.selectAllFolder == true &&
                                UtilsVariable.selectAllLocalVideo == true)
                            ? Checkbox(
                                value: fol,
                                onChanged: (value) {
                                  setState(() {
                                    fol = value!;
                                    UtilsVariable.selectAll = value!;
                                    UtilsVariable.FoldersBoolList =
                                        UtilsVariable.FoldersBoolList.map(
                                                (value) =>
                                                    UtilsVariable.selectAll)
                                            .toList();
                                    UtilsVariable.videoLocalList = UtilsVariable
                                        .videoLocalList
                                        .map((value) => UtilsVariable.selectAll)
                                        .toList();
                                  });
                                })
                            : Checkbox(
                                value: loc,
                                onChanged: (value) {
                                  setState(() {
                                    loc = value!;
                                    UtilsVariable.selectAll = value!;
                                    UtilsVariable.FoldersBoolList =
                                        UtilsVariable.FoldersBoolList.map(
                                                (value) =>
                                                    UtilsVariable.selectAll)
                                            .toList();
                                    UtilsVariable.videoLocalList = UtilsVariable
                                        .videoLocalList
                                        .map((value) => UtilsVariable.selectAll)
                                        .toList();
                                  });
                                }),
                        Text("Select All"),
                      ],
                    )
                  : InkWell(
                      onTap: () {
                        if (UtilsVariable.localVideoList.length != 0) {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => RecentlyAdded(
                                        onRefresh: () async {
                                          await loadFromSharedPreferences();
                                        },
                                      )));
                        }
                      },
                      child: Recently()),
              FolderView(
                onRefresh: () {
                  setState(() {});
                },
              ),
              LocalVideoView(
                onRefresh: () {
                  setState(() {});
                },
                showDelete: () {
                  setState(() {});
                },
                playAction: (contentId) {
                  _playVideoAction(contentId);
                  setState(() {});
                },
              ),
              // SizedBox(
              //   height: 10,
              // ),
              // Container(
              //   color: Colors.white,
              //     height: 200, child: MovieView()),
//               ElevatedButton(
//                   onPressed: () async {
//                     showModalBottomSheet<void>(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(10.0),
//                       ),
// // isDismissible: false,
//                       enableDrag: true,
//
//                       isScrollControlled: false,
//                       backgroundColor: Colors.white,
//                       context: context,
//
//                       builder: (BuildContext context) {
//                         return FractionallySizedBox(
//                             heightFactor: 0.8, child: DownloadServiceView());
//                       },
//                     ).then((value) => {
//                           setState(() {
//                             saveToSharedPreferences(); // Save the updated list to SharedPreferences
//                           }),
//                         });
//                   },
//                   child: Text("Download")),
//               ElevatedButton(
//                 onPressed: () {
//                   setState(() {
//                     FunctionOfMainPage.folderList.clear(); // Clear the list
//                     FunctionOfMainPage
//                         .saveItems(); // Save the updated list to SharedPreferences
//                   });
//                 },
//                 child: Text('Clear'),
//               ),
// TODO download screen
              // ElevatedButton(
              //   onPressed: () {
              //     giveContext();
              //     // initUniLinks();
              //     // Navigator.push(context,
              //     //     MaterialPageRoute(builder: (context) => MyHomePage()));
              //   },
              //   child: Text('local test button'),
              // ),

              ElevatedButton(
                onPressed: () {
                  if (Platform.isIOS) {
                    _getKollusContent();
                  }
                },
                child: Text('Kollus content IOS'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> loadFromSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();
    final myListString = prefs.getStringList('videoInfo') ?? [];

    setState(() {
      UtilsVariable.localVideoList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return VideoData.fromJson(jsonMap);
      }).toList();
      prefs.getString("sortItem");
      UtilsVariable.sort = prefs.getString("sortItem");

      if (UtilsVariable.sort == "0") {
        UtilsVariable.localVideoList.sort((a, b) => a.name.compareTo(b.name));
      } else {
        if (UtilsVariable.sort == "1") {}
      }
      if (UtilsVariable.sort == "2") {
        UtilsVariable.localVideoList.sort((b, a) => b.name.compareTo(a.name));
      }
    });
  }

  Future<void> saveToSharedPreferences() async {
    final prefs = await SharedPreferences.getInstance();

    final myListString = prefs.getStringList('videoInfo') ?? [];

    final name = UtilsVariable.videoName;
    final videoSize = UtilsVariable.videoSize;
    final dateAdded = UtilsVariable.videoSize;
    final duration = UtilsVariable.videoDuration;
    final videoPath = UtilsVariable.videoPath;

    final newData = VideoData(
        name: name,
        size: videoSize,
        dateAdded: dateAdded,
        videoDuration: duration,
        videoPath: videoPath);
    final newDataJson = newData.toJson();

    myListString.add(json.encode(newDataJson));

    await prefs.setStringList('videoInfo', myListString);

    UtilsVariable.videoName = '';
    UtilsVariable.videoSize = '';
    UtilsVariable.dateAdded = '';
    UtilsVariable.videoDuration = '';
    UtilsVariable.videoPath = '';

    setState(() {
      UtilsVariable.localVideoList = myListString.map((jsonString) {
        final jsonMap = json.decode(jsonString);
        return VideoData.fromJson(jsonMap);
      }).toList();
    });
    loadFromSharedPreferences();
  }

  void deleteFolderSelected(
    BuildContext context,
  ) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(8))),
            backgroundColor: Colors.white,
            elevation: 0,
            child: DeleteFolderSelected(),
          );
        }).then((value) => {setState(() {})});
  }

  void createFolder(BuildContext context) async {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return Dialog(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(8))),
              backgroundColor: Colors.white,
              elevation: 0,
              child: CreateFolder());
        }).then((value) => {setState(() {})});
  }

  Future<void> secureScreen() async {
    await FlutterWindowManager.addFlags(FlutterWindowManager.FLAG_SECURE);
  }

  Future<void> disposeScreen() async {
    await FlutterWindowManager.clearFlags(FlutterWindowManager.FLAG_SECURE);
  }
}
