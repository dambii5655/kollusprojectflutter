import 'package:flutter/material.dart';

class DownloadTimeCalculator extends StatefulWidget {
  @override
  _DownloadTimeCalculatorState createState() => _DownloadTimeCalculatorState();
}

class _DownloadTimeCalculatorState extends State<DownloadTimeCalculator> {
  final _formKey = GlobalKey<FormState>();

  double _fileSize = 0;
  double _downloadSpeed = 0;
  double _downloadTime = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Download Time Calculator'),
      ),
      body: Padding(
        padding: EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'File Size (in MB)',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter file size';
                  }
                  return null;
                },
                onSaved: (value) {
                  _fileSize = double.parse(value!);
                },
              ),
              TextFormField(
                keyboardType: TextInputType.number,
                decoration: InputDecoration(
                  labelText: 'Download Speed (in Mbps)',
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter download speed';
                  }
                  return null;
                },
                onSaved: (value) {
                  _downloadSpeed = double.parse(value!);
                },
              ),
              SizedBox(height: 16.0),
              Center(
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _formKey.currentState!.save();
                      setState(() {
                        _downloadTime = _fileSize / (_downloadSpeed * 8);
                      });
                    }
                  },
                  child: Text('Calculate Download Time'),
                ),
              ),
              SizedBox(height: 16.0),
              _downloadTime > 0
                  ? Text('Estimated Download Time: ${_downloadTime.toStringAsFixed(2)} seconds')
                  : Container(),
            ],
          ),
        ),
      ),
    );
  }
}