import 'dart:io';
import 'dart:typed_data';
import 'package:collus_player/service/language/lang_json.dart';
import 'package:collus_player/view/main_function.dart';
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_downloader/flutter_downloader.dart';
final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await FlutterDownloader.initialize(debug: true, ignoreSsl: true);

  ByteData data = await PlatformAssetBundle().load('assets/lets-encrypt-r3.pem');
  SecurityContext.defaultContext.setTrustedCertificatesBytes(data.buffer.asUint8List());
  await EasyLocalization.ensureInitialized();


 HttpOverrides.global=MyHttpOverrides();
  runApp(
    EasyLocalization(
      supportedLocales: const [
        Locale('eng'),
        Locale('ru'),
        Locale('uz'),
      ],
      path: 'assets/lang',
      assetLoader: JsonAssetLoader(),
      fallbackLocale: const Locale('uz'),
      child:  KollusPlayer(),
    ),
  );
}



class MyHttpOverrides extends HttpOverrides{
  @override
  HttpClient createHttpClient(SecurityContext? context){
    return super.createHttpClient(context)
      ..badCertificateCallback = (X509Certificate cert, String host, int port)=> true;
  }
}
