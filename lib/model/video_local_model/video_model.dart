class VideoData {
  final String name;
  final String size;
  final String dateAdded;
  final String videoDuration;
  final String videoPath;


  VideoData({required this.name, required this.size, required this.dateAdded, required this.videoDuration,required this.videoPath});

  factory VideoData.fromJson(Map<String, dynamic> json) {
    return VideoData(
      name: json['name']??'',
      size: json['size']??'',
      dateAdded: json['dateAdded']??'',
      videoDuration: json['videoDuration']??'',
      videoPath: json['videoPath']??'',
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'name': name,
      'size': size,
      'dateAdded': dateAdded,
      'videoDuration': videoDuration,
      'videoPath': videoPath,
    };
  }

  @override
  String toString() {
    return '$name - $size - $dateAdded- $videoDuration- $videoPath';
  }

}
