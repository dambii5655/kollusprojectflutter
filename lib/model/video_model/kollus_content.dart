class Result {
  Result({
    this.title,
    this.snapshot,
    this.drmExpireDate,
    this.duration,
    this.drmExpired,
    this.mediaContentKey,
    this.position,
    this.fileType,
    this.fileSize,
    this.contentID,
    this.contentIndex,
  });

  double? title;
  double? snapshot;
  String? drmExpireDate;
  double? duration;
  String? drmExpired;
  String? mediaContentKey;
  String? position;
  String? fileType;
  String? fileSize;
  String? contentID;
  String? contentIndex;

  factory Result.fromJson(Map<String, dynamic> json) => Result(
        title: json["title"],
        snapshot: json["snapshot"],
        drmExpireDate: json["drmExpireDate"],
        duration: json["duration"],
        drmExpired: json["drmExpired"],
        mediaContentKey: json["mediaContentKey"],
        position: json["position"],
        fileType: json["fileType"],
        fileSize: json["fileSize"],
        contentID: json["contentID"],
        contentIndex: json["contentIndex"],
      );

  Map<String, dynamic> toJson() => {
        "title": title,
        "snapshot": snapshot,
        "drmExpireDate": drmExpireDate,
        "duration": duration,
        "drmExpired": drmExpired,
        "mediaContentKey": mediaContentKey,
        "position": position,
        "fileType": fileType,
        "fileSize": fileSize,
        "contentID": contentID,
        "contentIndex": contentIndex,
      };
}
