part of 'switch_bloc.dart';

@immutable
 class SwitchState extends Equatable {
  final bool switchValue;

  SwitchState({required this.switchValue});

  @override
  // TODO: implement props
  List<Object?> get props => [switchValue];
}

class SwitchInitial extends SwitchState {
  SwitchInitial({required super.switchValue});
}
